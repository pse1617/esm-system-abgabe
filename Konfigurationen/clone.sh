#!/bin/sh
#This file clones and configures a local git repository
#from your user-account on bitbucket.

#Copy this file to the destination to clone
#and configure your local git-Repository to

read -p "Dein Bitbucket-Benutzername: " name
#Clone the repository
git clone https://${name}@bitbucket.org/${name}/esm-system.git
cd esm-system

#Configure the Repository
git remote add downstream https://${name}@bitbucket.org/pse1617/esm-system.git
git remote set-url --push downstream https://${name}@bitbucket.org/${name}/esm-system.git
git fetch downstream
