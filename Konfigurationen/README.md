# Anleitungen #

Ich beschreibe hier, wie verschiedene Einstellungen und Vorgänge ablaufen.

## Ablauf des Klonens:

Klonen, Team-Repository zum Download von Änderungen hinzufügen, Infos/Änderungen über/bei Branches herunterladen.

**Shell-Script konfiguriert das Repository zum nach dem klonen**

* `git clone https://<benutzername>@bitbucket.org/<benutzername>/esm-system.git`
* `git remote add downstream https://<benutzername>@bitbucket.org/pse1617/esm-system.git`
* `git remote set-url --push downstream https://<benutzername>@bitbucket.org/<benutzername>/esm-system.git`
* `git fetch downstream`

#### Änderungen vom Team-Repository herunterladen:

* `git pull downstream master`

#### Änderungen zum Team-Repository hinzufügen:

* `git push <branch name>`
* Auf Bitbucket von eigenem Benutzer-Repository Pull-Request erzeugen

# Intellij als merge-/difftool: #

* Lade dir Intellij herunter und installiere es für dein Betriebssystem
  [Jetbrains Intellij IDEA](https://www.jetbrains.com/idea/)
* [Hier](https://www.jetbrains.com/help/idea/2016.2/running-intellij-idea-as-a-diff-or-merge-command-line-tool.html) gibt es die Anleitung, wie man sein System konfigurieren muss, dass auf Intellij von der Kommandozeile aus zugegriffen werden kann.

Achtet darauf, dass die `idea` oder `idea.bat` Datei in eurem Pfad vorhanden ist.

Als nächstes muss dein lokales git-Repository so konfiguriert werden, dass Intellij standardmäßig von git eingesetzt wird. Die Konfigurationen kann man auch [hier](https://git-scm.com/book/tr/v2/Customizing-Git-Git-Configuration) nachlesen.

Falls du Intellij auch in anderen Projekten als merge-Tool benutzen willst, dann füge nach jedem `git config ` ein `--global` ein.

Natürlich müssen die Befehle von einer Kommandozeile im lokalen Repository aus ausgeführt werden.

* `git config merge.tool intellij`
* `git config mergetool.intellij.trustExitCode true`
* `git config diff.tool intellij`
* `git config difftool.intellij.trustExitCode true`
* Füge die folgenden Zeilen zu deiner *.git/config* Datei hinzu:

#### Ubuntu/MacOS:
    ...
    [difftool "intellij"]:
    cmd = idea diff $(cd $(dirname "$LOCAL") && pwd)/$(basename "$LOCAL") $(cd $(dirname "$REMOTE") && pwd)/$(basename "$REMOTE")
    ...
    [mergetool "intellij"]:
    cmd = idea merge $(cd $(dirname "$LOCAL") && pwd)/$(basename "$LOCAL") $(cd $(dirname "$REMOTE") && pwd)/$(basename "$REMOTE") $(cd $(dirname "$BASE") && pwd)/$(basename "$BASE") $(cd $(dirname "$MERGED") && pwd)/$(basename "$MERGED")
    ...
#### Windows:
    ...
    [difftool "intellij"]
    cmd = cmd.exe //c "\"<Installationspfad zu Intellij IDEA>/bin/idea.bat\" diff \"$LOCAL\" \"$REMOTE\"
    ...
    [mergetool "intellij"]
    cmd = cmd.exe //c "\"<Installationspfad zu Intellij IDEA>/bin/idea.bat\" merge \"$LOCAL\" \"$REMOTE\" \"$BASE\" \"$MERGED\""
    ...

#### Im allgemeinen funktioniert das *mergen* so:

* Mit `git pull downstream master` holst du dir die neuesten Änderungen des Team-Repositories in deinen lokalen `master`-Branch
* Dann wird dir von git eine Ausgabe generiert, in der so etwas ähnliches wie: **Es bestehen Merge-Konflikte. Bitte löse die merge-Konflikte und commite dein Ergebnis!**
* mit `git mergetool -t intellij` wird nun intellij für jeden Konflikt aufgerufen. Falls du nicht bei jedem neuen Konflikt gefragt werden willst, ob Intellij gestartet werden soll, gib stattdessen: `git mergetool -y -t intellij` ein.

#### Des weiteren funktioniert das *diffen* so:

* Mit `git difftool -t intellij <path1> <path2>` vergeichst du zwei Branches miteinander. Wobei *path1* und *path2* die Branches beschreiben, die verglichen werden sollen (z.B. `git difftool -t intellij downstream/master master` vergleicht die master-Branch von deinem Benutzer-Repository auf Bitbucket mit deinem lokalen master-Branch).

Für mehr Infos, wie der mergetool-Befehl funktioniert usw. kann man sich mit `man difftool` bzw. `man mergetool` die Benutzeranleitungen anschauen.

## Quellen:

* [Intellij als diff-/mergetool von der Kommandozeile](https://www.jetbrains.com/help/idea/2016.2/running-intellij-idea-as-a-diff-or-merge-command-line-tool.html)
* [Konfiguration von git um Intellij zu benutzen](http://brian.pontarelli.com/2013/10/25/using-idea-for-git-merging-and-diffing/)
* [Konfiguration von git um Intellij zu benutzen (Windows)](https://coderwall.com/p/gc_hqw/use-intellij-or-webstorm-as-your-git-diff-tool-even-on-windows)
