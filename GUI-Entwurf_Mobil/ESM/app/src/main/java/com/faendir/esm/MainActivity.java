package com.faendir.esm;

import android.app.Activity;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.view.Menu;
import android.view.MenuItem;

/**
 * @author F43nd1r
 * @since 29.11.2016
 */

public class MainActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.start:
                setContentView(R.layout.start);
                break;
            case R.id.question1:
                setContentView(R.layout.question1);
                break;
            case R.id.question2:
                setContentView(R.layout.question2);
                break;
            case R.id.question3:
                setContentView(R.layout.question3);
                break;
            case R.id.end:
                setContentView(R.layout.end);
                break;
            case R.id.prefs:
                getFragmentManager().beginTransaction().replace(android.R.id.content, new PreferenceFragment() {
                    @Override
                    public void onCreate(Bundle savedInstanceState) {
                        super.onCreate(savedInstanceState);
                        addPreferencesFromResource(R.xml.prefs);
                    }
                }).commit();
        }
        return super.onOptionsItemSelected(item);
    }
}
