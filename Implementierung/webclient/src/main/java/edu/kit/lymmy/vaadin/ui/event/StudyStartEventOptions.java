package edu.kit.lymmy.vaadin.ui.event;

import edu.kit.lymmy.shared.sensor.SensorData;
import edu.kit.lymmy.shared.sensor.StudyStartData;
import edu.kit.lymmy.vaadin.language.LanguageManager;

/**
 * Created by marc on 04.03.17.
 */
public class StudyStartEventOptions extends EventOptions {

    public StudyStartEventOptions() {

    }
    @Override
    public SensorData getSensorData(LanguageManager languageManager) {
        return new StudyStartData();
    }

    @Override
    public EventOptions built(LanguageManager languageManager, SensorData sensorData) {
        return this;
    }
}
