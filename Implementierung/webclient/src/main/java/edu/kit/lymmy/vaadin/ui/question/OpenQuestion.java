package edu.kit.lymmy.vaadin.ui.question;

import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.ui.design.OpenQuestionDesign;

/**
 * Displays a Text that describes the OpenQuestion.
 *
 * @author Marc
 * @since 01.02.2017
 */
public class OpenQuestion extends OpenQuestionDesign {

    /**
     * Constructor that adds functionality to the OpenQuestionDesign.
     *
     * @param languageManager Language manager
     */
    public OpenQuestion(LanguageManager languageManager){

        textLabel.setValue(languageManager.getCaptions().getString("openQuestionTextLabel"));

    }
}
