package edu.kit.lymmy.vaadin.auth;

import com.vaadin.spring.annotation.VaadinSessionScope;
import edu.kit.lymmy.shared.user.VerifyCode;
import edu.kit.lymmy.vaadin.network.JsonService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpStatusCodeException;

import javax.annotation.PreDestroy;

import static edu.kit.lymmy.shared.messages.CommunicationConstants.*;

/**
 * Manages authentication of users
 *
 * @author Lukas
 * @since 04.01.2017
 */
@VaadinSessionScope
@Component
public class AuthenticationManager implements JsonService.SessionExpiryListener {

    private final JsonService jsonService;
    private final Logger logger;
    private String username;

    /**
     * Creates a new instance
     *
     * @param jsonService the service used to communicate with the backend
     */
    @Autowired
    public AuthenticationManager(JsonService jsonService) {
        this.jsonService = jsonService;
        logger = LoggerFactory.getLogger(AuthenticationManager.class);
        jsonService.addSessionExpiryListener(this);
    }

    @PreDestroy
    private void onDestroy() {
        jsonService.removeSessionExpiryListener(this);
    }

    /**
     * @return the username of the currently logged in user, or null if there is none
     */
    @Nullable
    public String getUsername() {
        return username;
    }

    /**
     * tries to login with the supplied parameters
     *
     * @param username the user identification
     * @param password the user password (plain text)
     * @return if the user was logged in successfully
     */
    @NotNull
    public LoginResult login(@NotNull String username, @NotNull String password) {
        try {
            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
            map.add(PARAM_USERNAME, username);
            map.add(PARAM_PASSWORD, password);
            LoginResult result = jsonService.request(LEADER_LOGIN).formEncoded().send(map, LoginResult.class);
            if(result == LoginResult.OK) {
                this.username = username;
            }
            return result != null ? result : LoginResult.UNKNOWN;
        } catch (HttpStatusCodeException e) {
            logger.warn("login of user " + username + " failed", e);
            return LoginResult.UNKNOWN;
        }
    }

    /**
     * registers a new user
     *
     * @param password the user password
     * @param email the user email, used as identification
     * @return if the request was successful (might fail e.g. if the email is already registered)
     */
    public boolean register(@NotNull String password, @NotNull String email) {
        try {
            Boolean result = jsonService.request(LEADER_REGISTER_START).queryParam(PARAM_USERNAME, Base64Utils.encodeToString(email.getBytes())).queryParam(PARAM_PASSWORD, Base64Utils.encodeToString(password.getBytes())).send(Boolean.class);
            if (result == null) result = false;
            return result;
        } catch (HttpStatusCodeException e) {
            logger.warn("registration with email " + email + " failed", e);
            return false;
        }
    }


    public boolean resendActivationCode(@NotNull String email) {
        try {
            Boolean result = jsonService.request(LEADER_RESEND_CODE).queryParam(PARAM_USERNAME, Base64Utils.encodeToString(email.getBytes())).send(Boolean.class);
            if (result == null) result = false;
            return result;
        } catch (HttpStatusCodeException e) {
            logger.warn("failed to resend activation code to " + email, e);
            return false;
        }
    }

    /**
     * activates an existing user
     * @param verifyCode the code used to verify the email, base64-encoded
     * @return if the request was successful
     */
    public boolean activate(@NotNull String email, @NotNull String verifyCode) {
        try {
            Boolean result = jsonService.request(LEADER_REGISTER_FINISH).send(new VerifyCode(email, Base64Utils.decodeFromString(verifyCode)), Boolean.class);
            if (result == null) result = false;
            return result;
        } catch (HttpStatusCodeException e) {
            logger.warn("activation failed", e);
            return false;
        }
    }

    /**
     * logs out the current user
     *
     * @return if the request was successful
     */
    public boolean logout() {
        try {
            jsonService.request(LEADER_LOGOUT).send(Void.class);
            username = null;
            return true;
        } catch (HttpStatusCodeException e) {
            logger.warn("Logout failed", e);
            return false;
        }
    }

    @Override
    public void onSessionExpired() {
        username = null;
    }
}
