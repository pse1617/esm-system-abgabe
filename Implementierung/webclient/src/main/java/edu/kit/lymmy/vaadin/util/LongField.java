package edu.kit.lymmy.vaadin.util;

import com.vaadin.event.FieldEvents;
import com.vaadin.ui.AbstractTextField;
import com.vaadin.ui.TextField;

/**
 * Created by marc on 04.03.17.
 */
public class LongField extends TextField implements FieldEvents.TextChangeListener {
    private String lastValue;

    public LongField() {
        setImmediate(true);
        setTextChangeEventMode(AbstractTextField.TextChangeEventMode.EAGER);
        addTextChangeListener(this);
    }

    @Override
    public void textChange(FieldEvents.TextChangeEvent event) {
        String text = event.getText();
        try {
            new Long(text);
            lastValue = text;
        } catch (NumberFormatException e) {
            if(lastValue!=null){
                setValue(lastValue);
            } else {
                setValue("0");
            }

        }
    }
}
