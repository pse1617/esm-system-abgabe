package edu.kit.lymmy.vaadin.ui;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import edu.kit.lymmy.shared.Study;
import edu.kit.lymmy.vaadin.builder.StudyBuilder;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.navigation.NavigationManager;
import edu.kit.lymmy.vaadin.ui.design.NewStudiesLayoutDesign;
import edu.kit.lymmy.vaadin.util.DeleteStudyPopupWindow;
import edu.kit.lymmy.vaadin.util.StudyIDPopupWindow;

import java.util.List;

/**
 * This page displays the new studies and provides the functionality edit, delete and add studies.
 *
 * @author Marc
 * @since 19.01.2017
 */
public class NewStudiesLayout extends NewStudiesLayoutDesign implements View {
    private static Study selectedStudy;

    /**
     * Constructor that adds functionality to the NewStudiesLayoutDesign.
     *
     * @param navigationManager Navigation Manager
     * @param studies List of studies
     * @param languageManager Language manager
     */
    public NewStudiesLayout(NavigationManager navigationManager, List<Study> studies, LanguageManager languageManager) {

        newStudiesLabel.setValue(languageManager.getCaptions().getString("newStudiesLabel"));
        deleteBtn.setCaption(languageManager.getCaptions().getString("deleteBtn"));
        editBtn.setCaption(languageManager.getCaptions().getString("editBtn"));
        newBtn.setCaption(languageManager.getCaptions().getString("newBtn"));
        newStudiesTable.addContainerProperty(languageManager.getCaptions().getString("studyName"), String.class, null);
        newStudiesTable.addContainerProperty(languageManager.getCaptions().getString("starts"), String.class, null);
        newStudiesTable.addContainerProperty(languageManager.getCaptions().getString("id"), CssLayout.class, null);
        idBtn.addClickListener(e -> {
            if(selectedStudy!=null)
                new StudyIDPopupWindow(selectedStudy, languageManager);
        });

        for (Study study : studies) {
            String startDate;
            if(study.getStartDate()==null) {
                startDate = "-";
            } else {
                startDate = study.getStartDate().getDayOfMonth()+". "+languageManager.getCaptions().getString(study.getStartDate().getMonth().toString())+" "+study.getStartDate().getYear();
            }
            newStudiesTable.addItem(new Object[]{study.getName(), startDate, new CssLayout(new Label(study.getId()))}, study);
        }
        newStudiesTable.addValueChangeListener(e -> selectedStudy = (Study) e.getProperty().getValue());
        newStudiesTable.addItemClickListener(e -> {
            selectedStudy = (Study) e.getItemId();
            if(e.isDoubleClick())
                edit(navigationManager, languageManager);
        });

        newBtn.addClickListener(e -> newStudy(navigationManager));

        editBtn.addClickListener(e ->  edit(navigationManager, languageManager));

        deleteBtn.addClickListener(e -> delete(navigationManager, languageManager));



    }

    /**
     * Creates a new study and navigates to the study editor.
     *
     * @param navigationManager Navigation manager
     */
    private void newStudy(NavigationManager navigationManager) {
        StudyBuilder study = navigationManager.getNewStudy();
        if(study != null) {
            navigationManager.editStudy(study);
            selectedStudy = null;
        }
        //TODO display warning if unable to get new study
    }

    /**
     * Deletes the selected study.
     *
     * @param navigationManager Navigation manager
     * @param languageManager Language manager
     */
    private void delete(NavigationManager navigationManager, LanguageManager languageManager) {
        if (selectedStudy != null) {
            DeleteStudyPopupWindow deleteMessage = new DeleteStudyPopupWindow(navigationManager, languageManager, selectedStudy, newStudiesTable);
        }else {
            Notification notification = new Notification(languageManager.getCaptions().getString("selectStudy"), Notification.Type.WARNING_MESSAGE);
            notification.setDelayMsec(2000);
            notification.show(Page.getCurrent());
    }
    }

    /**
     * Navigates to the study editor with the selected study.
     *
     * @param navigationManager Navigation manager
     * @param languageManager Language manager
     */
    private void edit(NavigationManager navigationManager, LanguageManager languageManager) {
        if(selectedStudy!=null) {
            navigationManager.editStudy(new StudyBuilder(selectedStudy));
            selectedStudy = null;
        } else {
            Notification notification = new Notification(languageManager.getCaptions().getString("selectStudy"), Notification.Type.WARNING_MESSAGE);
            notification.setDelayMsec(2000);
            notification.show(Page.getCurrent());
        }
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
    }
}
