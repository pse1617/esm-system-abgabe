package edu.kit.lymmy.vaadin.ui.question;

import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.ui.design.YesNoQuestionDesign;

/**
 *  Displays a Text that describes the YesNoQuestion:
 *
 * @author Marc
 * @since 01.02.2017
 */
public class YesNoQuestion extends YesNoQuestionDesign {

    /**
     * Constructor that adds functionality to the YesNoQuestionDesign.
     *
     * @param languageManager Language manager
     */
    public YesNoQuestion(LanguageManager languageManager){
        yesNoTextLabel.setValue(languageManager.getCaptions().getString("yesNoTextLabel"));
    }
}
