package edu.kit.lymmy.vaadin.ui;

import com.vaadin.ui.Button;
import com.vaadin.ui.Window;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.ui.design.DeleteMessageDesign;

/**
 * This page displays an over handed message and adds the overhanded clickListener to the confirmationBtn.
 *
 * @author Marc
 * @since 29.01.2017
 */
public class DeleteMessage extends DeleteMessageDesign {

    /**
     * Constructor that adds functionality to the DeleteMessageDesign.
     *
     * @param message String to set as Label
     * @param listener Listener for the confirmationBtn
     * @param languageManager Language manager
     */
    public DeleteMessage(String message, Button.ClickListener listener, LanguageManager languageManager){

        messageLabel.setValue(message);
        confirmationBtn.addClickListener(listener);
        declineBtn.addClickListener(e -> ((Window)this.getParent()).close());
        declineBtn.setCaption(languageManager.getCaptions().getString("declineBtn"));
        confirmationBtn.setCaption(languageManager.getCaptions().getString("confirmBtn"));
        this.setSizeUndefined();
    }
}
