package edu.kit.lymmy.vaadin.ui;

import com.vaadin.server.ThemeResource;
import com.vaadin.spring.annotation.VaadinSessionScope;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.ui.design.LanguageSelectionDesign;
import org.springframework.stereotype.Component;

/**
 * This Panel displays the available languages and provides the functionality to change them.
 *
 * @author Marc
 * @since 01.02.2017
 */
@Component
@VaadinSessionScope
public class LanguageSelection extends LanguageSelectionDesign {

    /**
     * Constructor that adds functionality to the LanguageSelectionDesign.
     *
     * @param languageManager Language manager
     */
    public LanguageSelection(LanguageManager languageManager){
        deBtn.setIcon(new ThemeResource("icon_de.png"));
        enBtn.setIcon(new ThemeResource("icon_us.png"));
        languageLabel.setValue(languageManager.getCaptions().getString("language"));
        deBtn.addClickListener(e -> {
            languageManager.setLanguage("de");
            languageManager.setCountry("DE");
            languageManager.refreshLocale();
            languageManager.refreshCaptions();
            languageLabel.setValue(languageManager.getCaptions().getString("language"));
        });
        enBtn.addClickListener(e -> {
            languageManager.setLanguage("en");
            languageManager.setCountry("US");
            languageManager.refreshLocale();
            languageManager.refreshCaptions();
            languageLabel.setValue(languageManager.getCaptions().getString("language"));
        });
    }
}
