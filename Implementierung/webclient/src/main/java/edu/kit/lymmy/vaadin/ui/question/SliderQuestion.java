package edu.kit.lymmy.vaadin.ui.question;

import com.vaadin.ui.TextField;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.ui.design.SliderQuestionDesign;


/**
 * Provides an interface to edit a sliderQuestion.
 *
 * @author Marc
 * @since 27.01.2017
 */
public class SliderQuestion extends SliderQuestionDesign {

    /**
     * Constructor that adds functionality to the SliderQuestionDesign.
     *
     * @param languageManager Language manager
     */
    public SliderQuestion(LanguageManager languageManager) {
        stepsPanel.setVisible(false);

        leftLabelPanel.setCaption(languageManager.getCaptions().getString("leftLabelPanel"));
        rightLabelPanel.setCaption(languageManager.getCaptions().getString("rightLabelPanel"));

    }

    public TextField getLeftLabelField(){
        return leftLabelField;
    }

    public TextField getRightLabelField(){
        return rightLabelField;
    }
}
