package edu.kit.lymmy.vaadin.network;

import com.vaadin.spring.annotation.VaadinSessionScope;
import edu.kit.lymmy.shared.Study;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.context.annotation.ApplicationScope;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static edu.kit.lymmy.shared.messages.CommunicationConstants.*;

/**
 * @author Lukas
 * @since 04.01.2017
 */
@VaadinSessionScope
@Component
public class ContentConnector {

    private final JsonService jsonService;
    private final Logger logger;

    @Autowired
    public ContentConnector(JsonService jsonService) {
        this.jsonService = jsonService;
        logger = LoggerFactory.getLogger(ContentConnector.class);
    }

    @NotNull
    public List<Study> loadStudies() {
        try {
            Study[] studies = jsonService.request(GET_STUDIES)
                    .send(Study[].class);
            if (studies != null) {
                return Arrays.asList(studies);
            }
        } catch (HttpStatusCodeException e) {
            logger.warn("loading studies failed with exception", e);
        }
        return new ArrayList<>();
    }

    public boolean saveStudy(@NotNull Study study) {
        try {
            Boolean result = jsonService.request(STORE_STUDY).send(study, Boolean.class);
            if(result == null) result = false;
            return result;
        } catch (HttpStatusCodeException e) {
            logger.warn("saving study " + study.getId() + " failed with exception", e);
            return false;
        }
    }

    @Nullable
    public String getCsvForStudy(@NotNull Study study) {
        try {
            return jsonService.request(EXPORT_STUDY).queryParam(PARAM_STUDY_ID, study.getId()).send(String.class);
        }catch (HttpStatusCodeException e){
            logger.warn("failed to get csv for study "+study.getId(), e);
            return null;
        }
    }

    public boolean deleteStudy(@NotNull Study study) {
        try {
            Boolean result = jsonService.request(DELETE_STUDY).queryParam(PARAM_STUDY_ID, study.getId()).send(Boolean.class);
            if(result == null) result = false;
            return result;
        }catch (HttpStatusCodeException e) {
            logger.warn("deleting study " + study.getId() + " failed", e);
            return false;
        }
    }

    @Nullable
    public String getNewStudyId() {
        try {
            return jsonService.request(CREATE_STUDY_ID).send(String.class);
        }catch (HttpStatusCodeException e){
            logger.warn("failed to get new study id", e);
            return null;
        }
    }
}
