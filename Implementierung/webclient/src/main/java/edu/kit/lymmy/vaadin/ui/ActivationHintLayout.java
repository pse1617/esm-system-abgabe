package edu.kit.lymmy.vaadin.ui;

import com.vaadin.spring.annotation.UIScope;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.ui.design.ActivationHintLayoutDesign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * The page that is displayed if a not activated user tries to log in.
 *
 * Created by Marc on 09.02.2017.
 */
@UIScope
@Component
public class ActivationHintLayout extends ActivationHintLayoutDesign {

    /**
     * Constructor that adds functionality to the ActivationHintLayoutDesign.
     *
     * @param ui Web user interface
     * @param languageManager Language Manager
     * @param context Application Context
     */
    @Autowired
    public ActivationHintLayout(WebUi ui, LanguageManager languageManager, ApplicationContext context) {
        hintLabel.setValue(languageManager.getCaptions().getString("hintLabel"));
        manualInputBtn.setCaption(languageManager.getCaptions().getString("manualInputBtn"));
        backToLoginBtn.setCaption(languageManager.getCaptions().getString("backBtn"));

        manualInputBtn.addClickListener(e -> ui.getPage().setLocation("/register"));
        backToLoginBtn.addClickListener(e -> ui.setContentBean(LoginLayout.class));
    }
}
