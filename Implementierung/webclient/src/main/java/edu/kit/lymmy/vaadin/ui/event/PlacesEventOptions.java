package edu.kit.lymmy.vaadin.ui.event;

import com.vaadin.ui.ComboBox;
import edu.kit.lymmy.shared.sensor.LocationTypeData;
import edu.kit.lymmy.shared.sensor.SensorData;
import edu.kit.lymmy.vaadin.language.LanguageManager;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by marc on 16.03.2017.
 */
public class PlacesEventOptions extends EventOptions {

    private ComboBox placesSelection;
    private Map<String, LocationTypeData.Locationtype> locationtypeMap = new HashMap<>();

    public PlacesEventOptions(LanguageManager languageManager) {
        placesSelection = new ComboBox(languageManager.getCaptions().getString("places"));
        for (LocationTypeData.Locationtype locationtype : LocationTypeData.Locationtype.values()) {
            placesSelection.addItem(languageManager.getCaptions().getString(locationtype.toString()));
            locationtypeMap.put(languageManager.getCaptions().getString(locationtype.toString()), locationtype);
        }
        placesSelection.setNullSelectionAllowed(false);
        placesSelection.setValue(LocationTypeData.Locationtype.accounting.toString());
        addComponent(placesSelection);
        placesSelection.setWidth("200px");

    }

    @Override
    public SensorData getSensorData(LanguageManager languageManager) {
        return new LocationTypeData(locationtypeMap.get(placesSelection.getValue()));
    }

    @Override
    public EventOptions built(LanguageManager languageManager, SensorData sensorData) {
        PlacesEventOptions placesEventOptions = this;
        placesSelection.setValue(((LocationTypeData)sensorData).getLocationType().toString());
        return placesEventOptions;
    }
}
