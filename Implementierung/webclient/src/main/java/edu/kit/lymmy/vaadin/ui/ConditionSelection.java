package edu.kit.lymmy.vaadin.ui;

import com.vaadin.data.validator.NullValidator;
import com.vaadin.server.Page;
import com.vaadin.ui.Notification;
import edu.kit.lymmy.shared.question.Condition;
import edu.kit.lymmy.shared.question.QuestionType;
import edu.kit.lymmy.vaadin.builder.QuestionBuilder;
import edu.kit.lymmy.vaadin.builder.QuestionnaireBuilder;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.ui.design.ConditionSelectionDesign;
import edu.kit.lymmy.vaadin.util.PopupWindow;

import java.util.HashMap;
import java.util.Map;

/**
 * This page provides the possibility to add a condition to an question.
 *
 * @author Marc
 * @since 07.02.2017
 */
class ConditionSelection extends ConditionSelectionDesign {

    private final Map<String, QuestionBuilder> questionMap = new HashMap<>();
    private QuestionBuilder selectedQuestion;

    /**
     * Constructor that adds functionality to the ConditionSelectionDesign.
     *
     * @param languageManager Language manager
     * @param questionnaireBuilder Questionnaire builder
     * @param questionBuilder Question Builder
     * @param questionEditorLayout Question editor page
     */
    public ConditionSelection(LanguageManager languageManager, QuestionnaireBuilder questionnaireBuilder, QuestionBuilder questionBuilder, QuestionEditorLayout questionEditorLayout) {

        setSizeUndefined();
        conditionLabel.setValue(languageManager.getCaptions().getString("conditionLabel"));
        conditionQuestionPanel.setCaption(languageManager.getCaptions().getString("conditionQuestionPanel"));
        conditionAnswerPanel.setCaption(languageManager.getCaptions().getString("conditionAnswerPanel"));
        deleteConditionBtn.setCaption(languageManager.getCaptions().getString("deleteBtn"));
        saveConditionBtn.setCaption(languageManager.getCaptions().getString("saveBtn"));
        conditionQuestionCB.addValidator(new NullValidator(languageManager.getCaptions().getString("questionValidationMsg"),false));
        conditionAnswerCB.addValidator(new NullValidator(languageManager.getCaptions().getString("answerValidationMsg"),false));
        conditionQuestionCB.setValidationVisible(false);
        conditionAnswerCB.setValidationVisible(false);

        for (QuestionBuilder question : questionnaireBuilder.getQuestions()) {
            if(question.getType().equals(QuestionType.MULTIPLE_CHOICE) || question.getType().equals(QuestionType.SINGLE_CHOICE) || question.getType().equals(QuestionType.YES_NO)) {
                if(question != questionBuilder) {
                    conditionQuestionCB.addItem(question.getText());
                    questionMap.put(question.getText(), question);
                } else {
                    break;
                }
            }
        }

        if(questionBuilder.getCondition().getChoice() != -1 && questionBuilder.getCondition().getQuestion() != -1 ) {
            QuestionBuilder qb = questionnaireBuilder.getQuestions().get(questionBuilder.getCondition().getQuestion());
            selectedQuestion = qb;
            conditionQuestionCB.setValue(qb.getText());
            for (String answer : selectedQuestion.getAnswers()) {
                conditionAnswerCB.addItem(answer);
            }
            conditionAnswerCB.setValue(qb.getAnswers().get(questionBuilder.getCondition().getChoice()));
        }

        conditionQuestionCB.setNullSelectionAllowed(false);
        conditionAnswerCB.setNullSelectionAllowed(false);

        conditionQuestionCB.addValueChangeListener(e -> {
            //noinspection SuspiciousMethodCalls
            conditionAnswerCB.removeAllItems();
            selectedQuestion = questionMap.get(e.getProperty().getValue());
            for (String answer : selectedQuestion.getAnswers()) {
                conditionAnswerCB.addItem(answer);
            }
        });


        saveConditionBtn.addClickListener(e -> {
            if (conditionQuestionCB.isValid() && conditionAnswerCB.isValid()) {
                //noinspection SuspiciousMethodCalls
                Condition condition = new Condition(questionnaireBuilder.getQuestions().indexOf(selectedQuestion), selectedQuestion.getAnswers().indexOf(conditionAnswerCB.getValue()));
                questionBuilder.setCondition(condition);
                Notification saveNotification = new Notification(languageManager.getCaptions().getString("saveSuccessMsg"));
                saveNotification.setDelayMsec(2000);
                saveNotification.show(Page.getCurrent());
                ((PopupWindow)this.getParent()).close();
            } else {
                conditionQuestionCB.setValidationVisible(true);
                conditionAnswerCB.setValidationVisible(true);
            }
        });

        deleteConditionBtn.addClickListener(e -> {
            questionBuilder.setCondition(new Condition(-1, -1));
            Notification notification = new Notification(languageManager.getCaptions().getString("deleteSuccessMsg"));
            notification.setDelayMsec(2000);
            notification.show(Page.getCurrent());
            ((PopupWindow)this.getParent()).close();
        });



    }
}
