package edu.kit.lymmy.vaadin.ui;

import com.vaadin.data.Validator;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.server.Page;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import edu.kit.lymmy.vaadin.auth.AuthenticationManager;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.navigation.NavigationManager;
import edu.kit.lymmy.vaadin.ui.design.RegistrationLayoutDesign;
import edu.kit.lymmy.vaadin.validators.PasswordEqualsValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * Created by Marc on 28.01.2017.
 */
@SpringUI
@UIScope
@Component
public class RegistrationLayout extends RegistrationLayoutDesign {

    @Autowired
    public RegistrationLayout(WebUi ui, AuthenticationManager authenticationManager, NavigationManager navigationManager, ApplicationContext context, LanguageManager languageManager){


        PasswordField password = new PasswordField();
        PasswordField passwordConfirmation = new PasswordField();

        passwordPanel.addComponent(password);
        passwordConfirmationPanel.addComponent(passwordConfirmation);

        registerBtn.setCaption(languageManager.getCaptions().getString("registrationBtn"));
        backToLoginBtn.setCaption(languageManager.getCaptions().getString("backBtn"));
        registrationLabel.setValue(languageManager.getCaptions().getString("registrationLabel"));
        passwordConfirmationPanel.setCaption(languageManager.getCaptions().getString("passwordConfirmationPanel"));
        passwordPanel.setCaption(languageManager.getCaptions().getString("passwordPanel"));
        mailPanel.setCaption(languageManager.getCaptions().getString("mailPanel"));


        //adding Validators
        passwordConfirmation.addValidator(new PasswordEqualsValidator(password, passwordConfirmation, languageManager));
        password.addValidator(new StringLengthValidator(languageManager.getCaptions().getString("passwordValidator"), 4, Integer.MAX_VALUE, false));
        mail.addValidator(new EmailValidator(languageManager.getCaptions().getString("mailValidator")));

        password.setValidationVisible(false);
        passwordConfirmation.setValidationVisible(false);
        mail.setValidationVisible(false);




        registerBtn.addClickListener(e -> {
            boolean registrationValid = true;
            password.setValidationVisible(false);
            passwordConfirmation.setValidationVisible(false);
            mail.setValidationVisible(false);
            try{
                password.validate();
                passwordConfirmation.validate();
                mail.validate();
            } catch (Validator.InvalidValueException exeption ) {
                registrationValid = false;
                password.setValidationVisible(true);
                passwordConfirmation.setValidationVisible(true);
                mail.setValidationVisible(true);
                Notification exception = new Notification(exeption.getMessage(), Notification.Type.WARNING_MESSAGE);
                exception.setDelayMsec(2000);
                exception.show(Page.getCurrent());
            }
            if(registrationValid){
                if(authenticationManager.register(password.getValue(), mail.getValue())){
                    ui.setContent(context.getBean(RegistrationConfirmationLayout.class));
                } else {
                    Notification registrationFail = new Notification("Registration failed", Notification.Type.ERROR_MESSAGE);
                    registrationFail.setDelayMsec(2000);
                    registrationFail.show(Page.getCurrent());
                }
            }

        });
        backToLoginBtn.addClickListener(e -> ui.setContent(context.getBean(LoginLayout.class)));
    }
}
