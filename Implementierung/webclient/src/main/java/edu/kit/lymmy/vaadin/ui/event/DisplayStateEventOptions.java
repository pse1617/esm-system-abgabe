package edu.kit.lymmy.vaadin.ui.event;

import com.vaadin.ui.ComboBox;
import edu.kit.lymmy.shared.sensor.DisplayStateData;
import edu.kit.lymmy.shared.sensor.SensorData;
import edu.kit.lymmy.vaadin.language.LanguageManager;

/**
 * Created by marc on 04.03.17.
 */
public class DisplayStateEventOptions extends EventOptions {

    private final ComboBox statesCB;

    public DisplayStateEventOptions (LanguageManager languageManager) {

        statesCB = new ComboBox(languageManager.getCaptions().getString("DISPLAY_STATE"));
        for (DisplayStateData.DisplayState displayState : DisplayStateData.DisplayState.values()) {
            statesCB.addItem(languageManager.getCaptions().getString(displayState.toString()));
        }
        statesCB.setNullSelectionAllowed(false);
        statesCB.setValue(languageManager.getCaptions().getString("ON"));
        addComponent(statesCB);
        statesCB.setWidth("150px");

    }

    @Override
    public SensorData getSensorData(LanguageManager languageManager) {
        if (statesCB.getValue().equals(languageManager.getCaptions().getString("ON"))) {
            return new DisplayStateData(DisplayStateData.DisplayState.ON);
        } else {
            if (statesCB.getValue().equals(languageManager.getCaptions().getString("OFF"))) {
                return new DisplayStateData(DisplayStateData.DisplayState.OFF);
            } else {
                return new DisplayStateData(DisplayStateData.DisplayState.UNLOCKED);
            }
        }
    }

    @Override
    public EventOptions built(LanguageManager languageManager, SensorData sensorData) {
        EventOptions eventOptions = this;
        DisplayStateData data = (DisplayStateData) sensorData;
        this.statesCB.setValue(languageManager.getCaptions().getString(data.getState().toString()));
        return eventOptions;
    }
}
