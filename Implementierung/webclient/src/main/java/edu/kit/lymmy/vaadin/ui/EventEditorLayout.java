package edu.kit.lymmy.vaadin.ui;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.ui.Notification;
import edu.kit.lymmy.vaadin.builder.EventBuilder;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.navigation.NavigationManager;
import edu.kit.lymmy.vaadin.ui.design.EventEditorLayoutDesign;

/**
 * The page that provides the possibility of editing an event.
 *
 * @author Marc
 * @since 24.01.2017
 */
public class EventEditorLayout extends EventEditorLayoutDesign implements View {

    /**
     * Constructor that adds functionality to the EventEditorLayoutDesign.
     *
     * @param navigationManager Navigation manager
     * @param eventBuilder Event builder
     * @param languageManager Language manager
     */

    public EventEditorLayout(NavigationManager navigationManager, EventBuilder eventBuilder, LanguageManager languageManager){
        backBtn.addClickListener(e -> navigationManager.navigateUp());
        backBtn.setCaption(languageManager.getCaptions().getString("backBtn"));
        saveBtn.setCaption(languageManager.getCaptions().getString("saveBtn"));
        eventEditorLabel.setValue(languageManager.getCaptions().getString("eventEditorLabel"));

        EventBuilderLayout eventBuilderLayout = new EventBuilderLayout(languageManager, eventBuilder);
        eventBuilderPanel.addComponent(eventBuilderLayout);


        saveBtn.addClickListener(e -> save(eventBuilderLayout, eventBuilder, navigationManager, languageManager));

    }

    private void save(EventBuilderLayout eventBuilderLayout, EventBuilder eventBuilder, NavigationManager navigationManager, LanguageManager languageManager) {
        eventBuilderLayout.setEvent(eventBuilder, languageManager);
        navigationManager.saveStudy();
        Notification notification = new Notification(languageManager.getCaptions().getString("saveSuccessMsg"));
        notification.setDelayMsec(2000);
        notification.show(Page.getCurrent());
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }
}
