package edu.kit.lymmy.vaadin.ui;

import com.vaadin.server.Page;
import com.vaadin.server.Sizeable;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.ui.design.ChoiceQuestionsLayoutDesign;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is the blueprint for MultipleChoiceQuestion and SingleChoiceQuestion.
 *
 * @author Marc
 * @since 27.01.2017
 */
public class ChoiceQuestionsLayout extends ChoiceQuestionsLayoutDesign {

    private List<TextField> answers = new ArrayList<>();
    private int amountOfAnswers;

    /**
     * Constructor that adds functionality to the ChoiceQuestionsLayoutDesign.
     * @param languageManager Language Manager
     */
    protected ChoiceQuestionsLayout(LanguageManager languageManager){
        addAnswer(languageManager);
        addAnswer(languageManager);
        addBtn.addClickListener(e -> addAnswer(languageManager));
        deleteBtn.addClickListener(e -> removeAnswer(languageManager));
        addBtn.setCaption(languageManager.getCaptions().getString("addBtn"));
        deleteBtn.setCaption(languageManager.getCaptions().getString("deleteBtn"));
    }

    /**
     * Removes the last added TextField of the answers list if there are at least 2 remaining TextFields.
     * @param languageManager Language Manager
     */
    private void removeAnswer(LanguageManager languageManager) {
        if(amountOfAnswers > 2) {
            answerPanel.removeComponent(answers.get(amountOfAnswers - 1));
            amountOfAnswers += -1;
        } else {
            Notification notification = new Notification(languageManager.getCaptions().getString("min2Answers"), Notification.Type.WARNING_MESSAGE);
            notification.setDelayMsec(2000);
            notification.show(Page.getCurrent());
        }
    }

    /**
     * Creates a new TextField and adds it to the answers list and the answersPanel.
     * @param languageManager Language Manager
     */
    private void addAnswer(LanguageManager languageManager) {
        answers.add(new TextField());
        amountOfAnswers+=1;
        answerPanel.addComponent(answers.get(amountOfAnswers-1));
        answers.get(amountOfAnswers-1).addStyleName(ValoTheme.TEXTFIELD_TINY);
        answers.get(amountOfAnswers-1).setInputPrompt(languageManager.getCaptions().getString("emptyAnswerField"));
        answers.get(amountOfAnswers-1).setWidth(300, Sizeable.Unit.PIXELS);
    }

    /**
     * This method removes all componets of the answerPanel and the answers list and creates new ones by using the over handed list of answer Strings.
     * The purpose of this Method is to fill the layout with the content of an existing question.
     * @param answerList List of answer Strings
     */
    public void setAnswers(List<String> answerList) {
        answerPanel.removeAllComponents();
        List<TextField> answerFieldList = new ArrayList<>();
        amountOfAnswers = 0;
        for ( String answer : answerList) {
            answerFieldList.add(new TextField(null, answer));
            amountOfAnswers +=1;
        }
        answers = answerFieldList;
        for (TextField tf : answerFieldList) {
            tf.addStyleName(ValoTheme.TEXTFIELD_TINY);
            tf.setWidth(300, Sizeable.Unit.PIXELS);
            answerPanel.addComponent(tf);
        }

    }

    /**
     * Returns the answers list of TextFields.
     * @return returns answers
     */
    public List<TextField> getAnswers() {
        return answers;
    }

}

