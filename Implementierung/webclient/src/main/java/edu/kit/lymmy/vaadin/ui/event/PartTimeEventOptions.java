package edu.kit.lymmy.vaadin.ui.event;

import com.vaadin.ui.ComboBox;
import edu.kit.lymmy.shared.sensor.PartTimeData;
import edu.kit.lymmy.shared.sensor.SensorData;
import edu.kit.lymmy.vaadin.language.LanguageManager;

/**
 * Created by marc on 04.03.17.
 */
public class PartTimeEventOptions extends EventOptions {

    private final ComboBox operatorCB;
    private final ComboBox timeCB;

    public PartTimeEventOptions(LanguageManager languageManager) {
        operatorCB = new ComboBox(languageManager.getCaptions().getString("Operator"));
        operatorCB.addItem("=");
        operatorCB.addItem("!=");
        operatorCB.setValue("=");
        operatorCB.setNullSelectionAllowed(false);
        operatorCB.setWidth("70px");
        addComponent(operatorCB);
        timeCB = new ComboBox(languageManager.getCaptions().getString("PARTTIME"));
        for (PartTimeData.Timetype timetype : PartTimeData.Timetype.values()) {
            timeCB.addItem(languageManager.getCaptions().getString(timetype.toString()));
        }
        timeCB.setValue(languageManager.getCaptions().getString("Morning"));
        timeCB.setNullSelectionAllowed(false);
        addComponent(timeCB);
    }

    @Override
    public SensorData getSensorData(LanguageManager languageManager) {
        PartTimeData.Timetype timetype;
        Boolean not;
        not = !operatorCB.getValue().equals("=");
        if(timeCB.getValue().equals(languageManager.getCaptions().getString("Morning"))) {
            timetype= PartTimeData.Timetype.Morning;
        } else {
            if(timeCB.getValue().equals(languageManager.getCaptions().getString("Afternoon"))) {
                timetype= PartTimeData.Timetype.Afternoon;
            } else {
                if(timeCB.getValue().equals(languageManager.getCaptions().getString("Evening"))) {
                    timetype= PartTimeData.Timetype.Evening;
                } else {
                    timetype= PartTimeData.Timetype.Night;
                }
            }
        }
        return new PartTimeData(timetype, not);
    }

    @Override
    public EventOptions built(LanguageManager languageManager, SensorData sensorData) {
        PartTimeEventOptions partTimeEventOptions = this;
        PartTimeData data = (PartTimeData) sensorData;
        partTimeEventOptions.timeCB.setValue(languageManager.getCaptions().getString(data.getTimeType().toString()));
        if(data.getNot()) {
            partTimeEventOptions.operatorCB.setValue("!=");
        } else {
            partTimeEventOptions.operatorCB.setValue("=");
        }
        return  partTimeEventOptions;
    }
}
