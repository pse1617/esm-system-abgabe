package edu.kit.lymmy.vaadin.ui;

import com.vaadin.annotations.Theme;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Component;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import edu.kit.lymmy.vaadin.auth.AuthenticationManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

/**
 * @author Lukas
 * @since 02.01.2017
 */
@SpringUI
@Theme("mytheme")
public class WebUi extends UI {


    private final ApplicationContext context;
    private final AuthenticationManager authenticationManager;

    @Autowired
    public WebUi(ApplicationContext context, AuthenticationManager authenticationManager) {
        this.context = context;
        this.authenticationManager = authenticationManager;
    }

    @Override
    protected void init(VaadinRequest request) {
        String error = (String) getSession().getAttribute("error");
        getSession().setAttribute("error", "");
        Notification errorMsg = new Notification(error, Notification.Type.WARNING_MESSAGE);
        errorMsg.setDelayMsec(2000);
        if(error != null && !error.equals("")) errorMsg.show(Page.getCurrent());
        if (authenticationManager.getUsername() != null) {
            setContentBean(MainLayout.class);
        } else {
            setContentBean(LoginLayout.class);
        }
    }

    void setContentBean(Class<? extends Component> contentBean){
        setContent(context.getBean(contentBean));
    }

}
