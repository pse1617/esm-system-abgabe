package edu.kit.lymmy.vaadin.ui;

import com.vaadin.data.validator.EmailValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.ui.Window;
import edu.kit.lymmy.vaadin.auth.AuthenticationManager;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.ui.design.RequestAktivationCodeLayoutDesign;

/**
 * Created by marc on 05.03.17.
 */
public class RequestAktivationCodeLayout extends RequestAktivationCodeLayoutDesign {

    public RequestAktivationCodeLayout(LanguageManager languageManager, AuthenticationManager authenticationManager) {
        requestCodeBtn.setCaption(languageManager.getCaptions().getString("requestCodeAgainBtn"));
        mailField.setCaption(languageManager.getCaptions().getString("mailPanel"));
        mailField.addValidator(new EmailValidator(languageManager.getCaptions().getString("mailValidator")));
        mailField.addValidator(new StringLengthValidator(languageManager.getCaptions().getString("mailValidator"), 1, Integer.MAX_VALUE, false));
        mailField.setValidationVisible(false);
        requestCodeBtn.addClickListener(e -> {
            mailField.setValidationVisible(true);
            if(mailField.isValid()) {
                authenticationManager.resendActivationCode(mailField.getValue());
                ((Window)this.getParent()).close();
            }
        });
        setSizeUndefined();
    }
}
