package edu.kit.lymmy.vaadin.builder;

import edu.kit.lymmy.shared.event.CompositeEvent;
import edu.kit.lymmy.shared.event.Event;
import edu.kit.lymmy.shared.LogicalOperator;
import edu.kit.lymmy.shared.sensor.SensorData;
import edu.kit.lymmy.shared.event.SensorEvent;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Used to build an {@link Event}
 *
 * @author Lukas
 * @since 03.01.2017
 */
public class EventBuilder {
    private final LogicalNode root;

    /**
     * Creates a new instance with default values
     */
    EventBuilder() {
        root = new LogicalNode();
    }

    /**
     * Creates a new instance and loads values from the passed event
     *
     * @param event the event to load from
     */
    EventBuilder(@NotNull CompositeEvent event) {
        root = new LogicalNode(event);
    }

    /**
     * @return the root node
     */
    @NotNull
    public LogicalNode getRoot() {
        return root;
    }

    /**
     * @return a new {@link Event} with the values of this builder
     */
    @NotNull
    CompositeEvent build() {
        return root.toEvent();
    }

    /**
     * Represents one part of an event
     */
    public interface Node {
        @NotNull
        Event toEvent();
    }

    private static class NodeFactory {

        @NotNull
        static Node create(@NotNull Event event) {
            switch (event.getType()) {
                case COMPOSITE:
                    return new LogicalNode((CompositeEvent) event);
                case SENSOR:
                    return new SensorNode(((SensorEvent) event).getData());
                default:
                    throw new IllegalArgumentException();
            }
        }
    }

    /**
     * Represents a leaf part of the event, containing sensor data
     */
    public static class SensorNode implements Node {
        private SensorData data;

        /**
         * Creates a new instance with the given data
         *
         * @param data the sensorData
         */
        public SensorNode(@NotNull SensorData data) {
            this.data = data;
        }

        /**
         * @return this nodes sensorData
         */
        @NotNull
        public SensorData getData() {
            return data;
        }

        /**
         * set this nodes sensorData
         *
         * @param data the value to set to
         */
        public void setData(@NotNull SensorData data) {
            this.data = data;
        }

        @NotNull
        @Override
        public SensorEvent toEvent() {
            return new SensorEvent(data);
        }
    }

    /**
     * Represents a logical combination of several nodes
     */
    public static class LogicalNode implements Node {
        private LogicalOperator operator;
        private final List<Node> children;

        /**
         * Creates a new instance with default Operator {@link LogicalOperator#AND}
         */
        public LogicalNode() {
            operator = LogicalOperator.AND;
            children = new ArrayList<>();
        }

        /**
         * Creates a new instance and deep copies the given event
         *
         * @param event the event to copy data from
         */
        LogicalNode(@NotNull CompositeEvent event) {
            operator = event.getOperator();
            children = event.getEvents().stream().map(NodeFactory::create).collect(Collectors.toCollection(ArrayList::new));
        }

        /**
         * @return the operator used to combine children of this node
         */
        @NotNull
        public LogicalOperator getOperator() {
            return operator;
        }

        /**
         * Set the operator used to combine children of this node
         *
         * @param operator the value to set to
         */
        public void setOperator(@NotNull LogicalOperator operator) {
            this.operator = operator;
        }

        /**
         * @return a modifiable List of child nodes
         */
        @NotNull
        public List<Node> getChildren() {
            return children;
        }

        /**
         * @return an event with the content of this node
         */
        @NotNull
        @Override
        public CompositeEvent toEvent() {
            return new CompositeEvent(children.stream().map(Node::toEvent).collect(Collectors.toList()), operator);
        }
    }
}
