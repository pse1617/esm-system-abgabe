package edu.kit.lymmy.vaadin.ui;


import com.vaadin.ui.Component;
import edu.kit.lymmy.vaadin.builder.EventBuilder;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.ui.design.EventBuilderLayoutDesign;
import edu.kit.lymmy.vaadin.ui.event.EventOptions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marc on 13.02.2017.
 */
class EventBuilderLayout extends EventBuilderLayoutDesign {

    private EventLevelLayout firstLevel;

    public EventBuilderLayout(LanguageManager languageManager, EventBuilder eventBuilder) {

        if(!eventBuilder.getRoot().getChildren().isEmpty()){
            fillEventBuilder(eventBuilder, languageManager);
        } else {
            firstLevel = new EventLevelLayout(languageManager, false);
            eventPanel.addComponent(firstLevel);
        }



    }

    private void fillEventBuilder(EventBuilder eventBuilder, LanguageManager languageManager) {
        firstLevel = new EventLevelLayout(languageManager, true).buildLevel(eventBuilder.getRoot(), languageManager);
        eventPanel.addComponent(firstLevel);
    }

    /**
     * Builds the event.
     * @param eventBuilder
     */
    public void setEvent(EventBuilder eventBuilder, LanguageManager languageManager) {
        eventBuilder.getRoot().setOperator(firstLevel.linking);
        List<Component> componentList = new ArrayList<>();

        for (int i = 1; i < firstLevel.getLevelPanel().getComponentCount(); i++) {
            componentList.add(firstLevel.getLevelPanel().getComponent(i));
        }
        eventBuilder.getRoot().getChildren().clear();

        for (Component component : componentList) {
            if (component.getClass().equals(EventLevelLayout.class)) {
                eventBuilder.getRoot().getChildren().add(((EventLevelLayout) component).getLogicalNode(languageManager));
            } else {
                eventBuilder.getRoot().getChildren().add(((EventLayout) component).getSensorNode((EventOptions)((EventLayout)component).getEventOptionsPanel().getComponent(0), languageManager));
            }
        }
    }
}
