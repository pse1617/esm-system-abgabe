package edu.kit.lymmy.vaadin.ui;

import com.vaadin.ui.Component;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.HorizontalLayout;
import edu.kit.lymmy.shared.sensor.SensorType;
import edu.kit.lymmy.vaadin.builder.EventBuilder;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.ui.design.EventLayoutDesign;
import edu.kit.lymmy.vaadin.ui.event.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Marc on 13.02.2017.
 */
public class EventLayout extends EventLayoutDesign {

    private final Map<String, EventOptions> eventOptionsMap = new HashMap<>();
    private final Map<SensorType, EventOptions> eventOptionsBySensorMap = new HashMap<>();

    public EventLayout(LanguageManager languageManager, EventLevelLayout eventLevel, boolean build) {

        eventOptionsBySensorMap.put(SensorType.TIME, new TimeEventOptions(languageManager));
        eventOptionsBySensorMap.put(SensorType.WEATHER, new WeatherEventOptions(languageManager));
        eventOptionsBySensorMap.put(SensorType.ACCELERATION, new AccelerationEventOptions(languageManager));
        eventOptionsBySensorMap.put(SensorType.DISPLAY_STATE, new DisplayStateEventOptions(languageManager));
        eventOptionsBySensorMap.put(SensorType.STUDY_START, new StudyStartEventOptions());
        eventOptionsBySensorMap.put(SensorType.PARTTIME, new PartTimeEventOptions(languageManager));
        eventOptionsBySensorMap.put(SensorType.COARSE_LOCATION, new GPSEventOptions(languageManager));
        eventOptionsBySensorMap.put(SensorType.LOCATIONTYPE, new PlacesEventOptions(languageManager));


        eventOptionsMap.put(languageManager.getCaptions().getString("time"), new TimeEventOptions(languageManager));
        eventOptionsMap.put(languageManager.getCaptions().getString("Weather"), new WeatherEventOptions(languageManager));
        eventOptionsMap.put(languageManager.getCaptions().getString("ACCELERATION"), new AccelerationEventOptions(languageManager));
        eventOptionsMap.put(languageManager.getCaptions().getString("DISPLAY_STATE"), new DisplayStateEventOptions(languageManager));
        eventOptionsMap.put(languageManager.getCaptions().getString("STUDY_START"), new StudyStartEventOptions());
        eventOptionsMap.put(languageManager.getCaptions().getString("PARTTIME"), new PartTimeEventOptions(languageManager));
        eventOptionsMap.put(languageManager.getCaptions().getString("COARSE_LOCATION"), new GPSEventOptions(languageManager));
        eventOptionsMap.put(languageManager.getCaptions().getString("LOCATIONTYPE"), new PlacesEventOptions(languageManager));


        eventCB.addItem(languageManager.getCaptions().getString("PARTTIME"));
        eventCB.addItem(languageManager.getCaptions().getString("time"));
        eventCB.addItem(languageManager.getCaptions().getString("WEATHER"));
        eventCB.addItem(languageManager.getCaptions().getString("ACCELERATION"));
        eventCB.addItem(languageManager.getCaptions().getString("DISPLAY_STATE"));
        eventCB.addItem(languageManager.getCaptions().getString("STUDY_START"));
        eventCB.addItem(languageManager.getCaptions().getString("COARSE_LOCATION"));
        eventCB.addItem(languageManager.getCaptions().getString("LOCATIONTYPE"));
        eventCB.setNullSelectionAllowed(false);
        eventCB.setCaption(languageManager.getCaptions().getString("sensor"));

        addEventBtn.addClickListener(e -> eventLevel.getLevelPanel().addComponent(new EventLayout(languageManager, eventLevel, false)));

        addNewLevelBtn.addClickListener(e -> eventLevel.getLevelPanel().addComponent(new EventLevelLayout(languageManager, false)));

        deleteEventBtn.addClickListener(e -> {
            int eventCount = 0;
            for (Component component : eventLevel.getLevelPanel()) {
                if(component.getClass().equals(EventLayout.class)) {
                    eventCount +=1;
                }
            }

            if(eventCount==1) {
                if(eventLevel.getParent().getParent().getParent().getClass().equals(EventLevelLayout.class)) {
                    ComponentContainer parent2 = (ComponentContainer) eventLevel.getParent();
                    parent2.removeComponent(eventLevel);
                }

            } else {
                ComponentContainer parent = (ComponentContainer) this.getParent();
                parent.removeComponent(this);
            }
        });

        eventCB.addValueChangeListener(e -> {
            eventOptionsPanel.removeAllComponents();
            //noinspection SuspiciousMethodCalls
            eventOptionsPanel.addComponent(eventOptionsMap.get(eventCB.getValue()));
        });

        if(!build) {
            eventCB.setValue(languageManager.getCaptions().getString("PARTTIME"));
            //noinspection SuspiciousMethodCalls
            eventOptionsPanel.addComponent(eventOptionsMap.get(eventCB.getValue()));

        }
    }

    public EventBuilder.Node getSensorNode(EventOptions eventOptions, LanguageManager languageManager) {
        return new EventBuilder.SensorNode(eventOptions.getSensorData(languageManager));
    }

    public EventLayout builtEvent(EventBuilder.SensorNode sensorNode, LanguageManager languageManager, EventLevelLayout eventLevelLayout) {

        EventLayout eventLayout = new EventLayout(languageManager, eventLevelLayout, true);
        eventLayout.eventCB.setValue(languageManager.getCaptions().getString(sensorNode.getData().getType().toString()));
        EventOptions eventOptions = eventOptionsBySensorMap.get(sensorNode.getData().getType()).built(languageManager, sensorNode.getData());
        eventLayout.eventOptionsPanel.removeAllComponents();
        eventLayout.eventOptionsPanel.addComponent(eventOptions);
        return  eventLayout;
    }

    public HorizontalLayout getEventOptionsPanel(){
        return eventOptionsPanel;
    }

}
