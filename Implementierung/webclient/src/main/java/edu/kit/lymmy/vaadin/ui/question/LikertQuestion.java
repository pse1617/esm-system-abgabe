package edu.kit.lymmy.vaadin.ui.question;

import com.vaadin.ui.TextField;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.ui.design.SliderQuestionDesign;


/**
 * Created by Marc on 26.02.2017.
 */
public class LikertQuestion extends SliderQuestionDesign {

    private int sliderValue = 5;

    public LikertQuestion(LanguageManager languageManager) {

        stepsField.setValue(Integer.toString(sliderValue));
        stepsField.setEnabled(false);

        leftLabelPanel.setCaption(languageManager.getCaptions().getString("leftLabelPanel"));
        rightLabelPanel.setCaption(languageManager.getCaptions().getString("rightLabelPanel"));
        stepsPanel.setCaption(languageManager.getCaptions().getString("stepsPanel"));

        increaseBtn.addClickListener(e -> {
            sliderValue += 1;
            stepsField.setValue(Integer.toString(sliderValue));
        });
        decreaseBtn.addClickListener(e -> {
            if(sliderValue>2) sliderValue+=-1;
            stepsField.setValue(Integer.toString(sliderValue));
        });

    }

    public TextField getLeftLabelField(){
        return leftLabelField;
    }

    public TextField getRightLabelField(){
        return rightLabelField;
    }

    public TextField getStepsField(){
        return stepsField;
    }
}
