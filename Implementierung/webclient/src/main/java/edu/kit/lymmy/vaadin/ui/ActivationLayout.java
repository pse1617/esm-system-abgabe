package edu.kit.lymmy.vaadin.ui;

import com.vaadin.spring.annotation.UIScope;
import edu.kit.lymmy.vaadin.auth.AuthenticationManager;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.util.PopupWindow;
import edu.kit.lymmy.vaadin.ui.design.ActivationLayoutDesign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Page that is displayed if the user follows the Link of the activation eMail or if he clicks the manualInputBtn of the ActivationHintLayout.
 *
 * @author Marc
 * @author Lukas
 * @since 07.02.2017
 */
@UIScope
@Component
public class ActivationLayout extends ActivationLayoutDesign {

    /**
     * Constructor that adds functionality to the ActivationLayoutDesign.
     *
     * @param ui Activation user interface
     * @param languageManager Language Manager
     */
    @Autowired
    public ActivationLayout(ActivationUi ui, LanguageManager languageManager, AuthenticationManager authenticationManager){

        confirmBtn.setCaption(languageManager.getCaptions().getString("activateBtn"));
        requestCodeAgainBtn.setCaption(languageManager.getCaptions().getString("requestCodeAgainBtn"));
        codeInput.setCaption(languageManager.getCaptions().getString("codeInput"));
        mailInput.setCaption(languageManager.getCaptions().getString("mailPanel"));
        confirmBtn.addClickListener(e -> ui.activate(mailInput.getValue(), codeInput.getValue()));

        requestCodeAgainBtn.addClickListener(e -> {
            PopupWindow pop = new PopupWindow();
            pop.setContent(new RequestAktivationCodeLayout(languageManager, authenticationManager));
        });
    }
}