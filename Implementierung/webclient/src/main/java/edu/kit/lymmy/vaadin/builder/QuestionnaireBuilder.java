package edu.kit.lymmy.vaadin.builder;

import edu.kit.lymmy.shared.Questionnaire;
import edu.kit.lymmy.shared.event.CompositeEvent;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Used to build a {@link Questionnaire}
 *
 * @author Lukas
 * @since 03.01.2017
 */
public class QuestionnaireBuilder {
    private String name;
    private final EventBuilder event;
    private long coolDownTime;
    private final List<QuestionBuilder> questions;

    /**
     * Creates a new instance with default values
     */
    QuestionnaireBuilder() {
        name = "";
        event = new EventBuilder();
        coolDownTime = 0;
        questions = new ArrayList<>();
    }

    /**
     * Creates a new instance with values from the passed questionnaire
     *
     * @param questionnaire the questionnaire to copy from
     */
    public QuestionnaireBuilder(@NotNull Questionnaire questionnaire) {
        name = questionnaire.getName();
        event = new EventBuilder((CompositeEvent) questionnaire.getEvent());
        coolDownTime = questionnaire.getCoolDownTime();
        questions = questionnaire.getQuestions().stream().map(QuestionBuilder::new).collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * Set the name
     *
     * @param name the value to set to
     * @return this
     */
    public QuestionnaireBuilder setName(@NotNull String name) {
        this.name = name;
        return this;
    }

    /**
     * @return the name
     */
    @NotNull
    public String getName() {
        return name;
    }

    /**
     * @return the time between occurrences
     */
    public long getCoolDownTime() {
        return coolDownTime;
    }

    /**
     * Set the cool-down-time
     * @param coolDownTime the value to set to
     * @return this
     */
    public QuestionnaireBuilder setCoolDownTime(long coolDownTime) {
        this.coolDownTime = coolDownTime;
        return this;
    }

    /**
     * @return an EventBuilder allowing to change the event triggering this
     */
    @NotNull
    public EventBuilder getEvent() {
        return event;
    }

    /**
     * @return a modifiable list of questions
     */
    @NotNull
    public List<QuestionBuilder> getQuestions() {
        return questions;
    }

    /**
     * @return a new QuestionBuilder added to this
     */
    @NotNull
    public QuestionBuilder newQuestion() {
        QuestionBuilder question = new QuestionBuilder();
        questions.add(question);
        return question;
    }

    /**
     * Remove a question
     *
     * @param question the question to remove
     * @return this
     */
    public QuestionnaireBuilder removeQuestion(QuestionBuilder question) {
        questions.remove(question);
        return this;
    }

    /**
     * @return a new Questionnaire with the values of this
     */
    @NotNull
    Questionnaire build() {
        return new Questionnaire(event.build(), coolDownTime, questions.stream().map(QuestionBuilder::build).collect(Collectors.toList()), Collections.emptyList(), name);
    }
}
