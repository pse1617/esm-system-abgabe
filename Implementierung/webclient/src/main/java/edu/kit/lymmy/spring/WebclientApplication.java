package edu.kit.lymmy.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Lukas
 * @since 02.01.2017
 */
@SpringBootApplication(scanBasePackages = "edu.kit.lymmy")
public class WebclientApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebclientApplication.class, args);
	}
}
