package edu.kit.lymmy.vaadin.ui.event;

import com.vaadin.ui.ComboBox;
import edu.kit.lymmy.shared.sensor.SensorData;
import edu.kit.lymmy.shared.sensor.WeatherData;
import edu.kit.lymmy.vaadin.language.LanguageManager;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by marcs on 03.03.2017.
 */
public class WeatherEventOptions extends EventOptions {

    private static WeatherData weatherData;
    private WeatherData.WeatherType weatherType;
    private  boolean not;
    private ComboBox weatherCB;
    private ComboBox operatorCB;
    private Map<String, WeatherData.WeatherType> weatherTypeMap = new HashMap<>();

    public WeatherEventOptions(LanguageManager languageManager) {

        operatorCB = new ComboBox(languageManager.getCaptions().getString("Operator"));
        operatorCB.addItem("=");
        operatorCB.addItem("!=");
        operatorCB.setValue("=");
        operatorCB.setNullSelectionAllowed(false);
        addComponent(operatorCB);
        operatorCB.addValueChangeListener(e -> not = e.getProperty().getValue().equals("!="));
        operatorCB.setWidth("70px");

        weatherCB = new ComboBox(languageManager.getCaptions().getString("Weather"));
        for (WeatherData.WeatherType type : WeatherData.WeatherType.values()) {
            weatherCB.addItem(languageManager.getCaptions().getString(type.toString()));
            weatherTypeMap.put(languageManager.getCaptions().getString(type.toString()), type);
        }

        weatherCB.setValue(languageManager.getCaptions().getString(WeatherData.WeatherType.Clear.toString()));
        weatherCB.setNullSelectionAllowed(false);
        addComponent(weatherCB);
        weatherCB.setWidth("200px");
        weatherCB.addValueChangeListener(e -> weatherType = weatherTypeMap.get(weatherCB.getValue().toString()));


    }

    @Override
    public SensorData getSensorData(LanguageManager languageManager) {
        weatherType = weatherTypeMap.get(weatherCB.getValue());
        not = operatorCB.getValue().equals("!=");
        return new WeatherData(weatherType, not);
    }

    @Override
    public EventOptions built(LanguageManager languageManager, SensorData sensorData) {
        WeatherEventOptions eventOptions = this;
        if(((WeatherData)sensorData).isNot()) {
            operatorCB.setValue("!=");
        } else {
            operatorCB.setValue("=");
        }
        weatherCB.setValue(languageManager.getCaptions().getString(((WeatherData)sensorData).getWeather().toString()));

        return eventOptions;
    }
}
