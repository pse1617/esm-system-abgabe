package edu.kit.lymmy.vaadin.network;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaadin.spring.annotation.VaadinSessionScope;
import com.vaadin.ui.UI;
import org.intellij.lang.annotations.MagicConstant;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.annotation.ApplicationScope;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static edu.kit.lymmy.shared.messages.CommunicationConstants.*;

/**
 * @author Lukas
 * @since 26.01.2017
 */
@VaadinSessionScope
@Component
public class JsonService {
    private final RestTemplate restTemplate;
    private String cookie;
    private final List<SessionExpiryListener> listeners;

    @Autowired
    public JsonService(ObjectMapper objectMapper) {
        this(objectMapper, new RestTemplate());
    }

    public JsonService(ObjectMapper objectMapper, RestTemplate restTemplate){
        this.restTemplate = restTemplate;
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(objectMapper);
        restTemplate.getMessageConverters().removeIf(m -> m.getClass().equals(MappingJackson2HttpMessageConverter.class));
        restTemplate.getMessageConverters().add(converter);
        listeners = new ArrayList<>();
    }

    public void addSessionExpiryListener(SessionExpiryListener listener){
        listeners.add(listener);
    }

    public void removeSessionExpiryListener(SessionExpiryListener listener){
        listeners.remove(listener);
    }

    @NotNull
    public JsonRequest request(@NotNull @MagicConstant(stringValues = {CREATE_STUDY_ID, DELETE_STUDY, STORE_STUDY, GET_STUDIES, EXPORT_STUDY,
            LEADER_DELETE, LEADER_LOGIN, LEADER_LOGOUT, LEADER_REGISTER_FINISH, LEADER_REGISTER_START, LEADER_RESEND_CODE}) String path) {
        return new JsonRequest(HOST + path);
    }

    public class JsonRequest {
        private final UriComponentsBuilder uriComponentsBuilder;
        private boolean formEncoded = false;

        JsonRequest(@NotNull String url) {
            this.uriComponentsBuilder = UriComponentsBuilder.fromHttpUrl(url);
        }

        @NotNull
        public JsonRequest queryParam(@NotNull @MagicConstant(stringValues = PARAM_STUDY_ID) String name, @NotNull Object value) {
            uriComponentsBuilder.queryParam(name, value);
            return this;
        }

        @NotNull
        public JsonRequest formEncoded() {
            formEncoded = true;
            return this;
        }

        @Nullable
        public <T> T send(@NotNull Class<T> returnType) throws HttpStatusCodeException {
            return send(null, returnType);
        }

        @Nullable
        public <T> T send(@Nullable Object body, @NotNull Class<T> returnType) throws HttpStatusCodeException {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(formEncoded ? MediaType.APPLICATION_FORM_URLENCODED : MediaType.APPLICATION_JSON);
            if (cookie != null) headers.add("Cookie", cookie);
            HttpEntity<Object> requestEntity = new HttpEntity<>(body, headers);
            try {
                ResponseEntity<T> responseEntity = restTemplate.postForEntity(uriComponentsBuilder.build().toUri(), requestEntity, returnType);
                cookie = Optional.ofNullable(responseEntity.getHeaders().getFirst("Set-Cookie")).orElse(cookie);
                return responseEntity.getBody();
            } catch (HttpStatusCodeException e) {
                if (e.getStatusCode() == HttpStatus.UNAUTHORIZED) {
                    cookie = null;
                    listeners.forEach(SessionExpiryListener::onSessionExpired);
                    try {
                        UI.getCurrent().getSession().setAttribute("error", "Session expired");
                        UI.getCurrent().getPage().reload();
                    }catch (Exception ignored){
                    }
                    return null;
                }
                throw e;
            }
        }
    }

    public interface SessionExpiryListener{
        void onSessionExpired();
    }
}
