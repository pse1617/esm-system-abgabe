package edu.kit.lymmy.vaadin.util;

import com.vaadin.server.FileDownloader;
import com.vaadin.server.StreamResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinResponse;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.util.Pair;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.function.Supplier;

/**
 * This specializes {@link FileDownloader} to allow the file name to be determined dynamically
 *
 * @author Lukas
 * @since 05.02.2017
 */
public class OnDemandFileDownloader extends FileDownloader {
    @NotNull
    private final Supplier<@Nullable Pair<@NotNull InputStream, @NotNull String>> provider;

    public OnDemandFileDownloader(@NotNull Supplier<@Nullable Pair<@NotNull InputStream, @NotNull String>> provider) {
        super(new StreamResource(() -> new ByteArrayInputStream(new byte[0]), ""));
        this.provider = provider;
    }

    @Override
    public boolean handleConnectorRequest(VaadinRequest request, VaadinResponse response, String path)
            throws IOException {
        Pair<InputStream, String> source = provider.get();
        if (source != null) {
            getResource().setStreamSource(source::getFirst);
            getResource().setFilename(source.getSecond());
            return super.handleConnectorRequest(request, response, path);
        }
        return false;
    }

    @NotNull
    private StreamResource getResource() {
        return (StreamResource) getFileDownloadResource();
    }

}
