package edu.kit.lymmy.vaadin.ui;

import com.vaadin.ui.Component;
import com.vaadin.ui.VerticalLayout;
import edu.kit.lymmy.shared.LogicalOperator;
import edu.kit.lymmy.vaadin.builder.EventBuilder;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.ui.design.EventLevelLayoutDesign;
import edu.kit.lymmy.vaadin.ui.event.EventOptions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marc on 13.02.2017.
 */
public class EventLevelLayout extends EventLevelLayoutDesign {

    LogicalOperator linking = LogicalOperator.AND;

    public EventLevelLayout(LanguageManager languageManager, boolean build) {

        linkingCB.addItem(languageManager.getCaptions().getString("AND"));
        linkingCB.addItem(languageManager.getCaptions().getString("OR"));
        linkingCB.setNullSelectionAllowed(false);



        linkingCB.addValueChangeListener(e -> {
            if(linkingCB.getValue().equals(languageManager.getCaptions().getString("AND"))) {
                linking = LogicalOperator.AND;
            } else {
                linking = LogicalOperator.OR;
            }
        });

        if(!build) {
            levelPanel.addComponent(new EventLayout(languageManager, this, false));
            linkingCB.setValue(languageManager.getCaptions().getString("AND"));
        }



    }

    public EventBuilder.Node getLogicalNode(LanguageManager languageManager){
        EventBuilder.LogicalNode node = new EventBuilder.LogicalNode();
        node.setOperator(linking);
        List<Component> componentList = new ArrayList<>();

        for (int i = 1; i < levelPanel.getComponentCount(); i++) {
            componentList.add(levelPanel.getComponent(i));
        }

        for (Component component : componentList) {
            if (component.getClass().equals(EventLevelLayout.class)) {
                node.getChildren().add(((EventLevelLayout) component).getLogicalNode(languageManager));
            } else {
                node.getChildren().add(((EventLayout) component).getSensorNode((EventOptions)((EventLayout)component).getEventOptionsPanel().getComponent(0), languageManager));
            }
        }
        return node;
    }

    public EventLevelLayout buildLevel(EventBuilder.LogicalNode logicalNode, LanguageManager languageManager) {
        EventLevelLayout eventLevelLayout = this;
        this.linkingCB.setValue(languageManager.getCaptions().getString(logicalNode.getOperator().toString()));
        for (EventBuilder.Node node : logicalNode.getChildren()) {
            if(node.getClass().equals(EventBuilder.LogicalNode.class)) {
                eventLevelLayout.levelPanel.addComponent(new EventLevelLayout(languageManager, true).buildLevel((EventBuilder.LogicalNode)node, languageManager));
            } else {
                eventLevelLayout.levelPanel.addComponent(new EventLayout(languageManager, this, false).builtEvent((EventBuilder.SensorNode) node, languageManager, eventLevelLayout));
            }
        }
        return eventLevelLayout;
    }

    public VerticalLayout getLevelPanel(){
        return levelPanel;
    }


}
