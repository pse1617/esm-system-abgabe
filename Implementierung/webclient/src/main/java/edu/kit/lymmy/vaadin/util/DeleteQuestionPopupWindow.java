package edu.kit.lymmy.vaadin.util;

import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import edu.kit.lymmy.vaadin.builder.QuestionBuilder;
import edu.kit.lymmy.vaadin.builder.QuestionnaireBuilder;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.navigation.NavigationManager;
import edu.kit.lymmy.vaadin.ui.DeleteMessage;

/**
 * Created by Marc on 24.02.2017.
 */
public class DeleteQuestionPopupWindow extends PopupWindow {

    public DeleteQuestionPopupWindow(NavigationManager navigationManager, LanguageManager languageManager, QuestionBuilder selectedQuestion, Table questionsTable, QuestionnaireBuilder questionnaireBuilder) {
        super();
        final QuestionBuilder questionToDelete = selectedQuestion;
        VerticalLayout popupContent = new DeleteMessage(languageManager.getCaptions().getString("deleteQuestionMesg") +"  " + questionToDelete.getText(), event -> {
            questionnaireBuilder.removeQuestion(questionToDelete);
            questionsTable.removeItem(questionToDelete);
            close();
        }, languageManager);
        setContent(popupContent);
        questionsTable.unselect(selectedQuestion);
    }
}
