package edu.kit.lymmy.vaadin.util;

import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import edu.kit.lymmy.shared.Study;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.navigation.NavigationManager;
import edu.kit.lymmy.vaadin.ui.DeleteMessage;

/**
 * Created by Marc on 24.02.2017.
 */
public class DeleteStudyPopupWindow extends PopupWindow {

    public DeleteStudyPopupWindow(NavigationManager navigationManager, LanguageManager languageManager, Study selectedStudy, Table studyTable) {
        super();
        final Study studyToDelete = selectedStudy;
        VerticalLayout popupContent = new DeleteMessage(languageManager.getCaptions().getString("deleteMessage") +"  " + selectedStudy.getName(), event -> {
            navigationManager.deleteStudy(studyToDelete);
            studyTable.removeItem(studyToDelete);
            close();
        }, languageManager);
        setContent(popupContent);
        studyTable.unselect(selectedStudy);
    }
}
