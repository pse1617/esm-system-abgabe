package edu.kit.lymmy.vaadin.builder;

import edu.kit.lymmy.shared.question.*;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.util.Pair;

import java.util.ArrayList;
import java.util.List;

/**
 * Used to build a {@link Question}
 *
 * @author Lukas
 * @since 03.01.2017
 */
public class QuestionBuilder {

    private QuestionType type;
    private Condition condition;
    private String text;
    private List<String> answers;
    private int steps;
    private Pair<String, String> sliderTexts;

    /**
     * Create a new instance with default values
     */
    QuestionBuilder() {
        type = QuestionType.OPEN;
        condition = new ValidCondition();
        text = "";
        answers = new ArrayList<>();
        steps = 100;
        sliderTexts = Pair.of("", "");
    }

    /**
     * create a new instance and copy values from the passed question
     *
     * @param question the question to copy from
     */
    QuestionBuilder(@NotNull Question question) {
        this();
        type = question.getType();
        condition = question.getCondition();
        text = question.getText();
        if (type == QuestionType.SINGLE_CHOICE || type == QuestionType.MULTIPLE_CHOICE || type == QuestionType.YES_NO) {
            answers = ((ChoiceQuestion) question).getAnswers();
        } else if(type == QuestionType.SLIDER || type == QuestionType.LIKERT){
            sliderTexts = Pair.of(((StepQuestion) question).getLeftSliderText(), ((StepQuestion) question).getRightSliderText());
            if (type == QuestionType.LIKERT) {
                steps = ((LikertQuestion) question).getSteps();
            }
        }
    }

    /**
     * @return type of this question
     */
    @NotNull
    public QuestionType getType() {
        return type;
    }

    /**
     * set type of this question
     *
     * @param type the value to set to
     * @return this
     */
    public QuestionBuilder setType(@NotNull QuestionType type) {
        this.type = type;
        return this;
    }

    /**
     * @return the condition this is bound to
     */
    @NotNull
    public Condition getCondition() {
        return condition;
    }

    /**
     * Set the condition this is bound to
     *
     * @param condition the value to set to
     * @return this
     */
    public QuestionBuilder setCondition(@NotNull Condition condition) {
        this.condition = condition;
        return this;
    }

    /**
     * @return the question text
     */
    @NotNull
    public String getText() {
        return text;
    }

    /**
     * Set the question text
     *
     * @param text the text to set to
     * @return this
     */
    public QuestionBuilder setText(@NotNull String text) {
        this.text = text;
        return this;
    }

    /**
     * @return the answers
     */
    @NotNull
    public List<String> getAnswers() {
        return answers;
    }

    /**
     * Set the answers
     *
     * @param answers the list of answers
     * @return this
     */
    public QuestionBuilder setAnswers(@NotNull List<String> answers) {
        this.answers = answers;
        return this;
    }

    /**
     * @return the number of steps
     */
    public int getSteps() {
        return steps;
    }

    /**
     * Set the number of steps
     *
     * @param steps the value to set to
     * @return this
     */
    public QuestionBuilder setSteps(int steps) {
        this.steps = steps;
        return this;
    }

    /**
     * @return the texts displayed at the end of the slider
     */
    public Pair getSliderTexts() {
        return sliderTexts;
    }

    /**
     * Set the texts displayed at the end of the slider
     *
     * @param left  left end text
     * @param right right end text
     * @return this
     */
    public QuestionBuilder setSliderTexts(String left, String right) {
        this.sliderTexts = Pair.of(left, right);
        return this;
    }

    /**
     * @return a new Question with the values of this builder
     */
    @NotNull
    Question build() {
        return QuestionFactory.create(type, condition, text, answers, steps, sliderTexts);
    }
}
