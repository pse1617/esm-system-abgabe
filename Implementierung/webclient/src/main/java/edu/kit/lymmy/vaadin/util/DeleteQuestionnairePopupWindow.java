package edu.kit.lymmy.vaadin.util;

import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import edu.kit.lymmy.vaadin.builder.QuestionnaireBuilder;
import edu.kit.lymmy.vaadin.builder.StudyBuilder;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.ui.DeleteMessage;

/**
 * Created by Marc on 24.02.2017.
 */
public class DeleteQuestionnairePopupWindow extends  PopupWindow {

    public DeleteQuestionnairePopupWindow(LanguageManager languageManager, QuestionnaireBuilder selectedQuestionnaire, Table questionnaireTable, StudyBuilder studyBuilder) {
        super();
        final QuestionnaireBuilder questionnaireToDelete = selectedQuestionnaire;
        VerticalLayout popupContent = new DeleteMessage(languageManager.getCaptions().getString("deleteMsgQuestionnaire") +"  " + questionnaireToDelete.getName(), event -> {
            studyBuilder.removeQuestionaire(questionnaireToDelete);
            questionnaireTable.removeItem(questionnaireToDelete);
            close();
        }, languageManager);
        setContent(popupContent);
        questionnaireTable.unselect(selectedQuestionnaire);
    }
}
