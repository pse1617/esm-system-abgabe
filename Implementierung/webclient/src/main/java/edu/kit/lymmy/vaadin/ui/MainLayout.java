package edu.kit.lymmy.vaadin.ui;


import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.VerticalLayout;
import edu.kit.lymmy.vaadin.auth.AuthenticationManager;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.navigation.NavigationManager;
import edu.kit.lymmy.vaadin.ui.design.MainLayoutDesign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * This is the part of the GUI that's always visible after the login. It provides the main navigation possibilities.
 *
 * @author Marc
 * @since 19.01.2017
 */
@UIScope
@Component
public class MainLayout extends MainLayoutDesign {

    /**
     * Constructor that adds functionality to the MainLayoutDesign.
     *
     * @param ui Web user interface
     * @param navigationManager Navigation manager
     * @param authenticationManager Authentication manager
     * @param context Application context
     * @param languageManager Language manager
     */
    @Autowired
    public MainLayout(WebUi ui, NavigationManager navigationManager, AuthenticationManager authenticationManager, ApplicationContext context, LanguageManager languageManager) {
        super();
        newStudiesBtn.setCaption(languageManager.getCaptions().getString("newStudiesBtn"));
        studiesInProgressBtn.setCaption(languageManager.getCaptions().getString("studiesInProgressBtn"));
        finishedStudiesBtn.setCaption(languageManager.getCaptions().getString("finishedStudiesBtn"));
        logoutBtn.setCaption(languageManager.getCaptions().getString("logoutBtn"));

        VerticalLayout languageSelection = new LanguageSelection(languageManager);

        languageSelectionSpace.addComponent(languageSelection);
        languageSelectionSpace.setVisible(true);

        navigationManager.init(ui, scrollPanel);
        newStudiesBtn.addClickListener(event -> navigationManager.showNewStudies());
        studiesInProgressBtn.addClickListener(event -> navigationManager.showRunningStudies());
        finishedStudiesBtn.addClickListener(event -> navigationManager.showFinishedStudies());
        logoutBtn.addClickListener(event -> {
            authenticationManager.logout();
            ui.setContent(context.getBean(LoginLayout.class));
        });
    }
}
