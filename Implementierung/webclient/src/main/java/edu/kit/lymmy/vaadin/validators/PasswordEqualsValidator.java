package edu.kit.lymmy.vaadin.validators;

import com.vaadin.data.Validator;
import com.vaadin.ui.PasswordField;
import edu.kit.lymmy.vaadin.language.LanguageManager;

/**
 * This validator checks if the password equals the passwordConfirmation
 *
 * @author Marc
 * @since 01.02.2017
 */
public class PasswordEqualsValidator implements Validator {

    private final PasswordField password;
    private final PasswordField passwordConfirmation;
    private final LanguageManager languageManager;

    /**
     * Constructor that sets password, passwordConfirmation and languageManager.
     *
     * @param password PasswordField
     * @param passwordConfirmation Second PasswordField
     * @param languageManager Language manager
     */
    public PasswordEqualsValidator(PasswordField password, PasswordField passwordConfirmation, LanguageManager languageManager) {
        this.password = password;
        this.passwordConfirmation = passwordConfirmation;
        this.languageManager = languageManager;
    }

    /**
     * Calls isValid. Throws InvalidValueException if isValid returns false.
     *
     * @param value Value
     * @throws InvalidValueException
     */
    @Override
    public void validate(Object value) throws InvalidValueException {
        if(!isValid()) {
            throw new InvalidValueException(languageManager.getCaptions().getString("passwordEqualsMessage"));
        }

    }

    /**
     * Checks if the value of password equals passwordConfirmation.
     *
     * @return returns true if equal and false if not
     */
    private boolean isValid(){
        return password.getValue().equals(passwordConfirmation.getValue());

    }
}
