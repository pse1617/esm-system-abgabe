package edu.kit.lymmy.vaadin.ui.event;

import com.vaadin.ui.TextField;
import edu.kit.lymmy.shared.sensor.CoarseLocationData;
import edu.kit.lymmy.shared.sensor.SensorData;
import edu.kit.lymmy.vaadin.language.LanguageManager;

/**
 * Created by marc on 05.03.17.
 */
public class GPSEventOptions extends EventOptions {

    private TextField cityField;
    private TextField streetField;

    public GPSEventOptions(LanguageManager languageManager) {
        cityField = new TextField(languageManager.getCaptions().getString("city"));
        streetField = new TextField(languageManager.getCaptions().getString("street"));
        addComponent(cityField);
        addComponent(streetField);

    }

    @Override
    public SensorData getSensorData(LanguageManager languageManager) {
        return new CoarseLocationData(cityField.getValue()+", "+streetField.getValue());
    }

    @Override
    public EventOptions built(LanguageManager languageManager, SensorData sensorData) {
        GPSEventOptions eventOptions = this;
        CoarseLocationData data = (CoarseLocationData) sensorData;
        String[] adress = data.getAddress().split(", ");
        eventOptions.cityField.setValue(adress[0]);
        eventOptions.streetField.setValue(adress[1]);
        return  eventOptions;
    }
}
