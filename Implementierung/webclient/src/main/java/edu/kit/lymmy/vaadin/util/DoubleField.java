package edu.kit.lymmy.vaadin.util;

import com.vaadin.event.FieldEvents;
import com.vaadin.ui.TextField;

public class DoubleField extends TextField implements FieldEvents.TextChangeListener {
    private String lastValue;

    public DoubleField() {
        setImmediate(true);
        setTextChangeEventMode(TextChangeEventMode.EAGER);
        addTextChangeListener(this);
    }

    @Override
    public void textChange(FieldEvents.TextChangeEvent event) {
        String text = event.getText();
        try {
            new Double(text);
            lastValue = text;
        } catch (NumberFormatException e) {
            if(lastValue!=null){
                setValue(lastValue);
            } else {
                setValue("");
            }

        }
    }
}