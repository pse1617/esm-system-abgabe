package edu.kit.lymmy.vaadin.ui;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import edu.kit.lymmy.shared.Study;
import edu.kit.lymmy.vaadin.builder.StudyBuilder;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.navigation.NavigationManager;
import edu.kit.lymmy.vaadin.ui.design.LandingPageLayoutDesign;

import java.time.LocalDateTime;
import java.util.List;

/**
 * The Page that is displayed after the login.
 *
 * @author Marc
 * @since 24.01.2017
 */
public class LandingPageLayout extends LandingPageLayoutDesign implements View {
    private int amountOfNewStudies;
    private int amountOfStudiesInProgress;
    private int amountOfFinishedStudies;

    /**
     * Constructor that adds functionality to the LandingPageLayoutDesign.
     *
     * @param navigationManager Navigation manager
     * @param languageManager Language manager
     * @param studies List of studies
     */
    public LandingPageLayout(NavigationManager navigationManager, LanguageManager languageManager, List<Study> studies) {
        newstudiesPanel.setCaption(languageManager.getCaptions().getString("newStudiesLabel")+":");
        newstudiesPanel.addLayoutClickListener(e -> {
            if(e.isDoubleClick()){
                navigationManager.showNewStudies();
            }
        });
        studiesInProgressPanel.setCaption(languageManager.getCaptions().getString("studiesInProgressLabel")+":");
        studiesInProgressPanel.addLayoutClickListener(e -> {
            if(e.isDoubleClick()){
                navigationManager.showRunningStudies();
            }
        });
        finishedStudiesPanel.setCaption(languageManager.getCaptions().getString("finishedStudiesLabel")+":");
        finishedStudiesPanel.addLayoutClickListener(e -> {
            if(e.isDoubleClick()){
                navigationManager.showFinishedStudies();
            }
        });
        newStudyBtn.setCaption(languageManager.getCaptions().getString("startNewStudyBtn"));
        welcomeLabel.setValue(languageManager.getCaptions().getString("welcomeLabel"));
        for (Study study : studies) {
            if(study.getStartDate()==null || study.getStartDate().isAfter(LocalDateTime.now().plusDays(1))) {
                amountOfNewStudies +=1;
            } else if(study.getEndDate()!=null && study.getEndDate().isAfter(LocalDateTime.now())) {
                amountOfStudiesInProgress +=1;
            } else {
                amountOfFinishedStudies +=1;
            }
        }
        newstudiesLabel.setValue(Integer.toString(amountOfNewStudies));
        studiesInProgressLabel.setValue(Integer.toString(amountOfStudiesInProgress));
        finishedStudiesLabel.setValue(Integer.toString(amountOfFinishedStudies));

        newStudyBtn.addClickListener(e -> {
            StudyBuilder study = navigationManager.getNewStudy();
            if(study != null) {
                navigationManager.editStudy(study);
            }
        });

    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }
}
