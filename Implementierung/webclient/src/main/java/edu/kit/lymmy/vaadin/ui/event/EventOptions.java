package edu.kit.lymmy.vaadin.ui.event;

import com.vaadin.ui.HorizontalLayout;
import edu.kit.lymmy.shared.sensor.SensorData;
import edu.kit.lymmy.vaadin.language.LanguageManager;

/**
 * Created by Marc on 13.02.2017.
 */
public abstract class EventOptions extends HorizontalLayout {


    public EventOptions() {
        this.setSizeUndefined();
    }

    public abstract SensorData getSensorData(LanguageManager languageManager);

    public abstract EventOptions built(LanguageManager languageManager, SensorData sensorData);
}
