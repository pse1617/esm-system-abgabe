package edu.kit.lymmy.vaadin.ui.question;


import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.ui.ChoiceQuestionsLayout;

/**
 * Extension of ChoiceQuestionsLayout.
 *
 * @author Marc
 * @since 26.01.2017
 */
public class SingleChoiceQuestion extends ChoiceQuestionsLayout {

    /**
     * Calls the constructor of ChoiceQuestionsLayout.
     *
     * @param languageManager Language Manager
     */
    public SingleChoiceQuestion(LanguageManager languageManager){
        super(languageManager);
    }
}
