package edu.kit.lymmy.vaadin.ui;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.ui.Notification;
import edu.kit.lymmy.shared.Questionnaire;
import edu.kit.lymmy.shared.Study;
import edu.kit.lymmy.vaadin.builder.QuestionBuilder;
import edu.kit.lymmy.vaadin.builder.QuestionnaireBuilder;
import edu.kit.lymmy.vaadin.builder.StudyBuilder;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.navigation.NavigationManager;
import edu.kit.lymmy.vaadin.ui.design.QuestionnaireEditorLayoutDesign;
import edu.kit.lymmy.vaadin.util.DeleteQuestionPopupWindow;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Page that provides the possibility to edit a questionnaire.
 *
 * @author Marc
 * @since 24.01.2017
 */
public class QuestionnaireEditorLayout extends QuestionnaireEditorLayoutDesign implements View {

    private static long minutes;
    private static long hours;
    private static long days;
    private static QuestionBuilder selectedQuestion;
    private final Map<String, Questionnaire>  questionnaireMap = new HashMap<>();

    /**
     * Constructor that adds functionality to the QuestionnaireEditorLayoutDesign.
     *
     * @param navigationManager Navigation manager
     * @param questionnaireBuilder Questionnaire builder
     * @param languageManager Language manager
     * @param studies List of studies
     * @param studyBuilder Study builder
     */
    public QuestionnaireEditorLayout(NavigationManager navigationManager, QuestionnaireBuilder questionnaireBuilder, LanguageManager languageManager, List<Study> studies, StudyBuilder studyBuilder) {

        questionsTable.addContainerProperty(languageManager.getCaptions().getString("questions"), String.class, null);
        backBtn.setCaption(languageManager.getCaptions().getString("backBtn"));
        addQuestionBtn.setCaption(languageManager.getCaptions().getString("addBtn"));
        clearBtn.setCaption(languageManager.getCaptions().getString("clearBtn"));
        deleteQuestionBtn.setCaption(languageManager.getCaptions().getString("deleteBtn"));
        editQuestionBtn.setCaption(languageManager.getCaptions().getString("editBtn"));
        goToEventBuilderBtn.setCaption(languageManager.getCaptions().getString("eventBuilderBtn"));
        saveBtn.setCaption(languageManager.getCaptions().getString("saveBtn"));
        questionnaireEditorLabel.setValue(languageManager.getCaptions().getString("questionnaireEditorLabel"));
        cooldownTimePanel.setCaption(languageManager.getCaptions().getString("cooldownTimePanel"));
        eventPanel.setCaption(languageManager.getCaptions().getString("eventPanel"));
        namePanel.setCaption(languageManager.getCaptions().getString("namePanel"));
        hoursLabel.setValue(languageManager.getCaptions().getString("hoursLabel"));
        minutesLabel.setValue(languageManager.getCaptions().getString("minutesLabel"));
        daysLabel.setValue(languageManager.getCaptions().getString("daysLabel"));
        existingQuestionnairesCB.setCaption(languageManager.getCaptions().getString("existingQuestionnairesCB"));

        existingQuestionnairesCB.setNewItemsAllowed(false);
        minutesSelect.setNullSelectionAllowed(false);
        hoursSelect.setNullSelectionAllowed(false);
        daysSelect.setNullSelectionAllowed(false);
        minutesSelect.setNewItemsAllowed(false);
        hoursSelect.setNewItemsAllowed(false);
        daysSelect.setNewItemsAllowed(false);

        for (Study study : studies) {
            for (Questionnaire questionnaire : study.getQuestionnaires()) {
                existingQuestionnairesCB.addItem(questionnaire.getName());
                questionnaireMap.put(questionnaire.getName(), questionnaire);
            }
        }

        existingQuestionnairesCB.addValueChangeListener(e -> {
            if(e.getProperty().getValue()!=null) {
                QuestionnaireBuilder selected = new QuestionnaireBuilder(questionnaireMap.get(e.getProperty().getValue().toString()));
                studyBuilder.removeQuestionaire(questionnaireBuilder);
                studyBuilder.getQuestionaires().add(selected);
                navigationManager.editQuestionaire(selected);
                fillForm(selected);
            }
        });

        questionsTable.addItemClickListener(e -> {
            selectedQuestion = (QuestionBuilder) e.getItemId();
            if(e.isDoubleClick())
                edit(navigationManager, languageManager);
        });

        minutesSelect.addValueChangeListener(e -> {
            minutes = (long) Integer.parseInt(e.getProperty().getValue().toString());
            questionnaireBuilder.setCoolDownTime(minutes * 60000 + hours * 3600000 + days * 86400000);
        });

        hoursSelect.addValueChangeListener(e -> {
            hours = (long) Integer.parseInt(e.getProperty().getValue().toString());
            questionnaireBuilder.setCoolDownTime(minutes * 60000 + hours * 3600000 + days * 86400000);
        });
        daysSelect.addValueChangeListener(e -> {
            days = (long) Integer.parseInt(e.getProperty().getValue().toString());
            questionnaireBuilder.setCoolDownTime(minutes * 60000 + hours * 3600000 + days * 86400000);
        });

        fillForm(questionnaireBuilder);


        buildQuestionTable(questionnaireBuilder);

        questionnaireName.addTextChangeListener(e -> questionnaireBuilder.setName(e.getText()));

        backBtn.addClickListener(e -> navigationManager.navigateUp());

        goToEventBuilderBtn.addClickListener(e -> navigationManager.editEvent(questionnaireBuilder.getEvent()));

        saveBtn.addClickListener(e -> save(navigationManager, questionnaireBuilder, languageManager));

        clearBtn.addClickListener(e ->  clear());

        questionsTable.addValueChangeListener(e -> selectedQuestion = (QuestionBuilder) e.getProperty().getValue());

        addQuestionBtn.addClickListener(e -> {
            if(questionnaireName.getValue() != null && !questionnaireName.getValue().equals("")) {
                navigationManager.editQuestion(questionnaireBuilder.newQuestion());
            } else  {
                Notification notification = new Notification(languageManager.getCaptions().getString("questionnaireNameMsg"), Notification.Type.WARNING_MESSAGE);
                notification.setDelayMsec(2000);
                notification.show(Page.getCurrent());
            }
        });

        editQuestionBtn.addClickListener(e -> edit(navigationManager, languageManager));

        deleteQuestionBtn.addClickListener(e -> delete(navigationManager, questionnaireBuilder, languageManager));



    }

    /**
     * Navigates to the question editor with the selected question.
     *
     * @param navigationManager Navigation manager
     * @param languageManager Language manager
     */
    private void edit(NavigationManager navigationManager, LanguageManager languageManager) {
        if(questionnaireName.getValue() != null && !questionnaireName.getValue().equals("") && selectedQuestion != null) {
            navigationManager.editQuestion(selectedQuestion);
            selectedQuestion = null;
        } else  {
            Notification notification = new Notification(languageManager.getCaptions().getString("questionnaireNameMsg"), Notification.Type.WARNING_MESSAGE);
            notification.setDelayMsec(2000);
            notification.show(Page.getCurrent());
        }
    }

    /**
     * Deletes the selected question.
     *
     * @param questionnaireBuilder Questionnaire builder
     * @param languageManager Language manager
     */
    private void delete(NavigationManager navigationManager, QuestionnaireBuilder questionnaireBuilder, LanguageManager languageManager) {
        if(selectedQuestion != null) {
            DeleteQuestionPopupWindow deleteQuestionPopupWindow = new DeleteQuestionPopupWindow(navigationManager, languageManager, selectedQuestion, questionsTable, questionnaireBuilder);
        } else {
            Notification notification = new Notification(languageManager.getCaptions().getString("selectStudy"), Notification.Type.WARNING_MESSAGE);
            notification.setDelayMsec(2000);
            notification.show(Page.getCurrent());
        }
    }


    /**
     * Clears the form.
     */
    private void clear() {
        questionnaireName.setValue("");
        minutesSelect.setValue("0");
        hoursSelect.setValue("0");
        daysSelect.setValue("0");
    }

    /**
     * Saves the study.
     *
     * @param navigationManager Navigation manager
     * @param questionnaireBuilder Questionnaire builder
     * @param languageManager Language manager
     */
    private void save(NavigationManager navigationManager, QuestionnaireBuilder questionnaireBuilder, LanguageManager languageManager) {
        if(questionnaireName.getValue() != null && !questionnaireName.getValue().equals("")) {
            questionnaireBuilder.setName(questionnaireName.getValue());
            questionnaireBuilder.setCoolDownTime(minutes * 60000 + hours * 3600000 + days * 86400000);
            navigationManager.saveStudy();
            Notification saveNotification = new Notification(languageManager.getCaptions().getString("saveSuccessMsg"));
            saveNotification.setDelayMsec(2000);
            saveNotification.show(Page.getCurrent());
        } else  {
            Notification notification = new Notification(languageManager.getCaptions().getString("questionnaireNameMsg"), Notification.Type.WARNING_MESSAGE);
            notification.setDelayMsec(2000);
            notification.show(Page.getCurrent());
        }
    }

    /**
     * Fills the question table.
     *
     * @param questionnaireBuilder Questionnaire builder
     */
    private void buildQuestionTable(QuestionnaireBuilder questionnaireBuilder) {
        for (QuestionBuilder questionBuilder : questionnaireBuilder.getQuestions()) {
            questionsTable.addItem(new String[]{questionBuilder.getText()}, questionBuilder);
        }
    }

    /**
     * Fills the form if the selected Questionnaire is not new.
     *
     * @param questionnaireBuilder Questionnaire builder.
     */
    private void fillForm(QuestionnaireBuilder questionnaireBuilder) {
        questionnaireName.setValue(questionnaireBuilder.getName());
        if(questionnaireBuilder.getCoolDownTime() == -1) {
            minutesSelect.select(Integer.toString(0));
            hoursSelect.select(Integer.toString(0));
            daysSelect.setValue(Integer.toString(0));
        } else {
            minutesSelect.setValue(Long.toString((questionnaireBuilder.getCoolDownTime()%3600000)/60000));
            hoursSelect.setValue(Long.toString((questionnaireBuilder.getCoolDownTime()%86400000)/3600000));
            daysSelect.setValue(Long.toString(questionnaireBuilder.getCoolDownTime()/86400000));
        }
        minutes = Long.parseLong(minutesSelect.getValue().toString());
        hours = Long.parseLong(hoursSelect.getValue().toString());
        days = Long.parseLong(daysSelect.getValue().toString());
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }

}
