package edu.kit.lymmy.vaadin.ui;


import com.vaadin.ui.Window;
import edu.kit.lymmy.shared.Study;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.ui.design.StudyInformationLayoutDesign;

/**
 * Displays information about the over handed study.
 *
 * @author Marc
 * @since 26.01.2017
 */
class StudyInformationLayout extends StudyInformationLayoutDesign {

    /**
     * Constructor that adds functionality to the StudyInformationLayoutDesign.
     *
     * @param selectedStudy Study
     * @param languageManager Language manager
     */
    public StudyInformationLayout(Study selectedStudy, LanguageManager languageManager) {
        closeBtn.setCaption(languageManager.getCaptions().getString("closeBtn"));
        studyInformationTable.addContainerProperty(languageManager.getCaptions().getString("id"), String.class, null);
        studyInformationTable.addContainerProperty(languageManager.getCaptions().getString("description"), String.class, null);
        studyInformationTable.addContainerProperty(languageManager.getCaptions().getString("startDate"), String.class, null);
        studyInformationTable.addContainerProperty(languageManager.getCaptions().getString("endDate"), String.class, null);
        studyInformationTable.addContainerProperty(languageManager.getCaptions().getString("participants"), String.class, null);

        //noinspection ConstantConditions dates can only be null for not started studies
        studyInformationTable.addItem(new String[]{selectedStudy.getId(), selectedStudy.getDescription(), selectedStudy.getStartDate().getDayOfMonth()+". "+languageManager.getCaptions().getString(selectedStudy.getStartDate().getMonth().toString())+" "+selectedStudy.getStartDate().getYear(),
                selectedStudy.getEndDate().getDayOfMonth()+". "+languageManager.getCaptions().getString(selectedStudy.getEndDate().getMonth().toString())+" "+selectedStudy.getEndDate().getYear(), String.valueOf(selectedStudy.getRegisteredProbands())}, selectedStudy);
        closeBtn.addClickListener(e -> {
            ((Window)this.getParent()).close();
            studyInformationTable.removeAllItems();
        });
    }
}
