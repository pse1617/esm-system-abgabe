package edu.kit.lymmy.vaadin.ui;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.ui.Notification;
import edu.kit.lymmy.shared.Study;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.navigation.NavigationManager;
import edu.kit.lymmy.vaadin.ui.design.StudiesInProgressLayoutDesign;
import edu.kit.lymmy.vaadin.util.DeleteStudyPopupWindow;
import edu.kit.lymmy.vaadin.util.PopupWindow;
import edu.kit.lymmy.vaadin.util.StudyIDPopupWindow;

import java.util.List;

/**
 * This page displays the studies in progress and provides the functionality to show more information.
 *
 * @author Marc
 * @since 22.01.17
 */
public class StudiesInProgressLayout extends StudiesInProgressLayoutDesign implements View {

    private static Study selectedStudy;

    /**
     * Constructor that adds functionality to the StudiesInProgressLayoutDesign.
     *
     * @param navigationManager Navigation manager.
     * @param studies List of studies.
     * @param languageManager Language manager.
     */
    public StudiesInProgressLayout(NavigationManager navigationManager, List<Study> studies, LanguageManager languageManager){

        studiesInProgressTable.addContainerProperty(languageManager.getCaptions().getString("studyName"), String.class, null);
        studiesInProgressTable.addContainerProperty(languageManager.getCaptions().getString("endedOn"), String.class, null);
        studiesInProgressTable.addContainerProperty(languageManager.getCaptions().getString("id"), String.class, null);
        showStudyInformationBtn.setCaption(languageManager.getCaptions().getString("showStudyInformationBtn"));
        deleteBtn.setCaption(languageManager.getCaptions().getString("deleteBtn"));
        studiesInProgressLabel.setValue(languageManager.getCaptions().getString("studiesInProgressLabel"));


        for (Study study : studies) {
            //noinspection ConstantConditions dates can only be null for not started studies
            studiesInProgressTable.addItem(new String[]{study.getName(), study.getEndDate().getDayOfMonth()+". "+languageManager.getCaptions().getString(study.getEndDate().getMonth().toString())+" "+study.getEndDate().getYear(), study.getId()}, study);
        }
        studiesInProgressTable.addValueChangeListener(e -> selectedStudy = (Study) e.getProperty().getValue());


        showStudyInformationBtn.addClickListener( e -> showInformation(languageManager));
        deleteBtn.addClickListener(e -> delete(navigationManager, languageManager));
        idBtn.addClickListener(e -> {
            if(selectedStudy!=null)
                new StudyIDPopupWindow(selectedStudy, languageManager);
        });

    }

    private void delete(NavigationManager navigationManager, LanguageManager languageManager) {
        if (selectedStudy != null) {
            DeleteStudyPopupWindow deleteMessage = new DeleteStudyPopupWindow(navigationManager, languageManager, selectedStudy, studiesInProgressTable);
        }else {
            Notification selectMsg = new Notification(languageManager.getCaptions().getString("selectStudy"), Notification.Type.WARNING_MESSAGE);
            selectMsg.setDelayMsec(2000);
            selectMsg.show(Page.getCurrent());
        }
    }

    /**
     * Creates a popup with a table with more information about the selected study.
     *
     * @param languageManager Language manager.
     */
    private void showInformation(LanguageManager languageManager) {
        if(selectedStudy!=null) {
            PopupWindow informationPopup = new PopupWindow();
            StudyInformationLayout popupContent = new StudyInformationLayout(selectedStudy, languageManager);
            informationPopup.setContent(popupContent);
        } else {
            Notification selectMsg = new Notification(languageManager.getCaptions().getString("selectStudy"), Notification.Type.WARNING_MESSAGE);
            selectMsg.setDelayMsec(2000);
            selectMsg.show(Page.getCurrent());
        }
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
    }
}
