package edu.kit.lymmy.vaadin.ui;

import com.vaadin.spring.annotation.UIScope;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.ui.design.ActivationConfirmationLayoutDesign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The confirmation page that is displayed after successful user activation.
 *
 * @author Marc
 * @since 07.02.2017
 */
@UIScope
@Component
public class ActivationConfirmationLayout extends ActivationConfirmationLayoutDesign {

    /**
     * Constructor that adds functionality to the ActivationConfirmationLayoutDesign.
     *
     * @param ui Activation User Interface
     * @param languageManager Language Manager
     */
    @Autowired
    public ActivationConfirmationLayout(ActivationUi ui, LanguageManager languageManager) {
        backToLoginBtn.addClickListener(event -> ui.getPage().setLocation("/"));
        activationMsg.setValue(languageManager.getCaptions().getString("activationMsg"));
        backToLoginBtn.setCaption(languageManager.getCaptions().getString("backToLoginBtn"));

    }
}
