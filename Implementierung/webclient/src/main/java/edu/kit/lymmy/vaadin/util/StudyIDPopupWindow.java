package edu.kit.lymmy.vaadin.util;

import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import edu.kit.lymmy.shared.Study;
import edu.kit.lymmy.vaadin.language.LanguageManager;

/**
 * Created by Marc on 26.02.2017.
 */
public class StudyIDPopupWindow extends PopupWindow{

    public StudyIDPopupWindow(Study selectedStudy, LanguageManager languageManager) {
        super();
        VerticalLayout content = new VerticalLayout();
        VerticalLayout labelPanel = new VerticalLayout();
        content.setSizeUndefined();
        Label iDLabel = new Label(selectedStudy.getId());
        labelPanel.addComponent(iDLabel);
        content.addComponent(labelPanel);
        labelPanel.setMargin(true);
        setContent(content);
    }
}
