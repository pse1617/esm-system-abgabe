package edu.kit.lymmy.vaadin.ui;

import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.annotation.UIScope;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.ui.design.RegistrationConfirmationLayoutDesign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * Page that displays a message after successful registration.
 *
 * @author Marc
 * @since 28.01.2017
 */
@SpringUI
@UIScope
@Component
public class RegistrationConfirmationLayout extends RegistrationConfirmationLayoutDesign {

    /**
     * Constructor that adds functionality to the RegistrationConfirmationLayoutDesign.
     *
     * @param ui Web user interface
     * @param context Application context
     * @param languageManager
     */
    @Autowired
    public RegistrationConfirmationLayout(WebUi ui, ApplicationContext context, LanguageManager languageManager){
        backToLoginBtn.addClickListener(e -> ui.setContent(context.getBean(LoginLayout.class)));

        backToLoginBtn.setCaption(languageManager.getCaptions().getString("continueBtn"));
        registrationMsg.setValue(languageManager.getCaptions().getString("registrationMsg"));

    }

}
