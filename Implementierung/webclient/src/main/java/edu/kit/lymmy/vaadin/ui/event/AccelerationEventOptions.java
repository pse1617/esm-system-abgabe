package edu.kit.lymmy.vaadin.ui.event;

import com.vaadin.ui.ComboBox;
import edu.kit.lymmy.shared.sensor.AccelerationData;
import edu.kit.lymmy.shared.sensor.SensorData;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.util.DoubleField;

/**
 * Created by marc on 03.03.17.
 */
public class AccelerationEventOptions extends EventOptions {

    private final DoubleField acceleration;
    private final ComboBox operatorCB;
    private boolean bigger;


    public AccelerationEventOptions(LanguageManager languageManager) {
        operatorCB = new ComboBox(languageManager.getCaptions().getString("Operator"));
        operatorCB.addItem("<");
        operatorCB.addItem(">");
        operatorCB.setNullSelectionAllowed(false);
        operatorCB.setValue(">");
        addComponent(operatorCB);
        operatorCB.setWidth("70px");

        acceleration = new DoubleField();
        acceleration.setCaption(languageManager.getCaptions().getString("acceleration"));
        acceleration.setWidth("140px");


        addComponent(acceleration);
        operatorCB.addValueChangeListener(e -> bigger = e.getProperty().getValue().equals(">"));


    }
    @Override
    public SensorData getSensorData(LanguageManager languageManager) {
        bigger = operatorCB.getValue().equals(">");
        return new AccelerationData(Double.parseDouble(acceleration.getValue()), bigger);
    }

    @Override
    public EventOptions built(LanguageManager languageManager, SensorData sensorData) {
        AccelerationData data = (AccelerationData) sensorData;
        AccelerationEventOptions accelerationEventOptions = this;
        accelerationEventOptions.acceleration.setValue(Double.toString(data.getAcceleration()));
        accelerationEventOptions.bigger = data.isBigger();
        if(bigger){
            operatorCB.setValue(">");
        } else {
            operatorCB.setValue("<");
        }
        return accelerationEventOptions;
   }
}
