package edu.kit.lymmy.vaadin.navigation;

import com.vaadin.navigator.View;
import edu.kit.lymmy.shared.Study;
import edu.kit.lymmy.vaadin.auth.AuthenticationManager;
import edu.kit.lymmy.vaadin.builder.EventBuilder;
import edu.kit.lymmy.vaadin.builder.QuestionBuilder;
import edu.kit.lymmy.vaadin.builder.QuestionnaireBuilder;
import edu.kit.lymmy.vaadin.builder.StudyBuilder;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.ui.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Lukas
 * @since 03.01.2017
 */
enum ViewDeclaration {
    MAIN {
        @NotNull
        @Override
        public View createView(@NotNull NavigationManager navigationManager, @NotNull AuthenticationManager authenticationManager,
                               @Nullable StudyBuilder study, @Nullable QuestionnaireBuilder questionnaire, @Nullable EventBuilder event, @Nullable QuestionBuilder question,
                               @NotNull List<Study> studies, @NotNull LanguageManager languageManager) {
            return new LandingPageLayout(navigationManager, languageManager, studies);
        }
    },
    STUDY {
        @NotNull
        @Override
        public View createView(@NotNull NavigationManager navigationManager, @NotNull AuthenticationManager authenticationManager,
                               @Nullable StudyBuilder study, @Nullable QuestionnaireBuilder questionnaire, @Nullable EventBuilder event, @Nullable QuestionBuilder question,
                               @NotNull List<Study> studies, @NotNull LanguageManager languageManager) {
            return new StudyEditorLayout(navigationManager, study, languageManager);
        }
    },
    QUESTIONNAIRE {
        @NotNull
        @Override
        public View createView(@NotNull NavigationManager navigationManager, @NotNull AuthenticationManager authenticationManager,
                               @Nullable StudyBuilder study, @Nullable QuestionnaireBuilder questionnaire, @Nullable EventBuilder event, @Nullable QuestionBuilder question,
                               @NotNull List<Study> studies, @NotNull LanguageManager languageManager) {
            return new QuestionnaireEditorLayout(navigationManager, questionnaire, languageManager, studies, study);
        }
    },
    EVENT {
        @NotNull
        @Override
        public View createView(@NotNull NavigationManager navigationManager, @NotNull AuthenticationManager authenticationManager,
                               @Nullable StudyBuilder study, @Nullable QuestionnaireBuilder questionnaire, @Nullable EventBuilder event, @Nullable QuestionBuilder question,
                               @NotNull List<Study> studies, @NotNull LanguageManager languageManager) {
            return new EventEditorLayout(navigationManager, event, languageManager);
        }
    },
    QUESTION {
        @NotNull
        @Override
        public View createView(@NotNull NavigationManager navigationManager, @NotNull AuthenticationManager authenticationManager, @Nullable StudyBuilder study,
                               @Nullable QuestionnaireBuilder questionnaire, @Nullable EventBuilder event, @Nullable QuestionBuilder question,
                               @NotNull List<Study> studies, @NotNull LanguageManager languageManager) {
            return new QuestionEditorLayout(navigationManager, question, languageManager, questionnaire);
        }
    },
    NEW_STUDIES {
        @NotNull
        @Override
        public View createView(@NotNull NavigationManager navigationManager, @NotNull AuthenticationManager authenticationManager,
                               @Nullable StudyBuilder study, @Nullable QuestionnaireBuilder questionnaire, @Nullable EventBuilder event, @Nullable QuestionBuilder question,
                               @NotNull List<Study> studies, @NotNull LanguageManager languageManager) {
            return new NewStudiesLayout(navigationManager, studies.stream()
                    .filter(s -> s.getStartDate() == null || s.getStartDate().isAfter(LocalDateTime.now().plusDays(1))).collect(Collectors.toList()), languageManager);
        }
    },
    STUDIES_IN_PROGRESS {
        @NotNull
        @Override
        public View createView(@NotNull NavigationManager navigationManager, @NotNull AuthenticationManager authenticationManager,
                               @Nullable StudyBuilder study, @Nullable QuestionnaireBuilder questionnaire, @Nullable EventBuilder event, @Nullable QuestionBuilder question,
                               @NotNull List<Study> studies, @NotNull LanguageManager languageManager) {
            return new StudiesInProgressLayout(navigationManager, studies.stream()
                    .filter(s -> s.getStartDate() != null && s.getEndDate() != null && s.getStartDate().isBefore(LocalDateTime.now().plusDays(1)) && s.getEndDate().isAfter(LocalDateTime.now()))
                    .collect(Collectors.toList()), languageManager);
        }
    },
    FINISHED_STUDIES {
        @NotNull
        @Override
        public View createView(@NotNull NavigationManager navigationManager, @NotNull AuthenticationManager authenticationManager,
                               @Nullable StudyBuilder study, @Nullable QuestionnaireBuilder questionnaire, @Nullable EventBuilder event, @Nullable QuestionBuilder question,
                               @NotNull List<Study> studies, @NotNull LanguageManager languageManager) {
            return new FinishedStudiesLayout(navigationManager, studies.stream()
                    .filter(s -> s.getEndDate() != null && s.getEndDate().isBefore(LocalDateTime.now())).collect(Collectors.toList()), languageManager);
        }
    };

    @NotNull
    public abstract View createView(@NotNull NavigationManager navigationManager, @NotNull AuthenticationManager authenticationManager,
                                    @Nullable StudyBuilder study, @Nullable QuestionnaireBuilder questionnaire, @Nullable EventBuilder event, @Nullable QuestionBuilder question,
                                    @NotNull List<Study> studies, @NotNull LanguageManager languageManager);
}
