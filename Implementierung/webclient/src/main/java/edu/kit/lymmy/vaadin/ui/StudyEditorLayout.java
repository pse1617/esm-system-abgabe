package edu.kit.lymmy.vaadin.ui;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.ui.Notification;
import edu.kit.lymmy.vaadin.builder.QuestionBuilder;
import edu.kit.lymmy.vaadin.builder.QuestionnaireBuilder;
import edu.kit.lymmy.vaadin.builder.StudyBuilder;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.navigation.NavigationManager;
import edu.kit.lymmy.vaadin.ui.design.StudyEditorLayoutDesign;
import edu.kit.lymmy.vaadin.util.DeleteQuestionnairePopupWindow;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * This page provides the functionality to edit a study.
 *
 * @author Marc
 * @since 24.01.2017
 */
public class StudyEditorLayout extends StudyEditorLayoutDesign implements View {

    private static QuestionnaireBuilder selectedQuestionnaire;

    /**
     * Constructor that adds functionality to the StudyEditorLayoutDesign.
     *
     * @param navigationManager Navigation manager.
     * @param studyBuilder Study builder.
     * @param languageManager Language manager.
     */
    public StudyEditorLayout(NavigationManager navigationManager, StudyBuilder studyBuilder, LanguageManager languageManager) {

        questionnairesTable.addContainerProperty(languageManager.getCaptions().getString("questionnaire"), String.class, null);
        questionnairesTable.addContainerProperty(languageManager.getCaptions().getString("questions"), String.class, null);

        studyStart.setTextFieldEnabled(false);
        studyStart.setRangeStart(new Date(new Date().getTime() + (1000*60*60*24)));
        studyEnd.setTextFieldEnabled(false);
        studyEnd.setRangeStart(new Date(new Date().getTime() + (1000*60*60*24*2)));

        backBtn.setCaption(languageManager.getCaptions().getString("backBtn"));
        clearStudyBtn.setCaption(languageManager.getCaptions().getString("clearBtn"));
        addQuestionnaireBtn.setCaption(languageManager.getCaptions().getString("addBtn"));
        deleteBtn.setCaption(languageManager.getCaptions().getString("deleteBtn"));
        editQuestionnaireBtn.setCaption(languageManager.getCaptions().getString("editBtn"));
        saveBtn.setCaption(languageManager.getCaptions().getString("saveBtn"));
        questionnairesLabel.setValue(languageManager.getCaptions().getString("questionnairesLabel"));
        descriptionPanel.setCaption(languageManager.getCaptions().getString("descriptionPanel"));
        timescalePanel.setCaption(languageManager.getCaptions().getString("timescalePanel"));
        timescalePanel.setDescription(languageManager.getCaptions().getString("timescalePanelTooltip"));
        studyNamePanel.setCaption(languageManager.getCaptions().getString("studyNamePanel"));
        studyEditorLabel.setValue(languageManager.getCaptions().getString("studyEditorLabel"));
        studyStart.setCaption(languageManager.getCaptions().getString("studyStart"));
        studyEnd.setCaption(languageManager.getCaptions().getString("studyEnd"));

        clearStudyBtn.addClickListener(e -> clearForm());

        saveBtn.addClickListener(e -> save(navigationManager, studyBuilder, languageManager));

        backBtn.addClickListener(e -> navigationManager.navigateUp());

        //fillForm
        studyName.setValue(studyBuilder.getName());
        studyDescription.setValue(studyBuilder.getDescription());
        if(studyBuilder.getEndDate()!= null) {
            studyStart.setValue(Date.from(studyBuilder.getStartDate().atZone(ZoneId.systemDefault()).toInstant()));
            studyEnd.setValue(Date.from(studyBuilder.getEndDate().atZone(ZoneId.systemDefault()).toInstant()));
        }

        //Questionnaires
        buildTable(studyBuilder);

        questionnairesTable.addItemClickListener(e -> {
            selectedQuestionnaire = (QuestionnaireBuilder) e.getItemId();
            if(e.isDoubleClick())
                edit(navigationManager, languageManager, studyBuilder);
        });

        questionnairesTable.addValueChangeListener(e -> selectedQuestionnaire = (QuestionnaireBuilder) e.getProperty().getValue());

        addQuestionnaireBtn.addClickListener(e -> addQuestionnaire(navigationManager, studyBuilder, languageManager));

        editQuestionnaireBtn.addClickListener(e -> edit(navigationManager, languageManager, studyBuilder));

        deleteBtn.addClickListener(e -> delete(navigationManager, studyBuilder, languageManager));

    }

    private void addQuestionnaire(NavigationManager navigationManager, StudyBuilder studyBuilder, LanguageManager languageManager) {
        if(isFormValid(languageManager, studyBuilder, navigationManager)){
            navigationManager.editQuestionaire(studyBuilder.newQuestionaire());
        }
    }

    /**
     * Deletes the selected questionnaire.
     *
     * @param studyBuilder Study builder
     * @param languageManager Language manager
     */
    private void delete(NavigationManager navigationManager, StudyBuilder studyBuilder, LanguageManager languageManager) {
        if(selectedQuestionnaire != null) {
            DeleteQuestionnairePopupWindow deletePopup = new DeleteQuestionnairePopupWindow(languageManager, selectedQuestionnaire, questionnairesTable, studyBuilder);
        } else {
            Notification notification = new Notification(languageManager.getCaptions().getString("selectQuestionnaire"), Notification.Type.WARNING_MESSAGE);
            notification.setDelayMsec(2000);
            notification.show(Page.getCurrent());
        }
    }

    /**
     * Navigaters to QuestionnaireEditor with the selected questionnaire.
     *
     * @param navigationManager Navigation manager
     * @param languageManager Language manager
     * @param studyBuilder Study builder.
     */
    private void edit(NavigationManager navigationManager, LanguageManager languageManager, StudyBuilder studyBuilder) {
        if(selectedQuestionnaire != null) {
            if(isFormValid(languageManager, studyBuilder, navigationManager)){
                save(navigationManager, studyBuilder, languageManager);
                navigationManager.editQuestionaire(selectedQuestionnaire);
                selectedQuestionnaire = null;
            }
        } else {
            Notification notification = new Notification(languageManager.getCaptions().getString("selectQuestionnaire"), Notification.Type.WARNING_MESSAGE);
            notification.setDelayMsec(2000);
            notification.show(Page.getCurrent());
        }
    }

    /**
     * Saves the study.
     *
     * @param navigationManager Navigation manager
     * @param studyBuilder Study builder
     * @param languageManager Language manager
     */
    private void save(NavigationManager navigationManager, StudyBuilder studyBuilder, LanguageManager languageManager) {
        if(isFormValid(languageManager, studyBuilder, navigationManager)) {
            if(studyStart.getValue() == null) {
                studyBuilder.setName(studyName.getValue());
                studyBuilder.setDescription(studyDescription.getValue());
                navigationManager.saveStudy();
                Notification saveNotification = new Notification(languageManager.getCaptions().getString("saveSuccessMsg"));
                saveNotification.setDelayMsec(2000);
                saveNotification.show(Page.getCurrent());
            } else {
                studyBuilder.setName(studyName.getValue());
                studyBuilder.setStartDate(LocalDateTime.of(studyStart.getValue().toInstant().atZone(ZoneId.systemDefault()).toLocalDate(), LocalTime.MIDNIGHT));
                studyBuilder.setEndDate(LocalDateTime.of(studyEnd.getValue().toInstant().atZone(ZoneId.systemDefault()).toLocalDate(), LocalTime.MIDNIGHT));
                studyBuilder.setDescription(studyDescription.getValue());
                navigationManager.saveStudy();
                Notification saveNotification = new Notification(languageManager.getCaptions().getString("saveSuccessMsg"));
                saveNotification.setDelayMsec(2000);
                saveNotification.show(Page.getCurrent());
            }
        }
    }

    /**
     * Clears the form.
     */
    private void clearForm() {
        studyName.clear();
        studyStart.clear();
        studyEnd.clear();
        studyDescription.clear();
    }

    /**
     * Fills the Table with the questionnaires of the currently selected study.
     *
     * @param studyBuilder Study builder
     */
    private void buildTable(StudyBuilder studyBuilder) {
        for (QuestionnaireBuilder questionnaireBuilder : studyBuilder.getQuestionaires()) {
            int questions = 0;
            for(QuestionBuilder questionBuilder : questionnaireBuilder.getQuestions()) {
                questions += 1;
            }
            questionnairesTable.addItem(new String[]{questionnaireBuilder.getName(), Integer.toString(questions)}, questionnaireBuilder);
        }
    }

    private boolean isFormValid(LanguageManager languageManager, StudyBuilder studyBuilder, NavigationManager navigationManager){
        if(studyName.getValue().equals("")){
            Notification notification = new Notification(languageManager.getCaptions().getString("fillForm"), Notification.Type.WARNING_MESSAGE);
            notification.setDelayMsec(2000);
            notification.show(Page.getCurrent());
            return false;
        } else {
            if(studyStart.getValue()==null || studyEnd.getValue() == null) {
                if(studyStart.getValue() == studyEnd.getValue()) {
                    return true;
                } else {
                    Notification notification = new Notification(languageManager.getCaptions().getString("noDateMsg"), Notification.Type.WARNING_MESSAGE);
                    notification.setDelayMsec(2000);
                    notification.show(Page.getCurrent());
                    return false;
                }
            } else {
                if (studyStart.getValue().before(studyEnd.getValue())) {
                    return true;
                } else {
                    Notification notification = new Notification(languageManager.getCaptions().getString("endDateAfterStart"), Notification.Type.WARNING_MESSAGE);
                    notification.setDelayMsec(2000);
                    notification.show(Page.getCurrent());
                    return false;
                }
            }

        }
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }
}
