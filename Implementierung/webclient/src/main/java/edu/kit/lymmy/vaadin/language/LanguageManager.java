package edu.kit.lymmy.vaadin.language;

import com.vaadin.server.VaadinService;
import com.vaadin.spring.annotation.VaadinSessionScope;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * This class provides Strings for the corresponding keys in the chosen language.
 *
 * @author Marc
 * @since 02.02.17
 */

@Component
@VaadinSessionScope
public class LanguageManager {

    private String language="en";
    private String country="US";
    private Locale locale = new Locale(language,country);
    private ResourceBundle captions = ResourceBundle.getBundle("Messages",locale);

    /**
     * empty constructor
     */
    public LanguageManager() {

        Cookie cookie = new Cookie("languageCookie", "en_US");

        for (Cookie cook : VaadinService.getCurrentRequest().getCookies()) {
            if(cook.getName().equals("languageCookie")) {
                cookie = cook;
                String[] languageAndCountry = cookie.getValue().split("_");
                language = languageAndCountry[0];
                country = languageAndCountry[1];
                refreshLocale();
                refreshCaptions();
            }
        }
        cookie.setMaxAge(60*60*24*365*10);
        VaadinService.getCurrentResponse().addCookie(cookie);

    }

    /**
     *
     * @return gets the captions
     */
    public ResourceBundle getCaptions() {
        return captions;
    }

    /**
     * Sets the locale to by using language and country.
     */
    public void refreshLocale() {

        locale = new Locale(language,country);
        Cookie cookie = new Cookie("languageCookie", language+"_"+country);
        cookie.setMaxAge(60*60*24*365*10);
        cookie.setPath("/");
        VaadinService.getCurrentResponse().addCookie(cookie);

    }

    /**
     * Sets the captions by using the locale.
     */
    public void refreshCaptions() {
        captions = ResourceBundle.getBundle("Messages",locale);
    }

    /**
     * Sets the language.
     * @param language Language in form of a String
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * Sets the country.
     * @param country Country in form of a String
     */
    public void setCountry(String country) {
        this.country = country;
    }

}
