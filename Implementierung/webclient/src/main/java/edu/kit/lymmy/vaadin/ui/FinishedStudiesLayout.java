package edu.kit.lymmy.vaadin.ui;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.ui.Notification;
import edu.kit.lymmy.shared.Study;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.navigation.NavigationManager;
import edu.kit.lymmy.vaadin.ui.design.FinishedStudiesLayoutDesign;
import edu.kit.lymmy.vaadin.util.DeleteStudyPopupWindow;

import java.util.List;

/**
 * This page displays the finished Studies and provides functionality to delete or export them.
 *
 * @author Marc
 * @since 24.01.2017
 */
public class FinishedStudiesLayout extends FinishedStudiesLayoutDesign implements View {

    private static Study selectedStudy;

    /**
     * Constructor that adds functionality to the FinishedStudiesLayoutDesign.
     *
     * @param navigationManager Navigation manager
     * @param studies List of studies
     * @param languageManager Language Manager
     */

    public FinishedStudiesLayout(NavigationManager navigationManager, List<Study> studies, LanguageManager languageManager) {

        finishedStudiesLabel.setValue(languageManager.getCaptions().getString("finishedStudiesLabel"));
        deleteBtn.setCaption(languageManager.getCaptions().getString("deleteBtn"));
        exportBtn.setCaption(languageManager.getCaptions().getString("exportBtn"));
        navigationManager.prepareComponentForExport(exportBtn, () -> getSelectedStudy(languageManager));
        finishedStudiesTable.addContainerProperty(languageManager.getCaptions().getString("studyName"), String.class, null);
        finishedStudiesTable.addContainerProperty(languageManager.getCaptions().getString("endedOn"), String.class, null);

        for (Study study : studies) {
            //noinspection ConstantConditions dates can only be null for not started studies
            finishedStudiesTable.addItem(new String[]{study.getName(), study.getEndDate().getDayOfMonth()+". "+languageManager.getCaptions().getString(study.getEndDate().getMonth().toString())+" "+study.getEndDate().getYear()}, study);
        }
        finishedStudiesTable.addValueChangeListener(e -> selectedStudy = (Study) e.getProperty().getValue());

        deleteBtn.addClickListener(e -> delete(navigationManager, languageManager));



    }

    /**
     * Returns selected study.
     *
     * @param languageManager Language manager
     * @return returns the selected study ( shows a warning message if there is no study selected and returns null)
     */
    private Study getSelectedStudy(LanguageManager languageManager) {
        Study study = selectedStudy;
        if (selectedStudy != null) {
            finishedStudiesTable.unselect(selectedStudy);
            selectedStudy=null;
        }else {
            Notification notification = new Notification(languageManager.getCaptions().getString("selectStudy"), Notification.Type.WARNING_MESSAGE);
            notification.setDelayMsec(2000);
            notification.show(Page.getCurrent());
        }
        return study;
    }

    /**
     * Deletes the selected study.
     *
     * @param navigationManager Navigation manager
     * @param languageManager Language manager
     */
    private void delete(NavigationManager navigationManager, LanguageManager languageManager) {
        if (selectedStudy != null) {
            DeleteStudyPopupWindow deleteMessage = new DeleteStudyPopupWindow(navigationManager, languageManager, selectedStudy, finishedStudiesTable);
        }else {
            Notification notification = new Notification(languageManager.getCaptions().getString("selectStudy"), Notification.Type.WARNING_MESSAGE);
            notification.setDelayMsec(2000);
            notification.show(Page.getCurrent());
        }
    }


    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }
}
