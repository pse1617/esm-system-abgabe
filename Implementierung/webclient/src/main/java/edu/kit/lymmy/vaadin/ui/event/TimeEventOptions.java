package edu.kit.lymmy.vaadin.ui.event;

import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.InlineDateField;
import com.vaadin.ui.TextField;
import edu.kit.lymmy.shared.sensor.SensorData;
import edu.kit.lymmy.shared.sensor.TimeData;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.util.LongField;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * Created by Marc on 13.02.2017.
 */
public class TimeEventOptions extends EventOptions {

    private InlineDateField date;
    private TextField days = new LongField();
    private TextField hours = new LongField();
    private TextField minutes = new LongField();

    public TimeEventOptions(LanguageManager languageManager) {


        date = new InlineDateField(languageManager.getCaptions().getString("date"));
        date.setResolution(Resolution.MINUTE);
        date.addStyleName("time-only");
        date.setHeight("37px");
        addComponent(date);

        days.setCaption(languageManager.getCaptions().getString("daysLabel"));
        addComponent(days);
        days.setValue("0");
        days.setWidth("70");

        hours.setCaption(languageManager.getCaptions().getString("hoursLabel"));
        addComponent(hours);
        hours.setValue("0");
        hours.setWidth("70");

        minutes.setCaption(languageManager.getCaptions().getString("minutesLabel"));
        addComponent(minutes);
        minutes.setValue("0");
        minutes.setWidth("70");


    }

    @Override
    public SensorData getSensorData(LanguageManager languageManager) {

        long minutesLong = Long.parseLong(minutes.getValue());
        long hoursLong = Long.parseLong(hours.getValue());
        long daysLong = Long.parseLong(days.getValue());
        Long periodLength = minutesLong*1000*60+hoursLong*1000*60*60+daysLong*1000*60*60*24;
        return new TimeData(LocalDateTime.ofInstant(date.getValue().toInstant(), ZoneId.systemDefault()).toLocalTime(), periodLength);
    }

    @Override
    public EventOptions built(LanguageManager languageManager, SensorData sensorData) {
        TimeEventOptions eventOptions = this;
        TimeData timeData =(TimeData) sensorData;
        long minutesLong = (timeData.getPeriodLength()%3600000)/60000;
        long hoursLong = (timeData.getPeriodLength()%86400000)/3600000;
        long daysLong = timeData.getPeriodLength()/86400000;
        eventOptions.date.setValue(Date.from(timeData.getDate().atDate(LocalDate.now()).atZone(ZoneId.systemDefault()).toInstant()));
        eventOptions.minutes.setValue(String.valueOf(minutesLong));
        eventOptions.hours.setValue(String.valueOf(hoursLong));
        eventOptions.days.setValue(String.valueOf(daysLong));
        return eventOptions;
    }

}
