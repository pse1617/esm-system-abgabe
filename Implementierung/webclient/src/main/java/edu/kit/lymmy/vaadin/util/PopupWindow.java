package edu.kit.lymmy.vaadin.util;

import com.vaadin.ui.UI;
import com.vaadin.ui.Window;

/**
 * Created by Marc on 24.02.2017.
 */
public class PopupWindow extends Window {

    public PopupWindow() {
        center();
        UI.getCurrent().addWindow(this);
    }
}
