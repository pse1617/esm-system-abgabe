package edu.kit.lymmy.vaadin.builder;

import edu.kit.lymmy.shared.Study;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Used to build a {@link Study}
 *
 * @author Lukas
 * @since 03.01.2017
 */
public class StudyBuilder {
    private final String id;
    private String name;
    private String description;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private final List<QuestionnaireBuilder> questionaires;
    private final String userId;

    /**
     * Create a new instance with default values
     *
     * @param id     the unique id of this study
     * @param userId the user creating this study
     */
    public StudyBuilder(@NotNull String id, @NotNull String userId) {
        this.id = id;
        name = "New Study";
        description = "";
        startDate = null;
        endDate = null;
        questionaires = new ArrayList<>();
        this.userId = userId;
    }

    /**
     * Create a new instance with values from the passed study
     *
     * @param study the values to copy
     */
    public StudyBuilder(@NotNull Study study) {
        id = study.getId();
        name = study.getName();
        description = study.getDescription();
        startDate = study.getStartDate();
        endDate = study.getEndDate();
        questionaires = study.getQuestionnaires().stream().map(QuestionnaireBuilder::new).collect(Collectors.toCollection(ArrayList::new));
        userId = study.getUserID();
    }

    /**
     * @return the id
     */
    @NotNull
    public String getId() {
        return id;
    }

    /**
     * @return the name
     */
    @NotNull
    public String getName() {
        return name;
    }

    /**
     * Set the name
     *
     * @param name the value to set to
     * @return this
     */
    public StudyBuilder setName(@NotNull String name) {
        this.name = name;
        return this;
    }

    /**
     * @return the description
     */
    @NotNull
    public String getDescription() {
        return description;
    }

    /**
     * Set the description
     *
     * @param description the value to set to
     * @return this
     */
    public StudyBuilder setDescription(@NotNull String description) {
        this.description = description;
        return this;
    }

    /**
     * @return the start date
     */
    public LocalDateTime getStartDate() {
        return startDate;
    }

    /**
     * Set the start date
     *
     * @param startDate the value to set to
     * @return this
     */
    public StudyBuilder setStartDate(@NotNull LocalDateTime startDate) {
        this.startDate = startDate;
        return this;
    }


    /**
     * @return the end date
     */
    public LocalDateTime getEndDate() {
        return endDate;
    }


    /**
     * Set the end date
     *
     * @param endDate the value to set to
     * @return this
     */
    public StudyBuilder setEndDate(@NotNull LocalDateTime endDate) {
        this.endDate = endDate;
        return this;
    }

    /**
     * @return a modifiable list of questionnaires
     */
    @NotNull
    public List<QuestionnaireBuilder> getQuestionaires() {
        return questionaires;
    }

    /**
     * @return a new QuestionnaireBuilder added to this
     */
    @NotNull
    public QuestionnaireBuilder newQuestionaire() {
        QuestionnaireBuilder questionaire = new QuestionnaireBuilder();
        questionaires.add(questionaire);
        return questionaire;
    }

    /**
     * remove a questionnaire
     *
     * @param questionaire the value to remove
     * @return this
     */
    public StudyBuilder removeQuestionaire(@NotNull QuestionnaireBuilder questionaire) {
        questionaires.remove(questionaire);
        return this;
    }

    /**
     * @return a new study with the values of this
     */
    @NotNull
    public Study build() {
        return new Study(id, userId, name, description, startDate, endDate, questionaires.stream().map(QuestionnaireBuilder::build).collect(Collectors.toList()));
    }
}
