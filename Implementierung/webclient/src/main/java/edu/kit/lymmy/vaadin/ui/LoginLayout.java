package edu.kit.lymmy.vaadin.ui;

import com.vaadin.server.Page;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.LoginForm;
import com.vaadin.ui.Notification;
import edu.kit.lymmy.shared.messages.CommunicationConstants;
import edu.kit.lymmy.vaadin.auth.AuthenticationManager;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.ui.design.LoginLayoutDesign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.EnumMap;

/**
 * This page provides the login functionality.
 *
 * @author Marc
 * @since 28.01.2017
 */
@SpringUI
@UIScope
@Component
public class LoginLayout extends LoginLayoutDesign {

    private final EnumMap<CommunicationConstants.LoginResult, Notification> loginResultNotificationMap = new EnumMap<>(CommunicationConstants.LoginResult.class);

    /**
     * Constructor that adds functionality to the LoginLayoutDesign.
     *
     * @param ui Web user interface
     * @param authenticationManager Authentication manager
     * @param languageManager Language manager
     */
    @Autowired
    public LoginLayout(WebUi ui, AuthenticationManager authenticationManager, LanguageManager languageManager){
        LanguageSelection languageSelection = new LanguageSelection(languageManager);
        languageSelectionPanel.addComponent(languageSelection);
        LoginForm login = new LoginForm();
        loginPanel.addComponent(login);
        loginLabel.setValue(languageManager.getCaptions().getString("loginLabel"));
        login.setUsernameCaption(languageManager.getCaptions().getString("mailPanel"));
        login.setPasswordCaption(languageManager.getCaptions().getString("password"));
        login.setLoginButtonCaption(languageManager.getCaptions().getString("loginBtn"));
        registerBtn.setCaption(languageManager.getCaptions().getString("registerBtn"));

        Notification ok = new Notification(languageManager.getCaptions().getString("loginResultOK"), Notification.Type.TRAY_NOTIFICATION);
        ok.setDelayMsec(2000);
        Notification badCredentials = new Notification(languageManager.getCaptions().getString("loginResultBAD_CREDENTIALS"), Notification.Type.ERROR_MESSAGE);
        badCredentials.setDelayMsec(2000);
        Notification notActive = new Notification(languageManager.getCaptions().getString("loginResultNOT_ACTIVE"), Notification.Type.WARNING_MESSAGE);
        notActive.setDelayMsec(1000);
        Notification notFound = new Notification(languageManager.getCaptions().getString("loginResultNOT_FOUND"), Notification.Type.ERROR_MESSAGE);
        notFound.setDelayMsec(2000);
        Notification unknown = new Notification(languageManager.getCaptions().getString("loginResultUNKNOWN"), Notification.Type.ERROR_MESSAGE);
        unknown.setDelayMsec(2000);

        loginResultNotificationMap.put(CommunicationConstants.LoginResult.OK, ok);
        loginResultNotificationMap.put(CommunicationConstants.LoginResult.BAD_CREDENTIALS, badCredentials);
        loginResultNotificationMap.put(CommunicationConstants.LoginResult.NOT_ACTIVE, notActive);
        loginResultNotificationMap.put(CommunicationConstants.LoginResult.NOT_FOUND, notFound);
        loginResultNotificationMap.put(CommunicationConstants.LoginResult.UNKNOWN, unknown);

        login.addLoginListener(e -> {
            CommunicationConstants.LoginResult result = authenticationManager.login(e.getLoginParameter("username"), e.getLoginParameter("password"));
            loginResultNotificationMap.get(result).show(Page.getCurrent());

                if (result == CommunicationConstants.LoginResult.OK) {
                               ui.setContentBean(MainLayout.class);
                }
                if (result == CommunicationConstants.LoginResult.NOT_ACTIVE) {
                    ui.setContentBean(ActivationHintLayout.class);
                }
        });

        registerBtn.addClickListener(e ->  ui.setContentBean(RegistrationLayout.class));



    }

}
