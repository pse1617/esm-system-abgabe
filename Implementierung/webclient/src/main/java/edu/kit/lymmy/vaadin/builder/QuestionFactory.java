package edu.kit.lymmy.vaadin.builder;

import edu.kit.lymmy.shared.question.*;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.util.Pair;

import java.util.List;

/**
 * @author Lukas
 * @since 09.01.2017
 */
class QuestionFactory {

    @NotNull
    static Question create(@NotNull QuestionType type, @NotNull Condition condition, @NotNull String text, @NotNull List<String> answers, int steps, @NotNull Pair<String, String> sliderText) {
        switch (type) {
            case OPEN:
                return new OpenQuestion(condition, text);
            case SINGLE_CHOICE:
                return new SingleChoiceQuestion(condition, text, answers);
            case MULTIPLE_CHOICE:
                return new MultipleChoiceQuestion(condition, text, answers);
            case LIKERT:
                return new LikertQuestion(condition, text, sliderText.getFirst(), sliderText.getSecond(), steps);
            case YES_NO:
                return new YesNoQuestion(condition, text, answers);
            case SLIDER:
                return new SliderQuestion(condition, text, sliderText.getFirst(), sliderText.getSecond());
            default:
                throw new IllegalArgumentException("Illegal question type");
        }
    }
}
