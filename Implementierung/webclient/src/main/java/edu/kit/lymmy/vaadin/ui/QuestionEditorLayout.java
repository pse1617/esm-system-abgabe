package edu.kit.lymmy.vaadin.ui;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import edu.kit.lymmy.shared.question.QuestionType;
import edu.kit.lymmy.vaadin.builder.QuestionBuilder;
import edu.kit.lymmy.vaadin.builder.QuestionnaireBuilder;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.navigation.NavigationManager;
import edu.kit.lymmy.vaadin.ui.design.OpenQuestionDesign;
import edu.kit.lymmy.vaadin.ui.design.QuestionEditorLayoutDesign;
import edu.kit.lymmy.vaadin.ui.design.YesNoQuestionDesign;
import edu.kit.lymmy.vaadin.ui.question.*;
import edu.kit.lymmy.vaadin.util.PopupWindow;
import org.apache.commons.collections.BidiMap;
import org.apache.commons.collections.bidimap.TreeBidiMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Marc
 * @since 24.01.2017
 */
public class QuestionEditorLayout extends QuestionEditorLayoutDesign implements View {

    private final Map<String, VerticalLayout> answerPanelMap = new HashMap<>();
    private final BidiMap questionTypeMap = new TreeBidiMap();
    private static String selectedQuestion;
    private final YesNoQuestionDesign yesNoQuestionDesign;
    private final MultipleChoiceQuestion multipleChoiceQuestion;
    private final SingleChoiceQuestion singleChoiceQuestion;
    private final SliderQuestion sliderQuestion;
    private final LikertQuestion likertQuestion;
    private final OpenQuestionDesign openQuestionDesign;
    private PopupWindow conditionPopup;

    public QuestionEditorLayout(NavigationManager navigationManager, QuestionBuilder questionBuilder, LanguageManager languageManager, QuestionnaireBuilder questionnaireBuilder) {
        yesNoQuestionDesign = new YesNoQuestion(languageManager);
        singleChoiceQuestion = new SingleChoiceQuestion(languageManager);
        multipleChoiceQuestion = new MultipleChoiceQuestion(languageManager);
        sliderQuestion = new SliderQuestion(languageManager);
        openQuestionDesign = new OpenQuestion(languageManager);
        likertQuestion = new LikertQuestion(languageManager);
        backBtn.setCaption(languageManager.getCaptions().getString("backBtn"));
        saveBtn.setCaption(languageManager.getCaptions().getString("saveBtn"));
        questionPanel.setCaption(languageManager.getCaptions().getString("questionPanel"));
        questionTypePanel.setCaption(languageManager.getCaptions().getString("questionTypePanel"));
        questionEditorLabel.setValue(languageManager.getCaptions().getString("questionEditorLabel"));
        editConditionBtn.setCaption(languageManager.getCaptions().getString("editConditionBtn"));
        conditionPanel.setCaption(languageManager.getCaptions().getString("conditionPanel"));

        questionTypeCB.addItem(languageManager.getCaptions().getString("multipleChoiceQuestion"));
        questionTypeCB.addItem(languageManager.getCaptions().getString("singleChoiceQuestion"));
        questionTypeCB.addItem(languageManager.getCaptions().getString("sliderQuestion"));
        questionTypeCB.addItem(languageManager.getCaptions().getString("likertQuestion"));
        questionTypeCB.addItem(languageManager.getCaptions().getString("openQuestion"));
        questionTypeCB.addItem(languageManager.getCaptions().getString("yesNoChoiceQuestion"));

        conditionPanel.setDescription(languageManager.getCaptions().getString("conditionTooltip"));
        questionTypeCB.setNewItemsAllowed(false);
        questionTypeCB.setNullSelectionAllowed(false);

        buildAnswerPanelMap(languageManager);
        buildQuestionTypeMap(languageManager);

        fillForm(questionBuilder);

        questionTypeCB.setTextInputAllowed(false);
        questionTypeCB.addValueChangeListener(e -> {
            answerPanel.removeAllComponents();
            if (e.getProperty().getValue() != null) {
                selectedQuestion = (String) e.getProperty().getValue();
                answerPanel.addComponent(answerPanelMap.get(e.getProperty().getValue()));
            }
        });

        saveBtn.addClickListener(e -> save(questionBuilder, languageManager, navigationManager));

        backBtn.addClickListener(e -> navigationManager.navigateUp());

        editConditionBtn.addClickListener(e -> {
            ConditionSelection popupContent = new ConditionSelection(languageManager, questionnaireBuilder, questionBuilder, this);
            conditionPopup = new PopupWindow();
            conditionPopup.setContent(popupContent);
        });


    }





    private void save(QuestionBuilder questionBuilder, LanguageManager languageManager, NavigationManager navigationManager) {
        if(question.getValue()!=null && !question.getValue().equals("")){
            questionBuilder.setText(question.getValue());
            questionBuilder.setType((QuestionType) questionTypeMap.get(selectedQuestion));
            if(selectedQuestion.equals(languageManager.getCaptions().getString("sliderQuestion"))){
                questionBuilder.setSteps(100);
                questionBuilder.setSliderTexts(sliderQuestion.getLeftLabelField().getValue(), sliderQuestion.getRightLabelField().getValue());
            }
            if(selectedQuestion.equals(languageManager.getCaptions().getString("likertQuestion"))){
                questionBuilder.setSteps(Integer.parseInt(likertQuestion.getStepsField().getValue()));
                questionBuilder.setSliderTexts(sliderQuestion.getLeftLabelField().getValue(), sliderQuestion.getRightLabelField().getValue());
            }

            questionBuilder.setAnswers(getText(languageManager, questionBuilder));
            questionBuilder.setCondition(questionBuilder.getCondition());
            navigationManager.saveStudy();
            Notification saveNotification = new Notification(languageManager.getCaptions().getString("saveSuccessMsg"));
            saveNotification.setDelayMsec(2000);
            saveNotification.show(Page.getCurrent());
        } else  {
            Notification notification = new Notification(languageManager.getCaptions().getString("noEmptyQuestionMsg"), Notification.Type.WARNING_MESSAGE);
            notification.setDelayMsec(2000);
            notification.show(Page.getCurrent());
        }
    }


    private void fillForm(QuestionBuilder questionBuilder) {
        question.setValue(questionBuilder.getText());
        switch (questionBuilder.getType()) {
            case MULTIPLE_CHOICE:
                multipleChoiceQuestion.setAnswers(questionBuilder.getAnswers());
                answerPanel.addComponent(multipleChoiceQuestion);
                break;
            case SINGLE_CHOICE:
                singleChoiceQuestion.setAnswers(questionBuilder.getAnswers());
                answerPanel.addComponent(singleChoiceQuestion);
                break;
            case SLIDER:
                sliderQuestion.getLeftLabelField().setValue(questionBuilder.getSliderTexts().getFirst().toString());
                sliderQuestion.getRightLabelField().setValue(questionBuilder.getSliderTexts().getSecond().toString());
                answerPanel.addComponent(sliderQuestion);
                break;
            case LIKERT:
                likertQuestion.getLeftLabelField().setValue(questionBuilder.getSliderTexts().getFirst().toString());
                likertQuestion.getRightLabelField().setValue(questionBuilder.getSliderTexts().getSecond().toString());
                likertQuestion.getStepsField().setValue(Integer.toString(questionBuilder.getSteps()));
                answerPanel.addComponent(likertQuestion);
                break;
            case OPEN:
                answerPanel.addComponent(openQuestionDesign);
                break;
            case YES_NO:
                answerPanel.addComponent(yesNoQuestionDesign);
                break;
            default:
        }
        questionTypeCB.setValue(questionTypeMap.getKey(questionBuilder.getType()).toString());
        selectedQuestion = questionTypeMap.getKey(questionBuilder.getType()).toString();

    }

    private List<String> getText(LanguageManager languageManager, QuestionBuilder questionBuilder){

        List<String> Text = new ArrayList<>();
        if(selectedQuestion.equals(languageManager.getCaptions().getString("multipleChoiceQuestion"))){
            for (TextField answer : multipleChoiceQuestion.getAnswers()) {
                Text.add(answer.getValue());
            }
        } else if (selectedQuestion.equals(languageManager.getCaptions().getString("singleChoiceQuestion"))) {
            for (TextField answer : singleChoiceQuestion.getAnswers()) {
                Text.add(answer.getValue());
            }
        } else if (selectedQuestion.equals(languageManager.getCaptions().getString("yesNoChoiceQuestion"))) {
            Text.add(languageManager.getCaptions().getString("yesMessage"));
            Text.add(languageManager.getCaptions().getString("noMessage"));
        }
        return Text;
    }

    private void buildQuestionTypeMap(LanguageManager languageManager) {
        questionTypeMap.put(languageManager.getCaptions().getString("yesNoChoiceQuestion"), QuestionType.YES_NO);
        questionTypeMap.put(languageManager.getCaptions().getString("multipleChoiceQuestion"), QuestionType.MULTIPLE_CHOICE);
        questionTypeMap.put(languageManager.getCaptions().getString("singleChoiceQuestion"), QuestionType.SINGLE_CHOICE);
        questionTypeMap.put(languageManager.getCaptions().getString("sliderQuestion"), QuestionType.SLIDER);
        questionTypeMap.put(languageManager.getCaptions().getString("likertQuestion"), QuestionType.LIKERT);
        questionTypeMap.put(languageManager.getCaptions().getString("openQuestion"), QuestionType.OPEN);
    }

    private void buildAnswerPanelMap(LanguageManager languageManager) {

        answerPanelMap.put(languageManager.getCaptions().getString("yesNoChoiceQuestion"), yesNoQuestionDesign);
        answerPanelMap.put(languageManager.getCaptions().getString("multipleChoiceQuestion"), multipleChoiceQuestion);
        answerPanelMap.put(languageManager.getCaptions().getString("singleChoiceQuestion"), singleChoiceQuestion);
        answerPanelMap.put(languageManager.getCaptions().getString("sliderQuestion"), sliderQuestion);
        answerPanelMap.put(languageManager.getCaptions().getString("likertQuestion"), likertQuestion);
        answerPanelMap.put(languageManager.getCaptions().getString("openQuestion"), openQuestionDesign);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }


    public PopupWindow getConditionPopup() {
        return conditionPopup;
    }

}
