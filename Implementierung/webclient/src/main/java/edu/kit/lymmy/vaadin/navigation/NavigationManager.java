package edu.kit.lymmy.vaadin.navigation;

import com.vaadin.navigator.NavigationStateManager;
import com.vaadin.navigator.Navigator.ComponentContainerViewDisplay;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewDisplay;
import com.vaadin.navigator.ViewProvider;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.UI;
import edu.kit.lymmy.shared.Study;
import edu.kit.lymmy.vaadin.auth.AuthenticationManager;
import edu.kit.lymmy.vaadin.builder.EventBuilder;
import edu.kit.lymmy.vaadin.builder.QuestionBuilder;
import edu.kit.lymmy.vaadin.builder.QuestionnaireBuilder;
import edu.kit.lymmy.vaadin.builder.StudyBuilder;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.network.ContentConnector;
import edu.kit.lymmy.vaadin.util.OnDemandFileDownloader;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

/**
 * @author Lukas
 * @since 03.01.2017
 */
@UIScope
@Component
public class NavigationManager {
    private final Navigator navigator;
    private final ContentConnector contentConnector;
    private final AuthenticationManager authenticationManager;
    private final LanguageManager languageManager;
    private StudyBuilder currentStudy;
    private QuestionnaireBuilder currentQuestionaire;
    private EventBuilder currentEvent;
    private QuestionBuilder currentQuestion;
    private List<Study> studies;

    @Autowired
    public NavigationManager(ContentConnector contentConnector, AuthenticationManager authenticationManager, LanguageManager languageManager) {
        this.contentConnector = contentConnector;
        this.authenticationManager = authenticationManager;
        this.languageManager = languageManager;
        navigator = new Navigator();
    }

    public void init(@NotNull UI ui, @NotNull ComponentContainer container) {
        navigator.init(ui, new ComponentContainerViewDisplay(container));
        navigator.addProvider(new InternalViewProvider());
        studies = contentConnector.loadStudies();
        navigator.navigateTo(ViewDeclaration.MAIN.name());
    }

    @Nullable
    public StudyBuilder getNewStudy() {
        String id = contentConnector.getNewStudyId();
        String user = authenticationManager.getUsername();
        return id == null || user == null ? null : new StudyBuilder(id, user);
    }

    public void editStudy(@NotNull StudyBuilder study) {
        currentStudy = study;
        navigator.navigateTo(ViewDeclaration.STUDY.name());
    }

    public void editQuestionaire(@NotNull QuestionnaireBuilder questionaire) {
        currentQuestionaire = questionaire;
        navigator.navigateTo(ViewDeclaration.QUESTIONNAIRE.name());
    }

    public void editEvent(@NotNull EventBuilder event) {
        currentEvent = event;
        navigator.navigateTo(ViewDeclaration.EVENT.name());
    }

    public void editQuestion(@NotNull QuestionBuilder question) {
        currentQuestion = question;
        navigator.navigateTo(ViewDeclaration.QUESTION.name());
    }

    public void navigateUp() {
        if (currentQuestion != null || currentEvent != null) {
            currentQuestion = null;
            currentEvent = null;
            navigator.navigateTo(ViewDeclaration.QUESTIONNAIRE.name());
        } else if (currentQuestionaire != null) {
            currentQuestionaire = null;
            navigator.navigateTo(ViewDeclaration.STUDY.name());
        } else if (currentStudy != null) {
            currentStudy = null;
            navigator.navigateTo(ViewDeclaration.NEW_STUDIES.name());
        } else {
            navigator.navigateTo(ViewDeclaration.MAIN.name());
        }
    }

    public void saveStudy() {
        contentConnector.saveStudy(currentStudy.build());
        studies = contentConnector.loadStudies();
    }

    public void deleteStudy(@NotNull Study study) {
        contentConnector.deleteStudy(study);
        studies = contentConnector.loadStudies();
    }

    public void showNewStudies() {
        navigator.navigateTo(ViewDeclaration.NEW_STUDIES.name());
    }

    public void showRunningStudies() {
        navigator.navigateTo(ViewDeclaration.STUDIES_IN_PROGRESS.name());
    }

    public void showFinishedStudies() {
        navigator.navigateTo(ViewDeclaration.FINISHED_STUDIES.name());
    }

    /**
     * Prepares a component for exporting studies
     * @param component the component to extend
     * @param studyProvider will be called on export, should return the study to export or null to cancel
     */
    public void prepareComponentForExport(@NotNull AbstractComponent component, @NotNull Supplier<@Nullable Study> studyProvider){
        OnDemandFileDownloader fileDownloader = new OnDemandFileDownloader(()-> {
            Study study = studyProvider.get();
            if(study == null) return null;
            String csv = contentConnector.getCsvForStudy(study);
            if(csv == null) return null;
            return Pair.of(new ByteArrayInputStream(csv.getBytes(StandardCharsets.UTF_8)), study.getName() + "_"+study.getId()+".csv");
        });
        fileDownloader.extend(component);
    }

    private class InternalViewProvider implements ViewProvider {
        @Override
        public String getViewName(String viewAndParameters) {
            if (null == viewAndParameters) {
                return null;
            }
            if ("".equals(viewAndParameters)) {
                return ViewDeclaration.MAIN.name();
            }
            return Arrays.stream(ViewDeclaration.values()).map(ViewDeclaration::name)
                    .filter(viewName -> viewAndParameters.equals(viewName) || viewAndParameters.startsWith(viewName + "/"))
                    .findAny().orElse(null);
        }

        @Override
        public View getView(String viewName) {
            try {
                return ViewDeclaration.valueOf(viewName)
                        .createView(NavigationManager.this, authenticationManager, currentStudy, currentQuestionaire, currentEvent, currentQuestion, studies, languageManager);
            } catch (IllegalArgumentException e) {
                return null;
            }
        }
    }

    private static class Navigator extends com.vaadin.navigator.Navigator {
        private final Logger logger;
        private boolean isInitialized = false;

        Navigator() {
            logger = LoggerFactory.getLogger(Navigator.class);
        }

        void init(@NotNull UI ui, @NotNull ViewDisplay display) {
            init(ui, null, display);
        }

        @Override
        public void init(@NotNull UI ui, @Nullable NavigationStateManager stateManager, @NotNull ViewDisplay display) {
            super.init(ui, stateManager, display);
            isInitialized = true;
        }

        @Override
        public void navigateTo(String navigationState) {
            if (isInitialized) {
                super.navigateTo(navigationState);
            } else {
                logger.warn("navigateTo called before init");
            }
        }
    }
}
