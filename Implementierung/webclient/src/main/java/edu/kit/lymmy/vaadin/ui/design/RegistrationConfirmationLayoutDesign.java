package edu.kit.lymmy.vaadin.ui.design;

import com.vaadin.annotations.AutoGenerated;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.declarative.Design;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Button;

/**
 * !! DO NOT EDIT THIS FILE !!
 * <p>
 * This class is generated by Vaadin Designer and will be overwritten.
 * <p>
 * Please make a subclass with logic and additional interfaces as needed,
 * e.g class LoginView extends LoginDesign implements View { }
 */
@DesignRoot
@AutoGenerated
@SuppressWarnings("serial")
public class RegistrationConfirmationLayoutDesign extends VerticalLayout {
    protected Label registrationMsg;
    protected Button backToLoginBtn;

    /**
     * Constructor that adds the design elements
     */
    public RegistrationConfirmationLayoutDesign() {
        Design.read(this);
    }
}
