package edu.kit.lymmy.vaadin.ui;

import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.UI;
import edu.kit.lymmy.vaadin.auth.AuthenticationManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import static edu.kit.lymmy.shared.messages.CommunicationConstants.CONFIRM_REGISTRATION;
import static edu.kit.lymmy.shared.messages.CommunicationConstants.PARAM_CODE;
import static edu.kit.lymmy.shared.messages.CommunicationConstants.PARAM_MAIL;

/**
 * @author Lukas
 * @since 06.02.2017
 */
@SpringUI(path = CONFIRM_REGISTRATION)
@Theme("mytheme")
public class ActivationUi extends UI{

    private final AuthenticationManager authenticationManager;
    private final ApplicationContext context;

    @Autowired
    public ActivationUi(AuthenticationManager authenticationManager, ApplicationContext context){
        this.authenticationManager = authenticationManager;
        this.context = context;
    }

    @Override
    protected void init(VaadinRequest request) {
        String code = request.getParameter(PARAM_CODE);
        String mail = request.getParameter(PARAM_MAIL);
        if(code != null && mail != null) {
            activate(mail, code);
        }else {
            setContent(context.getBean(ActivationLayout.class));
        }
    }

    public void activate(String email, String code){
        if(authenticationManager.activate(email, code)) {
            setContent(context.getBean(ActivationConfirmationLayout.class));
        }else {
            //TODO show alert
        }
    }
}
