package edu.kit.lymmy.vaadin.util;

import com.vaadin.server.FileDownloader;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinResponse;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.UI;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.util.Pair;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Lukas
 * @since 24.03.2017
 */
@RunWith(MockitoJUnitRunner.class)
public class OnDemandFileDownloaderTest {

    @Mock
    private VaadinRequest request;

    @Mock
    private VaadinResponse response;

    @Mock
    private UI ui;

    @Mock
    private VaadinSession vaadinSession;

    private FileDownloader fileDownloader;

    private Pair<InputStream, String> result;

    @Before
    public void setUp() throws Exception {
        when(ui.getSession()).thenReturn(vaadinSession);
        when(vaadinSession.hasLock()).thenReturn(true);
        fileDownloader = spy(new OnDemandFileDownloader(()->result));
        when(fileDownloader.getUI()).thenReturn(ui);
    }

    @Test
    public void handleConnectorRequest() throws Exception {
        result = null;
        assertFalse(fileDownloader.handleConnectorRequest(request, response, ""));
        assertFalse(fileDownloader.handleConnectorRequest(request, response, "dl/"));

        result = Pair.of(new ByteArrayInputStream(new byte[0]), "name");
        assertFalse(fileDownloader.handleConnectorRequest(request, response, ""));
        assertTrue(fileDownloader.handleConnectorRequest(request, response, "dl/"));
        verify(response).getOutputStream();
    }

}