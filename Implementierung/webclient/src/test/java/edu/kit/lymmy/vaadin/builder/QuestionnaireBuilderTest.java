package edu.kit.lymmy.vaadin.builder;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.kit.lymmy.shared.LogicalOperator;
import edu.kit.lymmy.shared.Questionnaire;
import edu.kit.lymmy.shared.event.CompositeEvent;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author Lukas
 * @since 16.03.2017
 */
public class QuestionnaireBuilderTest {
    private ObjectMapper mapper = new ObjectMapper();

    @Test
    public void name() throws Exception {
        QuestionnaireBuilder builder = new QuestionnaireBuilder();
        String name = "TestName";
        builder.setName(name);
        assertEquals(name, builder.getName());
        assertEquals(name, builder.build().getName());
    }

    @Test
    public void coolDownTime() throws Exception {
        QuestionnaireBuilder builder = new QuestionnaireBuilder();
        long time = 10L;
        builder.setCoolDownTime(time);
        assertEquals(time, builder.getCoolDownTime());
        assertEquals(time, builder.build().getCoolDownTime());
    }

    @Test
    public void event() throws Exception {
        QuestionnaireBuilder builder = new QuestionnaireBuilder();
        assertEquals(mapper.writeValueAsString(builder.getEvent().build()), mapper.writeValueAsString(builder.build().getEvent()));
    }

    @Test
    public void questions() throws Exception {
        QuestionnaireBuilder builder = new QuestionnaireBuilder();
        QuestionBuilder questionBuilder = builder.newQuestion();
        assertEquals(1, builder.getQuestions().size());
        assertEquals(questionBuilder, builder.getQuestions().get(0));
        assertTrue(EqualsBuilder.reflectionEquals(questionBuilder.build(), builder.build().getQuestions().get(0)));
        builder.removeQuestion(questionBuilder);
        assertEquals(0, builder.getQuestions().size());
    }

    @Test
    public void build() throws Exception {
        Questionnaire questionnaire = new Questionnaire(new CompositeEvent(Collections.emptyList(), LogicalOperator.AND), 0, Collections.emptyList(), Collections.emptyList(), "a");
        assertEquals(mapper.writeValueAsString(questionnaire), mapper.writeValueAsString(new QuestionnaireBuilder(questionnaire).build()));
    }

}