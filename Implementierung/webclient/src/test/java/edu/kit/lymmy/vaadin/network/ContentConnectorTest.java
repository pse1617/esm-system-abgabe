package edu.kit.lymmy.vaadin.network;

import edu.kit.lymmy.shared.Study;
import edu.kit.lymmy.shared.messages.CommunicationConstants;
import edu.kit.lymmy.vaadin.builder.StudyBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.HttpClientErrorException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Lukas
 * @since 16.03.2017
 */
@RunWith(MockitoJUnitRunner.class)
public class ContentConnectorTest {
    @Mock
    private JsonService jsonService;
    @Mock
    private JsonService.JsonRequest jsonRequest;
    @InjectMocks
    private ContentConnector contentConnector;

    @SuppressWarnings("MagicConstant")
    @Before
    public void setUp() throws Exception {
        when(jsonService.request(anyString())).thenReturn(jsonRequest);
        when(jsonRequest.queryParam(anyString(), any())).thenReturn(jsonRequest);
        when(jsonRequest.send(any())).thenCallRealMethod();
    }

    @Test
    public void loadStudies() throws Exception {
        when(jsonRequest.send(any(), any())).thenThrow(HttpClientErrorException.class).thenReturn(new Study[0]).thenReturn(new Study[]{new StudyBuilder("ID","User").build()});
        assertTrue(contentConnector.loadStudies().isEmpty());
        assertTrue(contentConnector.loadStudies().isEmpty());
        assertFalse(contentConnector.loadStudies().isEmpty());
        verify(jsonService, times(3)).request(CommunicationConstants.GET_STUDIES);
    }

    @Test
    public void saveStudy() throws Exception {
        Study study = new StudyBuilder("ID","User").build();
        when(jsonRequest.send(any(), any())).thenThrow(HttpClientErrorException.class).thenReturn(false, true);
        assertFalse(contentConnector.saveStudy(study));
        assertFalse(contentConnector.saveStudy(study));
        assertTrue(contentConnector.saveStudy(study));
        verify(jsonService, times(3)).request(CommunicationConstants.STORE_STUDY);
    }

    @Test
    public void getCsvForStudy() throws Exception {
        Study study = new StudyBuilder("ID","User").build();
        String csv = "fakecsvstring";
        when(jsonRequest.send(any(), any())).thenThrow(HttpClientErrorException.class).thenReturn(null, csv);
        assertNull(contentConnector.getCsvForStudy(study));
        assertNull(contentConnector.getCsvForStudy(study));
        assertEquals(csv, contentConnector.getCsvForStudy(study));
        verify(jsonService, times(3)).request(CommunicationConstants.EXPORT_STUDY);

    }

    @Test
    public void deleteStudy() throws Exception {
        Study study = new StudyBuilder("ID","User").build();
        when(jsonRequest.send(any(), any())).thenThrow(HttpClientErrorException.class).thenReturn(false, true);
        assertFalse(contentConnector.deleteStudy(study));
        assertFalse(contentConnector.deleteStudy(study));
        assertTrue(contentConnector.deleteStudy(study));
        verify(jsonService, times(3)).request(CommunicationConstants.DELETE_STUDY);
    }

    @Test
    public void getNewStudyId() throws Exception {
        String id = "ID";
        when(jsonRequest.send(any(), any())).thenThrow(HttpClientErrorException.class).thenReturn(null, id);
        assertNull(contentConnector.getNewStudyId());
        assertNull(contentConnector.getNewStudyId());
        assertEquals(id, contentConnector.getNewStudyId());
        verify(jsonService, times(3)).request(CommunicationConstants.CREATE_STUDY_ID);
    }

}