package edu.kit.lymmy.vaadin.builder;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.kit.lymmy.shared.Study;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

/**
 * @author Lukas
 * @since 16.03.2017
 */
public class StudyBuilderTest {
    private ObjectMapper mapper = new ObjectMapper();
    @Test
    public void id() throws Exception {
        StudyBuilder builder = new StudyBuilder("ID", "User");
        assertEquals("ID", builder.getId());
    }

    @Test
    public void name() throws Exception {
        StudyBuilder builder = new StudyBuilder("ID", "User");
        String name = "Test";
        builder.setName(name);
        assertEquals(name, builder.getName());
        assertEquals(name, builder.build().getName());
    }

    @Test
    public void description() throws Exception {
        StudyBuilder builder = new StudyBuilder("ID", "User");
        String description = "Test";
        builder.setDescription(description);
        assertEquals(description, builder.getDescription());
        assertEquals(description, builder.build().getDescription());
    }

    @Test
    public void startDate() throws Exception {
        StudyBuilder builder = new StudyBuilder("ID", "User");
        LocalDateTime date = LocalDateTime.now();
        builder.setStartDate(date);
        assertEquals(date, builder.getStartDate());
        assertEquals(date, builder.build().getStartDate());
    }

    @Test
    public void endDate() throws Exception {
        StudyBuilder builder = new StudyBuilder("ID", "User");
        LocalDateTime date = LocalDateTime.now();
        builder.setEndDate(date);
        assertEquals(date, builder.getEndDate());
        assertEquals(date, builder.build().getEndDate());
    }

    @Test
    public void questionaires() throws Exception {
        StudyBuilder builder = new StudyBuilder("ID", "User");
        QuestionnaireBuilder questionnaireBuilder = builder.newQuestionaire();
        assertEquals(1, builder.getQuestionaires().size());
        assertEquals(questionnaireBuilder, builder.getQuestionaires().get(0));
        assertEquals(mapper.writeValueAsString(questionnaireBuilder.build()), mapper.writeValueAsString(builder.build().getQuestionnaires().get(0)));
        builder.removeQuestionaire(questionnaireBuilder);
        assertEquals(0, builder.getQuestionaires().size());
    }

    @Test
    public void build() throws Exception {
        Study study = new Study("ID", "User", "Name", "Description", LocalDateTime.now(), LocalDateTime.now().plusHours(1), Collections.emptyList());
        assertEquals(mapper.writeValueAsString(study), mapper.writeValueAsString(new StudyBuilder(study).build()));
    }

}