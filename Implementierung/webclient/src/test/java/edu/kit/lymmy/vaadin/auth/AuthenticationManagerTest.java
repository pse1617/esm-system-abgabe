package edu.kit.lymmy.vaadin.auth;

import edu.kit.lymmy.shared.messages.CommunicationConstants;
import edu.kit.lymmy.shared.messages.CommunicationConstants.LoginResult;
import edu.kit.lymmy.vaadin.network.JsonService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.HttpClientErrorException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Lukas
 * @since 15.03.2017
 */
@SuppressWarnings("MagicConstant")
@RunWith(MockitoJUnitRunner.class)
public class AuthenticationManagerTest {

    @Mock
    private JsonService jsonServiceMock;
    @Mock
    private JsonService.JsonRequest jsonRequestMock;
    @InjectMocks
    private AuthenticationManager authenticationManager;

    @Before
    public void setUp() throws Exception {
        when(jsonServiceMock.request(anyString())).thenReturn(jsonRequestMock);
        when(jsonRequestMock.formEncoded()).thenReturn(jsonRequestMock);
        when(jsonRequestMock.queryParam(anyString(), any())).thenReturn(jsonRequestMock);
        when(jsonRequestMock.send(any())).thenCallRealMethod();
    }

    @Test
    public void getUsername() throws Exception {
        assertNull(authenticationManager.getUsername());
    }

    @Test
    public void login() throws Exception {
        String username = "TestUser";
        String password = "Password";
        when(jsonRequestMock.send(any(), any())).thenThrow(HttpClientErrorException.class).thenReturn(LoginResult.BAD_CREDENTIALS).thenReturn(LoginResult.OK);
        assertNull(authenticationManager.getUsername());
        assertEquals(LoginResult.UNKNOWN, authenticationManager.login(username, password));
        assertNull(authenticationManager.getUsername());
        assertEquals(LoginResult.BAD_CREDENTIALS, authenticationManager.login(username, password));
        assertNull(authenticationManager.getUsername());
        assertEquals(LoginResult.OK, authenticationManager.login(username, password));
        assertEquals(username, authenticationManager.getUsername());
        verify(jsonServiceMock, times(3)).request(CommunicationConstants.LEADER_LOGIN);
    }

    @Test
    public void register() throws Exception {
        String username = "TestUser";
        String password = "Password";
        when(jsonRequestMock.send(any(), any())).thenThrow(HttpClientErrorException.class).thenReturn(false).thenReturn(true);
        assertFalse(authenticationManager.register(password, username));
        assertFalse(authenticationManager.register(password, username));
        assertTrue(authenticationManager.register(password, username));
        verify(jsonServiceMock, times(3)).request(CommunicationConstants.LEADER_REGISTER_START);
    }

    @Test
    public void resendActivationCode() throws Exception {
        String username = "TestUser";
        when(jsonRequestMock.send(any(), any())).thenThrow(HttpClientErrorException.class).thenReturn(false).thenReturn(true);
        assertFalse(authenticationManager.resendActivationCode(username));
        assertFalse(authenticationManager.resendActivationCode(username));
        assertTrue(authenticationManager.resendActivationCode(username));
        verify(jsonServiceMock, times(3)).request(CommunicationConstants.LEADER_RESEND_CODE);
    }

    @Test
    public void activate() throws Exception {
        String username = "TestUser";
        String VerificationCode = "Code";
        when(jsonRequestMock.send(any(), any())).thenThrow(HttpClientErrorException.class).thenReturn(false).thenReturn(true);
        assertFalse(authenticationManager.activate(VerificationCode, username));
        assertFalse(authenticationManager.activate(VerificationCode, username));
        assertTrue(authenticationManager.activate(VerificationCode, username));
        verify(jsonServiceMock, times(3)).request(CommunicationConstants.LEADER_REGISTER_FINISH);
    }

    @Test
    public void logout() throws Exception {
        String username = "TestUser";
        ReflectionTestUtils.setField(authenticationManager, "username", username);
        when(jsonRequestMock.send(any(), any())).thenThrow(HttpClientErrorException.class).thenReturn(null);
        assertEquals(username, authenticationManager.getUsername());
        assertFalse(authenticationManager.logout());
        assertEquals(username, authenticationManager.getUsername());
        assertTrue(authenticationManager.logout());
        assertNull(authenticationManager.getUsername());
        verify(jsonServiceMock, times(2)).request(CommunicationConstants.LEADER_LOGOUT);
    }

    @Test
    public void onSessionExpired() throws Exception {
        String username = "TestUser";
        ReflectionTestUtils.setField(authenticationManager, "username", username);
        assertEquals(username, authenticationManager.getUsername());
        authenticationManager.onSessionExpired();
        assertNull(authenticationManager.getUsername());
    }

}