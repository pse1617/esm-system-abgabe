package edu.kit.lymmy.vaadin.network;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Lukas
 * @since 16.03.2017
 */
@SuppressWarnings("MagicConstant")
@RunWith(MockitoJUnitRunner.class)
public class JsonServiceTest {

    private ObjectMapper objectMapper = new ObjectMapper();
    @Mock
    private RestTemplate restTemplate;
    @Mock
    private JsonService.SessionExpiryListener listener;
    private JsonService jsonService;
    @Before
    public void setUp() throws Exception {
        jsonService = new JsonService(objectMapper, restTemplate);
    }

    @Test
    public void addSessionExpiryListener() throws Exception {
        when(restTemplate.postForEntity(any(), any(), any())).thenThrow(new HttpClientErrorException(HttpStatus.UNAUTHORIZED));
        jsonService.addSessionExpiryListener(listener);
        jsonService.request("").send(Object.class);
        verify(listener).onSessionExpired();


    }

    @Test
    public void removeSessionExpiryListener() throws Exception {
        when(restTemplate.postForEntity(any(), any(), any())).thenThrow(new HttpClientErrorException(HttpStatus.UNAUTHORIZED));
        jsonService.addSessionExpiryListener(listener);
        jsonService.removeSessionExpiryListener(listener);
        jsonService.request("").send(Object.class);
        verify(listener, never()).onSessionExpired();

    }

    @Test
    public void request() throws Exception {
        when(restTemplate.postForEntity(any(), any(), any())).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        jsonService.request("").send(Object.class);
        verify(restTemplate).postForEntity(any(), argThat(argument -> argument instanceof HttpEntity
                && ((HttpEntity) argument).getHeaders().getContentType().equals(MediaType.APPLICATION_JSON)), any());
        jsonService.request("").formEncoded().send(Object.class);
        verify(restTemplate).postForEntity(any(), argThat(argument -> argument instanceof HttpEntity
                && ((HttpEntity) argument).getHeaders().getContentType().equals(MediaType.APPLICATION_FORM_URLENCODED)), any());
        String param = "param";
        String value = "value";
        jsonService.request("").queryParam(param, value).send(Object.class);
        verify(restTemplate).postForEntity(argThat((URI argument) -> argument.toString().contains(param) && argument.toString().contains(value)), any(), any());
    }

}