package edu.kit.lymmy.vaadin.builder;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.kit.lymmy.shared.LogicalOperator;
import edu.kit.lymmy.shared.event.CompositeEvent;
import edu.kit.lymmy.shared.event.SensorEvent;
import edu.kit.lymmy.shared.sensor.SensorType;
import edu.kit.lymmy.shared.sensor.StudyStartData;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author Lukas
 * @since 15.03.2017
 */
public class EventBuilderTest {

    private ObjectMapper mapper = new ObjectMapper();

    @Test
    public void emptyEvent() throws Exception {
        EventBuilder builder = new EventBuilder();
        assertTrue(EqualsBuilder.reflectionEquals(new CompositeEvent(Collections.emptyList(), LogicalOperator.AND), builder.build()));
    }

    @Test
    public void complexEvent() throws Exception {
        EventBuilder builder = new EventBuilder();
        assertTrue(EqualsBuilder.reflectionEquals(new CompositeEvent(Collections.emptyList(), LogicalOperator.AND), builder.build()));
        builder.getRoot().getChildren().add(new EventBuilder.SensorNode(new StudyStartData()));
        assertEquals(builder.getRoot().getChildren().size(), builder.build().getEvents().size());
        assertTrue(builder.build().getEvents().get(0) instanceof SensorEvent);
        assertEquals(SensorType.STUDY_START, ((SensorEvent)builder.build().getEvents().get(0)).getData().getType());
        builder.getRoot().getChildren().add(new EventBuilder.LogicalNode());
        assertEquals(builder.getRoot().getChildren().size(), builder.build().getEvents().size());
        assertTrue(builder.build().getEvents().get(0) instanceof SensorEvent);
        assertTrue(builder.build().getEvents().get(1) instanceof CompositeEvent);
        builder.getRoot().setOperator(LogicalOperator.OR);
        assertEquals(LogicalOperator.OR, builder.build().getOperator());
        ObjectMapper mapper = new ObjectMapper();
        CompositeEvent event = new CompositeEvent(Arrays.asList(new SensorEvent(new StudyStartData()), new CompositeEvent(Collections.emptyList(), LogicalOperator.AND)), LogicalOperator.OR);
        assertEquals(mapper.writeValueAsString(event), mapper.writeValueAsString(builder.build()));
    }

    @Test
    public void passTroughEvent() throws Exception {
        CompositeEvent event = new CompositeEvent(Arrays.asList(new SensorEvent(new StudyStartData()), new CompositeEvent(Collections.emptyList(), LogicalOperator.AND)), LogicalOperator.OR);
        assertEquals(mapper.writeValueAsString(event), mapper.writeValueAsString(new EventBuilder(event).build()));
    }

}