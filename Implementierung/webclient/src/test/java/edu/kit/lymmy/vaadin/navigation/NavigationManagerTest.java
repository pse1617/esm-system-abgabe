package edu.kit.lymmy.vaadin.navigation;

import com.vaadin.server.Extension;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.Page;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.UI;
import edu.kit.lymmy.vaadin.auth.AuthenticationManager;
import edu.kit.lymmy.vaadin.builder.StudyBuilder;
import edu.kit.lymmy.vaadin.language.LanguageManager;
import edu.kit.lymmy.vaadin.network.ContentConnector;
import edu.kit.lymmy.vaadin.ui.EventEditorLayout;
import edu.kit.lymmy.vaadin.ui.FinishedStudiesLayout;
import edu.kit.lymmy.vaadin.ui.NewStudiesLayout;
import edu.kit.lymmy.vaadin.ui.QuestionEditorLayout;
import edu.kit.lymmy.vaadin.ui.QuestionnaireEditorLayout;
import edu.kit.lymmy.vaadin.ui.StudiesInProgressLayout;
import edu.kit.lymmy.vaadin.ui.StudyEditorLayout;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.Enumeration;
import java.util.ResourceBundle;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Lukas
 * @since 16.03.2017
 */
@RunWith(MockitoJUnitRunner.class)
public class NavigationManagerTest {
    @Mock
    private ContentConnector contentConnector;
    @Mock
    private ComponentContainer componentContainer;
    @Mock
    private UI ui;
    @Mock
    private Page page;
    @Mock
    private AuthenticationManager authenticationManager;
    @Mock
    private LanguageManager languageManager;
    @Mock
    private MockableAbstractComponent abstractComponent;
    @InjectMocks
    private NavigationManager navigationManager;

    @Before
    public void setUp() throws Exception {
        when(ui.getPage()).thenReturn(page);
        when(languageManager.getCaptions()).thenReturn(new ResourceBundle() {
            @Override
            protected Object handleGetObject(@NotNull String key) {
                return "FAKE";
            }

            @NotNull
            @Override
            public Enumeration<String> getKeys() {
                return Collections.emptyEnumeration();
            }
        });
        navigationManager.init(ui, componentContainer);
    }

    @Test
    public void init() throws Exception {
        verify(contentConnector).loadStudies();
    }

    @Test
    public void getNewStudy() throws Exception {
        when(contentConnector.getNewStudyId()).thenReturn(null, "ID", null, "ID");
        when(authenticationManager.getUsername()).thenReturn(null, null, "User", "User");
        assertNull(navigationManager.getNewStudy());
        assertNull(navigationManager.getNewStudy());
        assertNull(navigationManager.getNewStudy());
        assertNotNull(navigationManager.getNewStudy());
    }

    @Test
    public void editStudy() throws Exception {
        navigationManager.editStudy(new StudyBuilder("ID", "User"));
        verify(componentContainer).addComponent(any(StudyEditorLayout.class));
    }

    @Test
    public void editQuestionaire() throws Exception {
        navigationManager.editQuestionaire(new StudyBuilder("ID", "User").newQuestionaire());
        verify(componentContainer).addComponent(any(QuestionnaireEditorLayout.class));
    }

    @Test
    public void editEvent() throws Exception {
        navigationManager.editEvent(new StudyBuilder("ID", "User").newQuestionaire().getEvent());
        verify(componentContainer).addComponent(any(EventEditorLayout.class));
    }

    @Test
    public void editQuestion() throws Exception {
        navigationManager.editQuestion(new StudyBuilder("ID", "User").newQuestionaire().newQuestion());
        verify(componentContainer).addComponent(any(QuestionEditorLayout.class));
    }

    @Test
    public void navigateUp() throws Exception {
        navigationManager.editStudy(new StudyBuilder("ID", "User"));
        navigationManager.navigateUp();
        verify(componentContainer).addComponent(any(NewStudiesLayout.class));
    }

    @Test
    public void saveStudy() throws Exception {
        navigationManager.editStudy(new StudyBuilder("ID", "User"));
        navigationManager.saveStudy();
        verify(contentConnector).saveStudy(any());
    }

    @Test
    public void deleteStudy() throws Exception {
        navigationManager.deleteStudy(new StudyBuilder("ID", "User").build());
        verify(contentConnector).deleteStudy(any());
    }

    @Test
    public void showNewStudies() throws Exception {
        navigationManager.showNewStudies();
        verify(componentContainer).addComponent(any(NewStudiesLayout.class));
    }

    @Test
    public void showRunningStudies() throws Exception {
        navigationManager.showRunningStudies();
        verify(componentContainer).addComponent(any(StudiesInProgressLayout.class));
    }

    @Test
    public void showFinishedStudies() throws Exception {
        navigationManager.showFinishedStudies();
        verify(componentContainer).addComponent(any(FinishedStudiesLayout.class));
    }

    @Test
    public void prepareComponentForExport() throws Exception {
        navigationManager.prepareComponentForExport(abstractComponent, () -> new StudyBuilder("ID", "User").build());
        verify(abstractComponent).addExtension(any(FileDownloader.class));
    }

    private abstract class MockableAbstractComponent extends AbstractComponent{
        @Override
        public void addExtension(Extension extension) {
            super.addExtension(extension);
        }
    }

}