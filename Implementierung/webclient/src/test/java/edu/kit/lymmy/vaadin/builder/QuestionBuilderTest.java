package edu.kit.lymmy.vaadin.builder;

import edu.kit.lymmy.shared.question.ChoiceQuestion;
import edu.kit.lymmy.shared.question.Condition;
import edu.kit.lymmy.shared.question.LikertQuestion;
import edu.kit.lymmy.shared.question.MultipleChoiceQuestion;
import edu.kit.lymmy.shared.question.Question;
import edu.kit.lymmy.shared.question.QuestionType;
import edu.kit.lymmy.shared.question.StepQuestion;
import edu.kit.lymmy.shared.question.ValidCondition;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.junit.Test;
import org.springframework.data.util.Pair;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author Lukas
 * @since 16.03.2017
 */
public class QuestionBuilderTest {
    @Test
    public void type() throws Exception {
        QuestionBuilder builder = new QuestionBuilder();
        builder.setType(QuestionType.LIKERT);
        assertEquals(QuestionType.LIKERT, builder.getType());
        assertEquals(QuestionType.LIKERT, builder.build().getType());
    }

    @Test
    public void condition() throws Exception {
        QuestionBuilder builder = new QuestionBuilder();
        Condition condition = new Condition(2, 4);
        builder.setCondition(condition);
        assertEquals(condition, builder.getCondition());
        assertEquals(condition, builder.build().getCondition());
    }

    @Test
    public void text() throws Exception {
        QuestionBuilder builder = new QuestionBuilder();
        String text = "TestText";
        builder.setText(text);
        assertEquals(text, builder.getText());
        assertEquals(text, builder.build().getText());
    }

    @Test
    public void answers() throws Exception {
        QuestionBuilder builder = new QuestionBuilder();
        builder.setType(QuestionType.MULTIPLE_CHOICE);
        List<String> answers = Arrays.asList("a","b","c");
        builder.setAnswers(answers);
        assertEquals(answers, builder.getAnswers());
        assertEquals(answers, ((ChoiceQuestion)builder.build()).getAnswers());
    }

    @Test
    public void steps() throws Exception {
        QuestionBuilder builder = new QuestionBuilder();
        builder.setType(QuestionType.LIKERT);
        builder.setSteps(6);
        assertEquals(6, builder.getSteps());
        assertEquals(6, ((LikertQuestion)builder.build()).getSteps());
    }

    @Test
    public void sliderTexts() throws Exception {
        QuestionBuilder builder = new QuestionBuilder();
        builder.setType(QuestionType.SLIDER);
        Pair<String, String> texts = Pair.of("left", "right");
        builder.setSliderTexts(texts.getFirst(), texts.getSecond());
        assertEquals(texts, builder.getSliderTexts());
        assertEquals(texts.getFirst(), ((StepQuestion)builder.build()).getLeftSliderText());
        assertEquals(texts.getSecond(), ((StepQuestion)builder.build()).getRightSliderText());
    }

    @Test
    public void build() throws Exception {
        Question question = new MultipleChoiceQuestion(new ValidCondition(), "TestText", Arrays.asList("a","b","c"));
        assertTrue(EqualsBuilder.reflectionEquals(question, new QuestionBuilder(question).build()));
    }

}