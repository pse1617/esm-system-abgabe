package edu.kit.lymmy.shared.question;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Created by Marc on 04.02.2017.
 */
public class YesNoQuestion extends ChoiceQuestion {
    public YesNoQuestion(@NotNull Condition condition, @NotNull String text, @NotNull List<String> answers) {
        super(QuestionType.YES_NO, condition, text, answers);
    }
}
