package edu.kit.lymmy.shared;

import edu.kit.lymmy.shared.event.Event;
import edu.kit.lymmy.shared.question.Question;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Lukas
 * @since 21.12.2016
 */
public class Questionnaire {
    @NotNull
    private final Event event;
    private final long coolDownTime;
    @NotNull
    private final List<? extends Question> questions;
    @NotNull
    private final String name;
    /**
     * TODO: Which classes/parameters does the app need to know which sensor should be added to contextData?
     */
    @NotNull
    private final List<String> contextSensors;

    public Questionnaire(@NotNull Event event, long coolDownTime, @NotNull List<? extends Question> questions, @NotNull List<String> contextSensors, @NotNull String name) {
        if (coolDownTime < 0)
            throw new IllegalArgumentException("name: " + name + ", event: " + event + ", coolDownTime: " + coolDownTime +
                    ", questions: " + questions + ", contextSensors: " + contextSensors);
        this.event = event;
        this.coolDownTime = coolDownTime;
        this.questions = new ArrayList<>(questions);
        this.contextSensors = contextSensors;
        this.name = name;
    }

    @NotNull
    public Event getEvent() {
        return event;
    }

    public long getCoolDownTime() {
        return coolDownTime;
    }

    @NotNull
    public List<? extends Question> getQuestions() {
        return new ArrayList<>(questions);
    }

    @NotNull
    public List<String> getContextSensors() {
        return this.contextSensors;
    }

    @NotNull
    public String getName() {
        return name;
    }
}
