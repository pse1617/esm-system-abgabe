package edu.kit.lymmy.shared.sensor;

import org.jetbrains.annotations.NotNull;

/**
 * @author Lukas
 * @since 09.01.2017
 */
public class WeatherData extends SensorData {
    public enum WeatherType {
        Thunderstorm,
        Drizzle,
        Rain,
        Snow,
        Atmosphere,
        Clear,
        Clouds,
        Extreme,
        Additional
    }

    @NotNull
    private final WeatherData.WeatherType weather;
    private final boolean not;

    public WeatherData(@NotNull WeatherType weather,@NotNull boolean not) {
        super(SensorType.WEATHER);
        this.weather = weather;
        this.not = not;
    }

    @NotNull
    public WeatherType getWeather() {
        return weather;
    }

    public boolean isNot() {
        return not;
    }
}
