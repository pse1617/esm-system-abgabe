package edu.kit.lymmy.shared.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.security.cert.X509Certificate;

/**
 * Data Transport class between App and Backend.
 */
public class ProbandDetails {

    @NotNull
    private final Proband proband;
    @Nullable
    private final X509Certificate certificate;

    public ProbandDetails(@NotNull Proband proband, @Nullable X509Certificate certificate) {
        this.proband = proband;
        this.certificate = certificate;
    }

    @NotNull
    public Proband getProband() {
        return proband;
    }

    @Nullable
    public X509Certificate getCertificate() {
        return certificate;
    }
}
