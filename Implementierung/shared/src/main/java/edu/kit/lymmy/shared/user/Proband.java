package edu.kit.lymmy.shared.user;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Created by mo on 19.01.17.
 */
public class Proband {

    @NotNull
    private final String id;
    @NotNull
    private final String studyID;
    @NotNull
    private final String certificateSerialNumber;

    public Proband(@NotNull String id, @NotNull String studyID, @NotNull String certificateSerialNumber) {
        if(id.isEmpty())throw new IllegalArgumentException("id must not be blank");
        if(studyID.isEmpty()) throw new IllegalArgumentException("studyID must not be blank");
        if(certificateSerialNumber.isEmpty())throw new IllegalArgumentException("certificateSerialNumber must not be blank");

        this.id = id;
        this.studyID = studyID;
        this.certificateSerialNumber = certificateSerialNumber;
    }

    @NotNull
    public String getId() {
        return id;
    }

    @NotNull
    public String getStudyID() {
        return studyID;
    }

    @NotNull
    public String getCertificateSerialNumber() {
        return certificateSerialNumber;
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (o == null || !(o instanceof Proband)) {
            return false;
        } else {
            Proband object = (Proband) o;
            return this.id.equals(object.id);
        }
    }
}
