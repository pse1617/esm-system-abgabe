package edu.kit.lymmy.shared.question;

import org.jetbrains.annotations.NotNull;

/**
 * @author Lukas
 * @since 13.02.2017
 */
public class SliderQuestion extends StepQuestion {
    public SliderQuestion(@NotNull Condition condition, @NotNull String text, @NotNull String leftSliderText, @NotNull String rightSliderText) {
        super(QuestionType.SLIDER, condition, text, leftSliderText, rightSliderText);
    }
}
