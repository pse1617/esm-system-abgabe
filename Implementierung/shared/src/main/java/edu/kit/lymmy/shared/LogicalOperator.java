package edu.kit.lymmy.shared;

/**
 * @author Lukas
 * @since 26.12.2016
 */
public enum LogicalOperator {
    AND,
    OR
}
