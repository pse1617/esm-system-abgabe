package edu.kit.lymmy.shared.event;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.jetbrains.annotations.NotNull;

/**
 * @author Lukas
 * @since 21.12.2016
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public abstract class Event {
    @NotNull
    private final EventType type;

    Event(@NotNull EventType type) {
        this.type = type;
    }

    @NotNull
    public EventType getType() {
        return type;
    }
}
