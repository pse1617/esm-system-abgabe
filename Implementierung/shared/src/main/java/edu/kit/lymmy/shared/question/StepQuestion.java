package edu.kit.lymmy.shared.question;

import org.jetbrains.annotations.NotNull;

/**
 * @author Lukas
 * @since 13.02.2017
 */
public class StepQuestion extends Question {
    @NotNull
    private final String leftSliderText;
    @NotNull
    private final String rightSliderText;

    StepQuestion(@NotNull QuestionType type, @NotNull Condition condition, @NotNull String text, @NotNull String leftSliderText, @NotNull String rightSliderText) {
        super(type, condition, text);
        this.leftSliderText = leftSliderText;
        this.rightSliderText = rightSliderText;
    }

    @NotNull
    public String getLeftSliderText() {
        return leftSliderText;
    }

    @NotNull
    public String getRightSliderText() {
        return rightSliderText;
    }
}
