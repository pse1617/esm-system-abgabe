package edu.kit.lymmy.shared.event;

import edu.kit.lymmy.shared.LogicalOperator;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * @author Lukas
 * @since 21.12.2016
 */
public class CompositeEvent extends Event{
    @NotNull
    private final List<Event> events;
    @NotNull
    private final LogicalOperator operator;

    public CompositeEvent(@NotNull List<Event> events, @NotNull LogicalOperator operator) {
        super(EventType.COMPOSITE);
        this.events = events;
        this.operator = operator;
    }

    @NotNull
    public LogicalOperator getOperator() {
        return operator;
    }

    @NotNull
    public List<Event> getEvents() {
        return events;
    }
}
