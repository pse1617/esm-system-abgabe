package edu.kit.lymmy.shared.question;

import org.jetbrains.annotations.NotNull;

/**
 * @author Lukas
 * @since 21.12.2016
 */
public class OpenQuestion extends Question {
    public OpenQuestion(@NotNull Condition condition, @NotNull String text) {
        super(QuestionType.OPEN, condition, text);
    }
}
