package edu.kit.lymmy.shared.answer;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.validation.annotation.Validated;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * Created by mo on 19.01.17.
 */
@Validated
public class QuestionnaireAnswer {

    /**
     * Id is generated via SHA-256 to guarantee uniqueness. If the App sends a Answer multiple times it's
     * possible to know that by this id.
     */
    @NotNull
    private String id;
    @NotNull
    private final String probandID;
    @NotNull
    private final LocalDateTime timestamp;
    private final int questionnaireIndex;
    @NotNull
    private final List<String> contextData;
    @NotNull
    private final List<Answer> answers;

    public QuestionnaireAnswer(@NotNull String probandID, @NotNull LocalDateTime timestamp, int questionnaireIndex, @NotNull List<String> contextData, @NotNull List<Answer> answers) {
        if(probandID.isEmpty()) throw new IllegalArgumentException("probandID must not be blank");
        if(questionnaireIndex < 0) throw new IllegalArgumentException("questionnaireIndex must not be below 0");
        if(answers.isEmpty())throw new IllegalArgumentException("answers must not be empty");

        this.probandID = probandID;
        this.timestamp = timestamp;
        this.questionnaireIndex = questionnaireIndex;
        this.contextData = contextData;
        this.answers = answers;
        this.id = probandID + timestamp.toString();

    }

    @NotNull
    public String getId() {
        return this.id;
    }

    @NotNull
    public String getProbandID() {
        return probandID;
    }

    @NotNull
    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public int getQuestionnaireIndex() {
        return questionnaireIndex;
    }

    @NotNull
    public List<String> getContextData() {
        return contextData;
    }

    @NotNull
    public List<Answer> getAnswers() {
        return answers;
    }

    @Override
    public boolean equals(@Nullable Object o) {
        return !(o == null || !(o instanceof QuestionnaireAnswer)) && this.id.equals(((QuestionnaireAnswer) o).id);
    }
}
