package edu.kit.lymmy.shared.sensor;

/**
 * Created by marcs on 03.03.2017.
 */
public class AccelerationData extends SensorData {

    private final double acceleration;
    private final boolean bigger;

    public AccelerationData(double acceleration, boolean bigger) {
        super(SensorType.ACCELERATION);
        this.acceleration = acceleration;
        this.bigger = bigger;
    }

    public double getAcceleration() {
        return acceleration;
    }

    public boolean isBigger() {
        return bigger;
    }
}
