package edu.kit.lymmy.shared.sensor;

/**
 * @author Lukas
 * @since 09.01.2017
 */
public class StudyStartData extends SensorData{
    public StudyStartData() {
        super(SensorType.STUDY_START);
    }
}
