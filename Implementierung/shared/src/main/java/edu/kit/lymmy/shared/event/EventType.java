package edu.kit.lymmy.shared.event;

/**
 * @author Lukas
 * @since 11.01.2017
 */
public enum EventType {
    COMPOSITE,
    SENSOR
}
