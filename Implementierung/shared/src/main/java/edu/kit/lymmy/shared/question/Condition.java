package edu.kit.lymmy.shared.question;

/**
 * Created by mo on 19.01.17.
 */
public class Condition {

    private final int question;
    private final int choice;

    public Condition(int question, int choice) {
        this.question = question;
        this.choice = choice;
    }

    public int getQuestion() {
        return question;
    }

    public int getChoice() {
        return choice;
    }

    public boolean isValid(int choice) {
        return this.choice == choice;
    }
}
