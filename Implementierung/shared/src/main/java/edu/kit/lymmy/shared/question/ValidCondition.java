package edu.kit.lymmy.shared.question;

/**
 * Created by mo on 19.01.17.
 */
public class ValidCondition extends Condition {
    public ValidCondition(int questionIndex, int choice) {
        super(questionIndex, choice);
    }

    public ValidCondition() {
        this(-1, -1);
    }

    @Override
    public boolean isValid(int choice) {
        return true;
    }
}
