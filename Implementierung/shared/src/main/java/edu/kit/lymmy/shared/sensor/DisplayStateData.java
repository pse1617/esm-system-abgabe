package edu.kit.lymmy.shared.sensor;

import com.fasterxml.jackson.annotation.JsonCreator;
import org.jetbrains.annotations.NotNull;

/**
 * @author Lukas
 * @since 09.01.2017
 */
public class DisplayStateData extends SensorData{
    public enum DisplayState{
        ON,
        OFF,
        UNLOCKED
    }
    @NotNull
    private final DisplayState state;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public DisplayStateData(@NotNull DisplayState state) {
        super(SensorType.DISPLAY_STATE);
        this.state = state;
    }

    @NotNull
    public DisplayState getState() {
        return state;
    }
}
