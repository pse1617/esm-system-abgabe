package edu.kit.lymmy.shared;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Updated as http://www.oracle.com/technetwork/articles/java/jf14-date-time-2125367.html explains a new date api,
 * which is thread-safe and easier to handle.
 *
 * @author Lukas
 * @since 21.12.2016
 */
public final class Study {
    public static final long STUDY_CHANGE_DAY_BUFFER = 1;

    @NotNull
    private final String id;
    @NotNull
    private final String userID;
    @NotNull
    private final String name;
    @NotNull
    private final String description;
    @Nullable
    private final LocalDateTime startDate;
    @Nullable
    private final LocalDateTime endDate;
    @NotNull
    private final List<Questionnaire> questionnaires;
    private long registeredProbands;

    public Study(@NotNull String id, @NotNull String userID, @NotNull String name, @NotNull String description,
                 @Nullable LocalDateTime startDate, @Nullable LocalDateTime endDate, @NotNull List<Questionnaire> questionnaires) {
        this.id = id;
        this.userID = userID;
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.questionnaires = new ArrayList<>(questionnaires);
        this.registeredProbands = 0;
    }

    @NotNull
    public String getId() {
        return id;
    }

    @NotNull
    public String getUserID() {
        return userID;
    }

    @NotNull
    public String getName() {
        return name;
    }

    @NotNull
    public String getDescription() {
        return description;
    }

    @Nullable
    public LocalDateTime getStartDate() {
        return startDate;
    }

    @Nullable
    public LocalDateTime getEndDate() {
        return endDate;
    }

    @NotNull
    public List<Questionnaire> getQuestionnaires() {
        return new ArrayList<>(questionnaires);
    }

    public long getRegisteredProbands() {
        return registeredProbands;
    }

    public void setRegisteredProbands(int registeredProbands) {
        this.registeredProbands = registeredProbands;
    }

    @JsonIgnore
    public boolean isFinished() {
        return endDate != null && LocalDate.now().isAfter(endDate.toLocalDate());
    }

    @Override
    public boolean equals(@Nullable Object o) {

        return o != null && Study.class.isInstance(o) && this.id.equals(((Study)o).id);
    }
}
