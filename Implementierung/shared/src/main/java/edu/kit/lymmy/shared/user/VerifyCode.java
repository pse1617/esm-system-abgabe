package edu.kit.lymmy.shared.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.LocalDateTime;
import java.util.Arrays;

/**
 * Created by mo on 22.01.17.
 */
public class VerifyCode {

    private static final long VERIFY_CODE_DURATION_DAYS = 1;

    @NotNull
    private final String id;
    @NotNull
    private final LocalDateTime timestamp;
    @NotNull
    private final byte[] code;

    /**
     * MongoDB-required default constructor
     */
    @SuppressWarnings("unused")
    private VerifyCode(){
        this("",new byte[0]);
    }

    public VerifyCode(@NotNull String id, @NotNull byte[] code) {
        this(id, LocalDateTime.now(), code);
    }

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public VerifyCode(@NotNull String id, @NotNull LocalDateTime timestamp, @NotNull byte[] code) {
        this.id = id;
        this.code = code;
        this.timestamp = timestamp;
    }

    @NotNull
    public String getId() {
        return id;
    }

    @NotNull
    public byte[] getCode() {
        return code;
    }

    public boolean isExpired() {
        return LocalDateTime.now().isAfter(
                this.timestamp.plusDays(VERIFY_CODE_DURATION_DAYS));
    }

    @NotNull
    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    @Override
    public boolean equals(@Nullable Object o) {
        return o!=null && this.getClass().isInstance(o) &&
                this.id.equals(((VerifyCode)o).id) && Arrays.equals(code, ((VerifyCode) o).code);
    }
}
