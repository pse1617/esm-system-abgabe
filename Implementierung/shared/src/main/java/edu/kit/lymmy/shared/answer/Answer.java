package edu.kit.lymmy.shared.answer;

import org.jetbrains.annotations.NotNull;

/**
 * Created by mo on 19.01.17.
 */
public class Answer<T> {

    private final int questionIndex;
    @NotNull
    private final T choice;

    public Answer(int questionIndex, @NotNull T choice) {
        if (questionIndex < 0)
            throw new IllegalArgumentException("questionIndex: " + questionIndex + ", choice: " + choice);
        this.questionIndex = questionIndex;
        this.choice = choice;
    }

    public int getQuestionIndex() {
        return this.questionIndex;
    }

    @NotNull
    public T getChoice() {
        return choice;
    }
}
