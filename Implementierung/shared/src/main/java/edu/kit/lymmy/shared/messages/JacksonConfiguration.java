package edu.kit.lymmy.shared.messages;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Lukas
 * @since 04.02.2017
 */
@Configuration
public class JacksonConfiguration {

    @NotNull
    @Bean
    public Module parameterNamesModule(){
        return new ParameterNamesModule(JsonCreator.Mode.PROPERTIES);
    }
}
