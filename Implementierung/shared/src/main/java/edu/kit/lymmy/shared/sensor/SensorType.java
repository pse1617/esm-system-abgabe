package edu.kit.lymmy.shared.sensor;

/**
 * @author Lukas
 * @since 11.01.2017
 */
public enum SensorType {
    STUDY_START,
    TIME,
    COARSE_LOCATION,
    DISPLAY_STATE,
    WEATHER,
    ACCELERATION,
    PARTTIME,
    LOCATIONTYPE
}
