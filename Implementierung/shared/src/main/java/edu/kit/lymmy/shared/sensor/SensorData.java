package edu.kit.lymmy.shared.sensor;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.jetbrains.annotations.NotNull;

/**
 * @author Lukas
 * @since 04.01.2017
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public abstract class SensorData {
    @NotNull
    private final SensorType type;

    SensorData(@NotNull SensorType type){
        this.type = type;
    }

    @NotNull
    public SensorType getType() {
        return type;
    }
}
