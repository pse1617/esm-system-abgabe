package edu.kit.lymmy.shared.question;

/**
 * @author Lukas
 * @since 11.01.2017
 */
public enum QuestionType {
    OPEN,
    SINGLE_CHOICE,
    MULTIPLE_CHOICE,
    SLIDER,
    LIKERT,
    YES_NO
}
