package edu.kit.lymmy.shared.sensor;

import org.jetbrains.annotations.NotNull;

/**
 * Created by marc on 04.03.17.
 */
public class PartTimeData extends SensorData {

    public enum Timetype {
        Morning,
        Afternoon,
        Evening,
        Night
    }

    private final boolean not;
    @NotNull
    private final Timetype timeType;

    public PartTimeData(@NotNull Timetype timeType, boolean not) {
        super(SensorType.PARTTIME);
        this.timeType = timeType;
        this.not = not;
    }

    public boolean getNot() {
        return not;
    }

    @NotNull
    public Timetype getTimeType() {
        return timeType;
    }

}