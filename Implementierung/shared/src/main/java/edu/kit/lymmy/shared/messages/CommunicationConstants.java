package edu.kit.lymmy.shared.messages;

/**
 * Created by mo on 26.01.17.
 */
public final class CommunicationConstants {

    public static final String LEADER_REGISTER_START = "/leader/auth/register";
    public static final String LEADER_RESEND_CODE = "/leader/auth/resend";
    public static final String LEADER_REGISTER_FINISH = "/leader/auth/registerLogin";
    public static final String LEADER_DELETE = "/leader/auth/delete";
    public static final String LEADER_LOGIN = "/login";
    public static final String LEADER_LOGOUT = "/logout";

    public static final String GET_STUDIES = "/leader/studies";
    public static final String CREATE_STUDY_ID = "/leader/studies/createId";
    public static final String STORE_STUDY = "/leader/studies/store";
    public static final String DELETE_STUDY = "/leader/studies/delete";
    public static final String EXPORT_STUDY = "/leader/studies/export";

    public static final String PROBAND_REGISTER = "/proband/auth";
    public static final String PROBAND_GET_STARTDATE = "/proband/study/startdate";
    public static final String PROBAND_GET_STUDY = "/proband/study/fetch";
    public static final String PROBAND_STORE_ANSWER = "/proband/study/answers";

    public static final String PARAM_STUDY_ID = "studyID";
    public static final String PARAM_PASSWORD = "password";
    public static final String PARAM_USERNAME = "username";
    public static final String PARAM_MAIL = PARAM_USERNAME;
    public static final String PARAM_CLIENT_PUBKEY = "clientPubKey";
    public static final String PARAM_VERIFY_CODE = "verifyCode";
    public static final String PARAM_CODE = PARAM_VERIFY_CODE;

    public static final String CONFIRM_REGISTRATION = "/register";

    public static final String HOST = "https://psews16esm2.teco.edu:80";
    public static final String WEBCLIENT_HOST = "https://psews16esm2.teco.edu:443";
    public static final String EMAIL_REGISTER_VERIFY = WEBCLIENT_HOST + CONFIRM_REGISTRATION + "?" + PARAM_USERNAME + "={0}&" + PARAM_VERIFY_CODE + "={1}";

    public static final String HEADER_PROBAND_ID = "probandId";

    private CommunicationConstants() {
        //To guarantee this class won't be initialized
    }

    public enum LoginResult{
        OK,
        BAD_CREDENTIALS,
        NOT_ACTIVE,
        NOT_FOUND,
        UNKNOWN
    }
}
