package edu.kit.lymmy.shared.question;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Lukas
 * @since 21.12.2016
 */
public abstract class ChoiceQuestion extends Question {
    @NotNull
    private final List<String> answers;

    ChoiceQuestion(@NotNull QuestionType type, @NotNull Condition condition, @NotNull String text, @NotNull List<String> answers) {
        super(type, condition, text);
        this.answers = new ArrayList<>(answers);
    }

    @NotNull
    public List<String> getAnswers() {
        return new ArrayList<>(answers);
    }
}
