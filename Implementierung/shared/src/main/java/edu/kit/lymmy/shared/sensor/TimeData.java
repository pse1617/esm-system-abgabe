package edu.kit.lymmy.shared.sensor;

import org.jetbrains.annotations.NotNull;

import java.time.LocalTime;

/**
 * @author Lukas
 * @since 09.01.2017
 */
public class TimeData extends SensorData{
    @NotNull
    private final LocalTime date;
    private final long periodLength;

    public TimeData(@NotNull LocalTime date, long periodLength) {
        super(SensorType.TIME);
        this.date = date;
        this.periodLength = periodLength;
    }

    @NotNull
    public LocalTime getDate() {
        return date;
    }

    public long getPeriodLength() {
        return periodLength;
    }
}
