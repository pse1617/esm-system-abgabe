package edu.kit.lymmy.shared.question;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.jetbrains.annotations.NotNull;

/**
 * @author Lukas
 * @since 21.12.2016
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public abstract class Question {
    @NotNull
    private final QuestionType type;
    @NotNull
    private final Condition condition;
    @NotNull
    private final String text;

    Question(@NotNull QuestionType type, @NotNull Condition condition, @NotNull String text) {
        this.type = type;
        this.condition = condition;
        this.text = text;
    }

    @NotNull
    public String getText() {
        return text;
    }

    @NotNull
    public Condition getCondition() {
        return condition;
    }

    @NotNull
    public QuestionType getType() {
        return type;
    }
}
