package edu.kit.lymmy.shared.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import edu.kit.lymmy.shared.sensor.SensorData;
import org.jetbrains.annotations.NotNull;

/**
 * @author Lukas
 * @since 04.01.2017
 */
public class SensorEvent extends Event {
    @NotNull
    private final SensorData data;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public SensorEvent(@NotNull SensorData data) {
        super(EventType.SENSOR);
        this.data = data;
    }

    @NotNull
    public SensorData getData() {
        return data;
    }
}
