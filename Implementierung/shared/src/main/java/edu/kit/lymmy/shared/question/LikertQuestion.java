package edu.kit.lymmy.shared.question;

import org.jetbrains.annotations.NotNull;

/**
 * @author Lukas
 * @since 04.01.2017
 */
public class LikertQuestion extends StepQuestion {
    private final int steps;

    public LikertQuestion(@NotNull Condition condition, @NotNull String text, @NotNull String leftSliderText, @NotNull String rightSliderText, int steps) {
        super(QuestionType.LIKERT, condition, text, leftSliderText, rightSliderText);
        this.steps = steps;
    }

    public int getSteps() {
        return steps;
    }
}
