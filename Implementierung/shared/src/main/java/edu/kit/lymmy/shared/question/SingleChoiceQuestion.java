package edu.kit.lymmy.shared.question;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * @author Lukas
 * @since 21.12.2016
 */
public class SingleChoiceQuestion extends ChoiceQuestion {
    public SingleChoiceQuestion(@NotNull Condition condition, @NotNull String text, @NotNull List<String> answers) {
        super(QuestionType.SINGLE_CHOICE, condition, text, answers);
    }
}
