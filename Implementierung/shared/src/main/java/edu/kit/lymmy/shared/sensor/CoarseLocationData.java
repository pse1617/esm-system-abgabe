package edu.kit.lymmy.shared.sensor;

import com.fasterxml.jackson.annotation.JsonCreator;
import org.jetbrains.annotations.NotNull;

/**
 * @author Lukas
 * @since 09.01.2017
 *
 * @author Youheng
 * @since 04.03.2017
 */
public class CoarseLocationData extends SensorData {
    @NotNull
	private final String address;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public CoarseLocationData(@NotNull String address) {
		super(SensorType.COARSE_LOCATION);
		this.address = address;
    }

    @NotNull
    public String getAddress() {
        return this.address;
    }
}
