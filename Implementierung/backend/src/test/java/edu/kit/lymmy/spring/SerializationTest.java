package edu.kit.lymmy.spring;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.json.UTF8JsonGenerator;
import com.fasterxml.jackson.core.json.WriterBasedJsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import edu.kit.lymmy.shared.LogicalOperator;
import edu.kit.lymmy.shared.Questionnaire;
import edu.kit.lymmy.shared.Study;
import edu.kit.lymmy.shared.event.CompositeEvent;
import edu.kit.lymmy.shared.event.Event;
import edu.kit.lymmy.shared.event.SensorEvent;
import edu.kit.lymmy.shared.question.Condition;
import edu.kit.lymmy.shared.question.MultipleChoiceQuestion;
import edu.kit.lymmy.shared.question.Question;
import edu.kit.lymmy.shared.sensor.StudyStartData;
import edu.kit.lymmy.shared.user.ProbandDetails;
import edu.kit.lymmy.spring.auth.CodeGenerator;
import edu.kit.lymmy.spring.auth.ProbandManager;
import edu.kit.lymmy.spring.auth.encryption.BcryptEncryptor;
import edu.kit.lymmy.spring.auth.encryption.PasswordEncryptor;
import edu.kit.lymmy.spring.auth.user.User;
import edu.kit.lymmy.spring.database.DatabaseManager;
import edu.kit.lymmy.spring.messages.services.ProbandService;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.jackson.JsonComponentModule;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.security.KeyPair;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

/**
 * @author Lukas
 * @since 04.02.2017
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource(locations = "classpath:test.properties")
@ContextConfiguration(classes = SpringBackendApplication.class)
@EnableAutoConfiguration
public class SerializationTest {

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private ProbandManager probandManager;
    @Autowired
    private DatabaseManager manager;
    @Autowired
    private CodeGenerator generator;
    @Autowired
    private ProbandService service;

    private static final PasswordEncryptor encryptor = new BcryptEncryptor();
    private static final String userPassword = "password";
    private static final String encryptedPassword = encryptor.encrypt(userPassword);

    @Test
    public void testStudySerialization() throws Exception {
        Question question = new MultipleChoiceQuestion(new Condition(-1, -1), "TestQuestion", Arrays.asList("Yes", "No"));
        Event event = new CompositeEvent(new ArrayList<>(), LogicalOperator.AND);
        Questionnaire questionnaire = new Questionnaire(event, 0, Collections.singletonList(question), new ArrayList<>(), "TestQuestionnaire");
        Study in = new Study("TestID", "TestUser", "TestStudy", "TestDescription", LocalDateTime.now(), LocalDateTime.MAX, Collections.singletonList(questionnaire));
        String s = objectMapper.writeValueAsString(in);
        Study out = objectMapper.readValue(s, Study.class);
        assertReflectionEquals(in, out);
    }

   // @Test
    public void testCertificateSerialization() throws IOException {
        User user = new User("ex@mp.le", encryptedPassword, encryptor.getParameters());
        this.manager.storeLeader(user);
        Question question = new MultipleChoiceQuestion(new Condition(-1, -1), "TestQuestion", Arrays.asList("Yes", "No"));
        Event start = new SensorEvent(new StudyStartData());
        Event event = new CompositeEvent(Collections.singletonList(start), LogicalOperator.AND);
        Questionnaire questionnaire = new Questionnaire(event, 0, Collections.singletonList(question), new ArrayList<>(), "TestQuestionnaire");
        Study study = new Study("1234", user.getId(), "StudyName", "Description",
                LocalDateTime.now(), LocalDateTime.now().plusYears(1), Collections.singletonList(questionnaire));
        this.manager.storeStudy(study);

        Security.addProvider(new BouncyCastleProvider());

        KeyPair pair = generator.generateRSAKeyPair(4096);

        Assert.assertNotNull(pair);


        ProbandDetails probandDetails = this.service.handleRegister(study.getId(), pair.getPublic().getEncoded());
        X509Certificate actual = probandDetails.getCertificate();

        X509Certificate expected = probandManager.getCertificate(probandDetails.getProband().getId());

        String serializedActual = objectMapper.writeValueAsString(actual);
        String serializedExpected = objectMapper.writeValueAsString(expected);

        LoggerFactory.getLogger(getClass()).info("Serialized certificates: " + serializedActual);

        Assert.assertEquals(serializedExpected, serializedActual);

        X509Certificate actualDeserialized = objectMapper.readValue(serializedActual, X509Certificate.class);
        X509Certificate expectedDeserialized = objectMapper.readValue(serializedExpected, X509Certificate.class);

        Assert.assertEquals(expectedDeserialized, actualDeserialized);
    }
}
