package edu.kit.lymmy.spring.messages.services;

import edu.kit.lymmy.shared.Study;
import edu.kit.lymmy.shared.user.VerifyCode;
import edu.kit.lymmy.spring.SpringBackendApplication;
import edu.kit.lymmy.spring.database.mongodb.StudyRepository;
import edu.kit.lymmy.spring.database.mongodb.UserRepository;
import edu.kit.lymmy.spring.database.mongodb.VerifyCodeRepository;
import edu.kit.lymmy.spring.util.CsvBuilder;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Base64Utils;
import org.subethamail.wiser.Wiser;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Unit test class for the study management of the backend.
 *
 * @author Moritz Hepp
 * @version 0.0.1-SNAPSHOT
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource({"classpath:test.properties", "classpath:mail.properties"})
@ContextConfiguration(classes = SpringBackendApplication.class)
@EnableAutoConfiguration
public class StudyServiceTest {

    @Autowired
    private RegisterService regService;
    @Autowired
    private StudyService studyService;

    @Autowired
    private StudyRepository rStudy;
    @Autowired
    private UserRepository rUser;
    @Autowired
    private VerifyCodeRepository rVerify;

    private String userId;
    private String rawUserID;
    private Study study;

    @Value("${spring.mail.port}")
    private int port;
    private Wiser wiser;

    private void testRegisterFirstUser() {
        rawUserID = "ex@mp.le";
        userId = Base64Utils.encodeToString(rawUserID.getBytes());
        Assert.assertTrue(regService.handleRegister(userId, Base64Utils.encodeToString("password".getBytes())));
        Assert.assertEquals(1, rVerify.count());

        VerifyCode code = rVerify.findById(rawUserID);

        Assert.assertTrue(regService.handleRegisterVerify(code));
        Assert.assertTrue(rUser.findById(rawUserID).isActive());
        Assert.assertEquals(0, rVerify.count());
    }

    @Before
    public void setUp() throws Exception {
        this.rStudy.deleteAll();
        this.rUser.deleteAll();
        this.rVerify.deleteAll();

        this.wiser = new Wiser(port);
        this.wiser.start();
        testRegisterFirstUser();
    }

    /**
     * Tests if the backend doesn't contain any studies.
     */
    @Test
    @WithMockUser(authorities = "USER")
    public void testGetStudies() {
        List<Study> studies = studyService.handleGetStudies(() -> rawUserID);

        Assert.assertNotNull(studies);
        Assert.assertTrue(Arrays.toString(studies.toArray()), studies.isEmpty());
    }

    /**
     * Tests if the {@link StudyService#handleGetNewStudyID(Principal)} function is actually
     *     returning a not already existing study id.
     */
    @Test
    @WithMockUser(authorities = "USER")
    public void testGetNewStudyID() {
        Assert.assertFalse(this.rStudy.exists(studyService.handleGetNewStudyID(() -> rawUserID)));
    }

    private void testStore() {
        study = new Study("1234ID", rawUserID, "name", "desc", LocalDateTime.now().plusDays(2),
                LocalDateTime.now().plusDays(3), Collections.emptyList());

        Assert.assertTrue(studyService.handleStore(study, () -> rawUserID));
        Assert.assertEquals(1, rStudy.count());
        Assert.assertEquals(study, rStudy.findById(study.getId()));
    }

    /**
     * Tests the store {@link StudyService#handleStore(Study, Principal)} function.
     */
    @Test
    @WithMockUser(authorities = "USER")
    public void testStoreStudy() {
        this.testStore();
    }

    /**
     * Tests that a store of a changed study actually changes the study and doesn't create a new one.
     */
    @Test
    @WithMockUser(authorities = "USER")
    public void testChangeStudy() {
        this.testStore();

        Assert.assertNotNull(study);

        Study changed = new Study(study.getId(), study.getUserID(), "2name", study.getDescription(), LocalDateTime.now().plusDays(2),
                LocalDateTime.now().plusDays(3), Collections.emptyList());

        Assert.assertTrue(studyService.handleStore(changed, () -> rawUserID));
        Assert.assertEquals(1, rStudy.count());
        Assert.assertEquals(changed, rStudy.findById(study.getId()));
    }

    /**
     * Tests that a illegal change (altering the user id, etc.) doesn't have any effect
     *     on storing the study.
     */
    @Test
    @WithMockUser(authorities = "USER")
    public void testChangeStudyIllegal() {
        this.testStore();

        Assert.assertNotNull(study);

        Study changed = new Study(study.getId(), study.getUserID() + "12", "2name", study.getDescription(),
                LocalDateTime.now().plusDays(1), LocalDateTime.now().plusDays(2), Collections.emptyList());

        Assert.assertFalse(studyService.handleStore(changed, () -> rawUserID));
        Assert.assertEquals(1, rStudy.count());
        Assert.assertEquals(study, rStudy.findById(study.getId()));
    }

    @Test
    @WithMockUser(authorities = "USER")
    public void testExportNotFinishedStudy() {
        this.testStore();

        String actual = this.studyService.handleExport(study.getId(), () -> rawUserID);
        //String expected = new CsvBuilder(this.rStudy.findById(study.getId()), Collections.emptyList()).build();

        Assert.assertEquals(null, actual);
    }

    @Test
    @WithMockUser(authorities = "USER")
    public void testExport() {
        this.study = new Study("studyid", rawUserID, "Study Name", "Study Desc.", LocalDateTime.now().minusDays(30),
                LocalDateTime.now().minusDays(3), Collections.emptyList());

        Assert.assertEquals(this.study, this.rStudy.save(this.study));
        Assert.assertEquals(1, this.rStudy.count());

        String actual = this.studyService.handleExport(study.getId(), () -> rawUserID);
        String expected = new CsvBuilder(this.rStudy.findById(study.getId()), Collections.emptyList()).build();

        Assert.assertEquals(expected, actual);
    }

    @After
    public void tearDown() throws Exception {
        this.rStudy.deleteAll();
        this.rUser.deleteAll();
        this.rVerify.deleteAll();
        this.wiser.stop();
    }
}