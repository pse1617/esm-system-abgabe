package edu.kit.lymmy.spring.database;

import edu.kit.lymmy.shared.Questionnaire;
import edu.kit.lymmy.shared.Study;
import edu.kit.lymmy.shared.answer.QuestionnaireAnswer;
import edu.kit.lymmy.shared.event.SensorEvent;
import edu.kit.lymmy.shared.question.Condition;
import edu.kit.lymmy.shared.question.OpenQuestion;
import edu.kit.lymmy.shared.question.Question;
import edu.kit.lymmy.shared.sensor.TimeData;
import edu.kit.lymmy.shared.user.Proband;
import edu.kit.lymmy.spring.SpringBackendApplication;
import edu.kit.lymmy.spring.auth.encryption.BcryptEncryptor;
import edu.kit.lymmy.spring.auth.encryption.PasswordEncryptor;
import edu.kit.lymmy.spring.auth.user.User;
import edu.kit.lymmy.spring.database.mongodb.AnswerRepository;
import edu.kit.lymmy.spring.database.mongodb.ProbandRepository;
import edu.kit.lymmy.spring.database.mongodb.StudyRepository;
import edu.kit.lymmy.spring.database.mongodb.UserRepository;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Unit Test for Database Manager.
 *
 * @author Moritz Hepp
 * @version 0.0.1-SNAPSHOT
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource(locations = "classpath:test.properties")
@ContextConfiguration(classes = SpringBackendApplication.class)
@EnableAutoConfiguration
public class DatabaseManagerTest {

    @Autowired
    private DatabaseManager manager;
    @Autowired
    private ProbandRepository probandRepo;
    @Autowired
    private StudyRepository studyRepo;
    @Autowired
    private AnswerRepository answerRepo;
    @Autowired
    private UserRepository userRepo;

    private final String plaintextPassword = "leaderPassword";
    private final PasswordEncryptor encryptor = new BcryptEncryptor();
    private final String encryptedPassword = encryptor.encrypt(plaintextPassword);

    private Study study;

    @Test
    public void storeProband() throws Exception {
        Assert.assertNotNull(manager);

        User leader = new User("leaderid", encryptedPassword, encryptor.getParameters());
        Study study = new Study("1234ID", leader.getId(), "name", "desc", LocalDateTime.now(),
                LocalDateTime.now().plusDays(1), Arrays.asList(null, null));
        Proband proband = new Proband("1234", study.getId(), "hello");

        Assert.assertNull(manager.storeLeader(leader));

        Assert.assertEquals(proband, manager.storeProband(proband));

        Assert.assertNull(manager.storeStudy(study));

        Assert.assertNull(manager.storeProband(proband));

        Proband stored = manager.getProband("1234");
        Assert.assertEquals(proband.getId(), stored.getId());
        Assert.assertEquals(proband.getCertificateSerialNumber(), stored.getCertificateSerialNumber());
    }

    @Test
    public void storeStudy() throws Exception {
        Assert.assertNotNull(manager);

        User leader = new User("leaderid", encryptedPassword, encryptor.getParameters());

        Assert.assertNull(manager.storeLeader(leader));

        Study study = new Study("id", leader.getId() ,"Study name", "hello",
                LocalDateTime.now(), LocalDateTime.now().plusDays(1), Arrays.asList(null, null));
        manager.storeStudy(study);

        Study stored = manager.getStudy("id");

        Assert.assertEquals(study.getId(), stored.getId());
        Assert.assertEquals(study.getName(), stored.getName());
        Assert.assertEquals(study.getDescription(), stored.getDescription());
        Assert.assertEquals(study.getStartDate(), stored.getStartDate());
        Assert.assertEquals(study.getEndDate(), stored.getEndDate());
        Assert.assertEquals(study.getQuestionnaires(), stored.getQuestionnaires());
        Assert.assertEquals(study.getRegisteredProbands(), stored.getRegisteredProbands());
    }

    @Test
    public void storeLeader() {
        Assert.assertNotNull(manager);

        User leader = new User(
                "id",
                encryptedPassword,
                encryptor.getParameters());
        manager.storeLeader(leader);

        User stored = manager.getUser("id");

        Assert.assertNotNull(stored);
        Assert.assertEquals(leader.getId(), stored.getId());
        Assert.assertEquals(stored.getPassword(), leader.getPassword());
        Assert.assertEquals(leader.getParams(), stored.getParams());
        Assert.assertEquals(leader.isActive(), stored.isActive());
    }

    @Test
    public void storeOverAll() {
        Assert.assertNotNull(manager);

        User leader = new User("leaderid", encryptedPassword, encryptor.getParameters());

        List<Question> questions = Collections.singletonList(
                new OpenQuestion(
                        new Condition(0, 0),
                        "Text")
                );

        Questionnaire naire = new Questionnaire(
                new SensorEvent(
                        new TimeData(LocalTime.now(), 30)),
                2, questions, Collections.singletonList("contextData"),"hellName");


        study = new Study("studyID", leader.getId() , "testStudy", "A study for test",
                LocalDateTime.now(), LocalDateTime.now().plusDays(500), Collections.singletonList(naire));

        Proband proband = new Proband("probandID", study.getId(), "certificateNumber");

        QuestionnaireAnswer answer = new QuestionnaireAnswer(proband.getId(),
                LocalDateTime.now(), 0,
                Collections.singletonList("context"),
                Collections.singletonList(null));

        //Test Study store fails without leader
        Assert.assertEquals(study, manager.storeStudy(study));

        //Test User store
        Assert.assertNull(manager.storeLeader(leader));
        Assert.assertEquals(null, manager.storeLeader(leader));

        Assert.assertNull(manager.storeStudy(study));

        //Test study doesn't exists
        Proband prob = new Proband("12", "50", "blaa");
        Assert.assertEquals(prob, manager.storeProband(prob));
        Assert.assertNull(manager.storeProband(proband));

        //Test illegal arguments
        QuestionnaireAnswer illegalProbandID = new QuestionnaireAnswer("42",
                LocalDateTime.now(), 0, Collections.singletonList(null), Collections.singletonList(null));
        Assert.assertEquals(illegalProbandID, manager.storeQuestionnaireAnswers(illegalProbandID).get(illegalProbandID));

        QuestionnaireAnswer illegalNaireIndex = new QuestionnaireAnswer(proband.getId(),
                LocalDateTime.now(), 2, Collections.singletonList(null), Collections.singletonList(null));
        Assert.assertEquals(illegalNaireIndex, manager.storeQuestionnaireAnswers(illegalNaireIndex).get(illegalNaireIndex));

        Assert.assertTrue(manager.storeQuestionnaireAnswers(answer).isEmpty());

        Assert.assertTrue(manager.containsUser(leader.getId()));
        Assert.assertTrue(manager.containsStudy(study.getId()));
        Assert.assertTrue(manager.containsProband(proband.getId()));
        Assert.assertTrue(manager.containsAnswer(answer.getId()));
    }

    @Test
    public void testGetStudyData() {
        this.storeOverAll();
        Assert.assertNotNull(this.study);

        StudyData data = this.manager.getStudyData(this.study.getId());

        Assert.assertEquals(this.study, data.getStudy());
        Assert.assertEquals(1, data.getAnswers().size());
    }

    @Test(expected = NullPointerException.class)
    public void testNullIDGetStudyData() {
        this.manager.getStudyData(null);
    }

    @After
    public void tearDown() {
        probandRepo.deleteAll();
        studyRepo.deleteAll();

        answerRepo.deleteAll();
        userRepo.deleteAll();
    }
}