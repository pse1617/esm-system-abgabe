package edu.kit.lymmy.spring;

import com.fasterxml.jackson.databind.ser.Serializers;
import edu.kit.lymmy.shared.Study;
import edu.kit.lymmy.shared.messages.CommunicationConstants;
import edu.kit.lymmy.shared.user.ProbandDetails;
import edu.kit.lymmy.shared.user.VerifyCode;
import edu.kit.lymmy.spring.auth.CodeGenerator;
import edu.kit.lymmy.spring.auth.ProbandManager;
import edu.kit.lymmy.spring.database.mongodb.StudyRepository;
import edu.kit.lymmy.spring.database.mongodb.UserRepository;
import edu.kit.lymmy.spring.database.mongodb.VerifyCodeRepository;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.security.*;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

import edu.kit.lymmy.spring.scheduling.ScheduledExecutor;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Base64Utils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.subethamail.wiser.Wiser;

/**
 * Combines the register Testcase (Pflichtenheft "/T140/: Benutzerkonto wird angelegt") with updated steps to test.
 * The process of registration is tested in integration, with calls to the url the backend is located and
 * with unit testing, while only invoking methods of the {@link edu.kit.lymmy.spring.messages.services.RegisterService}.
 *
 * @author Moritz Hepp
 * @version 0.0.1-SNAPSHOT
 */
@RunWith(SpringRunner.class)
@TestPropertySource({"classpath:test.properties", "classpath:mail.properties"})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration
public class RegisterIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    private VerifyCodeRepository rVerify;
    @Autowired
    private UserRepository rUser;
    @Autowired
    private StudyRepository rStudy;
    @Autowired
    private ScheduledExecutor executor;
    @Autowired
    private CodeGenerator generator;
    @Autowired
    private ProbandManager probandManager;

    @Value("${spring.mail.port}")
    private int port;

    private final Logger logger = LoggerFactory.getLogger(RegisterIntegrationTest.class);

    private String userID;
    private String rawUserID;
    private String password;
    private String rawPassword;
    private String cookie;

    private Wiser wiser;

    @Before
    public void setUp() throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException {
        wiser = new Wiser(port);
        wiser.start();
    }

    private <T> ResponseEntity<T> sendParamRequest(String path, MultiValueMap<String, String> params, Class<T> response) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(params, headers);

        return restTemplate.exchange(path, HttpMethod.POST, entity, response);
    }

    public ResponseEntity<CommunicationConstants.LoginResult> sendLoginRequest(String username, String password) {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("username", username);
        map.add("password", password);

        return this.sendParamRequest(CommunicationConstants.LEADER_LOGIN, map, CommunicationConstants.LoginResult.class);
    }

    public ResponseEntity<String> sendCredentialsRequest(String path, String username, String password) {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("username", username);
        map.add("password", password);

        return this.sendParamRequest(path, map, String.class);
    }

    public <R> ResponseEntity<R> sendRequest(String path, Object body, Class<R> responseType) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<Object> entity = new HttpEntity<>(body, headers);

        return restTemplate.exchange(path, HttpMethod.POST, entity, responseType);
    }

    public <R, B> ResponseEntity<R> sendSessionRequest(String jsessionid, String path, B body, Class<R> responseType) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Cookie", jsessionid);

        HttpEntity<B> entity = new HttpEntity<>(body, headers);

        return restTemplate.exchange(path, HttpMethod.POST, entity, responseType);
    }

    /**
     * Tests if the backend is actually running.
     *
     * @throws NoSuchAlgorithmException If the algorithm for accessing server keystore doesn't match any existing.
     * @throws KeyStoreException If the access of the keystore fails.
     * @throws KeyManagementException If the read/write to a Keystore fails.
     */
    @Test
    public void testConnection() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        Map body = this.restTemplate.getForObject("/", Map.class);
        Assert.assertEquals(404, body.get("status"));
    }

    public void testRegisterStart() {
        this.rawUserID = "ex@mp.le";
        this.userID = Base64Utils.encodeToString(this.rawUserID.getBytes());
        this.rawPassword = "password";
        this.password = Base64Utils.encodeToString(this.rawPassword.getBytes());

        ResponseEntity<String> response = this.sendCredentialsRequest(CommunicationConstants.LEADER_REGISTER_START, this.userID, this.password);

        logger.info("Body: " + response.getBody());

        Assert.assertEquals(1, rUser.count());
        Assert.assertEquals("true", response.getBody());
        Assert.assertEquals(1, wiser.getMessages().size());
        Assert.assertEquals(new String(Base64Utils.decodeFromString(this.userID)), wiser.getMessages().get(0).getEnvelopeReceiver());
    }

    public void testRegisterVerify() {
        Assert.assertNotNull(this.userID);
        Assert.assertNotNull(this.password);

        ResponseEntity<String> response = this.sendRequest(CommunicationConstants.LEADER_REGISTER_FINISH, rVerify.findById(this.rawUserID), String.class);

        logger.info("StatusCode: " + response.getStatusCodeValue());
        logger.info("Body: " + response.getBody());

        Assert.assertTrue(response.getStatusCode().is2xxSuccessful());
        Assert.assertEquals(1, rUser.count());
    }

    /**
     * Tests the registration process with verifying that the e-mail was sent etc.
     */
    @Test
    public void testRegister() {
        this.testRegisterStart();

        this.testRegisterVerify();
    }

    /**
     * Performs the testRegister test and tests the login process with a registered user.
     */
    @Test
    public void testLogin() {
        if (userID == null)
            this.testRegister();

        Assert.assertNotNull(userID);
        Assert.assertNotNull(password);

        ResponseEntity<CommunicationConstants.LoginResult> response = this.sendLoginRequest(this.rawUserID, this.rawPassword);

        logger.info("ResponseCode: " + response.getStatusCode());

        Assert.assertTrue(response.getStatusCode().is2xxSuccessful());

        cookie = response.getHeaders().getFirst("Set-Cookie");

        Assert.assertNotNull(cookie);
        Assert.assertEquals(CommunicationConstants.LoginResult.OK, response.getBody());
        cookie = cookie.split(";")[0];

        logger.info(cookie);
    }

    /**
     * Performs the testLogin test and tests the logout process.
     */
    @Test
    public void testLogout() {
        this.testLogin();

        ResponseEntity<String> result = this.sendSessionRequest(cookie, CommunicationConstants.LEADER_LOGOUT, null, String.class);

        logger.info("Logout result: " + result);

        Assert.assertTrue(result.getStatusCode().is3xxRedirection());

        ResponseEntity<String> response = this.sendSessionRequest(cookie, CommunicationConstants.CREATE_STUDY_ID, null, String.class);

        logger.info("Try to use the old session key: " + response);

        Assert.assertTrue(response.getStatusCode().is4xxClientError());
    }

    /**
     * Performs the testLogin test and tests the delete action of a logged in user.
     */
    @Test
    public void testDeleteAuthorized() {
        this.testLogin();

        Assert.assertNotNull(userID);
        Assert.assertNotNull(password);
        Assert.assertNotNull(cookie);

        ResponseEntity<String> response = this.sendSessionRequest(cookie, CommunicationConstants.LEADER_DELETE, userID, String.class);

        logger.info("StatusCode: " + response.getStatusCodeValue());
        logger.info("Body: " + response.getBody());

        Assert.assertTrue(response.getStatusCode().is2xxSuccessful());
        Assert.assertEquals(0, rUser.count());
    }

    @Test
    public void testScheduledExecutor() {
        this.rVerify.save(new VerifyCode("ex@mp.le", LocalDateTime.now().minusDays(2), new byte[]{2, 3}));

        Assert.assertEquals(1, rVerify.count());

        this.executor.clearVerifyCodes();

        Assert.assertEquals(0, rVerify.count());
    }

    @Test
    public void testProbandRegister() throws UnsupportedEncodingException, CertificateEncodingException {
        Study study = new Study("1234", "userID", "Name", "Desc", LocalDateTime.now().plusDays(4), LocalDateTime.now().plusDays(14), Arrays.asList(null, null));
        this.rStudy.save(study);

        Security.addProvider(new BouncyCastleProvider());

        KeyPair pair = generator.generateRSAKeyPair(4096);

        Assert.assertNotNull(pair);

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(CommunicationConstants.PARAM_STUDY_ID, study.getId());
        headers.add(CommunicationConstants.PARAM_CLIENT_PUBKEY, Base64Utils.encodeToString(pair.getPublic().getEncoded()));

        ProbandDetails probandDetails = this.sendParamRequest(CommunicationConstants.PROBAND_REGISTER, headers, ProbandDetails.class).getBody();
        X509Certificate actual = probandDetails.getCertificate();

        X509Certificate expected = probandManager.getCertificate(probandDetails.getProband().getId());

        Assert.assertNotNull(actual);
        System.out.println(URLEncoder.encode(Base64Utils.encodeToString(pair.getPublic().getEncoded()), "ASCII"));
        Assert.assertArrayEquals(expected.getEncoded(), actual.getEncoded());
    }

    @After
    public void tearDown() {
        rUser.deleteAll();
        rVerify.deleteAll();
        userID = null;
        password = null;
        cookie = null;

        this.wiser.stop();
    }
}
