package edu.kit.lymmy.spring.messages.services;

import edu.kit.lymmy.spring.SpringBackendApplication;
import edu.kit.lymmy.spring.database.mongodb.UserRepository;
import edu.kit.lymmy.spring.database.mongodb.VerifyCodeRepository;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Base64Utils;
import org.subethamail.wiser.Wiser;

/**
 * Unit test class for the registration process.
 * In addition to the {@link edu.kit.lymmy.spring.RegisterIntegrationTest} this class also tests the resend
 * verification code functionality.
 *
 * @see RegisterService
 * @author Moritz Hepp
 * @version 0.0.1-SNAPSHOT
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource({"classpath:test.properties", "classpath:mail.properties"})
@ContextConfiguration(classes = SpringBackendApplication.class)
@EnableAutoConfiguration
public class RegisterServiceTest {

    @Autowired
    private RegisterService service;
    @Autowired
    private UserRepository rUser;
    @Autowired
    private VerifyCodeRepository rVerify;

    @Value("${spring.mail.port}")
    private int port;
    private String userId;
    private Wiser wiser;

    @Before
    public void setUp() {
        rUser.deleteAll();
        rVerify.deleteAll();
        this.userId = null;

        this.wiser = new Wiser(port);
        this.wiser.start();
    }

    private void testRegisterFirstUser() {
        userId = Base64Utils.encodeToString("ex@mp.le".getBytes());
        String password = Base64Utils.encodeToString("password".getBytes());
        Assert.assertTrue(service.handleRegister(userId,password));
        Assert.assertEquals(1, rVerify.count());
        Assert.assertEquals(1, this.wiser.getMessages().size());
        Assert.assertEquals(new String(Base64Utils.decodeFromString(this.userId)), this.wiser.getMessages().get(0).getEnvelopeReceiver());
    }

    private void testRegisterVerify() {
        Assert.assertNotNull(userId);

        Assert.assertTrue(service.handleRegisterVerify(rVerify.findById(new String(Base64Utils.decodeFromString(userId)))));
        Assert.assertTrue(rUser.findById(new String(Base64Utils.decodeFromString(userId))).isActive());
        Assert.assertEquals(0, rVerify.count());
    }

    private void testCoderesend() {
        Assert.assertNotNull(userId);

        Assert.assertTrue(service.handleResendVerifyCode(userId));
        Assert.assertFalse(rUser.findById(new String(Base64Utils.decodeFromString(userId))).isActive());
        Assert.assertEquals(1, rVerify.count());
    }

    @Test
    @WithMockUser
    public void testDuplicateId() {
        this.testRegisterFirstUser();

        Assert.assertFalse(service.handleRegister(userId, Base64Utils.encodeToString("pass".getBytes())));
        Assert.assertEquals(1, rUser.count());
        Assert.assertEquals(1, rVerify.count());
    }

    @Test
    @WithMockUser
    public void testResendCode() {
        this.testRegisterFirstUser();
        this.testCoderesend();
    }

    @Test
    @WithMockUser
    public void testResendVerifiedUserCode() {
        this.testRegisterFirstUser();
        this.testRegisterVerify();

        Assert.assertFalse(service.handleResendVerifyCode(userId));
        Assert.assertEquals(1, rUser.count());
        Assert.assertEquals(0, rVerify.count());
        Assert.assertEquals(1, this.wiser.getMessages().size());
    }

    @Test
    @WithMockUser
    public void testRegister() {
        this.testRegisterFirstUser();
    }

    @Test
    @WithMockUser
    public void testRegisterVerification() {
        this.testRegisterFirstUser();

        this.testRegisterVerify();
    }

    @Test
    @WithMockUser(authorities = "USER")
    public void testDeleteUnverifiedUser() {
        this.testRegisterFirstUser();

        Assert.assertNotNull(userId);

        Assert.assertTrue(service.handleDeleteUser(()->new String(Base64Utils.decodeFromString(userId))));

        Assert.assertEquals(0, rUser.count());
        Assert.assertEquals(0, rVerify.count());
    }

    @After
    public void tearDown() {
        rUser.deleteAll();
        rVerify.deleteAll();
        userId = null;

        this.wiser.stop();
    }

}
