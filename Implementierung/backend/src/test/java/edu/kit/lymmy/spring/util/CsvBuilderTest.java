package edu.kit.lymmy.spring.util;

import edu.kit.lymmy.shared.LogicalOperator;
import edu.kit.lymmy.shared.Questionnaire;
import edu.kit.lymmy.shared.Study;
import edu.kit.lymmy.shared.answer.Answer;
import edu.kit.lymmy.shared.answer.QuestionnaireAnswer;
import edu.kit.lymmy.shared.event.CompositeEvent;
import edu.kit.lymmy.shared.event.Event;
import edu.kit.lymmy.shared.question.Condition;
import edu.kit.lymmy.shared.question.MultipleChoiceQuestion;
import edu.kit.lymmy.shared.question.Question;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

/**
 * @author Lukas
 * @since 06.02.2017
 */
public class CsvBuilderTest {
    @Test
    public void build() throws Exception {
        Question question = new MultipleChoiceQuestion(new Condition(-1, -1), "TestQuestion", Arrays.asList("Yes", "No"));
        Event event = new CompositeEvent(new ArrayList<>(), LogicalOperator.AND);
        Questionnaire questionnaire = new Questionnaire(event, 0, Collections.singletonList(question), new ArrayList<>(), "TestQuestionnaire");
        Study study = new Study("TestID", "TestUser", "TestStudy", "TestDescription", LocalDateTime.now(), LocalDateTime.MAX, Collections.singletonList(questionnaire));
        Answer<String> answer = new Answer<>(0, "Yes");
        QuestionnaireAnswer questionnaireAnswer = new QuestionnaireAnswer("TestProband", LocalDateTime.of(2017, 2, 6, 19, 36), 0, new ArrayList<>(), Collections.singletonList(answer));
        String csv = new CsvBuilder(study, Collections.singletonList(questionnaireAnswer)).build();
        assertEquals("\"Proband-ID\",\"Timestamp\",\"TestQuestion\"\n\"TestProband\",\"2017-02-06T19:36:00\",\"Yes\"\n", csv);
    }

}