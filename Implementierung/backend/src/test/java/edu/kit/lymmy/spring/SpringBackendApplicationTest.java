package edu.kit.lymmy.spring;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.client.RestTemplateCustomizer;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.*;

/**
 * Unit Test for SpringBackendApplication, only for Coverage as the main method is not recognised.
 *
 * @author Moritz Hepp
 * @version 0.0.1-SNAPSHOT
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class SpringBackendApplicationTest {

    @Test(expected = Exception.class)
    public void main() throws Exception {
        SpringBackendApplication.main(new String[]{""});
    }

}