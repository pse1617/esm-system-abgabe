package edu.kit.lymmy.spring.messages;

import edu.kit.lymmy.shared.user.VerifyCode;
import edu.kit.lymmy.spring.SpringBackendApplication;
import edu.kit.lymmy.spring.auth.CodeGenerator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.subethamail.wiser.Wiser;

/**
 * Unit tests the EmailSender class with a local test smtp server.
 *
 * @author Moritz Hepp
 * @version 0.0.1-SNAPSHOT
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource({"classpath:test.properties", "classpath:mail.properties"})
@ContextConfiguration(classes = SpringBackendApplication.class)
@EnableAutoConfiguration
public class EmailSenderTest {

    private Wiser wiser;

    @Value("${spring.mail.port}")
    private int port;
    @Value("${spring.mail.host}")
    private String host;
    @Autowired
    private EmailSender sender;

    @Before
    public void setUp() {
        this.wiser = new Wiser(port);
        this.wiser.setHostname(host);
        this.wiser.start();
    }

    /**
     * Tests the {@link EmailSender#sendEmail(VerifyCode)} function and if the email is sent.
     */
    @Test
    public void testSendEmail() {
        VerifyCode code = new CodeGenerator().generateVerifyCode("ex@mp.le");
        sender.sendEmail(code);
        Assert.assertFalse(wiser.getMessages().isEmpty());
    }

    @After
    public void tearDown() {
        this.wiser.stop();
    }

}