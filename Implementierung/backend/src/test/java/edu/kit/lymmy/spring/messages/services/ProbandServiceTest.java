package edu.kit.lymmy.spring.messages.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.kit.lymmy.shared.LogicalOperator;
import edu.kit.lymmy.shared.Questionnaire;
import edu.kit.lymmy.shared.Study;
import edu.kit.lymmy.shared.answer.QuestionnaireAnswer;
import edu.kit.lymmy.shared.event.CompositeEvent;
import edu.kit.lymmy.shared.event.Event;
import edu.kit.lymmy.shared.event.SensorEvent;
import edu.kit.lymmy.shared.question.Condition;
import edu.kit.lymmy.shared.question.MultipleChoiceQuestion;
import edu.kit.lymmy.shared.question.Question;
import edu.kit.lymmy.shared.sensor.StudyStartData;
import edu.kit.lymmy.shared.user.ProbandDetails;
import edu.kit.lymmy.spring.RegisterIntegrationTest;
import edu.kit.lymmy.spring.SpringBackendApplication;
import edu.kit.lymmy.spring.auth.CodeGenerator;
import edu.kit.lymmy.spring.auth.ProbandManager;
import edu.kit.lymmy.spring.auth.encryption.BcryptEncryptor;
import edu.kit.lymmy.spring.auth.encryption.PasswordEncryptor;
import edu.kit.lymmy.spring.auth.user.User;
import edu.kit.lymmy.spring.database.DatabaseManager;
import edu.kit.lymmy.spring.database.mongodb.StudyRepository;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.OperatorCreationException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Base64Utils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Unit tests the proband service functionality.
 *
 * @author Moritz Hepp
 * @version 0.0.1-SNAPSHOT
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource(locations = "classpath:test.properties")
@ContextConfiguration(classes = SpringBackendApplication.class)
@EnableAutoConfiguration
public class ProbandServiceTest {

    @Autowired
    private ProbandService service;
    @Autowired
    private DatabaseManager manager;
    @Autowired
    private StudyRepository studyRepo;
    @Autowired
    private CodeGenerator generator;
    @Autowired
    private ProbandManager probandManager;
    @Autowired
    private ObjectMapper objectMapper;

    private Study study;
    private User user;

    private static final PasswordEncryptor encryptor = new BcryptEncryptor();
    private static final String userPassword = "password";
    private static final String encryptedPassword = encryptor.encrypt(userPassword);

    private ProbandDetails probandDetails;

    @Before
    public void setUp() {
        this.user = new User("ex@mp.le", encryptedPassword, encryptor.getParameters());
        this.manager.storeLeader(user);
        Question question = new MultipleChoiceQuestion(new Condition(-1, -1), "TestQuestion", Arrays.asList("Yes", "No"));
        Event start = new SensorEvent(new StudyStartData());
        Event event = new CompositeEvent(Collections.singletonList(start), LogicalOperator.AND);
        Questionnaire questionnaire = new Questionnaire(event, 0, Collections.singletonList(question), new ArrayList<>(), "TestQuestionnaire");
        this.study = new Study("1234", user.getId(), "StudyName", "Description",
                LocalDateTime.now(), LocalDateTime.now().plusYears(1), Collections.singletonList(questionnaire));
        this.manager.storeStudy(study);
    }

    /**
     * Tests the creation of a new Proband and certificate.
     *
     * @throws NoSuchAlgorithmException
     * @throws OperatorCreationException
     * @throws CertificateException
     * @throws KeyStoreException
     * @throws IOException
     * @throws NoSuchProviderException
     * @throws UnrecoverableKeyException
     */
    @WithMockUser(roles = "PROBAND")
    @Test
    public void testRegisterProband()
            throws NoSuchAlgorithmException, OperatorCreationException, CertificateException, KeyStoreException, IOException, NoSuchProviderException, UnrecoverableKeyException, URISyntaxException {
        Assert.assertNotNull(this.study);
        Assert.assertNotNull(this.user);

        Security.addProvider(new BouncyCastleProvider());

        KeyPair pair = generator.generateRSAKeyPair(4096);

        Assert.assertNotNull(pair);

        probandDetails = this.service.handleRegister(study.getId(), Base64Utils.encode(pair.getPublic().getEncoded()));
        X509Certificate actual = probandDetails.getCertificate();

        X509Certificate expected = probandManager.getCertificate(probandDetails.getProband().getId());

        Assert.assertNotNull(actual);
        System.out.println(URLEncoder.encode(Base64Utils.encodeToString(pair.getPublic().getEncoded()), "ASCII"));
        Assert.assertArrayEquals(expected.getEncoded(), actual.getEncoded());
        this.storePKCSKeystore(probandDetails.getProband().getId(), "pass".toCharArray(), pair.getPrivate(), probandDetails.getCertificate());
    }

    /**
     * Performs the registration of a proband, tests the get study functionality of RegisterService and the
     * serialization from Jackson with the study class.
     * @throws Exception
     */
    @WithMockUser(roles = "PROBAND")
    @Test
    public void testGetStudy() throws Exception{
        testRegisterProband();
        String actual = objectMapper.writeValueAsString(this.service.handleGetStudy(()->probandDetails.getProband().getId()));
        String expected = objectMapper.writeValueAsString(this.studyRepo.findByUserID(this.user.getId()).get(0));

        Assert.assertEquals(expected, actual);
    }

    @Test
    @WithMockUser(roles = "PROBAND")
    public void testGetStartDate() throws CertificateException, UnrecoverableKeyException, NoSuchAlgorithmException, OperatorCreationException, KeyStoreException, NoSuchProviderException, IOException, URISyntaxException {
        this.testRegisterProband();

        LocalDateTime time = this.service.handleGetStartDate(probandDetails.getProband()::getId);

        Assert.assertNotNull(this.study.getStartDate());
        Assert.assertEquals(this.study.getStartDate().toLocalDate(), time.toLocalDate());
    }

    @Test
    @WithMockUser(roles = "PROBAND")
    public void testStoreAnswer() throws CertificateException, UnrecoverableKeyException, NoSuchAlgorithmException, OperatorCreationException, KeyStoreException, NoSuchProviderException, IOException, URISyntaxException {
        this.testRegisterProband();

        QuestionnaireAnswer answer = new QuestionnaireAnswer(this.probandDetails.getProband().getId(), LocalDateTime.now(), 0, Collections.singletonList(null), Collections.singletonList(null));

        int storedCount = this.service.handleStoreAnswer(probandDetails.getProband()::getId, answer);

        Assert.assertEquals(1, storedCount);
    }

    private void storePKCSKeystore(String probandID, char[] keystorePass, PrivateKey privKey, X509Certificate certificate) throws NoSuchProviderException, KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException {
        KeyStore store = KeyStore.getInstance("PKCS12", "BC");
        store.load(null, keystorePass);
        store.setKeyEntry(probandID, privKey, keystorePass, new Certificate[] {certificate});

        FileOutputStream fos = new FileOutputStream( System.getProperty("user.home") + "/" + probandID + ".p12");
        store.store(fos, keystorePass);
        fos.flush();
        fos.close();
    }

    @After
    public void tearDown() {
        probandManager.deleteCertificates(probandDetails.getProband());
    }
}
