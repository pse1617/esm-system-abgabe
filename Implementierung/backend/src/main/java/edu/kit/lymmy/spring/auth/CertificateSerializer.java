package edu.kit.lymmy.spring.auth;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.util.Base64Utils;
import org.springframework.util.SerializationUtils;

import java.io.IOException;
import java.security.cert.X509Certificate;

/**
 * Serializer class for X509Certificates. Used for Serialization with Jackson while transferring Data between Backend
 * and App.
 *
 * @author Moritz Hepp
 */
public class CertificateSerializer extends JsonSerializer<X509Certificate> {

    @Override
    public void serialize(X509Certificate value, JsonGenerator gen, SerializerProvider serializers)
            throws IOException {
        gen.writeStartObject();
        gen.writeStringField("certificate",Base64Utils.encodeToString(SerializationUtils.serialize(value)));
        gen.writeEndObject();
    }
}
