package edu.kit.lymmy.spring.auth.security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import java.security.Security;

import static edu.kit.lymmy.shared.messages.CommunicationConstants.*;

/**
 * Configuration class for Spring Security. Enables web and Method Security.
 * Web security shows the spring container to configure the environment to use user management.
 * Method Security allows expressions as shown insise {@link edu.kit.lymmy.spring.messages.services.StudyService}.
 *
 * This class configures which paths will be accessible with and without authentication. and registers the
 * BouncyCaslte Provider for encryption.
 *
 * @author Moritz Hepp
 */
@Configuration
@EnableWebSecurity
@ComponentScan("edu.kit.lymmy.spring")
@EnableAutoConfiguration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {

    private RestAuthenticationEntryPoint restAuthenticationEntryPoint;
    private SavedRequestAwareAuthenticationSuccessHandler authenticationSuccessHandler;
    private AuthenticationFailureHandler failureHandler;
    private BackendAuthenticationProvider authenticationProvider;
    private ProbandHeaderFilter probandHeaderFilter;

    /**
     * Adds the BouncyCastleProvider to the system.
     */
    @Autowired
    public WebSecurityConfigurationAdapter() {
        Security.addProvider(new BouncyCastleProvider());
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider);
    }

    @Value("${server.http.allow}")
    private boolean allowHttp;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //if(!allowHttp) http.requiresChannel().anyRequest().requiresSecure();
        http.csrf().disable()
                .exceptionHandling()
                .authenticationEntryPoint(restAuthenticationEntryPoint)
                .and()
                    .authorizeRequests().antMatchers(
                            LEADER_REGISTER_START,
                            LEADER_RESEND_CODE,
                            LEADER_REGISTER_FINISH,
                            LEADER_LOGIN,
                            PROBAND_REGISTER)
                    .permitAll()
                .and()
                    .authorizeRequests().antMatchers(
                            LEADER_DELETE,
                            GET_STUDIES,
                            CREATE_STUDY_ID,
                            STORE_STUDY,
                            DELETE_STUDY,
                            EXPORT_STUDY,
                            LEADER_LOGOUT)
                    .authenticated()
                .and()
                    .addFilter(probandHeaderFilter)
                    .authorizeRequests().antMatchers(
                            PROBAND_GET_STARTDATE,
                            PROBAND_GET_STUDY,
                            PROBAND_STORE_ANSWER)
                    .authenticated()
                .and()
                .x509().subjectPrincipalRegex("CN=(.*),?.*")
                .userDetailsService(authenticationProvider)
                .and()
                .formLogin()
                    .successHandler(authenticationSuccessHandler)
                    .failureHandler(failureHandler)
                    .permitAll()
                .and()
                    .logout();
    }

    /**
     * @param restAuthenticationEntryPoint Sets the RestAuthenticationEntryPoint with Beans given from the spring container.
     */
    @Autowired
    public void setRestAuthenticationEntryPoint(RestAuthenticationEntryPoint restAuthenticationEntryPoint) {
        this.restAuthenticationEntryPoint = restAuthenticationEntryPoint;
    }

    /**
     * @param authenticationSuccessHandler Sets the AuthenticationSuccessHandler with Beans given from the spring container.
     */
    @Autowired
    public void setAuthenticationSuccessHandler(SavedRequestAwareAuthenticationSuccessHandler authenticationSuccessHandler) {
        this.authenticationSuccessHandler = authenticationSuccessHandler;
    }

    /**
     * @param handler Sets the StatusCodeAuthenticationFailureHandler with Beans given from the spring container.
     */
    @Autowired
    public void setAuthenticationFailureHandler(StatusCodeAuthenticationFailureHandler handler) {
        this.failureHandler = handler;
    }

    /**
     * @param provider Sets the BackendAuthenticationProvider with Beans given from the spring container.
     */
    @Autowired
    public void setAuthenticationProvider(BackendAuthenticationProvider provider) {
        this.authenticationProvider = provider;
    }

    @Autowired
    public void setProbandHeaderFilter(ProbandHeaderFilter probandHeaderFilter) {
        this.probandHeaderFilter = probandHeaderFilter;
    }
}
