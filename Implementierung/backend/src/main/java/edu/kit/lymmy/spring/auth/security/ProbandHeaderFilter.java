package edu.kit.lymmy.spring.auth.security;

import edu.kit.lymmy.shared.messages.CommunicationConstants;
import edu.kit.lymmy.spring.database.DatabaseManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Lukas
 * @since 23.02.2017
 */
@Component
public class ProbandHeaderFilter extends AbstractPreAuthenticatedProcessingFilter {

    private final DatabaseManager databaseManager;

    @Autowired
    public ProbandHeaderFilter(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
    }

    @Override
    protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
        String header = request.getHeader(CommunicationConstants.HEADER_PROBAND_ID);
        if (header != null) {
            if(databaseManager.containsProband(header)) {
                return header;
            }
        }
        return null;
    }

    @Override
    protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
        String header = request.getHeader(CommunicationConstants.HEADER_PROBAND_ID);
        if (header != null) {
            if(databaseManager.containsProband(header)) {
                return "";
            }
        }
        return null;
    }

    @Override
    @Autowired
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        super.setAuthenticationManager(authenticationManager);
    }
}
