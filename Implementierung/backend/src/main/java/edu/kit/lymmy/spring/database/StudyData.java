package edu.kit.lymmy.spring.database;

import edu.kit.lymmy.shared.Study;
import edu.kit.lymmy.shared.answer.QuestionnaireAnswer;
import edu.kit.lymmy.spring.util.CsvBuilder;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Combines a study definition and the answers given to it.
 *
 * @author Moritz Hepp
 */
public class StudyData {

    private final Study study;
    private final List<QuestionnaireAnswer> answers;

    /**
     * Creates a new instance.
     *
     * @param study the study
     * @param answers the answers given to the study
     */
    public StudyData(@NotNull Study study,@NotNull List<QuestionnaireAnswer> answers) {
        this.study = study;
        this.answers = answers;
    }

    /**
     * Getter of the study associated by this class.
     *
     * @return Study
     */
    public Study getStudy() {
        return this.study;
    }

    /**
     * Answers to the assocciated study.
     *
     * @return List of all answers to all questionnaires of the study.
     */
    public List<QuestionnaireAnswer> getAnswers() {
        return answers;
    }

    /**
     * @return a csv-formatted String containing the data of this
     */
    @NotNull
    public String toCSV() {
        return new CsvBuilder(study, answers).build();
    }
}
