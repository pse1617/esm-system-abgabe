package edu.kit.lymmy.spring.messages.services;

import edu.kit.lymmy.shared.user.VerifyCode;
import edu.kit.lymmy.spring.auth.CodeGenerator;
import edu.kit.lymmy.spring.auth.encryption.BcryptEncryptor;
import edu.kit.lymmy.spring.auth.encryption.EncryptionParams;
import edu.kit.lymmy.spring.auth.encryption.PasswordEncryptor;
import edu.kit.lymmy.spring.auth.user.User;
import edu.kit.lymmy.spring.database.DatabaseManager;
import edu.kit.lymmy.spring.messages.EmailSender;

import java.nio.charset.Charset;
import java.security.Principal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import static edu.kit.lymmy.shared.messages.CommunicationConstants.LEADER_DELETE;
import static edu.kit.lymmy.shared.messages.CommunicationConstants.LEADER_REGISTER_FINISH;
import static edu.kit.lymmy.shared.messages.CommunicationConstants.LEADER_REGISTER_START;
import static edu.kit.lymmy.shared.messages.CommunicationConstants.LEADER_RESEND_CODE;
import static edu.kit.lymmy.shared.messages.CommunicationConstants.PARAM_PASSWORD;
import static edu.kit.lymmy.shared.messages.CommunicationConstants.PARAM_USERNAME;

/**
 * Controller class containing the services for registering as a study leader.
 *
 * @author Moritz Hepp
 */
@RestController
public class RegisterService {

    private final Logger logger;

    private final DatabaseManager manager;
    private final CodeGenerator generator;
    private final EmailSender mailSender;

    /**
     * Constructor to be called by the spring container.
     *
     * @param manager Bean for database access.
     * @param generator Bean for code generation.
     * @param mailSender Bean for sending of mails.
     */
    @Autowired
    public RegisterService(DatabaseManager manager, CodeGenerator generator, EmailSender mailSender) {
        this.manager = manager;
        this.logger = LoggerFactory.getLogger(this.getClass());
        this.generator = generator;
        this.mailSender = mailSender;
    }

    /**
     * Receives the credentials of a user to register. Creates a new User and sends a verification mail.
     *
     * @param userId   Id of the user to register with base64 encoded.
     * @param password Plaintext password of the user to register with base64 encoded.
     * @return If the User was created.
     */
    @RequestMapping(value = LEADER_REGISTER_START, method = RequestMethod.POST)
    public boolean handleRegister(@RequestParam(PARAM_USERNAME) String userId,
                                  @RequestParam(PARAM_PASSWORD) String password) {
        boolean response = false;
        String decodedUserID = new String(Base64Utils.decodeFromString(userId));
        //Register a new Leader
        if (!manager.containsUser(decodedUserID)) {
            PasswordEncryptor encryptor = new BcryptEncryptor();
            User user;
            try {
                String encryptedPassword = encryptor.encrypt(new String(Base64Utils.decodeFromString(password)));
                EncryptionParams params = encryptor.getParameters();

                user = new User(decodedUserID, encryptedPassword, params, false);
            } catch (IllegalArgumentException e) {
                logger.error("Username or password are not base64 encoded");
                return false;
            }

            VerifyCode verifyCode = generator.generateVerifyCode(user.getId());

            //Generator shouldn't return null as the algorithm is recognized
            assert verifyCode != null;

            if (manager.storeLeader(user) == null &&
                    manager.storeVerifyCode(verifyCode) == null) {
                //Generate
                if (mailSender.sendEmail(verifyCode)) {
                    response = true;
                }
            }
        }
        if (response) logger.debug("SUCCESS: Registered the new user: " + userId);
        else logger.debug("FAILURE: Rejected registration of new user: " + userId);
        return response;
    }

    /**
     * Handles the request to resend the verify code of a not verified user.
     * Generates a new verify code and sends a new email
     *
     * @param username Id of the user to resend a verify code.
     * @return If a new verify code was send.
     */
    @RequestMapping(value = LEADER_RESEND_CODE, method = RequestMethod.POST)
    public boolean handleResendVerifyCode(@RequestParam(PARAM_USERNAME) String username) {
        assert username != null;

        boolean response = false;

        String decodedUsername = new String(Base64Utils.decodeFromString(username));

        if (manager.containsUser(decodedUsername) && !manager.getUser(decodedUsername).isActive()) {
            VerifyCode code = generator.generateVerifyCode(decodedUsername);
            assert code != null;
            VerifyCode stored = manager.storeVerifyCode(code);
            response = (stored == null || !stored.equals(code)) && mailSender.sendEmail(code);
        }
        if (response) logger.debug("SUCCESS: Created new verification code and send to user: " + username);
        else logger.debug("FAILURE: Rejected request of resend verification code of user: " + username);
        return response;
    }

    /**
     * Handles the data to verify the registration of a leader.
     *
     * @param code Verification code to use for verification.
     * @return If the user was verified.
     */
    @RequestMapping(value = LEADER_REGISTER_FINISH, method = RequestMethod.POST)
    public boolean handleRegisterVerify(
            @RequestBody VerifyCode code) {
        assert code != null;
        boolean response = false;

        if (manager.containsUser(code.getId())) {
            User user = manager.getUser(code.getId());
            VerifyCode expected = manager.getVerifyCode(code.getId());
            if (user != null && expected != null && !user.isActive() &&
                    expected.equals(code)) {
                //Delete old user and verify code, activate and save it
                manager.deleteLeader(user.getId());
                user.setActive(true);
                response = manager.storeLeader(user) == null;
            }
        }
        if (response) logger.debug("SUCCESS: Verified the registration of user: " + code.getId());
        else logger.debug("FAILURE: Rejected verify registration of user: " + code.getId());
        return response;
    }

    /**
     * This action can also be performed if a Study-Leader is not verified.
     *
     * @param principal Logged in user.
     * @return If the user was deleted.
     */
    @PreAuthorize("hasAuthority('USER')")
    @RequestMapping(value = LEADER_DELETE, method = RequestMethod.POST)
    public boolean handleDeleteUser(Principal principal) {
        boolean response = false;

        String userID = principal.getName();

        if (manager.containsUser(userID)) {
            manager.deleteLeader(userID);
            response = true;
        }
        if (response) logger.debug("SUCCESS: Deleted the user " + userID);
        else logger.debug("FAILURE: Rejected delete request for: " + userID);
        return response;
    }
}
