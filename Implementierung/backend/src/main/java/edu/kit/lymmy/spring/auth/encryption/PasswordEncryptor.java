package edu.kit.lymmy.spring.auth.encryption;

/**
 * Abstraction of a password encryptor to allow easy adding of new algorithms.
 */
public abstract class PasswordEncryptor<E extends EncryptionParams> {

    private final PasswordHashAlgorithm passwordHashAlgorithm;

    final E parameters;

    PasswordEncryptor(E params, PasswordHashAlgorithm passwordHashAlgorithm) {
        this.parameters = params;
        this.passwordHashAlgorithm = passwordHashAlgorithm;
    }

    /**
     * @return Gets the EncryptionParams
     */
    public E getParameters() {
        return this.parameters;
    }

    /**
     * Encrypts the plaintext password with the given algorithm.
     * @param plaintextPassword Password to encrypt.
     * @return Encrypted Password.
     */
    public abstract String encrypt(String plaintextPassword);

    /**
     * Verifies the validity of the plaintext password with the algorithm and params.
     *
     * @param plaintextPassword Password to verify.
     * @param encrypted Encrypted password.
     * @return If the plaintext password encrypted with this params and encryptr matches the encrypted.
     */
    public abstract boolean check(String plaintextPassword, String encrypted);
}
