package edu.kit.lymmy.spring.messages.services;

import edu.kit.lymmy.shared.Study;
import edu.kit.lymmy.spring.auth.CodeGenerator;
import edu.kit.lymmy.spring.database.DatabaseManager;
import edu.kit.lymmy.spring.database.StudyData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

import static edu.kit.lymmy.shared.messages.CommunicationConstants.*;

/**
 * Service for the interaction between web-client and backend.
 * All services represented by this class can only be accessed by a registered and verified study leader.
 *
 * @author Moritz Hepp
 */
@RestController
public class StudyService {

    private final Logger logger;

    private final DatabaseManager databaseManager;
    private final CodeGenerator generator;

    /**
     * The Constructor to be called by the spring container.
     *
     * @param manager Bean to access the Database.
     * @param generator Bean for generation of codes.
     */
    @Autowired
    public StudyService(DatabaseManager manager, CodeGenerator generator) {
        this.databaseManager = manager;
        this.logger = LoggerFactory.getLogger(StudyService.class);
        this.generator = generator;
    }

    /**
     * Handles the request for all studies the logged in user has created.
     *
     * @param principal Principal matching the logged in study leader.
     * @return List of all studies created by the principal or null if something went wrong.
     */
    @PreAuthorize("hasAuthority('USER')")
    @RequestMapping(value= GET_STUDIES, method = RequestMethod.POST)
    @ResponseBody
    public List<Study> handleGetStudies(Principal principal) {
        assert principal != null;

        String userID = principal.getName();

        List<Study> response = null;

        if (this.databaseManager.containsUser(userID) &&
                this.databaseManager.getUser(userID).isActive()) {
            response = databaseManager.getStudies(userID);
        }
        if (response != null) logger.debug("SUCCESS: Get studies of user: " + userID);
        else logger.debug("FAILURE: Rejedted get studies of user: " + userID);
        return response;
    }

    /**
     * Handles the request for generating a new study id for the logged in user.
     * @param principal Logged in user.
     * @return New generated study id.
     */
    @PreAuthorize("hasAuthority('USER')")
    @RequestMapping(value= CREATE_STUDY_ID, method = RequestMethod.POST)
    @ResponseBody
    public String handleGetNewStudyID(Principal principal) {
        String response = null;

        String userID = principal.getName();

        if (databaseManager.containsUser(userID) && databaseManager.getUser(userID).isActive()) {
            do {
                response = generator.generateStudyID();
            } while (databaseManager.containsStudy(response));
        }
        if (response != null) logger.debug("SUCCESS: Generated new study id for user: " + userID);
        else logger.debug("FAILURE: Rejected generation of a new study for user: " + userID);
        return response;
    }

    /**
     * Handles requests for storing a study.
     *
     * @param study Study to store.
     * @param principal Currently logged in user.
     * @return If the study was stored or a existing study was updated.
     */
    @PreAuthorize("hasAuthority('USER')")
    @RequestMapping(value= STORE_STUDY, method = RequestMethod.POST)
    public boolean handleStore(@RequestBody Study study, Principal principal) {
        assert study != null;
        boolean response = false;

        //TODO Bulk-Requests?
        //Check if user exists
        if (databaseManager.containsUser(study.getUserID()) &&
                databaseManager.getUser(study.getUserID()).isActive() &&
                principal.getName().equals(study.getUserID())) {
            //Differ between change of a study and creation of a new study
            if (databaseManager.containsStudy(study.getId())) {
                Study stored = databaseManager.getStudy(study.getId());
                //Check if Study already started
                if (stored.getUserID().equals(study.getUserID())) {
                    //Update request
                    databaseManager.deleteStudy(study.getId());
                    if (databaseManager.storeStudy(study) == null) {
                        response = true;
                        logger.debug("SUCCESS: Stored study: " + study.getId());
                    } else {
                        throw new IllegalStateException("Failed to store study");
                    }
                }
                //If the Study already started or a day before StartDate has reached the change won't take effect
            } else {
                if (databaseManager.storeStudy(study) == null) response = true;
                else throw new IllegalStateException("Failed to store study");
            }
        }
        if (!response) logger.debug("FAILURE: Rejected to store study: " + study.getId());
        return response;
    }

    /**
     * Handles the Requests for deleting a study owned by the current logged in user.
     *
     * @param studyID Study to delete.
     * @param principal Currently logged in user.
     * @return If the study was deleted.
     */
    @PreAuthorize("hasAuthority('USER')")
    @RequestMapping(value= DELETE_STUDY, method = RequestMethod.POST)
    public boolean handleDelete(@RequestParam(PARAM_STUDY_ID) String studyID,
                                    Principal principal) {
        assert studyID != null && principal != null;
        boolean response = false;

        String userID = principal.getName();

        if (databaseManager.containsStudy(studyID) && databaseManager.containsUser(userID) &&
                databaseManager.getUser(userID).isActive()) {
            Study study = databaseManager.getStudy(studyID);
            if (study.getUserID().equals(userID)) {
                databaseManager.deleteStudy(studyID);
                response = true;
            }
        }
        if (response) logger.debug("SUCCESS: Deleted study: " + studyID + " of user: " + userID);
        else logger.debug("FAILURE: Rejected delete request of study: " + studyID + " of user: " + userID);
        return response;
    }

    /**
     * Handles requests for exporting a study of the currently logged in user.
     * @param studyID Id of the study to export as CSV.
     * @param principal Currently logged in user.
     * @return The string representing the study answers as CSV format.
     */
    @PreAuthorize("hasAuthority('USER')")
    @RequestMapping(value= EXPORT_STUDY, method = RequestMethod.POST)
    @ResponseBody
    public String handleExport(@RequestParam(PARAM_STUDY_ID) String studyID,
                               Principal principal) {
        assert studyID != null && principal != null;
        String response = null;

        String userID = principal.getName();

        if (databaseManager.containsStudy(studyID) && databaseManager.containsUser(userID) &&
                databaseManager.getUser(userID).isActive()) {
            Study study = databaseManager.getStudy(studyID);
            //Check if End date reached
            if (study.isFinished()) {
                StudyData data = databaseManager.getStudyData(studyID);

                response = data.toCSV();
            }
        }
        if (response != null) logger.debug("SUCCESS: Exported data of study: " + studyID);
        else logger.debug("FAILURE: Rejected request to export data of study: " + studyID);
        return response;
    }
}
