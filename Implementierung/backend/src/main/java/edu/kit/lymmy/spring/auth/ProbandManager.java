package edu.kit.lymmy.spring.auth;

import edu.kit.lymmy.shared.user.Proband;
import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x500.style.IETFUtils;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.cert.CertIOException;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509ExtensionUtils;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.Random;

/**
 * Utility class to manage proband X.509-Certificates.
 *
 * The Study-Leader got 100 Years range for changing the end date of a study into the future.
 * This case won't be checked as normally this won't occur because the certificates will be thrown
 * away after the study is finished.
 *
 * @author Moritz Hepp
 */
@DependsOn("rootConfiguration")
@Component
public class ProbandManager {

    private static final int CERTIFICATE_YEAR_DURATION = 100;
    private static final Logger logger = LoggerFactory.getLogger(ProbandManager.class);

    private final String ca_alias;

    private final char[] password;

    private long timestamp;
    private File truststoreFile;
    private KeyStore truststoreCache;
    private KeyStore keystore;

    private final CodeGenerator generator;
    private final X509Certificate caCertificate;

    private final String keyAlias;

    /**
     * Constructor to be called by the spring container.
     * @param keystorePath Path to the keystore file.
     * @param truststorepath Path to the truststore file.
     * @param password Password of truststore and keystore.
     * @param ca_alias Alias of the ca-certificate.
     * @param generator Bean for generating of codes.
     * @param loader Bean for loading resources; initialized from spring.
     * @throws IOException If the truststore or keystore files couldn't read.
     * @throws CertificateException If the ca certificate is not contained in the keystore.
     * @throws KeyStoreException If the KeyStore files are from the wrong format.
     * @throws NoSuchAlgorithmException If the given Algorithm for key generation is invalid.
     */
    @Autowired
    public ProbandManager(@Value("${server.ssl.key-store}") String keystorePath,
                          @Value("${server.ssl.key-alias}") String keyAlias,
                          @Value("${server.ssl.trust-store}") String truststorepath,
                          @Value("${lymmy.keystores.password}") String password,
                          @Value("${lymmy.orga.ca-alias}") String ca_alias,
                          CodeGenerator generator, ResourceLoader loader)
            throws IOException, CertificateException, KeyStoreException, NoSuchAlgorithmException, UnrecoverableKeyException {
        //Init truststore information
        this.password = password.toCharArray();
        this.truststoreCache = null;
        this.generator = generator;

        Resource res = loader.getResource(truststorepath);
        this.truststoreFile = res.getFile();
        this.timestamp = truststoreFile.lastModified();

        //Load Keystore containing ca certificate and localhost/teco certificate
        Resource resKeystore = loader.getResource(keystorePath);
        this.keystore = this.loadKeyStore(resKeystore.getFile());

        Enumeration<String> aliases = this.keystore.aliases();
        while(aliases.hasMoreElements()) {
            String alias = aliases.nextElement();
            PrivateKey privKey = (PrivateKey) this.keystore.getKey(alias, this.password);
            if (privKey == null) {
                logger.info("PrivateKey not present for: " + alias);
            } else {
                logger.info("PrivateKey present for: " + alias);
            }
        }

        assert this.keystore != null;

        //Load certificate authority file
        caCertificate = (X509Certificate) this.keystore.getCertificate(ca_alias);

        //Load client cert infos from ca cert
        this.ca_alias = ca_alias;
        this.keyAlias = keyAlias;

        logger.info("File: " + this.truststoreFile.getPath());
    }

    private KeyStore reloadTruststore() {
        //Load only if changed
        if (this.truststoreFile.lastModified() != timestamp || truststoreCache == null) {
            this.truststoreCache = this.loadKeyStore(this.truststoreFile);
        }
        return truststoreCache;
    }

    private KeyStore loadKeyStore(File keystoreFile) {
        KeyStore keyStore = null;
        try {
            FileInputStream is = new FileInputStream(keystoreFile);
            keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            keyStore.load(is, password);
        } catch (IOException | CertificateException | NoSuchAlgorithmException | KeyStoreException e) {
            logger.error("Failed to load KeyStore, please check setup.", e);
        }
        return keyStore;
    }

    private X509Certificate generateProbandCertificate(String probandID, PublicKey clientPubKey, LocalDateTime endDate)
            throws UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, OperatorCreationException,
            CertIOException, CertificateException, InvalidKeyException, NoSuchProviderException, SignatureException {
        Security.addProvider(new BouncyCastleProvider());

        PrivateKey caPrivKey = (PrivateKey) this.keystore.getKey(ca_alias, this.password);

        assert caPrivKey != null;

        X500NameBuilder nameBuilder = new X500NameBuilder();
        nameBuilder.addRDN(BCStyle.CN, probandID);
        nameBuilder.addRDN(BCStyle.O, this.keyAlias);

        X500Name subject = nameBuilder.build();

        logger.info("name: " + subject.toString());

        BigInteger serial = BigInteger.valueOf(new Random().nextInt());

        Date notAfter = this.calcCertDurationNotAfter(endDate);

        X509v3CertificateBuilder builder = new JcaX509v3CertificateBuilder(caCertificate, serial, Date.from(LocalDateTime.now().minusDays(1).atZone(ZoneId.systemDefault()).toInstant()), notAfter, subject, clientPubKey);

        ContentSigner sigGen;
        sigGen = new JcaContentSignerBuilder("SHA512withRSA").setProvider("BC").build(caPrivKey);

        builder.addExtension(Extension.subjectKeyIdentifier, false, new JcaX509ExtensionUtils().createSubjectKeyIdentifier(clientPubKey));
        builder.addExtension(Extension.authorityKeyIdentifier, false, new JcaX509ExtensionUtils().createAuthorityKeyIdentifier(caCertificate.getPublicKey()));


        X509CertificateHolder certHolder = builder.build(sigGen);

        X509Certificate cert = new JcaX509CertificateConverter().setProvider("BC").getCertificate(certHolder);

        try {
            cert.verify(caCertificate.getPublicKey());
        } catch (InvalidKeyException | NoSuchProviderException | SignatureException e) {
            logger.error("Failed to verify certificate. <" + e.getMessage() + ">");
            throw e;
        }

        return cert;
    }

    private void addProbandCertificateToTrusted(Proband proband, X509Certificate certificate /*, PrivateKey privKey */)
            throws IOException, KeyStoreException, CertificateException, NoSuchAlgorithmException, NoSuchProviderException {
        KeyStore truststore = this.reloadTruststore();

        Logger logger = LoggerFactory.getLogger(CodeGenerator.class);

        truststore.setCertificateEntry(proband.getId(), certificate);
        //truststore.setKeyEntry(proband.getId(), privKey, this.password, new Certificate[]{certificate});

        logger.info("Storing new certificate for proband: <" + proband.getId() + ">");

        this.storeTrustStoreCache(truststore);
    }

    /**
     * Generates a new Client Certificate for a Proband and stores this certificate inside the truststore.
     * @param studyID Study to register the proband for.
     * @param clientPubKey Public Key generated from the proband.
     * @param endDate End Date of the study.
     * @return Generated proband if generation of certificate finished, null otherwise.
     */
    public Proband generateProband(String studyID, PublicKey clientPubKey, LocalDateTime endDate) {
        String probandID = this.generator.generateProbandID();
        return this.generateProband(probandID, studyID, clientPubKey, endDate);
    }

    private Proband generateProband(String probandID, String studyID, PublicKey clientPubKey, LocalDateTime endDate) {
        assert clientPubKey != null && probandID != null && endDate != null;

        if (!clientPubKey.getAlgorithm().equals("RSA") && !clientPubKey.getFormat().equals("X.509")) {
            logger.error("Unsupported public key: PasswordHashAlgorithm=<" + clientPubKey.getAlgorithm() + ">, Format=<" + clientPubKey.getFormat() + ">");
            return null;
        }

        //Generate Certificate signed with ca private key
        X509Certificate certificate;
        try {
            certificate = this.generateProbandCertificate(probandID, clientPubKey, endDate);
        } catch (InvalidKeyException | UnrecoverableKeyException | NoSuchAlgorithmException | OperatorCreationException |
                KeyStoreException | CertificateException | CertIOException | SignatureException | NoSuchProviderException e) {
            logger.error("Failed to generate Proband certificate", e);
            return null;
        }

        Proband result = new Proband(probandID, studyID, certificate.getSerialNumber().toString());

        //Store Certificate to other client certificates
        try {
            this.addProbandCertificateToTrusted(result, certificate /*, pair.getPrivate() */);
        } catch (NoSuchProviderException | CertificateException | IOException | KeyStoreException | NoSuchAlgorithmException e) {
            logger.error("Failed to add certificate to trusted store", e);
            return null;
        }

        return result;
    }

    /**
     * Getter of the certificate of a already present proband.
     * @param probandID Id of a proband already registered.
     * @return Certificate of the proband.
     * @throws IllegalStateException If the Certificate stored is not X.509 formatted.
     */
    public X509Certificate getCertificate(String probandID) {
        KeyStore truststore = this.reloadTruststore();
        Certificate cert = null;
        try {
            cert = truststore.getCertificate(probandID);
        } catch (KeyStoreException e) {
            logger.error("Failed to get certificate of truststore", e);
        }

        X509Certificate result = null;
        if (cert != null) {
            if (X509Certificate.class.isInstance(cert)) {
                result = (X509Certificate) cert;
            } else {
                throw new IllegalStateException("Received non x.509 Certificate for proband: <" + probandID + ">");
            }
        }
        return result;
    }

    /**
     * Deletes the certificates of given Probands.
     * @param probands Probands to delete certificates of.
     */
    public void deleteCertificates(Proband... probands) {
        assert probands != null;
        KeyStore truststore = this.reloadTruststore();

        Arrays.stream(probands).forEach((proband) -> {
            try {
                truststore.deleteEntry(proband.getId());
            } catch (KeyStoreException e) {
                logger.error("Failed to delete certificate of proband: " + proband);
            }
        });
        try {
            this.storeTrustStoreCache(truststore);
        } catch (IOException | CertificateException | KeyStoreException | NoSuchAlgorithmException e) {
            logger.error("Failed to store truststore", e);
        }
    }

    private void storeTrustStoreCache(KeyStore truststore)
            throws IOException, CertificateException, NoSuchAlgorithmException, KeyStoreException {
        FileOutputStream fos = new FileOutputStream(truststoreFile);
        truststore.store(fos, password);
        fos.close();
    }

    private Date calcCertDurationNotAfter(LocalDateTime studyEndDate) {
        //Add The standard duration of a certificate and convert to Date object
        return Date.from(studyEndDate.plusYears(CERTIFICATE_YEAR_DURATION).atZone(ZoneId.systemDefault()).toInstant());
    }
}
