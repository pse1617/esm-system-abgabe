package edu.kit.lymmy.spring.auth;

import edu.kit.lymmy.shared.user.VerifyCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

/**
 * Utility class responsible for generating codes and id's.
 * Uses RSA and SHA1PRNG Algorithms.
 *
 * @author Moritz Hepp
 */
@Component
public final class CodeGenerator {

    private static final Logger logger = LoggerFactory.getLogger(CodeGenerator.class);

    public CodeGenerator() {
    }

    public String generateProbandID() {
        byte[] bytes = new byte[18];
        getStandard().nextBytes(bytes);
        String string = Base64Utils.encodeToString(bytes);
        //TODO Do the .p12 files need to be named as the proband?
        return string.replace("/", "-").toLowerCase();
    }

    /**
     * Generates a new random study ID.
     * @return New random study id.
     */
    public String generateStudyID() {
        byte[] bytes = new byte[6];
        getStandard().nextBytes(bytes);
        return Base64Utils.encodeToString(bytes);
    }

    /**
     * Generates a new verify code.
     * @param userID Id of the user to generate verify code for.
     * @return VerifyCode with given id; null if something fails.
     */
    public VerifyCode generateVerifyCode(String userID) {
        if (userID == null) throw new NullPointerException();
        byte[] bytes = new byte[30];
        getStandard().nextBytes(bytes);
        return new VerifyCode(userID, bytes);
    }

    /**
     * Generates a new RSA KeyPair.
     * @param keySize Size of the Keys.
     * @return KeyPair generated.
     */
    public KeyPair generateRSAKeyPair(int keySize) {
        try {
            KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", "BC");
            generator.initialize(keySize, getStandard());
            return generator.generateKeyPair();
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            logger.error("Failed to generate RSA KeyPair");
            return null;
        }
    }

    /**
     * Decodes a given RSA Key.
     * @param encoded Byte encoded public Key
     * @return PublicKey instance of encoded value.
     */
    public PublicKey decodePublicKey(byte[] encoded) {
        try {
            return KeyFactory.getInstance("RSA", "BC").generatePublic(new X509EncodedKeySpec(encoded));
        } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeySpecException e) {
            logger.error("Failed to decode public key", e);
            return null;
        }
    }

    /**
     * Getter of a SecureRandom SHA1PRNG algorithm instance.
     * @return Secure Random with SHA1PRNG algorithm.
     */
    public static SecureRandom getStandard() {
        try {
            return SecureRandom.getInstance("SHA1PRNG");
        } catch (NoSuchAlgorithmException e) {
            logger.error("Failed to initialize secure random", e);
            return new SecureRandom();
        }
    }
}
