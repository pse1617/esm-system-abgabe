package edu.kit.lymmy.spring.auth.encryption;

import org.springframework.security.crypto.bcrypt.BCrypt;

/**
 * Implementation of the PasswordEncryption class for the Bcrypt password encryption algorithm.
 *
 * @author Moritz Hepp
 */
public final class BcryptEncryptor extends PasswordEncryptor<BcryptEncryptionParams> {

    public BcryptEncryptor() {
        this(new BcryptEncryptionParams(15));
    }

    public BcryptEncryptor(BcryptEncryptionParams params) {
        super(params, PasswordHashAlgorithm.BCRYPT);
    }

    @Override
    public String encrypt(String plaintextPassword) {
        return BCrypt.hashpw(plaintextPassword, this.parameters.getSalt());
    }

    @Override
    public boolean check(String plaintextPassword, String expected) {
        if (plaintextPassword == null || expected == null)
            throw new IllegalArgumentException("One of the parameters is null");

        String encrypted = this.encrypt(plaintextPassword);
        return encrypted.equals(expected);
    }
}
