package edu.kit.lymmy.spring.database.mongodb;

import edu.kit.lymmy.shared.answer.QuestionnaireAnswer;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data interface for database access to a mongo repository.
 *
 * @author Moritz Hepp
 */
@Repository
public interface AnswerRepository extends MongoRepository<QuestionnaireAnswer, String> {
    List<QuestionnaireAnswer> findByProbandID(String probandID);
    Long deleteByProbandID(String probandID);
}
