package edu.kit.lymmy.spring.auth;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.util.Base64Utils;
import org.springframework.util.SerializationUtils;

import java.io.IOException;
import java.security.cert.X509Certificate;

/**
 * Deserializer class for X509Certificates. Used for Deserialization with Jackson while transferring Data between Backend
 * and App.
 *
 * @author Moritz Hepp
 */
public class CertificateDeserializer extends JsonDeserializer<X509Certificate> {
    @Override
    public X509Certificate deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        ObjectMapper mapper = (ObjectMapper) p.getCodec();

        ObjectNode obj = mapper.readTree(p);

        X509Certificate certificate = null;

        if (obj != null && obj.fields().hasNext() && obj.get("certificate") != null) {
            JsonNode node = obj.get("certificate");
            String certString = node.asText();

            if (certString != null && !certString.isEmpty()) certificate = (X509Certificate) SerializationUtils.deserialize(Base64Utils.decodeFromString(certString));
        } else {
            throw new JsonParseException(p, "Could not deserialize: <" + (obj == null ? null : obj.toString()) + "> to X509Certificate");
        }
        return certificate;
    }
}
