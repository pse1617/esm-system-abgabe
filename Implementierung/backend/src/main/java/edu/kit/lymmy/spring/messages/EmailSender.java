package edu.kit.lymmy.spring.messages;

import edu.kit.lymmy.shared.user.VerifyCode;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URLEncoder;
import java.text.MessageFormat;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;


import static edu.kit.lymmy.shared.messages.CommunicationConstants.EMAIL_REGISTER_VERIFY;

/**
 * Utility class for sending emails to a study leader.
 * Uses the VelocityEngine to load templated HTML-Files. 
 *
 * @author Moritz Hepp
 */
@Component
public final class EmailSender {

    private static final Logger logger = LoggerFactory.getLogger(EmailSender.class);

    private final JavaMailSender sender;
    private final VelocityEngine engine;
    private final String mail;
    private final String host;

    /**
     * Constructor to be called by the Spring IoC container.
     * @param mailSender The mailSender Bean initlialized by spring with the properties of the application.properties file.
     * @param mail Standard-Email of the company hosting this backend instance.
     * @param host Host name to be inserted into the templated email.
     */
    @Autowired
    public EmailSender(JavaMailSender mailSender,
                       @Value("${lymmy.orga.mail}") String mail,
                       @Value("${spring.mail.host}") String host) throws IOException {
        this.sender = mailSender;
        this.host = host;

        VelocityEngine engine = new VelocityEngine();

        engine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        engine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        engine.init();

        this.engine = engine;
        this.mail = mail;
    }

    /**
     * Loads and sends the templated email to the given user with verification code.
     * 
     * @param code Verification code to send with the templated email.
     * @return If the email was send or something went wrong.
     */
    public boolean sendEmail(final VerifyCode code) {
        if (code == null) throw new IllegalArgumentException("Argument is null");
        logger.info("Sending a email to <" + code.getId() + "> with verifyCode: <" + Base64Utils.encodeToString(code.getCode()) + ">");

        MimeMessagePreparator preparator = mimeMessage -> {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
            message.setTo(code.getId());
            message.setFrom(this.mail);
            message.setSubject("Lymmy user registration");

            VelocityContext context = new VelocityContext();

            String url = MessageFormat.format(EMAIL_REGISTER_VERIFY,
                    URLEncoder.encode(code.getId(), "UTF-8"),
                    URLEncoder.encode(Base64Utils.encodeToString(code.getCode()), "UTF-8"));

            context.put("url", url);
            context.put("code", Base64Utils.encodeToString(code.getCode()));
            context.put("host", host);

            Template template = this.engine.getTemplate("templates/EmailTemplate.vm");
            StringWriter writer = new StringWriter();

            template.merge(context, writer);

            message.setText(writer.toString(), true);
        };

        if (this.host != null) {
            if (this.host.equalsIgnoreCase("null")) {
                logger.warn("Sending no email because in debug mode and no mail server is configured");
                return true;
            } else {
                this.sender.send(preparator);
                return true;
            }
        } else {
            return false;
        }
    }
}
