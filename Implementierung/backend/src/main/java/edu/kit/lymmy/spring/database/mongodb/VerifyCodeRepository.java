package edu.kit.lymmy.spring.database.mongodb;

import edu.kit.lymmy.shared.user.VerifyCode;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data interface for database access to a mongo repository.
 *
 * @author Moritz Hepp
 */
@Repository
public interface VerifyCodeRepository extends MongoRepository<VerifyCode, String> {
    VerifyCode findById(String id);
}
