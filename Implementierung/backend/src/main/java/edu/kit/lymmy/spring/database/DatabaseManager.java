package edu.kit.lymmy.spring.database;

import com.mongodb.util.JSON;
import edu.kit.lymmy.shared.Study;
import edu.kit.lymmy.shared.answer.QuestionnaireAnswer;
import edu.kit.lymmy.shared.user.Proband;
import edu.kit.lymmy.spring.auth.user.User;
import edu.kit.lymmy.shared.user.VerifyCode;
import edu.kit.lymmy.spring.auth.ProbandManager;
import edu.kit.lymmy.spring.database.mongodb.AnswerRepository;
import edu.kit.lymmy.spring.database.mongodb.ProbandRepository;
import edu.kit.lymmy.spring.database.mongodb.StudyRepository;
import edu.kit.lymmy.spring.database.mongodb.UserRepository;
import edu.kit.lymmy.spring.database.mongodb.VerifyCodeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Service class managing the database access.
 *
 * @author Moritz Hepp
 */
@Service
public class DatabaseManager {

    private final ProbandRepository rProband;
    private final StudyRepository rStudy;
    private final AnswerRepository rAnswer;
    private final UserRepository rUser;
    private final VerifyCodeRepository rVerify;
    private final ProbandManager manager;

    private final Logger logger;

    /**
     * Constructor to be called by the spring container.
     *
     * @param rProband Spring data interface for probands.
     * @param rStudy Spring data interface for studies.
     * @param rAnswer Spring data interface for answers.
     * @param rUser Spring data interface for users.
     * @param rVerify Spring data interface for verification codes.
     * @param manager Bean for creating proband credentials.
     */
    @Autowired
    public DatabaseManager(ProbandRepository rProband, StudyRepository rStudy, AnswerRepository rAnswer,
                           UserRepository rUser, VerifyCodeRepository rVerify, ProbandManager manager) {
        this.rProband = rProband;
        this.rStudy = rStudy;
        this.rAnswer = rAnswer;
        this.rUser = rUser;
        this.rVerify = rVerify;
        this.manager = manager;

        logger = LoggerFactory.getLogger(DatabaseManager.class);
    }

    /**
     * Stores the proband if the given StudyID does exist and the probandID is unique.
     * @param proband Proband to store.
     * @return proband parameter if study with given ID doesn't exist, another proband,
     * if the given probandID does already exist.
     */
    public Proband storeProband(Proband proband) {
        if (proband == null) {
            throw new NullPointerException();
        }

        if (this.containsProband(proband.getId()) ||
                !this.containsStudy(proband.getStudyID())) {
            return proband;
        }
        Proband saved = rProband.save(proband);
        return proband.equals(saved) ? null : saved;
    }

    /**
     * Stores all valid answers given.
     * @param answers Answers to store.
     * @return Map containing Entries with given answers as keys and result of storing them as value.
     * For fast check if all answers were stored, check for Empty map.
     * @throws NullPointerException If an entry of the List is null.
     * @throws IllegalStateException If the database is corrupted.
     */
    public Map<QuestionnaireAnswer,QuestionnaireAnswer> storeQuestionnaireAnswers(QuestionnaireAnswer... answers) {
        if (answers == null || answers.length == 0) {
            throw new NullPointerException();
        }

        Map<QuestionnaireAnswer, QuestionnaireAnswer> responses = new Hashtable<>();
        for(QuestionnaireAnswer answer : answers) {
            QuestionnaireAnswer result = this.storeQuestionnaireAnswer(answer);
            if (result != null) responses.put(answer, result);
        }

        return responses;
    }

    /**
     * Stores a QuestionnaireAnswer if it's contents are consistent.
     * @param answer Answer to store.
     * @return answer, if it contains illegal information, null if it's stored.
     * @throws NullPointerException If parameter is null.
     * @throws IllegalStateException If the database is corrupted.
     */
    private  QuestionnaireAnswer storeQuestionnaireAnswer(QuestionnaireAnswer answer) {
        if (answer == null) {
            throw new NullPointerException();
        }

        //Check already contained | Answers shouldn't be overwritten
        if (this.containsAnswer(answer.getId())) {
            return answer;
        } else {
            //Check proband exists
            if (!this.containsProband(answer.getProbandID())) {
                return answer;
            }
            Proband proband = this.getProband(answer.getProbandID());

            //Check Study exists/Should be checked while proband is stored
            if (!this.containsStudy(proband.getStudyID())) {
                throw new IllegalStateException("Database contains Proband with illegal study id! Proband=" + JSON.serialize(proband));
            }
            Study study = this.getStudy(proband.getStudyID());

            if (study.getQuestionnaires().size() <= answer.getQuestionnaireIndex()) {
                return answer;
            }

            return answer.equals(this.rAnswer.save(answer)) ? null : answer;
        }
    }

    /**
     * Stores a Study if the ID doesn't already exist.
     * @param study Study to store.
     * @return null if study is stored, study if already present.
     * @throws NullPointerException If parameter is null.
     */
    public Study storeStudy(Study study) {
        if (study == null) {
            throw new NullPointerException();
        }

        //Studies shouldn't be overwritten
        if (this.containsStudy(study.getId()) ||
                !(this.containsUser(study.getUserID()))) {
            return study;
        }

        return study.equals(rStudy.save(study)) ? null : study;
    }

    /**
     * Stores a leader or overwrites the information of a existing leader.
     *
     * @param leader user to store
     * @return null if user was stored or already stored user who is now updated.
     */
    public User storeLeader(User leader) {
        if (leader == null) {
            throw new NullPointerException();
        }

        if (this.containsUser(leader.getId())) {
            logger.debug("Overwriting infos of user: <" + leader.getId() + ">");
        }
        //Users can be overwritten
        User saved = rUser.save(leader);
        return leader.equals(saved) ? null : saved;
    }

    /**
     * Stores or overwrites a verification code.
     *
     * @param verifyCode Code to store.
     * @return Null if code was stored; Overwritten verifyCode.
     */
    public VerifyCode storeVerifyCode(VerifyCode verifyCode) {
        if (verifyCode == null) throw new NullPointerException();

        if (this.containsVerifyCode(verifyCode.getId())) {
            logger.info("Overwriting user verification code: <" + verifyCode.getId() + ">");
        }

        //VerifyCodes can be overwritten
        VerifyCode saved = this.rVerify.save(verifyCode);
        return verifyCode.equals(saved) ? null : saved;
    }

    /**
     * Getter of the user with given id.
     * @param id Id/E-Mail of a user maybe stored.
     * @return Null if user with id is not present, the user with given id instead.
     */
    public User getUser(String id) {
        return rUser.findById(id);
    }

    /**
     * Getter of the answers to questions of the study with given id.
     * @param id Id of a study.
     * @return Null if study doesn't exist, empty if no probands were registered or study answers.
     */
    public StudyData getStudyData(String id) {
        if (id == null) {
            throw new NullPointerException();
        }

        if (!this.containsStudy(id)) {
            return null;
        }
        Study study = this.getStudy(id);

        if (!this.containsProbands(study.getId())) {
            return new StudyData(study, new ArrayList<>());
        }

        List<Proband> probands = this.rProband.findByStudyID(study.getId());

        List<QuestionnaireAnswer> allAnswers = new LinkedList<>();

        //Adds all Answers of each proband registered for a study
        probands.forEach((proband) -> allAnswers.addAll(rAnswer.findByProbandID(proband.getId())));

        return new StudyData(study, allAnswers);
    }

    /**
     * Getter of a study with given id.
     * @param id Id of study to get.
     * @return Study if present, null otherwise.
     */
    public Study getStudy(String id) {
        return rStudy.findById(id);
    }

    /**
     * Getter of all studies of a user.
     * @param userID Id of a present user.
     * @return List of all studies stored from this user.
     */
    public List<Study> getStudies(String userID) {
        List<Study> studies = rStudy.findByUserID(userID);
        studies.forEach(study -> study.setRegisteredProbands(rProband.findByStudyID(study.getId()).size()));
        return studies;
    }

    /**
     * Getter of a Proband.
     * @param probandID Id of a stored proband.
     * @return Proband if present, null otherwise.
     */
    public Proband getProband(String probandID) {
        return this.rProband.findById(probandID);
    }

    /**
     * Getter of a verifyCode.
     * @param userID Id of the user associated with the verifyCode.
     * @return verifyCode if user present, null otherwise.
     */
    public VerifyCode getVerifyCode(String userID) {
        return this.rVerify.findById(userID);
    }

    /**
     * Getter of all verify codes.
     * @return All verifyCodes currently stored.
     */
    public List<VerifyCode> getVerifyCodes() {
        return this.rVerify.findAll();
    }

    /**
     * Checks if a verify code with the given user id is stored.
     * @param userID Id of the user with this verify code.
     * @return If a verify code is present.
     */
    public boolean containsVerifyCode(String userID) {
        return this.rVerify.exists(userID);
    }

    /**
     * Checks if a study with the given id is stored.
     * @param id Id of the study.
     * @return If a study with given id is present.
     */
    public boolean containsStudy(String id) {
        return rStudy.exists(id);
    }

    /**
     * Checks if a answer with the given id is stored.
     * @param answerId Id of the answer.
     * @return If a answer with given id is present.
     */
    public boolean containsAnswer(String answerId) {
        return rAnswer.exists(answerId);
    }

    /**
     * Checks if a proband with the given id is stored.
     * @param probandID Id of the proband.
     * @return If a proband with given id is present.
     */
    public boolean containsProband(String probandID) {
        return this.rProband.findById(probandID) != null;
    }

    /**
     * Checks if a user with the given user id is stored.
     * @param userID Id of the user with this user id.
     * @return If a user with given id is present.
     */
    public boolean containsUser(String userID) {
        return this.rUser.findById(userID) != null;
    }

    /**
     * Checks if a study with the given id is stored.
     * @param studyID Id of the study.
     * @return If a study is present.
     */
    public boolean containsProbands(String studyID) {
        return !this.rProband.findByStudyID(studyID).isEmpty();
    }

    /**
     * Deletes the verify code with the given id.
     * @param id Id of the verify code to delete.
     */
    public void deleteVerifyCode(String id) {
        this.rVerify.delete(id);
    }

    /**
     * Deletes the leader with the given id, together with the verify codes and studies the user owns.
     * @param id User id.
     */
    public void deleteLeader(String id) {
        this.rUser.delete(id);
        this.rVerify.delete(id);
        this.rStudy.findByUserID(id).forEach((study) -> this.deleteStudy(study.getId()));
    }

    /**
     * Deletes the study, all probands registered for this study and all answers.
     * @param id Id of the study to delete
     */
    public void deleteStudy(String id) {
        if (id != null && this.containsStudy(id)) {
            //TODO Maybe: Archive
            logger.debug("NOTE: UI should show a Notification before delete of a study!");

            this.rStudy.delete(id);
            List<Proband> probands = this.rProband.findByStudyID(id);
            probands.forEach((proband -> this.rAnswer.deleteByProbandID(proband.getId())));
            this.rProband.deleteByStudyID(id);

            this.manager.deleteCertificates(probands.toArray(new Proband[probands.size()]));
        }
    }
}
