package edu.kit.lymmy.spring.database.mongodb;

import edu.kit.lymmy.shared.Study;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data interface for database access to a mongo repository.
 *
 * @author Moritz Hepp
 */
@Repository
public interface StudyRepository extends MongoRepository<Study, String> {
    Study findById(String id);
    List<Study> findByUserID(String id);
}
