package edu.kit.lymmy.spring;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.cert.X509Certificate;
import java.util.stream.Stream;
import javax.annotation.PostConstruct;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.module.SimpleModule;
import edu.kit.lymmy.spring.auth.CertificateDeserializer;
import edu.kit.lymmy.spring.auth.CertificateSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.util.FileCopyUtils;

/**
 * Configuration class of the backend.
 * Enables the scheduling tasks for deleting expired verification codes. Loads the system properties of the application.properties resource file.
 * Configures spring to search inside the claspath for classes annotated with {@link org.springframework.stereotype.Component}
 * or {@link org.springframework.stereotype.Service}.
 * Initializes the configuration files inside the <code>.lymmy</code>-directory of the user home.
 * 
 * @author Moritz Hepp
 */
@Configuration
@PropertySource("classpath:application.properties")
@EnableScheduling
@ComponentScan
public class RootConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(RootConfiguration.class);

    private final ResourceLoader loader;
    private final String lymmaDataHome;

    /**
     * Constructor of this configuration class. Will be called by the spring IoC (Inversion of Control) container. The parameters will be injected by the container.
     * @param loader Initialized and configured by spring.
     * @param lymmyDataHome Property of the application.properties file.
     */
    @Autowired
    public RootConfiguration(ResourceLoader loader,
                             @Value("${lymmy.data.home}") String lymmyDataHome) {
        this.loader = loader;
        this.lymmaDataHome = lymmyDataHome;
    }

    /**
     * Initializes the ${user.home}/.lymmy directory and copies ressource-Templates to that destination. The PostConstruct annotation flags this method to be called while Spring initializes the Beans.
     * @throws IOException If the writing or reading of template/config files fails.
     **/
    @PostConstruct
    public void init() throws IOException {
        //TODO outsource configuration of files to application.properties (Path to "ca certificate and" localhost certificate)
        File dataHome = loader.getResource(lymmaDataHome).getFile();
        File keystoreDir = new File(dataHome, "keystore");

        if (dataHome != null) {
            //Check if firs exist and are not empty

            if (!dataHome.exists() && !dataHome.mkdir()) {
                throw new IOException("Could not create lymmy data root file directory: " + dataHome.getAbsolutePath());
            }

            //Get all template files of keystore configuration
            PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(loader);
            Resource[] resources = resolver.getResources("classpath:keystore/*.*");

            if (!keystoreDir.exists() && !keystoreDir.mkdir()) {
                throw new IOException("Could not create lymmy data keystore root file directory: " + keystoreDir.getAbsolutePath());
            }

            Stream<Path> paths = Files.find(Paths.get(keystoreDir.toURI()), 1, (path, bfa) -> true);

            logger.info("Copy files to <" + keystoreDir.getAbsolutePath() + ">");

            //Copy all ressources to root file
            for (Resource resource : resources) {
                File file = new File(keystoreDir.getAbsoluteFile() + System.getProperty("file.separator") + resource.getFilename());

                paths = paths.filter((path) -> path.toFile().getName().equals(resource.getFilename()));

                if (!file.exists()) {
                    int bytes = FileCopyUtils.copy(resource.getInputStream(),
                            new FileOutputStream(file));

                    logger.info("Copied: <" + resource.getFilename() + "> with <" + bytes + "> bytes");
                }
            }

            paths.forEach(path -> logger.info("Deleting: <" + path.toString() + "> return: <" + path.toFile().delete() + ">"));
        }
    }

    /**
     * Creates a module bean for the Jackson configuration. Registers {@link CertificateSerializer} and {@link CertificateDeserializer}.
     */
    @Bean
    public Module myDeSerializater() {
        SimpleModule module = new SimpleModule();
        module.addSerializer(X509Certificate.class, new CertificateSerializer());
        module.addDeserializer(X509Certificate.class, new CertificateDeserializer());
        return module;
    }
}
