package edu.kit.lymmy.spring.auth.encryption;

/**
 * Encryption Params for a encryption algorithm.
 * Abstraction to allow easy adding of Algorithms.
 *
 * @author Moritz Hepp
 */
public abstract class EncryptionParams {
    private final PasswordHashAlgorithm algorithm;

    EncryptionParams(PasswordHashAlgorithm algorithm) {
        this.algorithm = algorithm;
    }
}
