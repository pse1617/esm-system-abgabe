package edu.kit.lymmy.spring.auth.user;


import edu.kit.lymmy.spring.auth.encryption.EncryptionParams;

/**
 * User class for user management inside the backend.
 *
 * @author Moritz Hepp
 */
public final class User {

    private final String id;
    private final String password;
    private EncryptionParams params;
    private boolean active;

    /**
     * Default Condtructor as needed from Spring Data.
     */
    public User() {
        this.id = null;
        this.password = null;
        this.params = null;
        this.active = false;
    }

    /**
     * Constructor with initialization of inactive user.
     *
     * Verification Code can be null or empty, as at the registration a User without verificationCode will be
     * send to the backend.
     *
     * @param id Id of the user.
     * @param password Encrypted password of the user.
     * @param params Encryption Parameters of the user.
     */
    public User(String id, String password, EncryptionParams params) {
        if (id == null || id.isEmpty() ||
                password == null || params == null)
            throw new IllegalArgumentException("id: " + id + ", password: " + password + ", params: " + params);
        this.id = id;
        this.password = password;
        this.params = params;
        this.active = false;
    }

    /**
     * Constructor with given credentials.
     *
     * @param id Id of the user.
     * @param password Encrypted password of the user.
     * @param params EncryptionParameters of the user.
     * @param active State of the user.
     */
    public User(String id, String password, EncryptionParams params, boolean active) {
        this(id, password, params);
        this.active = active;
    }

    /**
     * @return Gets the Password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * @return Gets the id.
     */
    public String getId() {
        return id;
    }

    /**
     * @return Gets the Encryption Parameters of this user.
     */
    public EncryptionParams getParams() {
        return params;
    }

    /**
     * @return Gets the state of the user.
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Sets the state of the user.
     * @param value State to change the user to.
     */
    public void setActive(boolean value) {
        this.active = value;
    }
}
