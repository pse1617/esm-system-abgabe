package edu.kit.lymmy.spring.auth.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.kit.lymmy.shared.messages.CommunicationConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Handles the succeed of a login.
 *
 * @author Moritz Hepp
 */
@Component
public class SavedRequestAwareAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler{

    private RequestCache requestCache;
    private final ObjectMapper objectMapper;

    @Autowired
    public SavedRequestAwareAuthenticationSuccessHandler(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
        this.requestCache = new HttpSessionRequestCache();
    }

    @Override
    public void onAuthenticationSuccess(
            HttpServletRequest request,
            HttpServletResponse response,
            Authentication authentication) throws ServletException, IOException {

        //write response OK
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        objectMapper.writeValue(response.getWriter(), CommunicationConstants.LoginResult.OK);

        SavedRequest savedRequest = requestCache.getRequest(request, response);

        if (savedRequest == null) {
            super.clearAuthenticationAttributes(request);
            return;
        }

        String targetUrlParam = getTargetUrlParameter();

        if (isAlwaysUseDefaultTargetUrl() || (targetUrlParam != null && StringUtils.hasText(targetUrlParam))) {
            requestCache.removeRequest(request, response);
            super.clearAuthenticationAttributes(request);
            return;
        }

        super.clearAuthenticationAttributes(request);
    }

    public void setRequestCache(RequestCache requestCache) {
        this.requestCache = requestCache;
    }
}
