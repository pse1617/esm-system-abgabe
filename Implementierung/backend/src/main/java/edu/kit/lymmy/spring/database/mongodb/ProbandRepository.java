package edu.kit.lymmy.spring.database.mongodb;

import edu.kit.lymmy.shared.user.Proband;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data interface for database access to a mongo repository.
 *
 * @author Moritz Hepp
 */
@Repository
public interface ProbandRepository extends MongoRepository<Proband, String> {
    Proband findById(String probandID);
    List<Proband> findByStudyID(String studyID);
    long deleteByStudyID(String studyID);
}
