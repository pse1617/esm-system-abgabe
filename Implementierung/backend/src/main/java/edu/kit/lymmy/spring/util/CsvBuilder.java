package edu.kit.lymmy.spring.util;

import edu.kit.lymmy.shared.Questionnaire;
import edu.kit.lymmy.shared.Study;
import edu.kit.lymmy.shared.answer.Answer;
import edu.kit.lymmy.shared.answer.QuestionnaireAnswer;
import edu.kit.lymmy.shared.question.Question;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Helper class to convert a study and its answers to csv format
 *
 * @author Lukas
 * @since 06.02.2017
 */
public class CsvBuilder {
    private static final char SEPARATOR = ',';
    private static final char LINE_END = '\n';
    private static final char DELIMITER = '"';
    private final Study study;
    private final List<QuestionnaireAnswer> answers;
    private final Map<Integer, Integer> questionnaireToColumnMap;
    private final int columnCount;
    private final StringBuilder builder;
    private int currentCellCount;

    /**
     * Prepares building a csv-formatted String
     *
     * @param study   the study to build for (title of the columns)
     * @param answers the answers (content of the columns)
     */
    public CsvBuilder(@NotNull Study study, @NotNull List<@NotNull QuestionnaireAnswer> answers) {
        this.study = study;
        this.answers = answers;
        answers.sort(Comparator.comparing(QuestionnaireAnswer::getTimestamp));
        answers.sort(Comparator.comparing(QuestionnaireAnswer::getProbandID));
        questionnaireToColumnMap = new HashMap<>();
        int columnIndex = 2;
        List<Questionnaire> questionnaires = study.getQuestionnaires();
        for (int i = 0; i < questionnaires.size(); i++) {
            Questionnaire questionnaire = questionnaires.get(i);
            questionnaireToColumnMap.put(i, columnIndex);
            columnIndex += questionnaire.getQuestions().size() + questionnaire.getContextSensors().size();
        }
        columnCount = columnIndex;
        builder = new StringBuilder();
        currentCellCount = 0;
    }

    private void titleRow() {
        addValue("Proband-ID").addValue("Timestamp");
        study.getQuestionnaires().forEach(questionnaire -> addValues(questionnaire.getQuestions().stream().map(Question::getText).collect(Collectors.toList()))
                .addValues(questionnaire.getContextSensors()));
        newLine();
    }

    private void answer(@NotNull QuestionnaireAnswer answer) {
        addValue(answer.getProbandID()).addValue(answer.getTimestamp().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME))
                .emptyCells(questionnaireToColumnMap.get(answer.getQuestionnaireIndex()));
        List<Answer> answers = answer.getAnswers();
        answers.sort(Comparator.comparing(Answer::getQuestionIndex));
        for (Answer a : answers) {
            emptyCells(a.getQuestionIndex());
            addValue(String.valueOf(a.getChoice()));
        }
        addValues(answer.getContextData())
                .newLine();
    }

    @NotNull
    private CsvBuilder addValues(@NotNull List<String> values) {
        values.forEach(this::addValue);
        return this;
    }

    @NotNull
    private CsvBuilder addValue(@Nullable String value) {
        int length = builder.length();
        if (builder.length() != 0 && builder.charAt(length - 1) != LINE_END) {
            builder.append(SEPARATOR);
        }
        if (value != null) {
            builder.append(DELIMITER).append(value.replace("\"", "\"\"")).append(DELIMITER);
        }
        currentCellCount++;
        return this;
    }

    @NotNull
    private CsvBuilder emptyCells(int until) {
        for (; currentCellCount < until; currentCellCount++) {
            builder.append(SEPARATOR);
        }
        return this;
    }

    @NotNull
    private CsvBuilder newLine() {
        emptyCells(columnCount);
        builder.append(LINE_END);
        currentCellCount = 0;
        return this;
    }

    /**
     * @return a csv-formatted String with the contents of this builder
     */
    @NotNull
    public String build() {
        titleRow();
        answers.forEach(this::answer);
        return builder.toString();
    }
}
