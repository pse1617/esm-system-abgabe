package edu.kit.lymmy.spring.scheduling;

import edu.kit.lymmy.spring.database.DatabaseManager;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Class for scheduled execution of tasks.
 *
 * @author Moritz Hepp
 */
@Component
public final class ScheduledExecutor {

    private static final Logger logger = LoggerFactory.getLogger(ScheduledExecutor.class);

    private final DatabaseManager manager;

    /**
     * Constructor of the ScheduledExecutor. Will be called by the Spring IoC (Inversion of Control) container.
     * @param manager Bean/Component initialized by the Spring container.
     */
    @Autowired
    public ScheduledExecutor(DatabaseManager manager) {
        this.manager = manager;
    }

    /**
     * Deletes expired verify codes every day.
     */
    @Scheduled(cron = "0 0 0 1/1 * ?")
    public void clearVerifyCodes() {
        logger.info("----- Clearing verify Codes " + LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        manager.getVerifyCodes().forEach(code -> {
            if (code.isExpired()) {
                logger.info("Removing <" + code.getId() + ">");
                manager.deleteVerifyCode(code.getId());
            } else {
                logger.info("Not Expired: <" + code.getId() + ">");
            }
        });
        logger.info("----- Finished Clearing -----");
    }
}
