package edu.kit.lymmy.spring.auth.encryption;

import edu.kit.lymmy.spring.auth.CodeGenerator;
import org.springframework.security.crypto.bcrypt.BCrypt;

/**
 * Implementation of EncryptionParams class for the Bcrypt password encryption algorithm.
 *
 * @author Moritz Hepp
 */
public class BcryptEncryptionParams extends EncryptionParams {
    private final String salt;

    public BcryptEncryptionParams() {
        super(PasswordHashAlgorithm.BCRYPT);
        this.salt = null;
    }

    public BcryptEncryptionParams(String salt) {
        super(PasswordHashAlgorithm.BCRYPT);
        this.salt = salt;
    }

    public BcryptEncryptionParams(int cost) {
        super(PasswordHashAlgorithm.BCRYPT);
        if (cost < 4 || cost > 31)
            throw new IllegalArgumentException(", cost: " + cost);
        this.salt = BCrypt.gensalt(cost, CodeGenerator.getStandard());
    }

    public String getSalt() {
        return salt;
    }

    @Override
    public boolean equals(Object o) {
        return BcryptEncryptionParams.class.isInstance(o) &&
                this.salt.equals(((BcryptEncryptionParams)o).salt);
    }
}
