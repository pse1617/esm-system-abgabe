package edu.kit.lymmy.spring.auth.security;

import edu.kit.lymmy.spring.auth.encryption.BcryptEncryptionParams;
import edu.kit.lymmy.spring.auth.encryption.BcryptEncryptor;
import edu.kit.lymmy.spring.auth.encryption.PasswordEncryptor;
import edu.kit.lymmy.spring.auth.user.User;
import edu.kit.lymmy.spring.database.DatabaseManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedCredentialsNotFoundException;
import org.springframework.stereotype.Component;

/**
 * Provides the authentication of a user and probands. Will be called if someone tries to authenticate via
 * mutual authentication or username password.
 *
 * @author Moritz Hepp
 */
@Component
public class BackendAuthenticationProvider implements AuthenticationProvider, UserDetailsService {

    private static final Logger logger = LoggerFactory.getLogger(BackendAuthenticationProvider.class);

    private final DatabaseManager manager;

    @Autowired
    public BackendAuthenticationProvider(DatabaseManager manager) {
        this.manager = manager;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Authentication result = null;
        if (UsernamePasswordAuthenticationToken.class.isInstance(authentication)) {
            if (manager.containsUser(authentication.getName())) {
                logger.info("Authenticating: username=<" + authentication.getName() + ">");

                User user = manager.getUser(authentication.getName());
                if (!user.isActive()) {
                    throw new DisabledException("User " + authentication.getName() + " is not active");
                }
                String password = authentication.getCredentials().toString();

                if (!BcryptEncryptionParams.class.isInstance(user.getParams()))
                    throw new AuthenticationCredentialsNotFoundException("Couldn't cast to BcryptEncryptionParams");

                PasswordEncryptor encryptor = new BcryptEncryptor((BcryptEncryptionParams) user.getParams());

                if (encryptor.check(password, user.getPassword())) {
                    result = new UsernamePasswordAuthenticationToken(user.getId(), user.getPassword(), AuthorityUtils.createAuthorityList("USER"));
                } else {
                    throw new BadCredentialsException("Credentials don't equal");
                }
            } else {
                throw new UsernameNotFoundException("Unable to find user " + authentication.getName());
            }
        } else if (PreAuthenticatedAuthenticationToken.class.isInstance(authentication)) {
            logger.info("Preauthentication: " + authentication.getName());
            if (manager.containsProband(authentication.getName().replace("\"", ""))) {
                logger.info("Found");
                result = new PreAuthenticatedAuthenticationToken(authentication.getName(), "", AuthorityUtils.createAuthorityList("ROLE_PROBAND"));
            } else {
                logger.error("Couldn't find");
                throw new PreAuthenticatedCredentialsNotFoundException("Couldn't find proband: <" + authentication.getName() + ">");
            }
        }
        return result;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return UsernamePasswordAuthenticationToken.class.equals(authentication) ||
                PreAuthenticatedAuthenticationToken.class.equals(authentication);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (manager.containsProband(username)) {
            return new org.springframework.security.core.userdetails.User(username, "", AuthorityUtils.createAuthorityList("PROBAND"));
        } else {
            throw new UsernameNotFoundException("Couldn't find proband: <" + username + ">");
        }
    }
}
