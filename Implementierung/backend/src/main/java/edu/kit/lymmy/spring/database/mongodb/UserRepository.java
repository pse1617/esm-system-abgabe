package edu.kit.lymmy.spring.database.mongodb;

import edu.kit.lymmy.spring.auth.user.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data interface for database access to a mongo repository.
 *
 * @author Moritz Hepp
 */
@Repository
public interface UserRepository extends MongoRepository<User, String> {
    User findById(String id);
}
