package edu.kit.lymmy.spring.auth.encryption;

/**
 * Enumeration of password hash algorithms provided by the backend.
 *
 * @author Moritz Hepp
 */
public enum PasswordHashAlgorithm {
    BCRYPT
}
