package edu.kit.lymmy.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
* Entry Point for the Application start.
* Starts the backend application with spring boot.
* Loads the Configuration class {@link RootConfiguration}.
* @author Moritz Hepp
* @version 0.0.1-SNAPSHOT
*/
@SpringBootApplication(scanBasePackageClasses = RootConfiguration.class)
public class SpringBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBackendApplication.class, args);
	}

}
