package edu.kit.lymmy.spring.auth.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import org.springframework.stereotype.Component;


import static edu.kit.lymmy.shared.messages.CommunicationConstants.LoginResult.BAD_CREDENTIALS;
import static edu.kit.lymmy.shared.messages.CommunicationConstants.LoginResult.NOT_ACTIVE;
import static edu.kit.lymmy.shared.messages.CommunicationConstants.LoginResult.NOT_FOUND;

/**
 * Handles the authentication failures to show the backend what happened.
 *
 * @author Lukas
 */
@Component
public class StatusCodeAuthenticationFailureHandler implements AuthenticationFailureHandler {

    private final ObjectMapper objectMapper;

    @Autowired
    public StatusCodeAuthenticationFailureHandler(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        //write status based on exception class
        response.setStatus(HttpServletResponse.SC_OK);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        if(exception instanceof BadCredentialsException){
            objectMapper.writeValue(response.getWriter(), BAD_CREDENTIALS);
        } else if(exception instanceof DisabledException) {
            objectMapper.writeValue(response.getWriter(), NOT_ACTIVE);
        }else if(exception instanceof UsernameNotFoundException){
            objectMapper.writeValue(response.getWriter(), NOT_FOUND);
        }else {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}
