#!/bin/bash
set -e
DOMAIN="psews16esm2.teco.edu"
KEYSTOREPW="changeit"
LIVE="/etc/letsencrypt/live/$DOMAIN"
ALIAS="psews16esm2.teco.edu"

mkdir -p "$HOME/keystores"
cd "$HOME/keystores"

# Hardcoded Password!
echo "teco" | sudo -S openssl pkcs12 -export -in "$LIVE/fullchain.pem" -inkey "$LIVE/privkey.pem" -out cert_and_key.p12 -name "$ALIAS" -CAfile "$LIVE/chain.pem" -caname root -password pass:"$KEYSTOREPW"
echo "teco" | sudo -S keytool -importkeystore -destkeystore keystore.jks -srckeystore cert_and_key.p12 -srcstoretype PKCS12 -alias "$ALIAS" -srcstorepass "$KEYSTOREPW" -deststorepass "$KEYSTOREPW" -destkeypass "$KEYSTOREPW"
echo "teco" | sudo -S keytool -import -noprompt -trustcacerts -alias rlsoot -file "$LIVE/chain.pem" -keystore keystore.jks -srcstorepass "$KEYSTOREPW" -deststorepass "$KEYSTOREPW" -destkeypass "$KEYSTOREPW"

#sudo openssl pkcs12 -export -in "$LIVE/fullchain.pem" -inkey "$LIVE/privkey.pem" -out pkcs.p12 -name tomcat -password pass:"$KEYSTOREPW"
#sudo keytool -importkeystore -destkeystore keystore.jks -srckeystore pkcs.p12 -srcstoretype PKCS12 -alias tomcat -srcstorepass "$KEYSTOREPW" -deststorepass "$KEYSTOREPW" -destkeypass "$KEYSTOREPW"
#sudo openssl pkcs12 -export -in "$LIVE/fullchain.pem" -inkey "$LIVE/privkey.pem" -out pkcs.p12 -name s1as -password pass:"$KEYSTOREPW"
#sudo keytool -importkeystore -destkeystore keystore.jks -srckeystore pkcs.p12 -srcstoretype PKCS12 -alias s1as -srcstorepass "$KEYSTOREPW" -deststorepass "$KEYSTOREPW" -destkeypass "$KEYSTOREPW"

echo "teco" | sudo -S keytool -list -keystore keystore.jks -storepass "$KEYSTOREPW"