#!/bin/bash
# Stop running containers
sudo docker stop lymmy-backend
sudo docker stop lymmy-webclient
sudo docker stop mongodb

# Delete containers
sudo docker rm lymmy-backend
sudo docker rm lymmy-webclient

# Delete old images
sudo docker image rm -f moblaa/lymmy-backend
sudo docker image rm -f moblaa/lymmy-webclient
sudo docker image rm -f lymmy-backend
sudo docker image rm -f lymmy-webclient

# Re-create JavaKeystore files
wget https://bitbucket.org/pse1617/esm-system-abgabe/downloads/convertPEMtoJKS.sh -O convertPEMtoJKS.sh
chmod +x convertPEMtoJKS.sh
./convertPEMtoJKS.sh

if [ "$(sudo docker ps -a | grep mongodb)" ]; then 
    sudo docker start mongodb
else
    sudo docker run -p 27017:27017 --name mongodb -d mongo
fi

rm -rf backend_keystores
rm -rf webclient_keystores

mv -f keystores backend_keystores
cp -r backend_keystores webclient_keystores

sudo docker run -v $HOME/backend_keystores:/root/.lymmy/keystore --name lymmy-backend --net="host" -d moblaa/lymmy-backend:latest
sudo docker run -v $HOME/webclient_keystores:/root/.lymmy/keystore --name lymmy-webclient --net="host" -d moblaa/lymmy-webclient:latest