#!/bin/bash
#"backend-selfsigned" "webclient-selfsigned"
NAMES=("lymmy-backend" "lymmy-webclient" )
#"moblaa/backend-selfsigned" "moblaa/webclient-selfsigned"
NAMESWITHPREFIXES=("moblaa/lymmy-backend" "moblaa/lymmy-webclient" "${NAMES[@]}")

read -p "Upload to Docker hub?[y,n]" up
if [[ "$up" == "y" || "$up" == "Y" ]]; then
    result=0
    while [[ "$result" != "Login Succeeded" ]]; do
        read -p "Username: " USERNAME
        read -sp "Password: " PASSWORD
        echo ""
        result=$(sudo docker login -u "$USERNAME" -p "$PASSWORD")
    done
fi

# Stop running containers
for NAME in "${NAMES[@]}"
do
    echo "Stop $NAME"
    sudo docker stop "$NAME"
done

# Delete containers
for NAME in "${NAMES[@]}"
do
    echo "Remove $NAME"
    sudo docker rm "$NAME"
done

# Delete old images / Selfsigned
for NAME in "${NAMESWITHPREFIXES[@]}"
do
    echo "Remove image $NAME"
    sudo docker image rm -f "$NAME"
done

# Rebuild from dockerfile's
echo "Build lymmy-backend"
#--no-cache
sudo docker build -t lymmy-backend BackendDocker/
echo "Build lymmy-webclient"
#--no-cache
sudo docker build -t lymmy-webclient WebclientDocker/
#echo "Build backend-selfsigned"
#sudo docker build --no-cache -t backend-selfsigned SelfSignedDocker/Backend/
#echo "Build webclient-selfsigned"
#sudo docker build --no-cache -t webclient-selfsigned SelfSignedDocker/Webclient/

if [[ "$up" == "y" || "$up" == "Y" ]]; then
  # Read image id's and tag
    for NAME in "${NAMES[@]}"
    do
        if [ -n "$(sudo docker images | grep "$NAME")" ]; then
            ID="$(sudo docker images | grep "$NAME" | sed -rn 's/.*latest\s+([^ ]+)\s+[0-9]+.*/\1/p')"
            sudo docker tag "$ID" "$USERNAME/$NAME:latest"
            sudo docker push "$USERNAME/$NAME"
        else
            echo "No ID found for: $NAME"
        fi
    done
fi
