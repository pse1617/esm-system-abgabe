#!/bin/bash
# Stop running containers
sudo docker stop lymmy-backend
sudo docker stop lymmy-webclient
sudo docker stop mongodb

# Delete containers
sudo docker rm lymmy-backend
sudo docker rm lymmy-webclient

# Delete old images
sudo docker image rm -f moblaa/backend-selfsigned
sudo docker image rm -f moblaa/webclient-selfsigned
sudo docker image rm -f backend-selfsigned
sudo docker image rm -f webclient-selfsigned

if [ "$(sudo docker ps -a | grep mongodb)" ]; then 
    sudo docker start mongodb
else
    sudo docker run -p 27017:27017 --name mongodb -d mongo
fi

sudo docker run --name backend-selfsigned --net="host" -d moblaa/backend-selfsigned:latest
sudo docker run --name webclient-selfsigned --net="host" -d moblaa/webclient-selfsigned:latest
