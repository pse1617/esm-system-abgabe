﻿using Lymmy.view.Language;
using Xamarin.Forms;

namespace Lymmy.view
{
	/// <summary>
	/// NoQuestionnaire page appears when there is no availabe questionnaire to be answered.
	/// </summary>
	public partial class NoQuestionnairePage : ContentPage
	{
		private App app = (Lymmy.App)Application.Current;
		private ToolbarItem settings = new ToolbarItem();

		/// <summary>
		/// Initializes a new instance of the <see cref="T:Lymmy.view.NoQuestionnairePage"/> class.
		/// This page includes a toolbarItem, which refers to preferences, and a label, which shows user
		/// that there is no available questionnaire to be answered.
		/// </summary>
		public NoQuestionnairePage()
		{
			InitializeComponent();
			this.ToolbarItems.Add(settings);
			settings.Clicked += (sender, args) =>
				   {
					   Navigation.PushModalAsync(new NavigationPage(app.preferences));
				   };
		}

		/// <summary>
		/// Refreshs the page with the given language.
		/// </summary>
		private void refreshPage()
		{
			label.Text = AppResources.labelInNoQuestionnairePage;
			settings.Text = AppResources.preferences;
		    Title = AppResources.NoQuestionnairePageTitle;
		}

		/// <summary>
		/// Updates page when appears.
		/// </summary>
		protected override void OnAppearing()
		{
			base.OnAppearing();
			this.refreshPage();
		}
	}
}
