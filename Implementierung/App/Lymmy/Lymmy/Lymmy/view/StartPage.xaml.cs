﻿using Lymmy.view.Language;
using Xamarin.Forms;

namespace Lymmy.view
{
	/// <summary>
	/// Start page appears when the user joins a study, which has not reached its start time yet.
	/// </summary>
	public partial class StartPage : ContentPage
	{
		private App app = (Lymmy.App)Application.Current;
		private ToolbarItem settings = new ToolbarItem();

		/// <summary>
		/// Initializes a new instance of the <see cref="T:Lymmy.view.StartPage"/> class.
		/// This page includes a toolbarItem, which refers to preferences, and a label, which shows user
		/// the start time of the study.
		/// </summary>
		public StartPage()
		{
			InitializeComponent();
			this.ToolbarItems.Add(settings);
			settings.Clicked += (sender, args) =>
				   {
					   Navigation.PushModalAsync(new NavigationPage(app.preferences));
				   };
		}

		/// <summary>
		/// Refreshs the page with the given language.
		/// </summary>
		private void refreshPage()
		{
			label.Text = AppResources.labelInStartPage + app.pageBuilder.startTime.ToLocalTime().ToString();
			settings.Text = AppResources.preferences;
		    Title = AppResources.StartPageTitle;
		}

		/// <summary>
		/// Updates page when appears.
		/// </summary>
		protected override void OnAppearing()
		{
			base.OnAppearing();
			this.refreshPage();
		}
	}
}
