﻿using System.Collections.Generic;
using Xamarin.Forms;
using XLabs.Forms.Controls;
using Lymmy.view.Language;


namespace Lymmy.view
{
	/// <summary>
	/// Multiple choice question page.
	/// </summary>
	public partial class MultipleChoiceQuestionPage : ContentPage
	{
		private App app = (Lymmy.App)Application.Current;
		private ToolbarItem settings = new ToolbarItem();

		/// <summary>
		/// Initializes a new instance of the <see cref="T:Lymmy.view.MultipleChoiceQuestionPage"/> class.
		/// </summary>
		public MultipleChoiceQuestionPage()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="T:Lymmy.view.MultipleChoiceQuestionPage"/> class with the 
		/// given question and choices.
		/// </summary>
		/// <param name="question">Question.</param>
		/// <param name="choices">Choices of the question.</param>
		public MultipleChoiceQuestionPage(string question, List<string> choices)
		{
			InitializeComponent();
			this.refreshPage();
			this.ToolbarItems.Add(settings);
			settings.Clicked += (sender, args) =>
				   {
					   Navigation.PushModalAsync(new NavigationPage(app.preferences));
				   };
			label.Text = question;

			CheckBox[] cho = new CheckBox[choices.Count];
			for (int i = 0; i < cho.Length; i++)
			{
				cho[i] = new CheckBox();
				cho[i].DefaultText = choices[i];
				/*makes the length of answer pass to the screen*/
				cho[i].HorizontalOptions = LayoutOptions.FillAndExpand;
				/*makes the text to be seen in each device*/
				cho[i].TextColor = Device.OnPlatform(Color.Black, Color.Black, Color.White);
				stack.Children.Add(cho[i]);
			}

			/*when the answer is confirmed, go to next question*/
			btn.Clicked += (sender, args) =>
	  			{
					  app.pageBuilder.mul.answer(this.getAnswer(cho));
					  app.pageBuilder.nextPage();
	  			};

		}

		/// <summary>
		/// Gets the chosen choices.
		/// </summary>
		/// <returns>The list of choiceIndexes.</returns>
		/// <param name="choices">Choices.</param>
		public SortedSet<int> getAnswer(CheckBox[] choices)
		{
			SortedSet<int> answer = new SortedSet<int>();
			for (int i = 0; i < choices.Length; i++)
			{
				if (choices[i].Checked)
				{
					answer.Add(i);
				}
			}
			return answer;
		}

		/// <summary>
		/// Refreshs the page with the given language.
		/// </summary>
		private void refreshPage()
		{
			Title = AppResources.MultipleChoicePageTitle;
			btn.Text = AppResources.btnToConfirm;
			settings.Text = AppResources.preferences;
		}

		/// <summary>
		/// Updates page when appears.
		/// </summary>
		protected override void OnAppearing()
		{
			base.OnAppearing();
			this.refreshPage();
		}
	}
}
