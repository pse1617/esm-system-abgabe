﻿using System;
using System.Collections.Generic;
using Lymmy.view.Language;

using Xamarin.Forms;

namespace Lymmy.view
{
	/// <summary>
	/// Likert question page.
	/// </summary>
	public partial class LikertQuestionPage : ContentPage
	{
		private App app = (Lymmy.App)Application.Current;
		private ToolbarItem settings = new ToolbarItem();

		/// <summary>
		/// Initializes a new instance of the <see cref="T:Lymmy.view.LikertQuestionPage"/> class with the given question.
		/// This page is used to show the tedency of user. The answer, which is shown by radio button, can also be a 
		/// degree of something.
		/// </summary>
		/// <param name="question">Question.</param>
		/// <param name="min">Left text.</param>
		/// <param name="max">Right text.</param>
		/// <param name="steps">Number of choices</param>
		public LikertQuestionPage(String question, String min, String max, int steps)
		{
			InitializeComponent();
			label.Text = question;
			left.Text = min;
			right.Text = max;

			this.ToolbarItems.Add(settings);
			settings.Clicked += (sender, args) =>
				   {
					   Navigation.PushModalAsync(new NavigationPage(app.preferences));
				   };

			List<String> l = new List<string>(steps);

			for (int i = 0; i < steps; i++)
			{
				l.Insert(i, "");
			}

			answer.ItemsSource = l;
			btn.IsEnabled = false;
			answer.CheckedChanged += (sender, args) => { btn.IsEnabled = true; };
			/*When the answer is confirmed, go to next question*/
			btn.Clicked += (sender, args) =>
				  {
					  app.pageBuilder.step.answer(answer.SelectedIndex);
					  app.pageBuilder.nextPage();
				  };

		}

		/// <summary>
		/// Refreshs the page with the given language.
		/// </summary>
		private void refreshPage()
		{
			Title = AppResources.LikertPageTitle;
			btn.Text = AppResources.btnToConfirm;
			settings.Text = AppResources.preferences;
		}

		/// <summary>
		/// Updates page when appears.
		/// </summary>
		protected override void OnAppearing()
		{
			base.OnAppearing();
			this.refreshPage();
		}
	}
}

