﻿using System;
using System.Collections.Generic;
using Lymmy.view.Language;

using Xamarin.Forms;

namespace Lymmy.view
{
	/// <summary>
	/// Yes or no question page.
	/// </summary>
	public partial class YesOrNoQuestionPage : ContentPage
	{
		private App app = (Lymmy.App)Application.Current;
		private ToolbarItem settings = new ToolbarItem();

		/// <summary>
		/// Initializes a new instance of the <see cref="T:Lymmy.YesOrNoQuestionPage"/> class with the given question.
		/// This page is similar to SingleChoiceQuestionPage, but only includes two choices.
		/// </summary>
		/// <param name="question">Question.</param>
		public YesOrNoQuestionPage(String question)
		{
			InitializeComponent();
			label.Text = question;

			this.ToolbarItems.Add(settings);
			settings.Clicked += (sender, args) =>
				   {
					   Navigation.PushModalAsync(new NavigationPage(app.preferences));
				   };
			btn.IsEnabled = false;
			answer.CheckedChanged += (sender, args) => { btn.IsEnabled = true; };
			/*when the answer is confirmed, go to next question*/
			btn.Clicked += (sender, args) =>
	  			{
					  app.pageBuilder.sin.answer(answer.SelectedIndex);
					  app.pageBuilder.nextPage();
	  			};
			this.refreshPage();
		}

		/// <summary>
		/// Refreshs the page with the given language.
		/// </summary>
		private void refreshPage()
		{
			Title = AppResources.YesOrNoPageTitle;
			btn.Text = AppResources.btnToConfirm;
			settings.Text = AppResources.preferences;
			//only these two choices are available in Page
			answer.ItemsSource = new[]
			{
				AppResources.YesChoice,
				AppResources.NoChoice
			};
		}

		/// <summary>
		/// Updates page when appears.
		/// </summary>
		protected override void OnAppearing()
		{
			base.OnAppearing();
			this.refreshPage();
		}
	}

}
