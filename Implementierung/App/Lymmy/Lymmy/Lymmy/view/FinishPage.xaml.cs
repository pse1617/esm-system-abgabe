﻿using Lymmy.view.Language;
using Xamarin.Forms;

namespace Lymmy.view
{
	/// <summary>
	/// Finish page appears when there is no more questions for user to answer.
	/// </summary>
	public partial class FinishPage : ContentPage
	{
		private App app = (Lymmy.App)Application.Current;
		private ToolbarItem settings = new ToolbarItem();

		/// <summary>
		/// Initializes a new instance of the <see cref="T:Lymmy.view.FinishPage"/> class.
		/// This page includes a toolbarItem, which refers to preferences, and a label, which shows user
		/// that no more question is left to answer.
		/// </summary>
		public FinishPage()
		{
			InitializeComponent();
			this.ToolbarItems.Add(settings);
			settings.Clicked += (sender, args) =>
				   {
					   Navigation.PushModalAsync(new NavigationPage(app.preferences));
				   };
		}

		/// <summary>
		/// Refreshs the page with the given language.
		/// </summary>
		private void refreshPage()
		{
			Title = AppResources.FinishPageTitle;
			label.Text = AppResources.labelInFinishPage;
			settings.Text = AppResources.preferences;
		}

		/// <summary>
		/// Updates page when appears.
		/// </summary>
		protected override void OnAppearing()
		{
			base.OnAppearing();
			this.refreshPage();
		}
	}
}
