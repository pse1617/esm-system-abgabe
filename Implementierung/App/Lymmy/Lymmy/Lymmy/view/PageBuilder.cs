﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using edu.kit.lymmy.shared.question;
using edu.kit.lymmy.xamarin.model;
using edu.kit.lymmy.xamarin.model.question;
using Xamarin.Forms;
using edu.kit.lymmy.xamarin.presenter;

namespace Lymmy.view
{

	/// <summary>
	/// Page builder gets information from the presenter and uses them to creat pages.
	/// </summary>
	public class PageBuilder
	{
		private App app = (App)Application.Current;
		public QuestionnaireAM questionnaire;
		public QuestionAM question;
		public StepQuestionAM step;
		public MultipleChoiceQuestionAM mul;
		public SingleChoiceQuestionAM sin;
		public OpenQuestionAM open;

		public PageBuilder() { }

		public PageBuilder(QuestionnaireAM questionnaire)
		{
			this.questionnaire = questionnaire;
		}

		/// <summary>
		/// Asks for required information to build different kinds of question page.
		/// </summary>
		public async void nextPage()
		{
			if (questionnaire != null)
			{
				if (await questionnaire.moveNextQuestion())
				{
					question = questionnaire.getCurrentQuestion();
					String questionText = question.questionText;
					List<String> choices;
					var type = question.getType();
					switch (type)
					{
						case QuestionType.LIKERT:
							step = (StepQuestionAM)question;
							question = step;
							String min = step.leftSliderText;
							String max = step.rigthSliderText;
							int steps = step.steps;
							app.MainPage = new NavigationPage(new LikertQuestionPage(questionText, min, max, steps));
							break;
						case QuestionType.SINGLE_CHOICE:
							ChoiceQuestionAM sinP = (ChoiceQuestionAM)question;
							choices = sinP.answers;
							sin = (SingleChoiceQuestionAM)question;
							app.MainPage = new NavigationPage(new SingleChoiceQuestionPage(questionText, choices));
							break;
						case QuestionType.MULTIPLE_CHOICE:
							ChoiceQuestionAM mulP = (ChoiceQuestionAM)question;
							choices = mulP.answers;
							mul = (MultipleChoiceQuestionAM)question;
							app.MainPage = new NavigationPage(new MultipleChoiceQuestionPage(questionText, choices));
							break;
						case QuestionType.SLIDER:
							step = (StepQuestionAM)question;
							String left = step.leftSliderText;
							String right = step.rigthSliderText;
							app.MainPage = new NavigationPage(new SliderQuestionPage(questionText, left, right));
							break;
						case QuestionType.OPEN:
							open = (OpenQuestionAM)question;
							app.MainPage = new NavigationPage(new OpenQuestionPage(questionText));
							break;
						case QuestionType.YES_NO:
							sin = (SingleChoiceQuestionAM)question;
							app.MainPage = new NavigationPage(new YesOrNoQuestionPage(questionText));
							break;
						default:
							app.MainPage = new NavigationPage(new FinishPage());
							break;
					}
				}
				else
				{
					app.MainPage = new NavigationPage(new FinishPage());
				}
			}
			else
			{
				app.MainPage = new NavigationPage(new NoQuestionnairePage());
			}
		}

		/// <summary>
		/// Gets the study with the given studyID.
		/// </summary>
		/// <returns><c>true</c>, if study was gotten, <c>false</c> otherwise.</returns>
		/// <param name="study">Study.</param>
		public async Task<bool> getStudy(String study)
		{
			if (await StudyBuilderP.inputStudyID(study))
			{
				startTime = await PreferencesManagerP.getStartStudyDate();
				return true;
			}
			return false;
		}

		/// <summary>
		/// Gets or sets the start time of study.
		/// </summary>
		/// <value>The start time.</value>
		public DateTime startTime { get; set; }

		/// <summary>
		/// Checks if it is time to start the study.
		/// </summary>
		/// <returns><c>true</c>, if reaches the start time of study, <c>false</c> otherwise.</returns>
		public bool studyStart()
		{
			if (app.contextData.time >= startTime)
			{
				return true;
			}
			return false;
		}

		/// <summary>
		/// Ends the study.
		/// </summary>
		public void endStudy()
		{
			app.MainPage = new NavigationPage(new LymmyPage());
			StudyBuilderP.endStudyManually();
			app.preferences.StudyID = "";
			questionnaire = null;
		}

		/// <summary>
		/// Receives the questionnaire to be answered.
		/// </summary>
		public void receivequestionnaire(QuestionnaireAM questionnaireP)
		{
			questionnaire = questionnaireP;
		}
	}
}

