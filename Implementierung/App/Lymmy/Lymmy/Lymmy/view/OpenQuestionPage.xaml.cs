﻿using System;
using Lymmy.view.Language;
using Xamarin.Forms;

namespace Lymmy.view
{
	/// <summary>
	/// Open question page.
	/// </summary>
	public partial class OpenQuestionPage : ContentPage
	{
		private App app = (Lymmy.App)Application.Current;
		private ToolbarItem settings = new ToolbarItem();

		/// <summary>
		/// Initializes a new instance of the <see cref="T:Lymmy.TextQuestionPage"/> class with the given question.
		/// This page includes an entry for user to give the answer.
		/// </summary>
		/// <param name="question">Question.</param>
		public OpenQuestionPage(String question)
		{
			InitializeComponent();
			label.Text = question;

			this.ToolbarItems.Add(settings);
			settings.Clicked += (sender, args) =>
				   {
					   Navigation.PushModalAsync(new NavigationPage(app.preferences));
				   };
			btn.IsEnabled = false;
			entry.TextChanged += (sender, args) => { btn.IsEnabled = true; };
			/*when the answer is confirmed, go to next question*/
			btn.Clicked += (sender, args) =>
	  			{
					  app.pageBuilder.open.answer(entry.Text);
					  app.pageBuilder.nextPage();
	  			};

		}

		/// <summary>
		/// Refreshs the page with the given language.
		/// </summary>
		private void refreshPage()
		{
			Title = AppResources.OpenQuestionPageTitle;
			entry.Placeholder = AppResources.entryInOpenQuestion;
			btn.Text = AppResources.btnToConfirm;
			settings.Text = AppResources.preferences;
		}

		/// <summary>
		/// Updates page when appears.
		/// </summary>
		protected override void OnAppearing()
		{
			base.OnAppearing();
			this.refreshPage();
		}
	}
}
