﻿using System;
using Lymmy.view.Language;
using Xamarin.Forms;

namespace Lymmy.view
{
	/// <summary>
	/// Slider question page.
	/// </summary>
	public partial class SliderQuestionPage : ContentPage
	{
		private App app = (Lymmy.App)Application.Current;
		private ToolbarItem settings = new ToolbarItem();
		private int answer;

		/// <summary>
		/// Initializes a new instance of the <see cref="T:Lymmy.view.SliderQuestionPage"/> class with the given question.
		/// This page includes a slider, which is initialized with the given minimum and maximum. The answer can also be
		/// a degree of something.
		/// </summary>
		/// <param name="question">Question.</param>
		/// <param name="min">Minimum.</param>
		/// <param name="max">Maximum.</param>
		public SliderQuestionPage(String question, String min, String max)
		{
			InitializeComponent();
			label.Text = question;
			left.Text = min;
			right.Text = max;

			this.ToolbarItems.Add(settings);
			settings.Clicked += (sender, args) =>
				   {
					   Navigation.PushModalAsync(new NavigationPage(app.preferences));
				   };
			btn.IsEnabled = false;
			/*When the answer is confirmed, go to next question*/
			btn.Clicked += (sender, args) =>
			{
				app.pageBuilder.step.answer(answer);
				app.pageBuilder.nextPage();
			};

		}

		/// <summary>
		/// Refreshs the page with the given language.
		/// </summary>
		private void refreshPage()
		{
			Title = AppResources.SliderPageTitle;
			btn.Text = AppResources.btnToConfirm;
			settings.Text = AppResources.preferences;
		}

		/// <summary>
		/// When the slider value changes, the corresponding answer also changes.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="args">Arguments.</param>
		private void OnSliderValueChanged(object sender, ValueChangedEventArgs args)
		{
			answer = Convert.ToInt32(args.NewValue);
			btn.IsEnabled = true;
		}

		/// <summary>
		/// Updates page when appears.
		/// </summary>
		protected override void OnAppearing()
		{
			base.OnAppearing();
			this.refreshPage();
		}
	}
}
