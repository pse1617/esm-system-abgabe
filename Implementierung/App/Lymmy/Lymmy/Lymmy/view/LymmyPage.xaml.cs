﻿using Xamarin.Forms;
using Lymmy.view.Language;

namespace Lymmy.view
{
	/// <summary>
	/// Lymmy page is the start page of this application.
	/// </summary>
	public partial class LymmyPage : ContentPage
	{
		private App app = (Lymmy.App)Application.Current;
		private ToolbarItem settings = new ToolbarItem() { AutomationId = "settingsToolBarItem" };

		/// <summary>
		/// Initializes a new instance of the <see cref="T:Lymmy.view.LymmyPage"/> class.
		/// This page includes a toolbarItem, which refers to preferences, an entry for giving the study
		/// number to login a study and a button to confirm the input.
		/// </summary>
		public LymmyPage()
		{
			InitializeComponent();

			this.ToolbarItems.Add(settings);
			settings.Clicked += (sender, args) =>
				   {
					   Navigation.PushModalAsync(new NavigationPage(app.preferences));
				   };
			btn.IsEnabled = false;
			entry.TextChanged += (sender, args) => { btn.IsEnabled = true; };
			/*the current studyID is confirmed*/
			btn.Clicked += (sender, args) => { this.startStudy(entry.Text); };

			this.refreshPage();
		}

		/// <summary>
		/// If the given studyID is correct, turns into a question page. Otherwise the start page remains.
		/// </summary>
		async void startStudy(string study)
		{
			if (await app.pageBuilder.getStudy(study))
			{
				app.preferences.StudyID = study;
				if (app.pageBuilder.studyStart())
				{
					app.pageBuilder.nextPage();
				}
				else
				{
					app.MainPage = new NavigationPage(new StartPage());
				}
			}
			else
			{
				//Uncorrect studyID leads to a warning.
				await this.DisplayAlert(AppResources.displayAlertTitle, AppResources.displayAlertUncorrectContentInLymmy, "OK");
			}
		}

		/// <summary>
		/// Refreshs the page with the given language.
		/// </summary>
		private void refreshPage()
		{
			Title = AppResources.LymmyPageTitle;
			settings.Text = AppResources.preferences;
			label.Text = AppResources.labelInLymmyPage;
			entry.Placeholder = AppResources.entryInLymmyPage;
			btn.Text = AppResources.btnToJoin;
		}

		/// <summary>
		/// Updates page when appears.
		/// </summary>
		protected override void OnAppearing()
		{
			base.OnAppearing();
			this.refreshPage();
		}

	}
}
