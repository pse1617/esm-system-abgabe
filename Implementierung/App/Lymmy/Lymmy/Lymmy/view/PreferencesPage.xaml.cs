﻿using System;
using Lymmy.view.Language;
using edu.kit.lymmy.xamarin.presenter;

using Xamarin.Forms;

namespace Lymmy.view
{
	/// <summary>
	/// Preferences page.
	/// </summary>
	public partial class PreferencesPage : ContentPage
	{
		private App app = (Lymmy.App)Application.Current;
		private ToolbarItem back = new ToolbarItem();
		/// <summary>
		/// Initializes a new instance of the <see cref="T:Lymmy.view.PreferencesPage"/> class.
		/// </summary>
		public PreferencesPage()
		{
			InitializeComponent();
			/* returns to the original question page*/
			this.ToolbarItems.Add(back);
			back.Clicked += async (sender, args) =>
				   {
					   await Navigation.PopModalAsync();
				   };
			wlan.OnChanged += (sender, args) =>
			{
				PreferencesManagerP.setWlan(wlan.On);
			};
			data.Tapped += (sender, args) =>
				 {
					 this.getID();
				 };
			btnToLogout.Clicked += async (sender, args) =>
				  {
					  if (!this.StudyID.Equals(""))
					  {
						  var action = await this.DisplayActionSheet(AppResources.logoutMessage, AppResources.cancelLogout, AppResources.confirmLogout);
						  if (action != null)
						  {
							  if (action.Equals(AppResources.confirmLogout))
							  {
								  app.pageBuilder.endStudy();
								  await this.DisplayAlert(AppResources.displayAlertTitleOfEndStudy, AppResources.displayAlertContentOfEndStudy, "OK");
							  }
						  }
					  }
					  else
					  {
						  await this.DisplayAlert(AppResources.displayAlertTitle, AppResources.displayAlertNoContentInPreferences, "OK");
					  }
				  };
			this.refreshPage();

		}

		/// <summary>
		/// Gets or sets the study identifier.
		/// </summary>
		/// <value>The study identifier.</value>
		public String StudyID { get; set; }

		/// <summary>
		/// Gets different dialog box depending on whether user joins in a study.
		/// </summary>
		private void getID()
		{
			if (!this.StudyID.Equals(""))
			{
				this.DisplayAlert(AppResources.displayAlertTitleInPreferences, this.StudyID, "OK");
			}
			else
			{
				this.DisplayAlert(AppResources.displayAlertTitle, AppResources.displayAlertNoContentInPreferences, "OK");
			}
		}

		/// <summary>
		/// When the picker selected index changed.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="args">Arguments.</param>
		void OnPickerSelectedIndexChanged(object sender, EventArgs args)
		{
			picker = (Picker)sender;
			int selectedIndex = picker.SelectedIndex;
			if (selectedIndex == -1)
				return;
			string lan = picker.Items[selectedIndex];
			switch (picker.Items[picker.SelectedIndex])
			{
				case "Deutsch":
					var de = new System.Globalization.CultureInfo("de-DE");
					DependencyService.Get<ILocalize>().SetLocale(de);
					AppResources.Culture = de;
					break;
				case "English":
					var en = new System.Globalization.CultureInfo("en-US");
					DependencyService.Get<ILocalize>().SetLocale(en);
					AppResources.Culture = en;
					break;
			}
			this.refreshPage();
		}

		private void refreshPage()
		{
			Title = AppResources.PreferencesPageTitle;
			back.Text = AppResources.back;
			settings.Title = AppResources.settings;
			wlan.Text = AppResources.wlan;
			info.Title = AppResources.info;
			data.Text = AppResources.dataText;
			data.Detail = AppResources.dataDetail;
			language.Title = AppResources.language;
			picker.Title = AppResources.picker;
			logout.Title = AppResources.logout;
			btnToLogout.Text = AppResources.btnToLogout;
		}

	}
}
