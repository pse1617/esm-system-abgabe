﻿using System;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Lymmy.networking
{
    public class DateTimeJsonConverter : JsonConverter
    {
        public static DateTimeJsonConverter Instance { get; } = new DateTimeJsonConverter();

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            DateTime dateTime = (DateTime) value;
            int[] converted =
                {dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second};
            serializer.Serialize(writer, converted);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            int[] array = JArray.Load(reader).ToObject<int[]>();
            if (array.Length > 5)
            {
                return new DateTime(array[0], array[1], array[2], array[3], array[4], array[5]);
            }
            if (array.Length > 3)
            {
                return new DateTime(array[0], array[1], array[2]);
            }
            return null;
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof(DateTime).GetTypeInfo().IsAssignableFrom(objectType.GetTypeInfo());
        }
    }
}