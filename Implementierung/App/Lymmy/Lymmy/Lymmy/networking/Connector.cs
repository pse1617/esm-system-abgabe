﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using edu.kit.lymmy.shared;
using edu.kit.lymmy.shared.answer;
using edu.kit.lymmy.shared.messages;
using edu.kit.lymmy.shared.user;
using Lymmy.persistence;
using ModernHttpClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.X509;

namespace Lymmy.networking
{
    public class Connector
    {
        private readonly HttpClient client;

        public static Connector Instance { get; } = new Connector();

        private Connector()
        {
            client = new HttpClient(new NativeMessageHandler());
        }

        public async Task<bool> ShouldDownloadStudy(string studyId)
        {
            string response = await Send(CommunicationConstants.PROBAND_GET_STARTDATE, null);
            if (response == "") return false;
            DateTime startDate = JacksonProcessor.DeserializeObject<DateTime>(response);
            if (startDate == default(DateTime)) return false;
            DateTime downloadAfter = startDate.AddDays(-1);
            return downloadAfter <= DateTime.Now;
        }

        public async Task<Study> DownloadStudy(string studyId)
        {
            string response = await Send(CommunicationConstants.PROBAND_GET_STUDY, null);
            if (response == "") return null;
            return JacksonProcessor.DeserializeObject<Study>(response);
        }

        public async Task<bool> IsValidStudyId(string studyId)
        {
            ProbandDetails details = await Register(studyId);
            bool result = details != null;
            if (result)
            {
                await DataManager.Instance.Clear();
                await DataManager.Instance.SetStudyId(studyId);
                await DataManager.Instance.SetProbandDetails(details);
            }
            return result;
        }

        private async Task<ProbandDetails> Register(string studyId)
        {
            /*
            var generator = new RsaKeyPairGenerator();
            var exponent = BigInteger.ValueOf(65537);
            var param = new RsaKeyGenerationParameters(exponent, SecureRandom.GetInstance("SHA256PRNG"), 2048, 5);
            generator.Init(param);
            var keyPair = generator.GenerateKeyPair();
            
            var info = SubjectPublicKeyInfoFactory.CreateSubjectPublicKeyInfo(keyPair.Public);
            var encoded = Convert.ToBase64String(info.GetEncoded());
            */
            Dictionary<string, string> parameters = new Dictionary<string, string>
            {
                {CommunicationConstants.PARAM_STUDY_ID, studyId}
            };
            string response = await Send(CommunicationConstants.PROBAND_REGISTER,
                new FormUrlEncodedContent(parameters));

            return response == ""
                ? null
                : JacksonProcessor.DeserializeObject<ProbandDetails>(response);
        }

        public async Task<bool> SendAnswers(IList<QuestionnaireAnswer> answer)
        {
            string contentString = JacksonProcessor.SerializeObject(answer);
            StringContent content = new StringContent(contentString);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            string response = await Send(CommunicationConstants.PROBAND_STORE_ANSWER, content);
            try
            {
                JacksonProcessor.DeserializeObject<int>(response);
                return true;
            }
            catch (JsonException)
            {
                return false;
            }
        }

        public async Task<DateTime> GetStudyStartDate()
        {
            string response = await Send(CommunicationConstants.PROBAND_GET_STARTDATE, null);
            Debug.WriteLine("RESPONSE: " + response);
            return JacksonProcessor.DeserializeObject<DateTime>(response);
        }

        private async Task<string> Send(string url, HttpContent content)
        {
            try
            {
                HttpRequestMessage message = new HttpRequestMessage
                {
                    RequestUri = new Uri(CommunicationConstants.HOST + url),
                    Method = HttpMethod.Post,
                    Content = content
                };
                ProbandDetails details = await DataManager.Instance.GetProbandDetails();
                Debug.WriteLine("Details: " + (details == null));
                if (details != null)
                {
                    Debug.WriteLine("Adding id: " + details.Proband.Id);
                    message.Headers.Add(CommunicationConstants.HEADER_PROBAND_ID, details.Proband.Id);
                }
                HttpResponseMessage responseMessage = await client.SendAsync(message);
                return await responseMessage.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }
    }
}