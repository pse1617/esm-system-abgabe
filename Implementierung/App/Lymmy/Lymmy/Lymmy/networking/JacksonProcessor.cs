﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Lymmy.networking
{
    public class JacksonProcessor
    {
        private static readonly JsonSerializerSettings settings = new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.Auto,
            Converters = {DateTimeJsonConverter.Instance},
            ContractResolver = new CamelCasePropertyNamesContractResolver()
        };

        private static string PreProcess(string jacksonJson)
        {
            Regex regex = new Regex("\"@class\":\"([^\"]*)\"");
            string output = regex.Replace(jacksonJson, "\"$type\":\"$1, Shared\"");
            return output;
        }

        private static string PostProcess(string netJson)
        {
            Regex regex = new Regex("\"$type\":\"([^\"]*), Shared\"");
            string output = regex.Replace(netJson, "\"@class\":\"$1\"");
            return output;
        }

        public static T DeserializeObject<T>(string jacksonJson)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(PreProcess(jacksonJson), settings);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return default(T);
            }
        }

        public static string SerializeObject(object o)
        {
            try
            {
                return PostProcess(JsonConvert.SerializeObject(o, settings));
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }
    }
}