﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using edu.kit.lymmy.shared;
using edu.kit.lymmy.shared.answer;
using edu.kit.lymmy.shared.user;
using Lymmy.networking;
using Newtonsoft.Json;
using PCLStorage;

namespace Lymmy.persistence
{
    public class DataManager
    {
        private const string StudyId = "StudyId";
        private const string Study = "Study";
        private const string ProbandData = "Proband";
        private const string Folder = "Lymmy";
        private const string CurrentQuestionnaire = "CurrentQuestionnaire";
        private const string AnswerFolder = "Answers";
        private const string StudyStart = "StudyStart";
        private const string StartDate = "StartDateStudy";
        private readonly JsonSerializerSettings settings;

        private DataManager()
        {
            settings = new JsonSerializerSettings {TypeNameHandling = TypeNameHandling.Auto};
        }

        public static DataManager Instance { get; } = new DataManager();

        public async Task<DateTime> GetStudyStartDate()
        {
            string serialized = await Load(StartDate);
            Debug.WriteLine(serialized);
            return JacksonProcessor.DeserializeObject<DateTime>(serialized);
        }

        public async Task SetStudyStartDate(DateTime time)
        {
            await Store(StartDate, JacksonProcessor.SerializeObject(time));
        }
        
        /// <returns>The saved study ID, or an empty string if there is none</returns>
        public async Task<string> GetStudyId()
        {
            return await Load(StudyId);
        }

        public async Task SetStudyId(string id)
        {
            await Store(StudyId, id);
        }

        public async Task<Study> GetStudy()
        {
            string s = await Load(Study);
            return s == null ? null : JsonConvert.DeserializeObject<Study>(s, settings);
        }

        public async Task SetStudy(Study study)
        {
            await Store(Study, JsonConvert.SerializeObject(study, settings));
        }

        public async Task<int> GetCurrentQuestionnaire()
        {
            string s = await Load(CurrentQuestionnaire);
            return s == "" ? -1 : Int32.Parse(s);
        }

        public async Task SetCurrentQuestionnaire(int current)
        {
            await Store(CurrentQuestionnaire, current.ToString());
        }

        public async Task<ProbandDetails> GetProbandDetails()
        {
            string s = await Load(ProbandData);
            return s == "" ? null : JsonConvert.DeserializeObject<ProbandDetails>(s, settings);
        }

        public async Task SetProbandDetails(ProbandDetails details)
        {
            await Store(ProbandData, JsonConvert.SerializeObject(details, settings));
        }

        public async Task PutQuestionnaireAnswer(QuestionnaireAnswer answer)
        {
            IFolder answerFolder =
                await (await GetLymmyFolder()).CreateFolderAsync(AnswerFolder, CreationCollisionOption.OpenIfExists);
            await Store(answerFolder, answer.Id, JsonConvert.SerializeObject(answer, settings));
        }

        public async Task<bool> RemoveQuestionnaireAnswer(QuestionnaireAnswer answer)
        {
            return await IfAnswerFolderExists(
                async folder => await IfFileExists(folder, answer.Id, async file =>
                {
                    await file.DeleteAsync();
                    return true;
                }));
        }

        public async Task<IList<QuestionnaireAnswer>> GetQuestionnaireAnswers()
        {
            return await IfAnswerFolderExists(async folder =>
            {
                List<QuestionnaireAnswer> result = new List<QuestionnaireAnswer>();
                IList<IFile> files = await folder.GetFilesAsync();
                foreach (IFile file in files)
                {
                    try
                    {
                        QuestionnaireAnswer answer =
                            JsonConvert.DeserializeObject<QuestionnaireAnswer>(await file.ReadAllTextAsync());
                        result.Add(answer);
                    }
                    catch (JsonException)
                    {
                        //Cannot read file, it is probably invalid
                        await file.DeleteAsync();
                    }
                }
                return result;
            }) ?? new List<QuestionnaireAnswer>();
        }

        public async Task SetStudyStartTriggered(bool wasTrigggered)
        {
            await Store(StudyStart, wasTrigggered.ToString());
        }

        public async Task<bool> GetStudyStartTriggered()
        {
            string s = await Load(StudyStart);
            return s != "" && Boolean.Parse(s);
        }

        public async Task<bool> Clear()
        {
            return await IfLymmyFolderExists(async folder =>
            {
                await folder.DeleteAsync();
                return true;
            });
        }

        private async Task<IFolder> GetLymmyFolder()
        {
            IFolder root = FileSystem.Current.LocalStorage;
            return await root.CreateFolderAsync(Folder, CreationCollisionOption.OpenIfExists);
        }

        private async Task Store(string name, string content)
        {
            await Store(await GetLymmyFolder(), name, content);
        }

        private async Task Store(IFolder folder, string name, string content)
        {
            IFile file = await folder.CreateFileAsync(name, CreationCollisionOption.ReplaceExisting);
            await file.WriteAllTextAsync(content);
        }

        private async Task<string> Load(string name)
        {
            return await IfLymmyFolderExists(
                       async folder => await IfFileExists(folder, name, async file => await file.ReadAllTextAsync())) ??
                   "";
        }

        private async Task<T> IfLymmyFolderExists<T>(Func<IFolder, Task<T>> func)
        {
            IFolder root = FileSystem.Current.LocalStorage;
            if (await root.CheckExistsAsync(Folder) == ExistenceCheckResult.FolderExists)
            {
                return await func(await root.GetFolderAsync(Folder));
            }
            return default(T);
        }

        private async Task<T> IfAnswerFolderExists<T>(Func<IFolder, Task<T>> func)
        {
            return await IfLymmyFolderExists(async folder =>
            {
                if (await folder.CheckExistsAsync(AnswerFolder) == ExistenceCheckResult.FolderExists)
                {
                    return await func(await folder.GetFolderAsync(AnswerFolder));
                }
                return default(T);
            });
        }

        private async Task<T> IfFileExists<T>(IFolder folder, string name, Func<IFile, Task<T>> func)
        {
            if (await folder.CheckExistsAsync(name) == ExistenceCheckResult.FileExists)
            {
                return await func(await folder.GetFileAsync(name));
            }
            return default(T);
        }
    }
}