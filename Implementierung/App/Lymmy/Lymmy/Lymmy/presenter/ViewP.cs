
using System;
using edu.kit.lymmy.xamarin.model;
using Lymmy;
using Xamarin.Forms;

namespace edu.kit.lymmy.xamarin.presenter
{
    /// <summary>
    /// ViewP is a static class to provide methods for the Model to notify the View.
    /// </summary>
	public static class ViewP
	{
		/// <summary>
		/// Receives the questionnaire von model and sends it to view.
		/// </summary>
		/// <param name="questionnaireP">Questionnaire p.</param>
		public static void receiveQuestionnaire(QuestionnaireAM questionnaireP)
		{
			App app =(App)Application.Current;
			app.pageBuilder.receivequestionnaire(questionnaireP);
		}

        /// <summary>
        /// The view receives the message that the study has ended.
        /// </summary>
        public static void endStudy()
        {
			App app = (Lymmy.App)Application.Current;
			app.pageBuilder.endStudy();
        }


	}

}

