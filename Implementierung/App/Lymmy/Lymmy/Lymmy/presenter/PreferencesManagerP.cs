using System;
using System.Threading.Tasks;
using Lymmy;
using Xamarin.Forms;
using edu.kit.lymmy.xamarin.model;
using Lymmy.networking;
using Lymmy.persistence;

namespace edu.kit.lymmy.xamarin.presenter
{

	public static class PreferencesManagerP
	{
		//TODO singleton?

		//getStartDate, getEndDate, getWLANPreferences, setWLANPreferences
		//Yimin und Moritz da bitte absprechen

		/// <summary>
		/// Returns the StartDate of the current Study
		/// </summary>
		/// <returns>start date of study</returns>
		public static async Task<DateTime> getStartStudyDate()
		{
		    return await Connector.Instance.GetStudyStartDate();
		}

		/// <summary>
		/// Returns the EndDate of the current Study
		/// </summary>
		/// <returns>end date of study</returns>
		public static async Task<DateTime> getEndStudyDate()
		{
		    var study = await DataManager.Instance.GetStudy();
		    return study.EndDate;
		}

		/// <summary>
		/// Sets the wlan status nach the requirement of user
		/// </summary>
		/// <param name="status">If set to <c>true</c> status.</param>
		public static void setWlan(bool status)
		{			
			wlan = status;
			//TODO: with PreferencesManagerAM wlan status
		}

		/// <summary>
		/// Gets or sets a value indicating whether to transfer data when wlan is off <see cref="T:edu.kit.lymmy.xamarin.presenter.PreferencesManagerP"/>
		/// </summary>
		/// <value><c>true</c> if to transter data only when wlan is on; otherwise, <c>false</c>.</value>
		public static bool wlan{get;set;}

	}

}

