using edu.kit.lymmy.xamarin.model;
using System;
using System.Threading.Tasks;
using edu.kit.lymmy.xamarin.model.scheduler;
using Lymmy.networking;
using Lymmy.persistence;
using Xamarin.Forms;

namespace edu.kit.lymmy.xamarin.presenter
{
    /// <summary>
    /// Receives the StudyID for a new Study. Also can manually end a Study
    /// </summary>
	public static class StudyBuilderP
	{

        /// <summary>
        /// This method receives a StudyID string and relays the message to the model to get the StartDate.
        /// It returns whether or not the StudyID is valid.
        /// </summary>
        /// <param name="studyID"></param>
        /// <returns>whether or not the StudyID is valid</returns>
		public static async Task<bool> inputStudyID(String studyID)
        {
            bool result = await Connector.Instance.IsValidStudyId(studyID);
            if (result)
            {
                DependencyService.Get<ITaskStarter>().Start();
            }
            return result;
        }

        /// <summary>
        /// Manually ends the Study
        /// </summary>
        public static void endStudyManually()
        {
            StudyAM.Instance.endStudy();
        }

	}

}

