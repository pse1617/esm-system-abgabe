﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using edu.kit.lymmy.shared;
using edu.kit.lymmy.xamarin.model;
using edu.kit.lymmy.xamarin.model.builder;
using Lymmy.persistence;
using Lymmy.view;
using Lymmy.view.Language;
using ModernHttpClient;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace Lymmy
{
    public partial class App : Application
    {

        /// <summary>
        /// Gets or sets the kontext daten.
        /// </summary>
        /// <value>The context data.</value>
        public ContextDaten contextData { get; set; } = ContextDaten.Instance;

        /// <summary>
        /// Gets or sets the preferences page.
        /// </summary>
        /// <value>The preferences page.</value>
        public PreferencesPage preferences { get; set; }

        /// <summary>
        /// Gets or sets the page builder.
        /// </summary>
        /// <value>The pageBuilder.</value>
        public PageBuilder pageBuilder { get; set; }

        public App()
        {
            InitializeComponent();
            preferences = new PreferencesPage();
            pageBuilder = new PageBuilder();
            MainPage = new NavigationPage(new LymmyPage());

            //Set default language for app
            if (Device.OS == TargetPlatform.iOS || Device.OS == TargetPlatform.Android)
            {
                var ci = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
                AppResources.Culture = ci; // set the RESX for resource localization
                DependencyService.Get<ILocalize>().SetLocale(ci); // set the Thread for locale-aware methods
            }
        }

        protected override async void OnStart()
        {
            preferences.StudyID = await DataManager.Instance.GetStudyId();
            Study study = await DataManager.Instance.GetStudy();
            if (study != null)
            {
                contextData.time = study.StartDate;
                int questionnaire = await DataManager.Instance.GetCurrentQuestionnaire();
                if (questionnaire != -1)
                {
                    pageBuilder.receivequestionnaire(new StudyBuilderAM().buildStudy(study).Questionnaires[questionnaire]);
                }
            }
            // Handle when your app starts
            if (!preferences.StudyID.Equals(""))
            {
                if (pageBuilder.studyStart())
                {
                    pageBuilder.nextPage();
                }
                else
                {
                    MainPage = new NavigationPage(new StartPage());
                }
            }
            else
            {
                MainPage = new NavigationPage(new LymmyPage());
            }
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
