using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using edu.kit.lymmy.xamarin.model.builder;
using edu.kit.lymmy.xamarin.model.@event;
using edu.kit.lymmy.xamarin.model.question;
using Lymmy.persistence;

namespace edu.kit.lymmy.xamarin.model
{
    /// <summary>
    /// A QuestionnaireAM contains Questions for the User to answer.
    /// 
    /// </summary>
	public class QuestionnaireAM : IObserver<Boolean>
	{
        public readonly int questionnaireIndex;
        public readonly long cooldown;
        public readonly ConditionalQuestionList questions;
        public ConditionalQuestionListEnumerator questionsEnumerator {
            get;
            private set;
        }
        public readonly EventAM @event;
        ///contextSensors are computed again when the Questionnaire is finished (moveNextQuestion)
        public List<String> contextData;

        /// <summary>
        /// Builds a new QuestionnaireAM with given questionnaireIndex, Cooldown, questions, event
        /// </summary>
        /// <param name="questionnaireIndex">The index of the Questionnaire in the QuestionnaireList of the Study</param>
        /// <param name="cooldown">The Cooldown after which the annother Questionnaire may be answered</param>
        /// <param name="questions">A List of Questions for the User to Answer</param>
        /// <param name="event">The Event that has to trigger the Questionnaire</param>
        public QuestionnaireAM(int questionnaireIndex, long cooldown, List<QuestionAM> questions, EventAM @event)
        {
            this.questionnaireIndex = questionnaireIndex;
            this.cooldown = cooldown;
            this.questions = new ConditionalQuestionList(questions);
            this.questionsEnumerator = this.questions.GetEnumerator();

            this.@event = @event;
            this.@event.Subscribe(this);

        }
        
        /// <summary>
        /// Notifies the View that this Questionnaire can be answered
        /// Method is virtual to allow testing (mocking this method)
        /// </summary>
		public virtual async void start()
		{

            NotificationSender.sendNewQuestionnaireNotification();
		    await DataManager.Instance.SetCurrentQuestionnaire(questionnaireIndex);
		}

        /// <summary>
        /// Notifies the connected Event to start working, called in the beginning of the Study
        /// </summary>
		public void readyEvent()
		{
            @event.beginWorking();
		}
        /// <summary>
        /// Returns the current Question in the Questionnaire
        /// </summary>
        /// <returns> Question in the Questionnaire</returns>
		public QuestionAM getCurrentQuestion()
		{
            return questionsEnumerator.Current;
		}

        /// <summary>
        /// Moves to the next Question, sends the QuestionnaireAnswer
        /// </summary>
        /// <returns>whether the Questionnaire has a next Question</returns>
		public async Task<bool> moveNextQuestion()
        {
            if (questionsEnumerator == null)
            {
                questionsEnumerator = questions.GetEnumerator();
            }
            bool result = questionsEnumerator.MoveNext();
            if (!result)
            {
                questionsEnumerator = null;
                AnswerBuilderAM.sendQuestionnaireAnswer(this);
                await DataManager.Instance.SetCurrentQuestionnaire(-1);
            }

            return result;
        }

        /// <summary>
        /// The corresponding event tells the Questionnaire that is has been (de)triggered.
        /// </summary>
        /// <param name="value">Whether the event has been triggered or detriggered</param>
        public virtual void OnNext(bool value)
        {
            if(value)
            {
                this.fetchContextData();
                StudyAM.Instance.receiveQuestionnaire(this);
            } else
            {
                //TODO** in the Case the event has been detriggered
                throw new NotImplementedException();
            }
         
        }

        public virtual void fetchContextData()
        {
            contextData = ContextDaten.Instance.getContextData(@event.getContextSensors());
        }

        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }

        public void OnCompleted()
        {
            throw new NotImplementedException();
        }
    }

}

