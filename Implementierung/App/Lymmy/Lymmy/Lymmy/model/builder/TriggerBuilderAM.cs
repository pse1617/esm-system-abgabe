﻿using System;
using edu.kit.lymmy.xamarin.model.trigger;
using edu.kit.lymmy.shared.sensor;
using static edu.kit.lymmy.shared.sensor.WeatherData;
using static edu.kit.lymmy.shared.sensor.PartTimeData;
using static edu.kit.lymmy.shared.sensor.DisplayStateData;
using static edu.kit.lymmy.shared.sensor.LocationTypeData;

/// <summary>
/// Builds a TriggerAM from a shared.sensor.SensorData
/// </summary>
namespace edu.kit.lymmy.xamarin.model.builder
{
    /// <summary>
    /// Conditions are set in StudyBuilder.buildQuestionnaire
    /// </summary>
    internal static class TriggerBuilderAM
    {
        /// <summary>
        /// Builds a TriggerAM from a shared.sensor.SensorData
        /// </summary>
        internal static TriggerAM buildTrigger(SensorData trigger)
        {
            switch (trigger.Type)
            {
                case SensorType.COARSE_LOCATION:
                    return buildGPSTrigger((CoarseLocationData)trigger);

                case SensorType.DISPLAY_STATE:
                    return buildDisplayActivityTrigger((DisplayStateData)trigger);

                case SensorType.STUDY_START:
                    return buildStudyStartTrigger((StudyStartData)trigger);

                case SensorType.PARTTIME:
                    return buildPartTimeTrigger((PartTimeData)trigger);

                case SensorType.TIME:
                    return buildTimeTrigger((TimeData)trigger);

                case SensorType.WEATHER:
                    return buildWeatherTrigger((WeatherData)trigger);

				case SensorType.ACCELERATION:
					return buildAccelerationTrigger((AccelerationData)trigger);

				case SensorType.LOCATIONTYPE:
					return buildLocationTypeTrigger((LocationTypeData)trigger);
			}
			throw new NotImplementedException();
		}





        private static StudyStartTriggerAM buildStudyStartTrigger(StudyStartData trigger)
        {
            return new StudyStartTriggerAM();
        }

        private static PartTimeTriggerAM buildPartTimeTrigger(PartTimeData trigger)
        {
            Timetype timeType = trigger.TimeType;
            bool not = trigger.Not;
            return new PartTimeTriggerAM(timeType, not);
        }

        private static TimeTriggerAM buildTimeTrigger(TimeData trigger)
        {
            long periodLength = trigger.PeriodLength;
            DateTime dateTime = trigger.Date;
            return new TimeTriggerAM(dateTime, periodLength);
        }

		private static AccelerationTriggerAM buildAccelerationTrigger(AccelerationData trigger)
		{
			double acceleration = trigger.Acceleration;
			bool bigger = trigger.Bigger;
			return new AccelerationTriggerAM(acceleration, bigger);
		}


        private static TriggerAM buildWeatherTrigger(WeatherData trigger)
        {
            Weathertype weather = trigger.Weather;
            bool not = trigger.Not;
            return new WeatherTriggerAM(weather, not);
        }

        private static TriggerAM buildDisplayActivityTrigger(DisplayStateData trigger)
        {
            return new DisplayActivityTriggerAM(trigger.State);
        }

        private static TriggerAM buildGPSTrigger(CoarseLocationData trigger)
        {
            return new GPSTriggerAM(trigger.getAdress());
        }

        private static TriggerAM buildLocationTypeTrigger(LocationTypeData trigger)
        {
            Locationtype type = trigger.LocationType;
            return new LocationTypeTriggerAM(type);
        }
    }
}