﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using edu.kit.lymmy.shared.answer;
using edu.kit.lymmy.xamarin.model.question;
using Lymmy.persistence;
using edu.kit.lymmy.shared.sensor;
/// <summary>
/// Builds the QuestionnaireAnswer from a Questionnaire and sends it to the DataManager
/// </summary>
namespace edu.kit.lymmy.xamarin.model.builder
{
    /// <summary>
    /// Conditions are set in StudyBuilder.buildQuestionnaire
    /// </summary>
    public class AnswerBuilderAM
    {

        /// <summary>
        /// Returns the Answer of the Type: shared.answer.Answer of a questionIndex and the corresponding choice
        /// </summary>
        /// <returns>The Answer of the Question</returns>
        private static Answer<object> buildAnswer(QuestionAM question)
        {
            object choice = question.choice;
            int questionIndex = question.questionIndex;
            if (choice == null)
            {
                //Question should have been skipped when the Condition has not been met
                throw new UnauthorizedAccessException("Question has not been answered yet");
            }
            return new Answer<object>(questionIndex, choice);
        }

        private static async Task<QuestionnaireAnswer> buildQuestionnaireAnswer(QuestionnaireAM questionnaire)
        {
            string probandId = (await DataManager.Instance.GetProbandDetails()).Proband.Id;
            //TODO** see if that actually works on iOS
            DateTime localTime = DateTime.Now;

            int questionnaireIndex = questionnaire.questionnaireIndex;

            List<string> contextData = questionnaire.contextData ?? new List<string>();


            List<Answer<object>> questionAnswers = new List<Answer<object>>();
            foreach (QuestionAM question in questionnaire.questions)
            {
                questionAnswers.Add(buildAnswer(question));
            }

            QuestionnaireAnswer result = new QuestionnaireAnswer(probandId, localTime, questionnaireIndex, contextData, questionAnswers);
            return result;
        }

        

        /// <summary>
        /// receives a Questionnaire and sends the QuestionnaireAnswer to the DataManager
        /// </summary>
        /// <param name="questionnaireAM"></param>
        public static async void sendQuestionnaireAnswer(QuestionnaireAM questionnaireAM)
        {
            QuestionnaireAnswer questionnaireAnswer = await buildQuestionnaireAnswer(questionnaireAM);
            await DataManager.Instance.PutQuestionnaireAnswer(questionnaireAnswer);
        }
    }
}
