﻿using System;
using System.Collections.Generic;
using edu.kit.lymmy.shared.question;
using edu.kit.lymmy.xamarin.model.question;

/// <summary>
/// Builds a QuestionAM from a shared.question.Question
/// </summary>
namespace edu.kit.lymmy.xamarin.model.builder
{
    /// <summary>
    /// Conditions are set in StudyBuilder.buildQuestionnaire
    /// </summary>
    internal class QuestionBuilderAM
    {
        /// <summary>
        /// Builds a QuestionAM from a shared.question.Question
        /// </summary>
        internal static QuestionAM buildQuestion(int questionIndex, Question question)
        {
            String questionText = question.Text;
            switch (question.Type)
            {
                case QuestionType.MULTIPLE_CHOICE:
                    return buildMultipleChoiceQuestion(questionIndex, (MultipleChoiceQuestion)question);
                case QuestionType.SINGLE_CHOICE:
                    return buildSingleChoiceQuestion(questionIndex, (SingleChoiceQuestion)question);
                case QuestionType.OPEN:
                    return buildOpenQuestion(questionIndex, (OpenQuestion)question);
                case QuestionType.SLIDER:
                    return buildSliderQuestion(questionIndex, (SliderQuestion)question);
                case QuestionType.LIKERT:
                    return buildLikertQuestion(questionIndex, (LikertQuestion)question);
                case QuestionType.YES_NO:
                    return buildYesNoQuestion(questionIndex, (YesNoQuestion)question);
                default:
                    throw new NotImplementedException("This type has not been implemented");
            }
        }


        private static MultipleChoiceQuestionAM buildMultipleChoiceQuestion(int questionIndex, MultipleChoiceQuestion question)
        {
            String questionText = question.Text;
            List<String> answers = (List<String>) question.Answers;

            return new MultipleChoiceQuestionAM(questionIndex, questionText, answers);
        }

        private static SingleChoiceQuestionAM buildSingleChoiceQuestion(int questionIndex, SingleChoiceQuestion question)
        {
            String questionText = question.Text;
            List<String> answers = (List<String>) question.Answers;

            return new SingleChoiceQuestionAM(questionIndex, questionText, answers);
        }

        private static OpenQuestionAM buildOpenQuestion(int questionIndex, OpenQuestion question)
        {
            String questionText = question.Text;

            return new OpenQuestionAM(questionIndex, questionText);
        }

        private static SliderQuestionAM buildSliderQuestion(int questionIndex, SliderQuestion question)
        {
            String questionText = question.Text;
            String leftSliderText = question.LeftSliderText;
            String rightSliderText = question.RightSliderText;

            return new SliderQuestionAM(questionIndex, questionText, leftSliderText, rightSliderText);
        }

        private static LikertQuestionAM buildLikertQuestion(int questionIndex, LikertQuestion question)
        {
            String questionText = question.Text;
            String leftSliderText = question.LeftSliderText;
            String rightSliderText = question.RightSliderText;
            int steps = question.Steps;

            return new LikertQuestionAM(questionIndex, questionText, leftSliderText, rightSliderText, steps);
        }

        private static YesNoQuestionAM buildYesNoQuestion(int questionIndex, YesNoQuestion question)
        {
            String questionText = question.Text;

            return new YesNoQuestionAM(questionIndex, questionText);
        }
    }
}
