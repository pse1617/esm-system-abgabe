using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using edu.kit.lymmy.shared;
using edu.kit.lymmy.shared.@event;
using edu.kit.lymmy.shared.question;
using edu.kit.lymmy.xamarin.model.question;
using edu.kit.lymmy.xamarin.model.@event;
using edu.kit.lymmy.xamarin.model.scheduler;
using edu.kit.lymmy.xamarin.model.trigger;
using Lymmy.persistence;
using Newtonsoft.Json;
using edu.kit.lymmy.shared.sensor;

namespace edu.kit.lymmy.xamarin.model.builder
{
    /// <summary>
    /// The StudyBuilderAM is responsible for building the whole model (StudyAM,QuestionnaireAM,...) from
    /// the sharedClasses.Study.
    /// In the beginning the fetchStudy() method is called to fetch the Study from the DataManager. This method call also builds the study.
    /// </summary>
    public class StudyBuilderAM
    {
        public void initialize()
        {
        }

        /// <summary>
        /// fetches the Study from the DataManager and proceeds to build the StudyAM
        /// </summary>
        /// <returns></returns>
        public async Task<StudyAM> fetchStudy()
        {
            Study study = await DataManager.Instance.GetStudy();
            return study == null ? null : buildStudy(study);
        }

        /// <summary>
        /// Builds the StudyAM from a shared.Study
        /// </summary>
        /// <param name="study">the shared.Study from which the Study is build</param>
        /// <returns>The finished StudyAM</returns>
        public StudyAM buildStudy(Study study)
        {
            List<Questionnaire> sharedQuestionaireList = (List<Questionnaire>) study.Questionnaires;
            List<QuestionnaireAM> questionnaireList = new List<QuestionnaireAM>();

            //build Questionnaires
            int questionnaireIndex = 0;
            foreach (Questionnaire sharedQuestionnaire in sharedQuestionaireList)
            {
                QuestionnaireAM questionnaire = buildQuestionnaire(questionnaireIndex, sharedQuestionnaire);
                questionnaireList.Add(questionnaire);
                questionnaireIndex++;
            }
            DateTime startDate = study.StartDate;
            DateTime endDate = study.EndDate;
            return new StudyAM(questionnaireList, study);
        }

        private QuestionnaireAM buildQuestionnaire(int questionnaireIndex, Questionnaire questionnaire)
        {
            long cooldown = questionnaire.CoolDownTime;
            //TODO richtiges einf�gen von Kontextdaten
            List<Question> sharedQuestionsList = (List<Question>) questionnaire.Questions;
            List<QuestionAM> questions = new List<QuestionAM>();

            int questionIndex = 0;
            QuestionAM[] questionArray = new QuestionAM[sharedQuestionsList.Count];
            foreach (Question sharedQuestion in sharedQuestionsList)
            {
                //Building the Question:
                QuestionAM amQuestion = QuestionBuilderAM.buildQuestion(questionIndex, sharedQuestion);

                //Building Conditions: NOTE IF you want to add more Condition Types --> look at shared.question.QuestionType
                if (sharedQuestion.Condition.GetType() == typeof(ValidCondition))
                {
                    amQuestion.condition = new ValidConditionAM();
                }
                if (sharedQuestion.Condition.GetType() == typeof(Condition))
                {
                    try
                    {
                        int conditionQuestion = sharedQuestion.Condition.Question;
                        int conditionChoice = sharedQuestion.Condition.Choice;
                        amQuestion.condition =
                            conditionQuestion == -1
                                ? (ConditionAM) new ValidConditionAM()
                                : new ChoiceConditionAM((ChoiceQuestionAM) questionArray[conditionQuestion],
                                    conditionChoice);
                    }
                    catch (NullReferenceException)
                    {
                        throw new NullReferenceException("Referenced Question has not been initialized");
                    }
                    catch (InvalidCastException)
                    {
                        throw new ArgumentException("Referenced Question is not a ChoiceQuestion");
                    }
                }

                questions.Add(amQuestion);
                questionArray[questionIndex] = amQuestion;
                questionIndex++;
            }


            return new QuestionnaireAM(questionnaireIndex, cooldown, questions, BuildEvent(questionnaire.Event));
        }

        private EventAM BuildEvent(Event @event)
        {
            switch (@event.Type)
            {
                case EventType.COMPOSITE:
                    return BuildCompositeEvent((CompositeEvent) @event);

                case EventType.SENSOR:
                    return BuildSingleTriggerEvent((SensorEvent) @event);
                default:
                    throw new TypeNonExistantException("This EventType does not exist");
            }
        }


        private SingleTriggerEventAM BuildSingleTriggerEvent(SensorEvent @event)
        {
            SensorData sensorData = @event.Data;
            TriggerAM trigger = TriggerBuilderAM.buildTrigger(sensorData);
            return new SingleTriggerEventAM(trigger);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        private CompositeEventAM BuildCompositeEvent(CompositeEvent @event)
        {
            return new CompositeEventAM(@event.Operator, @event.Events.Select(BuildEvent).ToArray());
        }
    }


    internal class TypeNonExistantException : Exception
    {
        public TypeNonExistantException()
        {
        }

        public TypeNonExistantException(string message) : base(message)
        {
        }

        public TypeNonExistantException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}