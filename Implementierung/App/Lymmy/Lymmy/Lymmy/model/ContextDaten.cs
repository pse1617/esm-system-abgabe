﻿using System;
using Plugin.Geolocator;
using Plugin.Connectivity;
using System.Collections.Generic;
using edu.kit.lymmy.shared.sensor;
using edu.kit.lymmy.xamarin.model.trigger;

namespace edu.kit.lymmy.xamarin.model
{
	/// <summary>
	/// This Class handles the ContextData of the device. Used by QuestionnaireAM to get the Contextdata when the Proband answered the question
	/// </summary>
	public class ContextDaten
	{
		private static ContextDaten instance;
		/// <summary>
		/// Instance of the ContextDaten: Part of the Singleton-Pattern
		/// </summary>
		/// <remarks>It throws an exeption if the ContextDaten has not been initialised</remarks>
		public static ContextDaten Instance
		{
			get
			{
				if (instance == null)
				{
					instance = new ContextDaten();
				}
				return instance;
			}
		}

		/// <summary>
		/// The current time.
		/// </summary>
		public DateTime time;
		/// <summary>
		/// The current locator.
		/// </summary>
		public Plugin.Geolocator.Abstractions.IGeolocator locator;
		/// <summary>
		/// The current position.
		/// </summary>
		public Plugin.Geolocator.Abstractions.Position position;
		/// <summary>
		/// The current speed.
		/// </summary>
		public double speed;
		/// <summary>
		/// The current status of connectivity.
		/// </summary>
		public bool status;
		/// <summary>
		/// The current type of connectivity if is connected.
		/// </summary>
		public IEnumerable<Plugin.Connectivity.Abstractions.ConnectionType> type;
        /// <summary>
        /// Initializes a new instance of the <see cref="T:edu.kit.lymmy.xamarin.model.ContextDaten"/> class.
        /// </summary>
        public WeatherTriggerAM weatherTrigger;
        /// <summary>
        /// Initializes a new instance of the <see cref="T:edu.kit.lymmy.xamarin.model.trigger.WeatherTriggerAM"/> class.
        /// </summary>
		private ContextDaten()
		{
            weatherTrigger = new WeatherTriggerAM(WeatherData.Weathertype.Additional, true);
			locator = CrossGeolocator.Current;
			locator.DesiredAccuracy = 50;
			double latitude;
			double longitude;
			double altitude;
			locator.PositionChanged += async (sender, args) =>
			 {
				 position = await locator.GetPositionAsync(timeoutMilliseconds: 10000);
				 time = position.Timestamp.LocalDateTime;
				 latitude = position.Latitude;
				 longitude = position.Longitude;
				 altitude = position.Altitude;
				 speed = position.Speed;
			 };
			CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
			 {
				 status = CrossConnectivity.Current.IsConnected;
				 if (status)
				 {
                     //TODO Check
					 CrossConnectivity.Current.ConnectivityChanged += (s, a) =>
					   {
						   type = CrossConnectivity.Current.ConnectionTypes;
					   };
				 }
			 };

		}

		/// <summary>
		/// Returns the relevant contextData to a given Set of SensorTypes
		/// </summary>
		/// <param name="sensorTypes">A set of Sensor Types</param>
		/// <returns></returns>
		public List<string> getContextData(ISet<SensorType> sensorTypes)
		{
			List<string> result = new List<string>();
			foreach (SensorType sensorType in sensorTypes)
			{
				string sensorData = "";
				switch (sensorType)
				{
					case SensorType.COARSE_LOCATION:
                        sensorData = "Longitude;Latitude: " + position.Latitude.ToString() + ";" + position.Longitude.ToString();
                        break;
					case SensorType.DISPLAY_STATE:
						//No sensorData to add
						break;
					case SensorType.STUDY_START:
						//No sensorData to add
						break;
					case SensorType.TIME:
                        sensorData = "Time: " + time.ToString();
                        break;
					case SensorType.WEATHER:
                        weatherTrigger.getWeather(position.Latitude, position.Longitude);
						sensorData = "Weather: " + weatherTrigger.getActualWeather();
						break;
					case SensorType.PARTTIME:
						sensorData = "Time: " + time.ToString();
                        //TODO Currently copied from SensorType.TIME, should be different
                        break;
					case SensorType.LOCATIONTYPE:
						sensorData = "Longitude;Latitude: " + position.Latitude.ToString() + ";" + position.Longitude.ToString();
                        //TODO Currently copied from SensorType.COARSE_LOCATION, should be different
                        break;
					default:
						throw new NotImplementedException();
				}
				if (sensorData != "")
				{
					result.Add(sensorData);
				}
			}

			return result;
		}
	}
}
