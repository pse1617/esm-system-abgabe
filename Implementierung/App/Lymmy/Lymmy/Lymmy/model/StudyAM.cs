using System;
using System.Collections.Generic;
using edu.kit.lymmy.shared;
using edu.kit.lymmy.xamarin.model.scheduler;


namespace edu.kit.lymmy.xamarin.model
{
    /// <summary>
    /// The class StudyAM represents the whole Study in the App. The StudyData is handled in the DataManager
    /// StudyAM is a Singleton.
    /// It contains a list of all Quesionnaires in the Study.
    /// 
    /// StudyAM manages the Start of Questionnaires after their event has been triggered.
    /// </summary>
	public class StudyAM
    {
        private static StudyAM instance;
        public List<QuestionnaireAM> Questionnaires { get; }
        public Study Definition { get; }
        private Boolean hasActiveQuestionnaire = false;
        private Queue<QuestionnaireAM> waitingQuestionnaires = new Queue<QuestionnaireAM>();
        private CooldownScheduler cooldownScheduler = CooldownScheduler.Instance;
        private Boolean hasCooledDown = true;

        private StudyAM()
        {
            instance = null;
        }

        /// <summary>
        /// Constructor to build a StudyAM from a questionnaireList
        /// </summary>
        ///
        /// <param name="questionnaireList"> List of Questionnaires that are within the Study</param>
        public StudyAM(List<QuestionnaireAM> questionnaireList, Study definition)
        {
            //overwrites the old study
            instance = this;
            Questionnaires = questionnaireList;
            Definition = definition;
            this.hasCooledDown = true;
        }

        /// <summary>
        /// Instance of the Study: Part of the Singleton-Pattern
        /// </summary>
        /// <remarks>It throws an exeption if the Study has not been initialised</remarks>
        public static StudyAM Instance
        {
            get
            {
                if (instance == null)
                {
                    throw new AccessViolationException("Study should have already been created");
                }
                return instance;
            }
        }
        /// <summary>
        /// Starts the Study, notifies all Triggers to start working
        /// </summary>
        public void startStudy()
        {
            foreach(QuestionnaireAM questionnaire in Questionnaires)
            {
                questionnaire.readyEvent();
            }
        }

        /// <summary>
        /// Handles the notification from the Cooldownscheduler that the Cooldown for the waiting Questionnaires ended
        /// </summary>
        public void receiveCooldownEnded()
        {
            if (this.hasActiveQuestionnaire)
            {
                throw new AccessViolationException("Error: Cooldown can only exist while no active exceptions");             

            }
            this.hasCooledDown = true;
           this.startFirstWaitingQuestionnaire();
        }

        private void startFirstWaitingQuestionnaire()
        {
            if (this.waitingQuestionnaires.Count != 0)
            {
                this.hasCooledDown = false;
                this.hasActiveQuestionnaire = true;
                this.waitingQuestionnaires.Dequeue().start();                
            }
        }


        /// <summary> 
        /// Receives a new Questionnaire, that has been triggered.
        /// If the following conditions are met the Questionnaire is started
        /// <list type="number">
        /// <item>
        /// <description>No active Questionnaire</description>
        /// </item>
        /// <item>
        /// <description>Cooldown is ready</description>
        /// </item>
        /// </list>
        /// else the Questionnaire is put into the waiting queue
        /// </summary>
        public virtual void receiveQuestionnaire(QuestionnaireAM questionnaire)
		{
            //add Questionnaire to Queue
            this.waitingQuestionnaires.Enqueue(questionnaire);
            //start the first waiting Questionnaire
            if (!this.hasActiveQuestionnaire && this.hasCooledDown)
            {
                startFirstWaitingQuestionnaire();
            }
            
        }


        /// <summary>
        /// Notifies the study that the current active questionnaire has finished.
        /// start Cooldown with Cooldowntime of Questionnaire
        /// </summary>
        public void finishedQuestionnaire(QuestionnaireAM questionnaire)
        {
            this.hasActiveQuestionnaire = false;
            this.cooldownScheduler.startCooldown(questionnaire.cooldown);
        }

        /// <summary>
        /// Ends the Study, should be called either by StartEndScheduler, or by StudyBuilderP
        /// </summary>
        public void endStudy()
        {
            cooldownScheduler.stopAllTimers();
            //TODO preferences Manager
            StudyAM.instance = new StudyAM();
        }

    }
    /// <summary>
    /// An Exception, thrown when something tries to illegally access objects
    /// </summary>
    internal class AccessViolationException : Exception
    {
        public AccessViolationException()
        {
        }

        public AccessViolationException(string message) : base(message)
        {
        }

        public AccessViolationException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}

