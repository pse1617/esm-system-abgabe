﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using edu.kit.lymmy.shared;
using edu.kit.lymmy.shared.answer;
using edu.kit.lymmy.xamarin.model.builder;
using Lymmy.networking;
using Lymmy.persistence;
using Xamarin.Forms;
using XLabs;

namespace edu.kit.lymmy.xamarin.model.scheduler
{
    public class TaskManager
    {
        public async Task GetTasks()
        {
            Study study = await DataManager.Instance.GetStudy();
            if (study == null)
            {
                string studyId = await DataManager.Instance.GetStudyId();
                if (studyId != "")
                {
                    if (await Connector.Instance.ShouldDownloadStudy(studyId))
                    {
                        Study temp = await Connector.Instance.DownloadStudy(studyId);
                        if (temp != null)
                        {
                            await DataManager.Instance.SetStudy(temp);
                            startStudyIfNecessary(temp);
                        }
                    }
                    else
                    {
                        DependencyService.Get<ITaskStarter>().ScheduleNextStart(new TimeSpan(1, 0, 0, 0));
                    }
                }
            }
            else startStudyIfNecessary(study);
            IList<QuestionnaireAnswer> answers = await DataManager.Instance.GetQuestionnaireAnswers();
            if (answers.Count > 0 && await Connector.Instance.SendAnswers(answers))
            {
                answers.ForEach(async answer => await DataManager.Instance.RemoveQuestionnaireAnswer(answer));
            }
        }

        private void startStudyIfNecessary(Study study)
        {
            if (study.StartDate.CompareTo(DateTime.Now) <= 0 && study.EndDate.CompareTo(DateTime.Now) >= 0)
            {
                StudyAM studyAm = new StudyBuilderAM().buildStudy(study);
                studyAm.startStudy();
            }
        }
    }
}