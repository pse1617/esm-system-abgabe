using System;

using Xamarin.Forms;

namespace edu.kit.lymmy.xamarin.model.scheduler
{
    /// <summary>
    /// CooldownScheduler handles the Cooldowns between Questionnaires.
    /// It notifies StudyAM when a Cooldown has finished
    /// CooldownScheduler is a singleton
    /// </summary>
	/// 
	public class CooldownScheduler
	{
        //singleton
        private static CooldownScheduler instance;

        private CooldownScheduler() { }

        public static CooldownScheduler Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CooldownScheduler();
                }
                return instance;
            }
        }
        //singleton end

        /// <summary>
        /// Stops any running Cooldown
        /// </summary>
        public void stopAllTimers()
        {
            //TODO** check if it really stops the timers
            //maybe use this: http://stackoverflow.com/questions/41586553/how-to-cancel-a-timer-before-its-finished

            instance = new CooldownScheduler();
        }

        /// <summary>
        /// Starts a Cooldown with a given Time in MilliSeconds
        /// </summary>
        /// <param name="timeInMS">The Time in Milliseconds of the Cooldown</param>
        public void startCooldown(long timeInMS)
		{
            //convert long to Double then to Timespan
            TimeSpan timespan = TimeSpan.FromMilliseconds(Convert.ToDouble(timeInMS));

            Device.StartTimer(timespan, () =>
            {
                StudyAM.Instance.receiveCooldownEnded();
                //false: no reptition
                return false;
            });
        }
    }

}

