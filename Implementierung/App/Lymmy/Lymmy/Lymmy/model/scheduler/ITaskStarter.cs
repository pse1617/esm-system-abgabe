﻿using System;

namespace edu.kit.lymmy.xamarin.model.scheduler
{
    public interface ITaskStarter
    {
        void Start();

        void ScheduleNextStart(TimeSpan timeSpan);
    }
}