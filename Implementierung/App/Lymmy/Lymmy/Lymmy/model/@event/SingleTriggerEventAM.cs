using edu.kit.lymmy.xamarin.model.@event;
using System;
using edu.kit.lymmy.xamarin.model.trigger;
using edu.kit.lymmy.shared.sensor;
using System.Collections.Generic;
/// <summary>
/// The "Leaf" of the Composite design pattern.
/// The SingleTriggerEvent observes one Trigger and triggeres when the Trigger is triggered.
/// </summary>
namespace edu.kit.lymmy.xamarin.model.@event
{
	public class SingleTriggerEventAM : EventAM, IObserver<Boolean>
	{
        public readonly TriggerAM trigger;
        /// <summary>
        /// Builds a SingleTriggerEvent with a given Trigger
        /// </summary>
        /// <param name="trigger"></param>
		public SingleTriggerEventAM(TriggerAM trigger)
		{
            this.trigger = trigger;
		}

        public override void beginWorking()
        {
            
            trigger.Subscribe(this);
            trigger.startTrigger();
        }
        /// <summary>
        /// The event receives the Notification from the Trigger that the TriggerValue has changed.
        /// </summary>
        /// <param name="value">The TriggerValue aka: "triggered"/"notTriggered"</param>
        public void OnNext(bool value)
        {
            notify(value);
        }

        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }

        public void OnCompleted()
        {
            throw new NotImplementedException();
        }

        public override ISet<SensorType> getContextSensors()
        {
            ISet<SensorType> result = new HashSet<SensorType>();
            result.Add(trigger.getSensorType());

            return result;
        }
    }

}

