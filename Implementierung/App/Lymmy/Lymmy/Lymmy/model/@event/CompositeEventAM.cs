using System;
using System.Collections.Generic;
using System.Linq;
using edu.kit.lymmy.shared;
using edu.kit.lymmy.shared.sensor;
/// <summary>
/// The "Composite" of the Composite design pattern.
/// CompositeEventAM contains some Events and an LogicalOperator, which determines when the Event is triggered.
/// </summary>
namespace edu.kit.lymmy.xamarin.model.@event
{
    public class CompositeEventAM : EventAM
    {
        private readonly Dictionary<EventAM, bool> events;
        public readonly LogicalOperator op;
        private bool lastStatus;

        /// <summary>
        /// Builds a composite event using a logicalOperator an array of events.
        /// </summary>
        /// <param name="op">the LogicalOperator</param>
        /// <param name="events">The Events it is composed of</param>
        public CompositeEventAM(LogicalOperator op, params EventAM[] events)
        {
            this.op = op;
            this.events = new Dictionary<EventAM, bool>();
            foreach (EventAM e in events)
            {
                this.events.Add(e, false);
            }
            lastStatus = false;
        }

        public override void beginWorking()
        {
            foreach (EventAM e in events.Keys.ToList())
            {
                e.Subscribe(new Observer(this, e));
                e.beginWorking();
            }
        }
        private void CheckChanged()
        {
            bool status = ApplyLogicalOperator(events.Values, op);
            if (status != lastStatus)
            {
                lastStatus = status;
                notify(status);
            }
        }


        private class Observer : IObserver<Boolean>
        {
            private readonly EventAM e;
            private readonly CompositeEventAM outer;

            public Observer(CompositeEventAM outer, EventAM e)
            {
                this.e = e;
                this.outer = outer;
            }

            public void OnCompleted()
            {
                throw new NotImplementedException();
            }

            public void OnError(Exception error)
            {
                throw new NotImplementedException();
            }

            public void OnNext(bool value)
            {
                outer.events[e] = value;
                outer.CheckChanged();
            }
        }

        private static Boolean ApplyLogicalOperator(IEnumerable<Boolean> booleanList, LogicalOperator logicOperator)
        {

            switch (logicOperator)
            {
                case LogicalOperator.AND:
                    return !booleanList.Contains(false);

                case LogicalOperator.OR:
                    return booleanList.Contains(true);

                default:
                    throw new NotSupportedException("Applying this logicalOperator has not been implemented");
            }
        }

        public override ISet<SensorType> getContextSensors()
        {
            ISet<SensorType> result = new HashSet<SensorType>();
            foreach(EventAM @event in events.Keys)
            {
                result.Union(@event.getContextSensors());
            }

            return result;
        }
    }
}