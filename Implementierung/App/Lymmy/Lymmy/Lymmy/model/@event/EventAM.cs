using edu.kit.lymmy.xamarin.model.observerPattern;
using System;
using edu.kit.lymmy.xamarin.model.@event;
using System.Collections.Generic;
using edu.kit.lymmy.shared.sensor;

/// <summary>
/// EventAM is the Trigger for a Questionnaire. When an Event turns true, the Questionnaire is able to be answered.
/// EventAM is built on the CompositeDesignPattern:
/// <list type="bullet">
/// <item>
/// <description>EventAM is the Component</description>
/// </item>
/// <item>
/// <description>SingleTriggerEvent is the Leaf, it inherites from EventAM</description>
/// </item>
/// <item>
/// <description>CompositeEvent is the Composite, it inherites from EventAM</description>
/// </item>
/// </list>
/// </summary>
namespace edu.kit.lymmy.xamarin.model.@event
{
	public abstract class EventAM : BooleanPublisher
	{
        /// <summary>
        /// This method is called at the beginning of a study.
        /// It starts the Events and the Triggers it is dependant of.
        /// </summary>
		public abstract void beginWorking();


        /// <summary>
        /// Returns all the ContextSensors that were used to trigger the EventAM
        /// </summary>
        /// <returns>the ContextSensors that were used to trigger the EventAM</returns>
        public abstract ISet<SensorType> getContextSensors();
    }

    

}

