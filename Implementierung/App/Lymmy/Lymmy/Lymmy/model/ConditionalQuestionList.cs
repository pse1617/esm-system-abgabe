﻿using System;
using System.Collections;
using System.Collections.Generic;

using edu.kit.lymmy.xamarin.model.question;


namespace edu.kit.lymmy.xamarin.model
{
    /// <summary>
    /// A List of QuestionAM which can be iterated upon. It only returns questions that have their CondtionAM fulfilled
    /// </summary>
    public class ConditionalQuestionList : IEnumerable
    {
        public readonly int Count;
        private List<QuestionAM> questions;

        /// <summary>
        /// Creates a new ConditionalQUestionList with given Questions
        /// </summary>
        /// <param name="questions"></param>
        public ConditionalQuestionList(List<QuestionAM> questions)
        {
            this.questions = questions;
            this.Count = questions.Count;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }

        public ConditionalQuestionListEnumerator GetEnumerator()
        {
            return new ConditionalQuestionListEnumerator(questions);
        }
    }
}
