using System;
using System.Collections.Generic;

namespace edu.kit.lymmy.xamarin.model.observerPattern
{
    /// <summary>
    /// Boolean Publisher represents a Publisher in the ObserverPattern which publishes a bool
    /// It impletents the methods Subscribe(IObserver<Boolean> observer) and notify(Boolean newData)
    /// </summary>
	public abstract class BooleanPublisher: IObservable<Boolean>
	{
        private List<IObserver<Boolean>> observers = new List<IObserver<Boolean>>();

        /// <summary>
        /// Adds a new Observer to the ObserverList
        /// </summary>
        /// <param name="observer">the Observer to be added</param>
        /// <returns></returns>
        public IDisposable Subscribe(IObserver<Boolean> observer)
        {
            if (!observers.Contains(observer))
            {
                observers.Add(observer);
            }
            return new Unsubscriber<Boolean>(observers, observer);
        }
        /// <summary>
        /// notifies all subscribers of the new Data
        /// </summary>
        /// <param name="newData">The Data that has changed</param>
        public void notify(Boolean newData)
        {
            foreach (IObserver<Boolean> observer in observers)
            {
                observer.OnNext(newData);
            }
        }
    }

}

