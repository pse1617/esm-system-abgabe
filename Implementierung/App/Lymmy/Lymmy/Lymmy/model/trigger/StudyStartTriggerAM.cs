using edu.kit.lymmy.xamarin.model.observerPattern;
using System;
using edu.kit.lymmy.shared.sensor;
using Lymmy.persistence;

namespace edu.kit.lymmy.xamarin.model.trigger
{
    /// <summary>
    /// Inherites TriggerAM
    /// Triggers when the Study Starts
    /// </summary>
    public class StudyStartTriggerAM : TriggerAM
    {
        /// <summary>
        /// Creates a new StudyStartTrigger with no arguments
        /// </summary>
        public StudyStartTriggerAM()
        {
        }

        public override async void startTrigger()
        {
            if (!await DataManager.Instance.GetStudyStartTriggered())
            {
                notify(true);
                await DataManager.Instance.SetStudyStartTriggered(true);
            }
        }

        public override SensorType getSensorType()
        {
            return SensorType.STUDY_START;
        }
    }

}

