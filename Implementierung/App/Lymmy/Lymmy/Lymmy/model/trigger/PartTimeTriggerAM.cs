﻿using edu.kit.lymmy.xamarin.model.trigger;
using System;
using Plugin.Geolocator;
using edu.kit.lymmy.shared.sensor;

namespace edu.kit.lymmy.xamarin.model.trigger
{
	/// <summary>
	/// Inherites TriggerAM
	/// Triggeres when in the required part of time
	/// </summary>
	public class PartTimeTriggerAM : TriggerAM
	{
        public readonly PartTimeData.Timetype _timeType;
        public readonly bool _not;
        //Trigger with given part time of the day
        public PartTimeTriggerAM(PartTimeData.Timetype timeType, bool not)
		{
            _timeType = timeType;
            _not = not;
		}

		public override SensorType getSensorType()
		{
			return SensorType.PARTTIME;
		}

		public override void startTrigger()
		{
            CrossGeolocator.Current.PositionChanged += async (sender, args) =>
            {
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 50;
                var position = await locator.GetPositionAsync(timeoutMilliseconds: 10000);
                var ti = position.Timestamp.LocalDateTime;
                switch (_timeType)
                {
                    case PartTimeData.Timetype.Night:
                        if (_not)
                        {
                            if (ti.Hour >= 0 && ti.Hour < 6)
                            {
                                this.notify(true);
                            }
                        }
                        else
                        {
                            if (!(ti.Hour >= 0 && ti.Hour < 6))
                            {
                                this.notify(true);
                            }
                        }
                        break;
                    case PartTimeData.Timetype.Morning:
                        if (_not)
                        {
                            if (ti.Hour >= 6 && ti.Hour < 12)
                            {
                                this.notify(true);
                            }
                        }
                        else
                        {
                            if (!(ti.Hour >= 6 && ti.Hour < 12))
                            {
                                this.notify(true);
                            }
                        }
                        break;
                    case PartTimeData.Timetype.Afternoon:
                        if (_not)
                        {
                            if (ti.Hour >= 12 && ti.Hour < 18)
                            {
                                this.notify(true);
                            }
                        }
                        else
                        {
                            if (!(ti.Hour >= 12 && ti.Hour < 18))
                            {
                                this.notify(true);
                            }
                        }
                        break;
                    case PartTimeData.Timetype.Evening:
                        if (_not)
                        {
                            if (ti.Hour >= 18 && ti.Hour < 24)
                            {
                                this.notify(true);
                            }
                        }
                        else
                        {
                            if (!(ti.Hour >= 18 && ti.Hour < 24))
                            {
                                this.notify(true);
                            }
                        }
                        break;
                    default: break;
                }
            };
            CrossGeolocator.Current.StartListeningAsync(30000, 0);
		}

		public void stopTrigger()
		{
			CrossGeolocator.Current.StopListeningAsync();
		}

	}
}
