using System;
using edu.kit.lymmy.shared.sensor;
using edu.kit.lymmy.xamarin.model.trigger;
using Xamarin.Forms;

namespace edu.kit.lymmy.xamarin.model.trigger
{
    /// <summary>
    /// Inherites TriggerAM
    /// 
    /// A Trigger which triggers when the Display is active
    /// </summary>
    public class DisplayActivityTriggerAM : TriggerAM
    {
        public readonly DisplayStateData.DisplayState displayState;

        public DisplayActivityTriggerAM(DisplayStateData.DisplayState displayState)
        {
            this.displayState = displayState;
        }

        public override SensorType getSensorType()
        {
            return SensorType.DISPLAY_STATE;
        }

        public override void startTrigger()
        {
            DependencyService.Get<IDisplayActivityService>().StatusChanged += state => notify(state == displayState);
        }
    }

}

