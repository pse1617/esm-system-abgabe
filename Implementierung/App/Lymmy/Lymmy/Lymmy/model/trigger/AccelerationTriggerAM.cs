using System;
using DeviceMotion.Plugin;
using DeviceMotion.Plugin.Abstractions;
using edu.kit.lymmy.shared.sensor;
using edu.kit.lymmy.xamarin.model.trigger;
using System.Diagnostics;

namespace edu.kit.lymmy.xamarin.model.trigger
{
    /// <summary>C:\PSE\Lymmy\Implementierung\App\Lymmy\Lymmy\model\trigger\MotionTriggerAM.cs
    /// Inherites TriggerAM
    /// Triggers when the Device moves
    /// </summary>
	public class AccelerationTriggerAM : TriggerAM
	{
        public readonly double acceleration;
        public readonly bool bigger;
        public AccelerationTriggerAM(double acceleration, bool bigger)
        {
            this.acceleration = acceleration;
            this.bigger = bigger;
        }

        public override void startTrigger()
        {
            CrossDeviceMotion.Current.Start(MotionSensorType.Accelerometer);
            CrossDeviceMotion.Current.SensorValueChanged += (s, a) =>
            {
                double x = ((MotionVector)a.Value).X;
                double y = ((MotionVector)a.Value).Y;
                double z = ((MotionVector)a.Value).Z;
                double accelerationTemp = Math.Sqrt((x*x)+(y*y)+(z*z));

                if (bigger)
                {
                    if (accelerationTemp > acceleration)
                    {
                        notify(true);
                    }
                }
                else
                {
                    if (accelerationTemp < acceleration)
                    {
                        notify(true);
                    }
                }

            };
        }

        public void stopTrigger()
        {
            CrossDeviceMotion.Current.Stop(MotionSensorType.Accelerometer);
        }

        public override SensorType getSensorType()
        {
            return SensorType.ACCELERATION;
        }
    }

    
}

