﻿using edu.kit.lymmy.shared.sensor;

namespace edu.kit.lymmy.xamarin.model.trigger
{
    public delegate void OnStatusChanged(DisplayStateData.DisplayState state);

    public interface IDisplayActivityService
    {
        event OnStatusChanged StatusChanged;
    }
}