using System;
using System.Net.Http;
using System.Threading.Tasks;
using edu.kit.lymmy.shared.sensor;
using Newtonsoft.Json;
using Plugin.Geolocator;
using static edu.kit.lymmy.shared.sensor.WeatherData;

namespace edu.kit.lymmy.xamarin.model.trigger
{
    /// <summary>
    /// Inherites TriggerAM
    /// Triggers when certain wheC:\PSE\Lymmy\Implementierung\App\Lymmy\Lymmy\model\trigger\WeatherTriggerAM.csather conditions are fulfilled
    /// </summary>
    public class WeatherTriggerAM : TriggerAM
    {
        private Weathertype actualWeather;

        public readonly Weathertype desiredWeather;
        public readonly bool negation;
        private bool runTrigger;

        public WeatherTriggerAM(Weathertype desiredWeather, bool negation)
        {
            this.desiredWeather = desiredWeather;
            this.negation = negation;
        }

        public async override void startTrigger()
        {
            runTrigger = true;
            double latitude = Double.NaN;
            double longitude = Double.NaN;
            await CrossGeolocator.Current.StartListeningAsync(30000, 0);
            CrossGeolocator.Current.PositionChanged += async (sender, args) =>
            {
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 50;
                var position = await locator.GetPositionAsync(timeoutMilliseconds: 60000);
                latitude = position.Latitude;
                longitude = position.Longitude;
            };
            while (runTrigger)
            {
                if (latitude != Double.NaN && longitude != Double.NaN)
                {
                    getWeather(latitude, longitude);
                    if(!negation)
                    {
                        if (desiredWeather == actualWeather)
                        {
                            notify(true);
                        }
                    } else
                    {
                        if (desiredWeather != actualWeather)
                        {
                            notify(true);
                        }
                    }
                    
                }

                //5 minute delay to next refresh
                await Task.Delay(300000);
            }
        }

        public void stopTrigger()
        {
            runTrigger = false;
            //geolocator not stopped cause its probably needed for other triggers
        }

        public static async Task<dynamic> getDataFromService(string queryString)
        {
            HttpClient client = new HttpClient();
            var response = await client.GetAsync(queryString);

            dynamic data = null;
            if (response != null)
            {
                string json = response.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject(json);
            }

            return data;
        }

        public async void getWeather(double latitude, double longitude)
        {

            string key = "bd8aac3fc45ce9d853b6bc346444c4ee";
            string queryString = "http://api.openweathermap.org/data/2.5/weather?lat="
                + latitude + "&lon=" + longitude + "&appid=" + key + "&units=imperial";

            dynamic results = await getDataFromService(queryString).ConfigureAwait(false);

            if (results["weather"] != null)
            {
                string weather = (string)results["weather"][0]["main"];
                Enum.TryParse(weather, out actualWeather);
            }
        }

        public Weathertype getActualWeather()
        {
            return actualWeather;
        }

        public override SensorType getSensorType()
        {
            return SensorType.WEATHER;
        }
    }
    
}

