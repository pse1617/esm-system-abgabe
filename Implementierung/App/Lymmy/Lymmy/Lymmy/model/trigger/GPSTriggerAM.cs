using System;
using edu.kit.lymmy.shared.sensor;
using edu.kit.lymmy.xamarin.model.trigger;
using Plugin.Geolocator;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace edu.kit.lymmy.xamarin.model.trigger
{
	/// <summary>
	/// Inherites TriggerAM
	/// Triggeres when the Device is in a certain Location
	/// </summary>
	public class GPSTriggerAM : TriggerAM
	{
        public readonly string _address;

		public GPSTriggerAM(string address)
		{
            _address = address;
		}

		public override SensorType getSensorType()
		{
			return SensorType.COARSE_LOCATION;
		}

		public override void startTrigger()
		{
            double latitude = 0;
            double longitude = 0;
            Geocoder geoCoder = new Geocoder();
            CrossGeolocator.Current.PositionChanged += async (sender, args) =>
            {
                //get the accurate position of the given adress
                var approximateLocations = await geoCoder.GetPositionsForAddressAsync(_address);
                foreach (var pos in approximateLocations)
                {
                    latitude = pos.Latitude;
                    longitude = pos.Longitude;

                    //get the current position and compare each other
                    var locator = CrossGeolocator.Current;
                    locator.DesiredAccuracy = 50;
                    var position = await locator.GetPositionAsync(timeoutMilliseconds: 10000);
                    double la = position.Latitude;
                    double lo = position.Longitude;
                    if ((System.Math.Abs(la - latitude) < double.Epsilon) && (System.Math.Abs(lo - longitude) < double.Epsilon))
                    {
                        this.notify(true);
                    }
                }
            };
            CrossGeolocator.Current.StartListeningAsync(30000, 0);
		}

		public void stopTrigger()
		{
			CrossGeolocator.Current.StopListeningAsync();
		}
	}

}

