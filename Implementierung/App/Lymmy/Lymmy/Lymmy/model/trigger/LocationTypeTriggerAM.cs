﻿using System;
using System.Net.Http;
using Newtonsoft.Json;
using Plugin.Geolocator;
using edu.kit.lymmy.shared.sensor;
using static edu.kit.lymmy.shared.sensor.LocationTypeData;

namespace edu.kit.lymmy.xamarin.model.trigger
{
	/// <summary>
	/// Inherites TriggerAM
	/// Triggeres when the Device is in a certain type of location
	/// </summary>
	public class LocationTypeTriggerAM : TriggerAM
	{
        public readonly Locationtype _desiredType;

		public LocationTypeTriggerAM(Locationtype desiredType)
		{
            _desiredType = desiredType;
		}

		public override SensorType getSensorType()
		{
			return SensorType.LOCATIONTYPE;
		}

		public override void startTrigger()
		{
            CrossGeolocator.Current.PositionChanged += async (sender, args) =>
            {
                //get the current position and compare each other
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 50;
                var position = await locator.GetPositionAsync(timeoutMilliseconds: 10000);
                double latitude = position.Latitude;
                double longitude = position.Longitude;
                if (latitude != Double.NaN && longitude != Double.NaN)
                {
                    //using Google Places API Web Service
                    var client = new HttpClient();
                    var response = await client.GetStringAsync(string.Format(
                     "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location={0},{1}" +
                     "&radius=50&rankedBy=distance&key=AIzaSyBtOuKo9dW0ABDF3p7JIUImJPCTYfNbnms", latitude, longitude));
                    var addresses = JsonConvert.DeserializeObject<PlacesApiQueryResponse>(response);
                    foreach (var result in addresses.results)
                    {
                        string type = _desiredType.ToString();
                        if (result.types.Contains(type))
                        {
                            this.notify(true);
                            break;
                        }
                    }
                }
            };
            CrossGeolocator.Current.StartListeningAsync(30000, 0);
		}

		public void stopTrigger()
		{
			CrossGeolocator.Current.StopListeningAsync();
		}
	}
}
