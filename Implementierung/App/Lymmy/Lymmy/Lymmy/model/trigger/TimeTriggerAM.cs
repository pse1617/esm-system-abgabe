using edu.kit.lymmy.xamarin.model.trigger;
using System;
using Plugin.Geolocator;
using edu.kit.lymmy.shared.sensor;

namespace edu.kit.lymmy.xamarin.model.trigger
{
	/// <summary>
	/// Inherites TriggerAM
	/// Triggeres when a certain Date/Time is reached
	/// </summary>
	public class TimeTriggerAM : TriggerAM
	{
        public readonly DateTime _time;
        public readonly long _periodLength;
        //Trigger with intevall
        public TimeTriggerAM(DateTime time, long periodLength)
		{
            _time = time;
            _periodLength = periodLength;
			
		}

        public override SensorType getSensorType()
        {
            return SensorType.TIME;
        }

        public override void startTrigger()
		{
            CrossGeolocator.Current.PositionChanged += async (sender, args) =>
            {
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 50;
                var position = await locator.GetPositionAsync(timeoutMilliseconds: 10000);
                var ti = position.Timestamp.LocalDateTime;
                while (ti > _time)
                {
                    ti.AddMilliseconds(-_periodLength);
                    if (ti.Equals(_time))
                    {
                        this.notify(true);
                    }
                }
            };
            CrossGeolocator.Current.StartListeningAsync(30000, 0);
		}

		public void stopTrigger()
		{
			CrossGeolocator.Current.StopListeningAsync();
		}

	}
}
