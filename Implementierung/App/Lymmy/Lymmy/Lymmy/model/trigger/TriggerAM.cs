using edu.kit.lymmy.shared.sensor;
using edu.kit.lymmy.xamarin.model.observerPattern;

namespace edu.kit.lymmy.xamarin.model.trigger
{
    /// <summary>
    /// A Trigger represents a Reason for an Event to fire. e.g. StartStudyTrigger when the Study Starts
    /// </summary>
	public abstract class TriggerAM : BooleanPublisher
	{
        /// <summary>
        /// Starts the Trigger (e.g it now can notify events)
        /// Called when the Study is started by TriggerEvent.beginWorking
        /// </summary>
        public abstract void startTrigger();

        public abstract SensorType getSensorType();
            
	}

}

