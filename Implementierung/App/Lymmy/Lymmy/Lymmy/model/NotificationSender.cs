using System;
using Lymmy.view.Language;
using Plugin.LocalNotifications;

namespace edu.kit.lymmy.xamarin.model
{
    /// <summary>
    /// NotificationSender handles Sending Notification to the User
    /// </summary>
	public static class NotificationSender
	{
        /// <summary>
        /// Send the Notification that a new Questionnaire is ready to be answered
        /// </summary>
		public static void sendNewQuestionnaireNotification()
		{
			CrossLocalNotifications.Current.Show(AppResources.displayAlertTitleOfEndStudy, AppResources.displayAlertNewQuestionnaire);
		}

        /// <summary>
        /// Send the Notification that the current Study has started
        /// </summary>
        public static void sendStudyStartedNotification()
		{
			CrossLocalNotifications.Current.Show(AppResources.displayAlertTitleOfEndStudy, AppResources.displayAlertStudyStart);
		}

        /// <summary>
        /// Send the Notification that the current Study has ended
        /// </summary>
        public static void sendStudyEndedNotification()
		{
			CrossLocalNotifications.Current.Show(AppResources.displayAlertTitleOfEndStudy, AppResources.displayAlertStudyEnd);
		}

        /// <summary>
        /// Send the Notification that a Sensor that is needed for the Questionnaire is missing on the Device
        /// </summary>
        public static void sendMissingSensorNotification(String sensors)
		{
			CrossLocalNotifications.Current.Show(AppResources.displayAlertMissingSensor, sensors);

		}
	}
}
