using System.Collections.Generic;
using System.Collections;

using edu.kit.lymmy.xamarin.model.question;

namespace edu.kit.lymmy.xamarin.model
{
    /// <summary>
    /// The Corresponding Enumerator of the CondtionalList
    /// </summary>
    public class ConditionalQuestionListEnumerator : IEnumerator<QuestionAM>
    {
        private IEnumerator<QuestionAM> questionEnumerator;

        /// <summary>
        /// Creates a CondtionalListEnumerator with a given list of Questions
        /// </summary>
        /// <param name="questions"></param>
        public ConditionalQuestionListEnumerator(List<QuestionAM> questions)
        {
            this.questionEnumerator = questions.GetEnumerator();
        }


        public QuestionAM Current
        {
            get
            {
                return questionEnumerator.Current;
            }
        }

        object IEnumerator.Current
        {
            get
            {
                return questionEnumerator.Current;
            }
        }

        public void Dispose()
        {
            questionEnumerator.Dispose();
        }

        public bool MoveNext()
        {
            while(questionEnumerator.MoveNext())
            {
                if(this.Current.condition.isMet()) { return true; }
            }
            return false;
        }

        public void Reset()
        {
            questionEnumerator.Reset();
        }
    }

}

