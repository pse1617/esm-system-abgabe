using System;
using edu.kit.lymmy.shared.question;


namespace edu.kit.lymmy.xamarin.model.question
{
    /// <summary>
    /// QuestionAM represents a single Question, which the User has to answer
    /// </summary>
	public abstract class QuestionAM
    {
        /// <summary>
        /// The Conditon which has to be met in order for the Question to be asked.
        /// </summary>
        public ConditionAM condition = new ValidConditionAM();
        /// <summary>
        /// The index of the Question in the QUestionnaire
        /// </summary>
        public int questionIndex { get; protected set; }
        /// <summary>
        /// Choice: The answer of the proband: e.g the index of a singleChoiceQuestion, the String of an OpenQuestion
        /// </summary>
        public object choice { get; protected set; }

        /// <summary>
        /// The Question itself: e.g the String: "How are you feeling?"
        /// </summary>
        public readonly String questionText;



        /// <summary>
        /// Builds a QuestionAM with a given QuestionText
        /// </summary>
        /// <param name="questionIndex">The index of the Question in the Questionnaire</param>
        /// <param name="questionText">The Question that is asked</param>
		public QuestionAM(int questionIndex, String questionText)
        {

            this.questionText = questionText;
        }

        /// <summary>
        /// Return the Type of the Question
        /// </summary>
        /// <returns>The QuestionType</returns>
        public abstract QuestionType getType();


    }
}

