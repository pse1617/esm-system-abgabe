using System;
using System.Collections.Generic;


namespace edu.kit.lymmy.xamarin.model.question
{
    /// <summary>
    /// ChoiceQuestionAM is a Question where the User has select his answer from a list of answers.
    /// </summary>
	public abstract class ChoiceQuestionAM : QuestionAM
	{
        public virtual List<String> answers { get; private set; }

        /// <summary>
        /// Builds a ChoiceQuestion with a given QuestionIndex, a QuestionText and a list of Answers
        /// </summary>
        /// <param name="questionIndex">see QuestionAM</param>
        /// <param name="questionText">see QuestionAM</param>
        /// <param name="answers">The answers the User can select</param>
        public ChoiceQuestionAM(int questionIndex, String questionText, List<String> answers) : base(questionIndex, questionText)
        {
            this.answers = answers;
        }


    }

}

