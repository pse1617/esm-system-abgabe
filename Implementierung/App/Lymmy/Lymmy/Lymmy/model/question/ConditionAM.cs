﻿using System;

namespace edu.kit.lymmy.xamarin.model.question
{
    /// <summary>
    /// ConditionAM represents a Condition which has to be fulfilled for a Question to be asked.
    /// </summary>
    public abstract class ConditionAM
    {
        /// <summary>
        /// Checks whether the Condition is met
        /// </summary>
        /// <returns>whether the Condition is met</returns>
        public abstract Boolean isMet();
    }
}
