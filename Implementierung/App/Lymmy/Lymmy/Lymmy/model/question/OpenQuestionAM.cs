using System;
using edu.kit.lymmy.shared.question;


namespace edu.kit.lymmy.xamarin.model.question
{

    /// <summary>
    /// Inherites from QuestionAM
    /// In an OpenQuestion the User has to answer the Question with text.
    /// </summary>
    public class OpenQuestionAM : QuestionAM
    {

        public OpenQuestionAM(int questionIndex, String questionText) : base(questionIndex, questionText)
        {
        }

        public override QuestionType getType()
        {
            return QuestionType.OPEN;
        }

        /// <summary>
        /// answers the Question with a text
        /// </summary>
        /// <param name="probandAnswer">the answerText</param>
        public void answer(String probandAnswer)
        {
            this.choice = probandAnswer;
        }


    }

}

