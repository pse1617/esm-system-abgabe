using System;
using edu.kit.lymmy.shared.question;

namespace edu.kit.lymmy.xamarin.model.question
{

    /// <summary>
    /// Inherites from StepQuestionAM
    /// LikertQuestionAM is a Stepquestion with 3 to 7 steps. 
    /// Example: How much do you like Pizza: Choose between "i love it" and "i hate it"
    /// </summary>
    public class LikertQuestionAM : StepQuestionAM
	{
        /// <summary>
        /// Builds a Likert Question given questionIndex, questionText, leftSliderText, rigthSliderText, steps
        /// steps can only be an integer greater than 2
        /// </summary>
        /// <param name="questionIndex">see Superclass</param>
        /// <param name="questionText">see Superclass</param>
        /// <param name="leftSliderText">see Superclass</param>
        /// <param name="rigthSliderText">see Superclass</param>
        /// <param name="steps">see Superclass</param>
        public LikertQuestionAM(int questionIndex, String questionText, String leftSliderText, String rigthSliderText, int steps) :  base(questionIndex, questionText, leftSliderText, rigthSliderText, steps)
        {
            this.leftSliderText = leftSliderText;
            this.rigthSliderText = rigthSliderText;
			
			if(steps < 2) 
			{
				throw new ArgumentException("LikertQuestion may only have steps greater than 2");
			}
			
            this.steps = steps;
        }

        public override QuestionType getType()
        {
            return QuestionType.LIKERT;
        }


    }

}

