﻿using System;

/// <summary>
/// Inherites from ConditionAM
/// This Condition is the Standard Condition. IsMet always returns true.
/// </summary>
namespace edu.kit.lymmy.xamarin.model.question
{
    public class ValidConditionAM : ConditionAM
    {
        public override bool isMet()
        {
            return true;
        }
    }
}
