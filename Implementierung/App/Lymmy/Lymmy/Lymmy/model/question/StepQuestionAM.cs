using System;

namespace edu.kit.lymmy.xamarin.model.question
{

    /// <summary>
    /// Inherites from Question
    /// A StepQuestion is a Question where the User has to choose between many steps between two opposite answers:
    /// e.g. How much do you like Pizza on a scale from 1 to 100?
    /// </summary>
    public abstract class StepQuestionAM : QuestionAM
	{
        /// <summary>
        /// The text that appears at the left Side of the Slider
        /// </summary>
        public String leftSliderText { get; protected set; }
        /// <summary>
        /// The text that appears at the rigth Side of the Slider
        /// </summary>
        public String rigthSliderText { get; protected set; }
        /// <summary>
        /// The number of steps of the Question
        /// </summary>
        public int steps { get; protected set; }

        /// <summary>
        /// Build a StepQuestion given the QuestionAtrributes, a leftSliderText, rigthSliderText, steps
        /// </summary>
        /// <param name="questionIndex"></param>
        /// <param name="questionText"></param>
        /// <param name="leftSliderText">The text that appears at the left Side of the Slider</param>
        /// <param name="rigthSliderText">The text that appears at the rigth Side of the Slider</param>
        /// <param name="steps">The number of steps of the Question</param>
        public StepQuestionAM(int questionIndex, String questionText, String leftSliderText, String rigthSliderText, int steps) : base(questionIndex, questionText)
        {
            this.leftSliderText = leftSliderText;
            this.rigthSliderText = rigthSliderText;
            this.steps = steps;
        }

        /// <summary>
        /// Answers the Question with the Index of the selected step
        /// </summary>
        /// <param name="selectedAnswerIndex">the Index of the selected step</param>
        public void answer(int selectedAnswerIndex)
        {
            if (selectedAnswerIndex < 0 || selectedAnswerIndex >= steps)
            {
                throw new ArgumentOutOfRangeException("Answer index out of bounds");
            }
            this.choice = selectedAnswerIndex;
        }

    }

}

