﻿using System;
using System.Collections.Generic;
using edu.kit.lymmy.shared.question;

/// <summary>
/// Inherites from SingleChoiceQuestion
/// A SingleChoiceQuestion where the User has to choose between "Yes" and "No"
/// Do not use YesNoQuestion.answers
/// </summary>
namespace edu.kit.lymmy.xamarin.model.question
{
    public class YesNoQuestionAM : SingleChoiceQuestionAM
    {
        public override List<String> answers
        {
            get
            {
                throw new AccessViolationException("Can't get answers from YesNoQuestion");
            }
        }


        public YesNoQuestionAM(int questionIndex, string questionText) : base(questionIndex, questionText, new List<string>())
        {
        }

        public override QuestionType getType()
        {
            return QuestionType.YES_NO;
        }

        /// <summary>
        /// Answers the Question with the index of the selected Answer
        /// </summary>
        /// <param name="selectedAnswerIndex"></param>
        public override void answer(int selectedAnswerIndex)
        {
            if (selectedAnswerIndex < 0 || selectedAnswerIndex >= 2)
            {
                throw new ArgumentOutOfRangeException("Answer index out of bounds");
            }
            this.choice = selectedAnswerIndex;
        }


    }
}
