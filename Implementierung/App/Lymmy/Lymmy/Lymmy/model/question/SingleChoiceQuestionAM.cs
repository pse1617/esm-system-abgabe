using System;
using System.Collections.Generic;
using edu.kit.lymmy.shared.question;

namespace edu.kit.lymmy.xamarin.model.question
{
    /// <summary>
    /// Inherites from ChoiceQuestion
    /// A ChoiceQuestion where only one answer can be selected.
    /// </summary>
    public class SingleChoiceQuestionAM : ChoiceQuestionAM
    {
        /// <summary>
        /// see Superclass
        /// </summary>
        /// <param name="questionIndex"></param>
        /// <param name="questionText"></param>
        /// <param name="answers"></param>
        public SingleChoiceQuestionAM(int questionIndex, String questionText, List<String> answers) : base(questionIndex, questionText, answers)
        {
        }

        public override QuestionType getType()
        {
            return QuestionType.SINGLE_CHOICE;
        }

        /// <summary>
        /// Answers the Question with the index of the selected Answer
        /// </summary>
        /// <param name="selectedAnswerIndex"></param>
        public virtual void answer(int selectedAnswerIndex)
        {
            if (selectedAnswerIndex < 0 || selectedAnswerIndex >= answers.Count)
            {
                throw new ArgumentOutOfRangeException("Answer index out of bounds");
            }
            this.choice = selectedAnswerIndex;
        }

    }

}

