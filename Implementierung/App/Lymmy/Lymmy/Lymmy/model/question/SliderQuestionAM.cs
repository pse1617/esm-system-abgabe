using System;
using edu.kit.lymmy.shared.question;

namespace edu.kit.lymmy.xamarin.model.question
{

    /// <summary>
    /// Inherites from Stepquestion
    /// StepQuestion with 100 steps
    /// </summary>
    public class SliderQuestionAM : StepQuestionAM
	{
        /// <summary>
        /// Builds a Stepquestion with exactly 100 steps
        /// </summary>
        /// <param name="questionIndex"></param>
        /// <param name="questionText"></param>
        /// <param name="leftSliderText"></param>
        /// <param name="rigthSliderText"></param>
        public SliderQuestionAM(int questionIndex, String questionText, String leftSliderText, String rigthSliderText) : base(questionIndex, questionText, leftSliderText, rigthSliderText, 100)
        {
            this.leftSliderText = leftSliderText;
            this.rigthSliderText = rigthSliderText;
            this.steps = steps;
        }

        public override QuestionType getType()
        {
            return QuestionType.SLIDER;
        }
    }

}

