using System;
using System.Collections.Generic;
using edu.kit.lymmy.shared.question;


namespace edu.kit.lymmy.xamarin.model.question
{

    /// <summary>
    /// Inherites from ChoiceQuestionAM
    /// In a MultipleChoiceQuestion the User can choose as many answers of the list as he likes
    /// </summary>
    public class MultipleChoiceQuestionAM : ChoiceQuestionAM
    {
        /// <summary>
        /// See ChoiceQuestionAM
        /// </summary>
        /// <param name="questionIndex"></param>
        /// <param name="questionText"></param>
        /// <param name="answers"></param>
        public MultipleChoiceQuestionAM(int questionIndex, String questionText, List<String> answers) : base(questionIndex, questionText, answers)
        {
        }

        public override QuestionType getType()
        {
            return QuestionType.MULTIPLE_CHOICE;
        }

        /// <summary>
        /// Answers the Question with the indices of the selected Answers
        /// </summary>
        /// <param name="selectedAnswerIndices">the indices of the selected Answers</param>
        public void answer(SortedSet<int> selectedAnswerIndices)
        {
            if (selectedAnswerIndices.Min < 0 || selectedAnswerIndices.Max >= answers.Count)
            {
                throw new ArgumentOutOfRangeException("Answer index out of bounds");
            }
            this.choice = selectedAnswerIndices;
        }

    }

}

