﻿using System;
using System.Collections.Generic;
using edu.kit.lymmy.shared.question;

/// <summary>
/// CoiceConditionAM is a Condition, that looks whether a certain choice in a ChoiceQuestion has been made.
/// </summary>
namespace edu.kit.lymmy.xamarin.model.question
{
    public class ChoiceConditionAM : ConditionAM
    {
        private int choice;
        private ChoiceQuestionAM question;

        /// <summary>
        /// Inherites from ConditionAM
        /// Creates a new ChoiceCondition with a given ChoiceQuestion and the index of the choice
        /// </summary>
        /// <param name="question">The ChoiceQuestion</param>
        /// <param name="choice">The index of the Choice</param>
        public ChoiceConditionAM(ChoiceQuestionAM question, int choice)
        {
            this.question = question;
            this.choice = choice;
        }

        public override bool isMet()
        {
            switch(this.question.getType())
            {
                case QuestionType.MULTIPLE_CHOICE:
                    SortedSet<int> MCAnswer = (SortedSet<int>)question.choice;
                    foreach(int answer in MCAnswer)
                    {
                        if (answer==this.choice) { return true; }
                    }
                    return false;
                case QuestionType.SINGLE_CHOICE:
                    int SCAnswer = (int)question.choice;
                    return (SCAnswer == this.choice);
                default:
                    //For new ChoiceQuestions
                    throw new NotImplementedException();
            }
        }
    }
}
