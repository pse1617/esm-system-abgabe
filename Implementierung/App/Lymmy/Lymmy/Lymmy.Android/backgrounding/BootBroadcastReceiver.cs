﻿using Android.App;
using Android.Content;
using edu.kit.lymmy.xamarin.model.scheduler;
using Xamarin.Forms;

namespace Lymmy.Droid.backgrounding
{
    [BroadcastReceiver]
    [IntentFilter(new[] { Intent.ActionBootCompleted }, Priority = (int)IntentFilterPriority.LowPriority)]
    public class BootBroadcastReceiver : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent i)
        {
            DependencyService.Get<ITaskStarter>().Start();
        }
    }
}