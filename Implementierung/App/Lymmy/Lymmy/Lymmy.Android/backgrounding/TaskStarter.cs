﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using edu.kit.lymmy.xamarin.model.scheduler;
using Lymmy.Droid.backgrounding;

[assembly:Xamarin.Forms.Dependency(typeof(TaskStarter))]
namespace Lymmy.Droid.backgrounding
{
    public class TaskStarter: ITaskStarter
    {
        public void Start()
        {
            Application.Context.StartService(new Intent(Application.Context, typeof(BackgroundService)));
        }

        public void ScheduleNextStart(TimeSpan timeSpan)
        {
            AlarmManager alarmManager = (AlarmManager) Application.Context.GetSystemService(Context.AlarmService);
            alarmManager.Set(AlarmType.ElapsedRealtime, SystemClock.ElapsedRealtime() + timeSpan.Milliseconds,
                PendingIntent.GetService(Application.Context, 0, new Intent(Application.Context, typeof(BackgroundService)), PendingIntentFlags.UpdateCurrent));
        }
    }
}