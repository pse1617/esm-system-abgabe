﻿using Android.Content;
using edu.kit.lymmy.shared.sensor;
using edu.kit.lymmy.xamarin.model.trigger;
using Lymmy.Droid.backgrounding;

[assembly: Xamarin.Forms.Dependency(typeof(DisplayActivityService))]

namespace Lymmy.Droid.backgrounding
{
    public class DisplayActivityService : BroadcastReceiver, IDisplayActivityService
    {
        public event OnStatusChanged StatusChanged;

        public override void OnReceive(Context context, Intent intent)
        {
            switch (intent.Action)
            {
                case Intent.ActionScreenOn:
                    StatusChanged(DisplayStateData.DisplayState.ON);
                    break;
                case Intent.ActionScreenOff:
                    StatusChanged(DisplayStateData.DisplayState.OFF);
                    break;
                case Intent.ActionUserPresent:
                    StatusChanged(DisplayStateData.DisplayState.UNLOCKED);
                    break;
            }
        }
    }
}