﻿using Android.App;
using Android.Content;
using Android.OS;
using edu.kit.lymmy.xamarin.model.scheduler;
using edu.kit.lymmy.xamarin.model.trigger;
using Java.Lang;
using Xamarin.Forms;

namespace Lymmy.Droid.backgrounding
{
    [Service(IsolatedProcess = false)]
    public class BackgroundService : Service
    {
        private DisplayActivityService displayActivityService;

        public override IBinder OnBind(Intent intent)
        {
            throw new System.NotImplementedException();
        }

        public override void OnCreate()
        {
            base.OnCreate();
            IntentFilter filter = new IntentFilter();
            filter.AddAction(Intent.ActionScreenOn);
            filter.AddAction(Intent.ActionScreenOff);
            filter.AddAction(Intent.ActionUserPresent);
            displayActivityService = (DisplayActivityService) DependencyService.Get<IDisplayActivityService>();
            RegisterReceiver(displayActivityService, filter);
        }

        public override void OnDestroy()
        {
            UnregisterReceiver(displayActivityService);
            base.OnDestroy();
        }

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            new Thread(()=>new TaskManager().GetTasks().Wait()).Start();
            return StartCommandResult.Sticky;
        }
    }
}