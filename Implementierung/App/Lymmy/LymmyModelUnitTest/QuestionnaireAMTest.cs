﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using edu.kit.lymmy.xamarin.model;
using edu.kit.lymmy.xamarin.model.question;
using edu.kit.lymmy.xamarin.model.@event;
using edu.kit.lymmy.xamarin.model.trigger;
using System.Threading.Tasks;

namespace LymmyModelUnitTest
{
    /// <summary>
    /// Tests QuestionnaireAM
    /// </summary>
    [TestClass]
    public class QuestionnaireAMTest
    {
        private QuestionnaireAM _questionnaire;
        private QuestionAM _firstQuestion;

        private long _cooldown = 1000;
        

        [TestInitialize()]
        public void MyTestInitialize()
        {

            _firstQuestion = new OpenQuestionAM(0, "Hi");
             List<QuestionAM> _questionList = new List<QuestionAM>();


            _questionList.Add(_firstQuestion);

            _questionnaire = new QuestionnaireAM(0, _cooldown, _questionList, new SingleTriggerEventAM(new StudyStartTriggerAM()));
        }

        [TestMethod]
        public void QuestionnaireIsNotNullTest()
        {
            Assert.IsNotNull(_questionnaire);
        }

        [TestMethod]
        public void QuestionnaireCooldownSavedCorrectlyTest()
        {
            Assert.AreEqual(_cooldown, _questionnaire.cooldown);
        }

        [TestMethod]
        public async Task QuestionnaireGetFirstQuestionTest()
        {
            
            await _questionnaire.moveNextQuestion();
            QuestionAM actual = _questionnaire.getCurrentQuestion();
            Assert.AreEqual(_firstQuestion, actual);
        }
    }
}
