﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using edu.kit.lymmy.xamarin.model.question;
using edu.kit.lymmy.shared.question;
using System.Collections.Generic;

namespace edu.kit.lymmy.xamarin.test
{
    /// <summary>
    /// This class is the Test Class of OpenQuestionAM
    /// </summary>
    [TestClass]
    public class YesNoQuestionAMTest
    {
        YesNoQuestionAM question;

        [TestInitialize]
        public void TestInitialize()
        {
            question = new YesNoQuestionAM(1, "Are you drunk?");
        }

        [TestCleanup]
        public void TestCleanup()
        {
            question = null;
        }

        [TestMethod]
        public void YesNoAnswerTest()
        {
            int probandAnswer = 1;
            question.answer(probandAnswer);
            object actualAnswer = question.choice;

            Assert.AreEqual(probandAnswer, actualAnswer, "Input answer is not stored correctly");
        }
		
		[TestMethod]
        public void YesNoGetTypeTest()
        {
            QuestionType expected = QuestionType.YES_NO;
            QuestionType actual = question.getType();

            Assert.AreEqual(expected, actual, "Question does not return QuestionType.YES_NO");

        }
    }
}
