﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using edu.kit.lymmy.xamarin.model.question;

namespace edu.kit.lymmy.xamarin.test
{
    /// <summary>
    /// This class is the Test Class of OpenQuestionAM
    /// </summary>
    [TestClass]
    public class QuestionAMTest
    {
        [TestMethod]
        public void QuestionConstructorTest()
        {
            String questionText = "How was your Day?";
            OpenQuestionAM testQuestion = new OpenQuestionAM(1, questionText);

            String actualQuestionText = testQuestion.questionText;
            Assert.AreEqual(questionText, actualQuestionText, "QuestionText not build correctly");
			ConditionAM questionCondition = testQuestion.condition;

			Assert.IsInstanceOfType(questionCondition, typeof(ValidConditionAM), "Not a ValidConditionAM created");
        }
	
    }
}
