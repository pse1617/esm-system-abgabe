﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using edu.kit.lymmy.xamarin.model.question;
using edu.kit.lymmy.shared.question;

namespace edu.kit.lymmy.xamarin.test
{
    /// <summary>
    /// This class is the Test Class of SliderQuestionAM
    [TestClass]
    public class SliderQuestionAMTest
    {
        private SliderQuestionAM question;

        [TestInitialize]
        public void TestInitialize()
        {
            question = new SliderQuestionAM(1, "How good are you at football?", "very bad", "very good");
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            question = null;
        }

        [TestMethod]
        public void SliderAnswerTest()
        {
            int probandAnswer = 1;
            question.answer(probandAnswer);
            object actualAnswer = question.choice;

            Assert.AreEqual(probandAnswer, actualAnswer, "Input answer is not stored correctly");
        }

        [TestMethod]
        public void SliderGetTypeTest()
        {
            QuestionType actual = question.getType();

            Assert.AreEqual(QuestionType.SLIDER, actual, "Should return QuestionType.SLIDER");
        }
    }
}
