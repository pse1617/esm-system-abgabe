﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using edu.kit.lymmy.xamarin.model.question;
using edu.kit.lymmy.shared.question;
using System.Collections.Generic;

namespace edu.kit.lymmy.xamarin.test
{
    /// <summary>
    /// This class is the Test Class of OpenQuestionAM
    /// </summary>
    [TestClass]
    public class SingleChoiceQuestionAMTest
    {
        SingleChoiceQuestionAM scQuestion;

        [TestInitialize]
        public void TestInitialize()
        {
            List<string> answers = new List<string>(new string[] { "Good", "meh", "Bad" });
            scQuestion = new SingleChoiceQuestionAM(1, "How was your day?", answers);
        }

        [TestCleanup]
        public void TestCleanup()
        {
            scQuestion = null;
        }

        [TestMethod]
        public void SingleCAnswerTest()
        {
            int probandAnswer = 1;
            scQuestion.answer(probandAnswer);
            object actualAnswer = scQuestion.choice;

            Assert.AreEqual(probandAnswer, actualAnswer, "Input answer is not stored correctly");
        }
		
		[TestMethod]
        public void SingleCGetTypeTest()
        {
            QuestionType expected = QuestionType.SINGLE_CHOICE;
            QuestionType actual = scQuestion.getType();

            Assert.AreEqual(expected, actual, "SingleChoiceQuestion does not return QuestionType.Single_Choice");

        }
    }
}
