﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using edu.kit.lymmy.xamarin.model.question;
using edu.kit.lymmy.shared.question;

namespace edu.kit.lymmy.xamarin.test
{
    /// <summary>
    /// This class is the Test Class of LikertQuestionAM
    /// </summary>
    [TestClass]
    public class LikertQuestionAMTest
    {
        private LikertQuestionAM question;

        [TestInitialize]
        public void TestInitialize()
        {
            question = new LikertQuestionAM(1, "How good are you at football?", "very bad", "very good", 5);
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            question = null;
        }

        [TestMethod]
        public void LikertAnswerTest()
        {
            int probandAnswer = 1;
            question.answer(probandAnswer);
            object actualAnswer = question.choice;

            Assert.AreEqual(probandAnswer, actualAnswer, "Input answer is not stored correctly");
        }

        [TestMethod]
        public void LikertGetTypeTest()
        {
            QuestionType actual = question.getType();

            Assert.AreEqual(QuestionType.LIKERT, actual, "Should return QuestionType.LIKERT");
        }
    }
}
