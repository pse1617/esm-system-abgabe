﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using edu.kit.lymmy.xamarin.model.question;
using System.Collections.Generic;
using edu.kit.lymmy.shared.question;

namespace edu.kit.lymmy.xamarin.test
{
    /// <summary>
    /// This class is the Test Class of MultipleChoiceQuestionAM
    /// </summary>
    [TestClass]
    public class MultipleChoiceQuestionAMTest
    {

        MultipleChoiceQuestionAM mcQuestion;

        [TestInitialize]
        public void TestInitialize()
        {
            List<string> answers = new List<string>(new string[] { "Good", "meh", "Bad" });
            mcQuestion = new MultipleChoiceQuestionAM(1, "How was your day?", answers);
        }

        [TestCleanup]
        public void TestCleanup()
        {
            mcQuestion = null;
        }


        [TestMethod]
        public void MultipleCAnswerTest()
        {
            SortedSet<int> probandAnswer = new SortedSet<int> { 1, 2};
            mcQuestion.answer(probandAnswer);
            object actualAnswer = mcQuestion.choice;

            Assert.AreEqual(probandAnswer, actualAnswer, "Input answer is not stored correctly");

        }

        [TestMethod]
        public void MultipleCGetTypeTest()
        {
            QuestionType actual = mcQuestion.getType();

            Assert.AreEqual(QuestionType.MULTIPLE_CHOICE, actual, "Should return QuestionType.MULTIPLE_CHOICE");
        }
    }
}
