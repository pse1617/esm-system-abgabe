﻿using System;
using edu.kit.lymmy.xamarin.model.question;
using edu.kit.lymmy.shared.question;

namespace LymmyModelUnitTest.question
{
    /// <summary>
    /// Mock class to test QuestionAM
    /// </summary>
    public class QuestionAMMock : QuestionAM
    {
        public QuestionAMMock(int questionIndex, string questionText) : base(questionIndex, questionText)
        {
        }

        public override QuestionType getType()
        {
            throw new NotImplementedException();
        }
    }
}
