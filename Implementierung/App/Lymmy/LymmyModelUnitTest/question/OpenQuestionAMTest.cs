﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using edu.kit.lymmy.xamarin.model.question;
using edu.kit.lymmy.shared.question;

namespace edu.kit.lymmy.xamarin.test
{
    /// <summary>
    /// This class is the Test Class of OpenQuestionAM
    /// </summary>
    [TestClass]
    public class OpenQuestionAMTest
    {
        OpenQuestionAM openQuestion;

        [TestInitialize]
        public void TestInitialize()
        {
            openQuestion = new OpenQuestionAM(1, "How was your day?");
        }

        [TestCleanup]
        public void TestCleanup()
        {
            openQuestion = null;
        }

        [TestMethod]
        public void OpenAnswerTest()
        {
            String probandAnswer = "Very nice";
            openQuestion.answer(probandAnswer);
            object actualAnswer = openQuestion.choice;

            Assert.AreEqual(probandAnswer, actualAnswer, "Input answer is not stored correctly");
        }
		
		[TestMethod]
        public void OpenGetTypeTest()
        {
            QuestionType expected = QuestionType.OPEN;
            QuestionType actual = openQuestion.getType();

            Assert.AreEqual(expected, actual, "OpenQuestion does not return QuestionType.Open");

        }
    }
}
