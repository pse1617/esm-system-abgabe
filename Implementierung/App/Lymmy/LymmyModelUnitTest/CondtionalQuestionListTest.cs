﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using edu.kit.lymmy.xamarin.model;
using edu.kit.lymmy.xamarin.model.question;
using System.Collections.Generic;

namespace LymmyModelUnitTest
{

    [TestClass]
    public class ConditionalQuestionListTest
    {
        private ConditionalQuestionList _conditionalQuestionList;

        private QuestionAM _firstQuestion;
        private ChoiceQuestionAM _choiceQuestion;
        private QuestionAM _conditionalQuestion;

        private static string SINGLE_CHOICE = "singleChoice";
        private static string MUL_CHOICHE = "multipleChoice";
        private static List<string> _choiceQuestionTypes = new List<string> { SINGLE_CHOICE, MUL_CHOICHE };

        [TestInitialize]
        public void testInitialize()
        {

            _firstQuestion = new OpenQuestionAM(0, "First Question:");
            _choiceQuestion = new SingleChoiceQuestionAM(1, "Green or Blue?", new List<string> { "green", "blue" });
            _conditionalQuestion = new OpenQuestionAM(2, "Last Question:");

            _conditionalQuestion.condition = new ChoiceConditionAM(_choiceQuestion, 0);

            List<QuestionAM> questionList = new List<QuestionAM>();

            questionList.Add(_firstQuestion);
            questionList.Add(_choiceQuestion);
            questionList.Add(_conditionalQuestion);

            _conditionalQuestionList = new ConditionalQuestionList(questionList);
        }

        [TestMethod]
        public void CListGetFirstQuestionTest()
        {
            foreach (string questionType in _choiceQuestionTypes)
            {
                if (questionType == MUL_CHOICHE)
                {
                    SetUpWithMultipleChoice();
                }
                ConditionalQuestionListEnumerator _enumerator = _conditionalQuestionList.GetEnumerator();
                _enumerator.MoveNext();

                QuestionAM expected = _firstQuestion;
                QuestionAM actual = _enumerator.Current;
                Assert.AreEqual(expected, actual);
            }
        }

        [TestMethod]
        public void CListGetSecondQuestionTest()
        {
            foreach (string questionType in _choiceQuestionTypes)
            {
                if (questionType == MUL_CHOICHE)
                {
                    SetUpWithMultipleChoice();
                }
                ConditionalQuestionListEnumerator _enumerator = _conditionalQuestionList.GetEnumerator();
                _enumerator.MoveNext();


                ((OpenQuestionAM)_enumerator.Current).answer("hi");
                _enumerator.MoveNext();

                QuestionAM expected = _choiceQuestion;
                QuestionAM actual = _enumerator.Current;
                Assert.AreEqual(expected, actual);
            }
        }

        [TestMethod]
        public void CListConditionMetTest()
        {
            foreach (string questionType in _choiceQuestionTypes)
            {
                if (questionType == MUL_CHOICHE)
                {
                    SetUpWithMultipleChoice();
                }
                ConditionalQuestionListEnumerator _enumerator = _conditionalQuestionList.GetEnumerator();
                _enumerator.MoveNext();


                ((OpenQuestionAM)_enumerator.Current).answer("hi");
                _enumerator.MoveNext();

                if (questionType == SINGLE_CHOICE)
                {
                    ((SingleChoiceQuestionAM)_enumerator.Current).answer(0);
                } else if (questionType == MUL_CHOICHE)
                {
                    ((MultipleChoiceQuestionAM)_enumerator.Current).answer(new SortedSet<int> { 0, 1});
                }

                if (_enumerator.MoveNext())
                {
                    QuestionAM expected = _conditionalQuestion;
                    QuestionAM actual = _enumerator.Current;
                    Assert.AreEqual(expected, actual);
                }
                else Assert.IsTrue(false, "Question not activated");
            }

        }

        [TestMethod]
        public void CListConditionNotMetTest()
        {
            ConditionalQuestionListEnumerator _enumerator = _conditionalQuestionList.GetEnumerator();
            _enumerator.MoveNext();

            ((OpenQuestionAM)_enumerator.Current).answer("hi");
            _enumerator.MoveNext();

            ((SingleChoiceQuestionAM)_enumerator.Current).answer(1);
            if (_enumerator.MoveNext())
            {
                Assert.IsTrue(false, "Question should not be activated");
            }


        }

        private void SetUpWithMultipleChoice()
        {

            _firstQuestion = new OpenQuestionAM(0, "First Question:");
            _choiceQuestion = new MultipleChoiceQuestionAM(1, "Green or Blue?", new List<string> { "green", "blue" });
            _conditionalQuestion = new OpenQuestionAM(2, "Last Question:");

            _conditionalQuestion.condition = new ChoiceConditionAM(_choiceQuestion, 0);

            List<QuestionAM> questionList = new List<QuestionAM>();

            questionList.Add(_firstQuestion);
            questionList.Add(_choiceQuestion);
            questionList.Add(_conditionalQuestion);

            _conditionalQuestionList = new ConditionalQuestionList(questionList);
        }


    }
}
