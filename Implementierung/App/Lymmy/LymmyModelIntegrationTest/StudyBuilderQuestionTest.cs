﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

using edu.kit.lymmy.shared;
using edu.kit.lymmy.xamarin.model.builder;
using edu.kit.lymmy.xamarin.model;
using System.Linq;
using edu.kit.lymmy.shared.@event;
using edu.kit.lymmy.shared.question;
using edu.kit.lymmy.shared.sensor;
using System.Threading.Tasks;
using edu.kit.lymmy.xamarin.model.question;

namespace LymmyModelIntegrationTest
{
    [TestClass]
    public class StudyBuilderQuestionTest:StudyBuilderTest
    {

        private Study buildDefaultStudyWithQuestion(Question question)
        {

            IList<Question> questionList = new List<Question> { question };

          

            Questionnaire questionnaire = new Questionnaire(_defaultQuestionnaireEvent, _defaultQuestionnaireCooldownTime, questionList, _defaultQuestionnaireContextSensors, _defaultQuestionnaireName);
            return buildDefaultStudy(new List<Questionnaire> { questionnaire });
        }

        [TestMethod]
        public async Task buildMultipleChoiceQuestion()
        {
            string actualQuestionText = "questionText";
            IList<string> answers = new List<string> { "one", "two", "three" };
            Question question = new MultipleChoiceQuestion(new ValidCondition(), actualQuestionText, answers);

            Study study = buildDefaultStudyWithQuestion(question);

            new StudyBuilderAM().buildStudy(study);

            //Assertions


            QuestionnaireAM modelQuestionnaire = StudyAM.Instance.Questionnaires.First();
            await modelQuestionnaire.moveNextQuestion();
            QuestionAM modelQuestion = modelQuestionnaire.getCurrentQuestion();

            Assert.AreEqual(QuestionType.MULTIPLE_CHOICE, modelQuestion.getType());
            Assert.IsTrue(Enumerable.SequenceEqual<string>(((MultipleChoiceQuestionAM)modelQuestion).answers, answers));
            Assert.AreEqual(actualQuestionText, modelQuestion.questionText);
        }

        [TestMethod]
        public async Task buildSingleChoiceQuestion()
        {
            string actualQuestionText = "questionText";
            IList<string> answers = new List<string> { "one", "two", "three" };
            Question question = new SingleChoiceQuestion(new ValidCondition(), actualQuestionText, answers);

            Study study = buildDefaultStudyWithQuestion(question);

            new StudyBuilderAM().buildStudy(study);

            //Assertions


            QuestionnaireAM modelQuestionnaire = StudyAM.Instance.Questionnaires.First();
            await modelQuestionnaire.moveNextQuestion();
            QuestionAM modelQuestion = modelQuestionnaire.getCurrentQuestion();

            Assert.AreEqual(QuestionType.SINGLE_CHOICE, modelQuestion.getType());
            Assert.IsTrue(Enumerable.SequenceEqual<string>(((SingleChoiceQuestionAM)modelQuestion).answers, answers));
            Assert.AreEqual(actualQuestionText, modelQuestion.questionText);
        }

        [TestMethod]
        public async Task buildLikertQuestion()
        {
            string actualQuestionText = "questionText";
            string actualLeftSliderText = "left";
            string actualRightSliderText = "right";
            int steps = 5;

            Question question = new LikertQuestion(new ValidCondition(), actualQuestionText, actualLeftSliderText, actualRightSliderText, steps);

            Study study = buildDefaultStudyWithQuestion(question);

            new StudyBuilderAM().buildStudy(study);

            //Assertions


            QuestionnaireAM modelQuestionnaire = StudyAM.Instance.Questionnaires.First();
            await modelQuestionnaire.moveNextQuestion();
            QuestionAM modelQuestion = modelQuestionnaire.getCurrentQuestion();

            Assert.AreEqual(actualQuestionText, modelQuestion.questionText);
            Assert.AreEqual(QuestionType.LIKERT, modelQuestion.getType());
            LikertQuestionAM likertModelQuestion = (LikertQuestionAM)modelQuestion;

            Assert.AreEqual(actualLeftSliderText, likertModelQuestion.leftSliderText);
            Assert.AreEqual(actualRightSliderText, likertModelQuestion.rigthSliderText);
            Assert.AreEqual(steps, likertModelQuestion.steps);

        }

        [TestMethod]
        public async Task buildSliderQuestion()
        {
            string actualQuestionText = "questionText";
            string actualLeftSliderText = "left";
            string actualRightSliderText = "right";

            Question question = new SliderQuestion(new ValidCondition(), actualQuestionText, actualLeftSliderText, actualRightSliderText);

            Study study = buildDefaultStudyWithQuestion(question);

            new StudyBuilderAM().buildStudy(study);

            //Assertions


            QuestionnaireAM modelQuestionnaire = StudyAM.Instance.Questionnaires.First();
            await modelQuestionnaire.moveNextQuestion();
            QuestionAM modelQuestion = modelQuestionnaire.getCurrentQuestion();

            Assert.AreEqual(actualQuestionText, modelQuestion.questionText);
            Assert.AreEqual(QuestionType.SLIDER, modelQuestion.getType());
            SliderQuestionAM sliderModelQuestion = (SliderQuestionAM)modelQuestion;

            Assert.AreEqual(actualLeftSliderText, sliderModelQuestion.leftSliderText);
            Assert.AreEqual(actualRightSliderText, sliderModelQuestion.rigthSliderText);

        }

    }
}
