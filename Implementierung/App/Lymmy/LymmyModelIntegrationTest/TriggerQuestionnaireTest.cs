﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using edu.kit.lymmy.xamarin.model.@event;
using edu.kit.lymmy.xamarin.model;
using System.Collections.Generic;
using edu.kit.lymmy.xamarin.model.question;
using Moq;
using System;

namespace LymmyModelIntegrationTest
{
    [TestClass]
    public class TriggerQuestionnaireTest
    {
        [TestMethod]
        public void TriggerOneQuestionnaire()
        {
            ///
            /// Step 1 Initializing
            StudyStartTriggerDummy dummyTrigger = new StudyStartTriggerDummy();
            SingleTriggerEventAM @event = new SingleTriggerEventAM(dummyTrigger);
            int questionnaireIndex = 0;
            long cooldownTime = 1000;
            List<QuestionAM> questions = new List<QuestionAM>();


            //mocking Questionnaire to check if start() is called
            Mock<QuestionnaireAM> mockQuestionnaire = new Mock<QuestionnaireAM>(questionnaireIndex, cooldownTime, questions, @event);
            mockQuestionnaire.CallBase = true;
            mockQuestionnaire.Setup(mock => mock.start());

            //fetchContextData will be tested seperatly
            mockQuestionnaire.Setup(mock => mock.fetchContextData());

            //initialize Study
            new StudyAM(new List<QuestionnaireAM> { mockQuestionnaire.Object }, null);

            ///
            /// Step 2 Start the study
            StudyAM.Instance.startStudy();

            ///
            /// Step 3 Trigger the DummyTrigger:

            dummyTrigger.triggerManually();

           

            


            //Assert start is called
            mockQuestionnaire.Verify(mock => mock.start(), Times.Once(), "Questionnaire has not been started.");

        }
    }
}
