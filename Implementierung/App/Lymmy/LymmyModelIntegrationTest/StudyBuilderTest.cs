﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System;
using edu.kit.lymmy.shared;
using edu.kit.lymmy.xamarin.model.builder;
using edu.kit.lymmy.xamarin.model;
using System.Linq;
using edu.kit.lymmy.shared.@event;
using edu.kit.lymmy.shared.question;
using edu.kit.lymmy.xamarin.model.question;
using edu.kit.lymmy.shared.sensor;
using edu.kit.lymmy.xamarin.model.@event;
using edu.kit.lymmy.xamarin.model.trigger;
using System.Threading.Tasks;

namespace LymmyModelIntegrationTest
{
    /// <summary>
    /// This class whether the StudyBuilder builds correctly and if the constructors of the shared classes are correct.
    /// 
    /// In this test the default value for a Question is an OpenQuestion
    ///     the default value for SensorData is StudyStartData
    ///     
    /// Non-default cases have an exclusive test case
    /// </summary>
    [TestClass]
    public class StudyBuilderTest
    {
        //Study Defaults
        protected static string _defaultUserID = "0";
        protected static string _defaultStudyID = "0";
        protected static string _defaultName = "name";
        protected static string _defaultDescription = "description";
        protected static DateTime _defaultStartTime = DateTime.Today.AddDays(1);
        protected static DateTime _defaultEndTime = DateTime.Today.AddDays(2);

        //Questionnaire Defaults
        protected static string _defaultQuestionText = "questionText";
        protected static IList<Question> _defaultQuestionList = new List<Question> { new OpenQuestion(new ValidCondition(), _defaultQuestionText) };
        protected static long _defaultQuestionnaireCooldownTime = 1000;
        protected static IList<string> _defaultQuestionnaireContextSensors = new List<string>();
        protected static string _defaultQuestionnaireName = "questionnaireName";
        protected static Event _defaultQuestionnaireEvent = new SensorEvent(new StudyStartData());



        protected Study buildDefaultStudy(IList<Questionnaire> questionnaire)
        {
            return new Study(_defaultStudyID, _defaultUserID, _defaultName ,_defaultDescription, _defaultStartTime, _defaultEndTime, questionnaire);
        }

        /// <summary>
        /// Checks if a study with zero questions can be build
        /// </summary>
        [TestMethod]
        public void BuildEmptyStudy()
        {
            Study study = buildDefaultStudy(new List<Questionnaire>());
            new StudyBuilderAM().buildStudy(study);
            Assert.AreEqual(study, StudyAM.Instance.Definition);
            Assert.IsTrue(Enumerable.SequenceEqual<QuestionnaireAM>(new List<QuestionnaireAM>(), StudyAM.Instance.Questionnaires));
        }

        /// <summary>
        /// Checks if the simplest study can be build, (one of everything)
        /// </summary>
        [TestMethod]
        public async Task BuildSimpleStudy()
        {
            //Create Default Questionnaire

            Questionnaire questionnaire = new Questionnaire(_defaultQuestionnaireEvent, _defaultQuestionnaireCooldownTime, _defaultQuestionList, _defaultQuestionnaireContextSensors, _defaultQuestionnaireName);
            List<Questionnaire> questionnaireList = new List<Questionnaire> { questionnaire };

            //Build Study 
            Study study = buildDefaultStudy(questionnaireList);
            new StudyBuilderAM().buildStudy(study);

            //Assert that the definition stays the same
            Assert.AreEqual(study, StudyAM.Instance.Definition);

            //Assert that the Questionnaire stayed the same
            Assert.IsTrue(StudyAM.Instance.Questionnaires.Count == 1);

            QuestionnaireAM modelQuestionnaire = StudyAM.Instance.Questionnaires.First();
            Assert.AreEqual(_defaultQuestionnaireCooldownTime, modelQuestionnaire.cooldown);

            //Assert that the Question stayed the same
            Assert.IsTrue(modelQuestionnaire.questions.Count == 1);

            await modelQuestionnaire.moveNextQuestion();

            QuestionAM modelQuestion = modelQuestionnaire.getCurrentQuestion();

            Assert.AreEqual(typeof(ValidConditionAM), modelQuestion.condition.GetType());
            Assert.AreEqual(_defaultQuestionText, modelQuestion.questionText);

            //Assert that the Event is built properly
            EventAM modelEvent = modelQuestionnaire.@event;
            Assert.AreEqual(typeof(SingleTriggerEventAM), modelEvent.GetType());

            //Assert that the Trigger is built properly
            TriggerAM modelTrigger = ((SingleTriggerEventAM)modelEvent).trigger;

            Assert.AreEqual(typeof(StudyStartTriggerAM), modelTrigger.GetType());

        }

        /// <summary>
        /// Build Study with one Questionnaire with a CompositeEvent
        /// </summary>
        [TestMethod]
        public void buildCompositeEvents()
        {
            /// Create Questionnaire with CompositeEvent
            Event @event;

            //tree studyStartEvents
            Event firstEvent = new SensorEvent(new StudyStartData());
            Event secondEvent = new SensorEvent(new StudyStartData());
            Event thirdEvent = new SensorEvent(new StudyStartData());

            CompositeEvent twoThreeEvent = new CompositeEvent(new List<Event>() { secondEvent, thirdEvent }, LogicalOperator.OR);
            @event = new CompositeEvent(new List<Event> { firstEvent, twoThreeEvent }, LogicalOperator.AND);


            //build remaining part of questionnaire
            Questionnaire questionnaire = new Questionnaire(@event, _defaultQuestionnaireCooldownTime, _defaultQuestionList, _defaultQuestionnaireContextSensors, _defaultQuestionnaireName);
            List<Questionnaire> questionnaireList = new List<Questionnaire> { questionnaire };

            //build Study
            Study study = buildDefaultStudy(questionnaireList);
            (new StudyBuilderAM()).buildStudy(study);

            ///ASSERTIONS
            EventAM modelEvent = StudyAM.Instance.Questionnaires.First().@event;
            Assert.IsInstanceOfType(modelEvent, typeof(CompositeEventAM));
            Assert.AreEqual(LogicalOperator.AND,((CompositeEventAM)modelEvent).op);

            //TESTTODO It is not tested if studyStartEvents are created, also if the second event is also a composite
        }

        [TestMethod]
        public void buildConditionalQuestions()
        {
            ///Create Study

            //Build ConditionalQuestion
            Question yesNo = new YesNoQuestion(new ValidCondition(), "yesNo", new List<string> { "yes", "no" });
            Question condQuestion = new OpenQuestion(new Condition(0, 0), "testQuestion");
            List<Question> questions = new List<Question> { yesNo, condQuestion };

            Questionnaire questionnaire = new Questionnaire(_defaultQuestionnaireEvent, _defaultQuestionnaireCooldownTime, questions, _defaultQuestionnaireContextSensors, _defaultQuestionnaireName);
            Study study = buildDefaultStudy(new List<Questionnaire> { questionnaire });

            new StudyBuilderAM().buildStudy(study);

            ///ASSERTIONS
            Assert.IsTrue(StudyAM.Instance.Questionnaires.First().questions.Count == 2);

            //TESTTODO It is not tested if a Condition is actually created
        }

       
      
    }


}
