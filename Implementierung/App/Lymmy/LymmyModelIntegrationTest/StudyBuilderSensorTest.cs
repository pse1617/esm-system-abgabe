﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

using edu.kit.lymmy.shared;
using edu.kit.lymmy.xamarin.model.builder;
using edu.kit.lymmy.xamarin.model;
using System.Linq;
using edu.kit.lymmy.shared.@event;
using edu.kit.lymmy.shared.question;
using edu.kit.lymmy.shared.sensor;
using edu.kit.lymmy.xamarin.model.@event;
using edu.kit.lymmy.xamarin.model.trigger;
using System;

namespace LymmyModelIntegrationTest
{
    [TestClass]
    public class StudyBuilderSensorTest : StudyBuilderTest
    {
        private Study buildDefaultStudyWithSensor(SensorData sensorData)
        {
            Event @event = new SensorEvent(sensorData);

            Questionnaire questionnaire = new Questionnaire(@event, _defaultQuestionnaireCooldownTime, _defaultQuestionList, _defaultQuestionnaireContextSensors, _defaultQuestionnaireName);
            return buildDefaultStudy(new List<Questionnaire> { questionnaire });

        }


        [TestMethod]
        public void buildCoarseLocation()
        {
            string address = "address";
            SensorData coarseLocationData = new CoarseLocationData(address);
            Study study = buildDefaultStudyWithSensor(coarseLocationData);

            new StudyBuilderAM().buildStudy(study);

            ///Assertions

            QuestionnaireAM modelQuestionnaire = StudyAM.Instance.Questionnaires.First();
            EventAM modelEvent = modelQuestionnaire.@event;

            Assert.IsInstanceOfType(modelEvent, typeof(SingleTriggerEventAM));

            TriggerAM modelTrigger = ((SingleTriggerEventAM)modelEvent).trigger;

            Assert.IsTrue(modelTrigger.getSensorType() == SensorType.COARSE_LOCATION);
            Assert.AreEqual(address, ((GPSTriggerAM)modelTrigger)._address);
        }


        [TestMethod]
        public void buildDisplayState()
        {
            DisplayStateData.DisplayState displayState = DisplayStateData.DisplayState.ON;
            SensorData sensorData = new DisplayStateData(displayState);
            Study study = buildDefaultStudyWithSensor(sensorData);

            new StudyBuilderAM().buildStudy(study);

            ///Assertions
            QuestionnaireAM modelQuestionnaire = StudyAM.Instance.Questionnaires.First();
            EventAM modelEvent = modelQuestionnaire.@event;

            Assert.IsInstanceOfType(modelEvent, typeof(SingleTriggerEventAM));

            TriggerAM modelTrigger = ((SingleTriggerEventAM)modelEvent).trigger;

            Assert.IsTrue(modelTrigger.getSensorType() == SensorType.DISPLAY_STATE);
            Assert.AreEqual(displayState, ((DisplayActivityTriggerAM)modelTrigger).displayState);

        }


        [TestMethod]
        public void buildPartTime()
        {
            PartTimeData.Timetype timeType = PartTimeData.Timetype.Morning;
            bool negation = false;

            SensorData sensorData = new PartTimeData(timeType, negation);

            Study study = buildDefaultStudyWithSensor(sensorData);

            new StudyBuilderAM().buildStudy(study);

            ///Assertions
            QuestionnaireAM modelQuestionnaire = StudyAM.Instance.Questionnaires.First();
            EventAM modelEvent = modelQuestionnaire.@event;

            Assert.IsInstanceOfType(modelEvent, typeof(SingleTriggerEventAM));

            TriggerAM modelTrigger = ((SingleTriggerEventAM)modelEvent).trigger;

            Assert.IsTrue(modelTrigger.getSensorType() == SensorType.PARTTIME);
            Assert.AreEqual(timeType, ((PartTimeTriggerAM)modelTrigger)._timeType);
            Assert.AreEqual(negation, ((PartTimeTriggerAM)modelTrigger)._not);
        }

        [TestMethod]
        public void buildTime()
        {
            DateTime date = DateTime.Now;
            long periodlength = 1000;


            SensorData sensorData = new TimeData(date, periodlength);

            Study study = buildDefaultStudyWithSensor(sensorData);

            new StudyBuilderAM().buildStudy(study);

            ///Assertions
            QuestionnaireAM modelQuestionnaire = StudyAM.Instance.Questionnaires.First();
            EventAM modelEvent = modelQuestionnaire.@event;

            Assert.IsInstanceOfType(modelEvent, typeof(SingleTriggerEventAM));

            TriggerAM modelTrigger = ((SingleTriggerEventAM)modelEvent).trigger;

            Assert.IsTrue(modelTrigger.getSensorType() == SensorType.TIME);
            Assert.AreEqual(date, ((TimeTriggerAM)modelTrigger)._time);
            Assert.AreEqual(periodlength, ((TimeTriggerAM)modelTrigger)._periodLength);
        }


        [TestMethod]
        public void buildWeather()
        {
            WeatherData.Weathertype weatherType = WeatherData.Weathertype.Clear;
            bool negation = false;

            SensorData sensorData = new WeatherData(weatherType, negation);

            Study study = buildDefaultStudyWithSensor(sensorData);
            new StudyBuilderAM().buildStudy(study);

            ///Assertions
            QuestionnaireAM modelQuestionnaire = StudyAM.Instance.Questionnaires.First();
            EventAM modelEvent = modelQuestionnaire.@event;

            Assert.IsInstanceOfType(modelEvent, typeof(SingleTriggerEventAM));

            TriggerAM modelTrigger = ((SingleTriggerEventAM)modelEvent).trigger;

            Assert.IsTrue(modelTrigger.getSensorType() == SensorType.WEATHER);
            WeatherTriggerAM weatherModelTrigger = (WeatherTriggerAM)modelTrigger;

            Assert.AreEqual(weatherType, weatherModelTrigger.desiredWeather);
            Assert.AreEqual(negation, weatherModelTrigger.negation);

        }

        [TestMethod]
        public void buildAcceleration()
        {
            double acceleration = 1;
            bool bigger = false;

            SensorData sensorData = new AccelerationData(acceleration, bigger);

            Study study = buildDefaultStudyWithSensor(sensorData);
            new StudyBuilderAM().buildStudy(study);

            ///get to the Trigger
            QuestionnaireAM modelQuestionnaire = StudyAM.Instance.Questionnaires.First();
            EventAM modelEvent = modelQuestionnaire.@event;

            Assert.IsInstanceOfType(modelEvent, typeof(SingleTriggerEventAM));

            TriggerAM modelTrigger = ((SingleTriggerEventAM)modelEvent).trigger;

            Assert.IsTrue(modelTrigger.getSensorType() == SensorType.ACCELERATION);
            AccelerationTriggerAM accelerationModelTrigger = (AccelerationTriggerAM)modelTrigger;

            ///Assertions

            Assert.AreEqual(acceleration, accelerationModelTrigger.acceleration);
            Assert.AreEqual(bigger, accelerationModelTrigger.bigger);

        }


        [TestMethod]
        public void buildLocation()
        {
            LocationTypeData.Locationtype locationType = LocationTypeData.Locationtype.accounting;

            SensorData sensorData = new LocationTypeData(locationType);

            Study study = buildDefaultStudyWithSensor(sensorData);
            new StudyBuilderAM().buildStudy(study);

            ///get to the Trigger
            QuestionnaireAM modelQuestionnaire = StudyAM.Instance.Questionnaires.First();
            EventAM modelEvent = modelQuestionnaire.@event;

            Assert.IsInstanceOfType(modelEvent, typeof(SingleTriggerEventAM));

            TriggerAM modelTrigger = ((SingleTriggerEventAM)modelEvent).trigger;

            Assert.IsTrue(modelTrigger.getSensorType() == SensorType.LOCATIONTYPE);
            LocationTypeTriggerAM locationModelTrigger = (LocationTypeTriggerAM)modelTrigger;

            ///Assertions

            Assert.AreEqual(locationType, locationModelTrigger._desiredType);


        }
    }
}
