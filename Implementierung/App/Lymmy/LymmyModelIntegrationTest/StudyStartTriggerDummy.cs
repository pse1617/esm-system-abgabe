﻿using System;
using edu.kit.lymmy.xamarin.model.trigger;
using edu.kit.lymmy.shared.sensor;

namespace LymmyModelIntegrationTest
{
	/// <summary>
    /// A TriggerDummy for TestUses, its SensorType is SensorType.STUDY_START
    /// </summary>
    public class StudyStartTriggerDummy : TriggerAM
    {
		public StudyStartTriggerDummy()
        {

        }

        public override SensorType getSensorType()
        {
            return SensorType.STUDY_START;
        }

        public override void startTrigger()
        {
            //do Nothing
        }

        /// <summary>
        /// Triggers the trigger manually for testing purposes
        /// </summary>
        public void triggerManually()
        {
            this.notify(true);
        }

        /// <summary>
        /// DeTriggers the trigger manually for testing purposes
        /// </summary>
        public void deTriggerManually()
        {
            this.notify(false);
        }
    }
}
