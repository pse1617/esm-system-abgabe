﻿using System.Collections.Generic;
using System.Linq;
using Lymmy.view;
using NUnit.Framework;
using XLabs.Forms.Controls;


namespace AppTest
{
	public class MultipleChoicePageTest
	{
		private MultipleChoiceQuestionPage _mulPage = null;
		private readonly string _question = "Here is the question";
		private readonly List<string> _choices = new List<string> { "Choice 1", "Choice 2", "Choice 3" };
		private object[] _sourceLists = {new object[] {new SortedSet < int > {}},  //case 1
            new object[] {new SortedSet<int> {2}}, //case 2
			new object[] {new SortedSet<int> {1,2}}, //case 3
			new object[] {new SortedSet<int> {0,1,2}} //case 4
                                    };

		[SetUp]
		public void BeforeEachTest()
		{
			_mulPage = new MultipleChoiceQuestionPage(_question, _choices);
		}

		[TearDown]
		public void AfterEachTest()
		{
			_mulPage = null;
		}

		[Test]
		public void PageIsNotNullTest()
		{
			Assert.IsNotNull(_mulPage, "Multiple choice question page is not displaying correctly!");
		}

		[Test, TestCaseSource("_sourceLists")]
		public void getAnswer(SortedSet<int> _answer)
		{
			CheckBox[] cho = new CheckBox[_choices.Count];
			for (int i = 0; i < cho.Length; i++)
			{
				cho[i] = new CheckBox();
				cho[i].DefaultText = _choices[i];
			}
			for (int i = 0; i < _answer.Count; i++)
			{
				cho[_answer.ElementAt(i)].Checked = true;
			}
			Assert.AreEqual(_mulPage.getAnswer(cho), _answer, "The answer of multiple choice is incorrect!");
		}
	}
}
