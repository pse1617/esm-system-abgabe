﻿using System.Collections.Generic;
using Lymmy.view;
using NUnit.Framework;
using XLabs.Forms.Controls;

namespace AppTest
{
	public class SingleChoicePageTest
	{
		private SingleChoiceQuestionPage _sinPage;
		private readonly string _question = "Here is the question";
		private readonly List<string> _choices = new List<string> { "Choice 1", "Choice 2", "Choice 3" };

		[SetUp]
		public void BeforeEachTest()
		{
			_sinPage = new SingleChoiceQuestionPage(_question, _choices);
		}

		[TearDown]
		public void AfterEachTest()
		{
			_sinPage = null;
		}

		[Test]
		public void PageIsNotNullTest()
		{
			Assert.IsNotNull(_sinPage, "Single choice question page is not displaying correctly!");
		}

		[Test]
		[TestCase(0)]
		[TestCase(1)]
		[TestCase(2)]
		public void getAnswer(int _answer)
		{
			BindableRadioGroup cho = new BindableRadioGroup() { ItemsSource = _choices };
			cho.SelectedIndex = _answer;
			Assert.AreEqual(cho.SelectedIndex, _answer, "The answer of single choice is incorrect!");
		}
	}
}
