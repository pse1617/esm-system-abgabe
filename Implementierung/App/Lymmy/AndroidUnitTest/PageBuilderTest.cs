﻿using Lymmy.view;
using NUnit.Framework;

namespace AppTest
{
	public class PageBuilderTest
	{
		private PageBuilder _pageBuilder;

		[SetUp]
		public void BeforeEachTest()
		{
			_pageBuilder = new PageBuilder();
		}

		[Test]
		public void QuestionnaireIsNull()
		{
			Assert.IsNull(_pageBuilder.questionnaire,
						  "The questionnaire in PageBuilder is not null while iniatialization!");
		}
	}
}
