﻿using System;
using Lymmy.view;
using NUnit.Framework;

namespace AppTest
{
	public class SliderPageTest
	{
		private SliderQuestionPage _sliderPage;
		private readonly string _question = "Here is the question";
		private readonly string _left = "left";
		private readonly string _right = "right";

		[SetUp]
		public void BeforeEachTest()
		{
			_sliderPage = new SliderQuestionPage(_question, _left, _right);
		}

		[TearDown]
		public void AfterEachTest()
		{
			_sliderPage = null;
		}

		[Test]
		public void PageIsNotNullTest()
		{
			Assert.IsNotNull(_sliderPage, "Slider question page is not displaying correctly!");
		}

		[Test]
		[TestCase(0)]
		[TestCase(42.57)]
		[TestCase(100)]
		public void getAnswer(double _answer)
		{
			int _result = Convert.ToInt32(_answer);
			bool _compare = false;
			if ((_answer - _result < 1) && (_answer - _result > -1))
			{
				_compare = true;
			}
			Assert.IsTrue(_compare, "The answer of slider is incorrect!");
		}
	}
}
