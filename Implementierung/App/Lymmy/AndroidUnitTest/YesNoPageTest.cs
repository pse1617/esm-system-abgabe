﻿using System.Collections.Generic;
using Lymmy.view;
using NUnit.Framework;
using XLabs.Forms.Controls;

namespace AppTest
{
	public class YesNoPageTest
	{
		private YesOrNoQuestionPage _ynPage;
		private readonly string _question = "Here is the question";

		[SetUp]
		public void BeforeEachTest()
		{
			_ynPage = new YesOrNoQuestionPage(_question);
		}

		[TearDown]
		public void AfterEachTest()
		{
			_ynPage = null;
		}

		[Test]
		public void PageIsNotNullTest()
		{
			Assert.IsNotNull(_ynPage, "Yes or no question page is not displaying correctly!");
		}

		[Test]
		[TestCase(0)]
		[TestCase(1)]
		public void getAnswer(int _answer)
		{
			List<string> _choices = new List<string>{ "Yes", "No" };
			BindableRadioGroup cho = new BindableRadioGroup() { ItemsSource = _choices};
			cho.SelectedIndex = _answer;
			Assert.AreEqual(cho.SelectedIndex, _answer, "The answer of yes or no question is incorrect!");
		}
	}
}
