﻿namespace edu.kit.lymmy.shared.sensor
{

	/// <summary>
	/// @author Lukas
	/// @since 09.01.2017
    /// 
    /// @author Youheng
    /// @since 04.03.2017
	/// </summary>
	public class CoarseLocationData : SensorData
	{
		private readonly string adress;

		public CoarseLocationData(string adress) : base(SensorType.COARSE_LOCATION)
		{
			this.adress = adress;
		}

		public virtual string getAdress()
		{
			return adress;
		}
	}

}