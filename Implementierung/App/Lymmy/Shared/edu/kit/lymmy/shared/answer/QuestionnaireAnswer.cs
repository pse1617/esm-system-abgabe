﻿using System;
using System.Collections.Generic;
using System.Text;

namespace edu.kit.lymmy.shared.answer
{


	/// <summary>
	/// Created by mo on 19.01.17.
	/// </summary>
	public class QuestionnaireAnswer
	{

		/// <summary>
		/// Id is generated via SHA-256 to guarantee uniqueness. If the App sends a Answer multiple times it's
		/// possible to know that by this id.
		/// </summary>
		private string id;
		private readonly string probandID;
		private readonly DateTime timestamp;
		private readonly int questionnaireIndex;
		private readonly IList<string> contextData;
		private readonly IList<Answer<object>> answers;

		public QuestionnaireAnswer(string probandID, DateTime timestamp, int questionnaireIndex, IList<string> contextData, IList<Answer<object>> answers)
		{
			if (string.ReferenceEquals(probandID, null) || probandID.Length == 0 || timestamp == null || questionnaireIndex < 0 || contextData == null || answers == null || answers.Count == 0)
			{
				throw new System.ArgumentException("probandID: " + probandID + ", timestamp: " + timestamp + ", questionnaireIndex: " + questionnaireIndex + ", contextData: " + contextData + ", answers: " + answers);
			}

			this.probandID = probandID;
			this.timestamp = timestamp;
			this.questionnaireIndex = questionnaireIndex;
			this.contextData = contextData;
			this.answers = answers;

            //Create unique id
            id = probandID + timestamp.ToString();

		}

		public virtual string Id
		{
			get
			{
				return this.id;
			}
		}

		public virtual string ProbandID
		{
			get
			{
				return probandID;
			}
		}

		public virtual DateTime Timestamp
		{
			get
			{
				return timestamp;
			}
		}

		public virtual int QuestionnaireIndex
		{
			get
			{
				return questionnaireIndex;
			}
		}

		public virtual IList<string> ContextData
		{
			get
			{
				return contextData;
			}
		}

		public virtual IList<Answer<object>> Answers
		{
			get
			{
				return answers;
			}
		}

		public override bool Equals(object o)
		{
			if (o == null || !(o is QuestionnaireAnswer))
			{
				return false;
			}
			else
			{
				return this.id.Equals(((QuestionnaireAnswer) o).id);
			}
		}
	}

}