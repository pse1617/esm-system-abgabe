﻿using System.Collections.Generic;

namespace edu.kit.lymmy.shared.@event
{


	/// <summary>
	/// @author Lukas
	/// @since 21.12.2016
	/// </summary>
	public class CompositeEvent : Event
	{
		private readonly IList<Event> events;
		private readonly LogicalOperator @operator;

		public CompositeEvent(IList<Event> events, LogicalOperator @operator) : base(EventType.COMPOSITE)
		{
			this.events = events;
			this.@operator = @operator;
		}

		public virtual LogicalOperator Operator
		{
			get
			{
				return @operator;
			}
		}

		public virtual IList<Event> Events
		{
			get
			{
				return events;
			}
		}
	}

}