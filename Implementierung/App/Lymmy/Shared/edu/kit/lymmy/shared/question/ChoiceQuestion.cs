﻿using System.Collections.Generic;

namespace edu.kit.lymmy.shared.question
{


	/// <summary>
	/// @author Lukas
	/// @since 21.12.2016
	/// </summary>
	public abstract class ChoiceQuestion : Question
	{
		private readonly IList<string> answers;

		internal ChoiceQuestion(QuestionType type, Condition condition, string text, IList<string> answers) : base(type, condition, text)
		{
			this.answers = new List<string>(answers);
		}

		public virtual IList<string> Answers
		{
			get
			{
				return new List<string>(answers);
			}
		}
	}

}