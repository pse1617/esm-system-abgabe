﻿using System.Collections.Generic;

namespace edu.kit.lymmy.shared
{

	using Event = edu.kit.lymmy.shared.@event.Event;
	using Question = edu.kit.lymmy.shared.question.Question;


	/// <summary>
	/// @author Lukas
	/// @since 21.12.2016
	/// </summary>
	public class Questionnaire
	{
		private readonly Event @event;
		private readonly long coolDownTime;
//JAVA TO C# CONVERTER WARNING: Java wildcard generics have no direct equivalent in .NET:
//ORIGINAL LINE: private final java.util.List<? extends edu.kit.lymmy.shared.question.Question> questions;
		private readonly IList<Question> questions;
		private readonly string name;
		/// <summary>
		/// TODO: Which classes/parameters does the app need to know which sensor should be added to contextData?
		/// </summary>
		private readonly IList<string> contextSensors;

		public Questionnaire(Event @event, long coolDownTime, IList<Question> questions, IList<string> contextSensors, string name)
		{
			if (coolDownTime < 0 || string.ReferenceEquals(name, null) || name.Equals(""))
			{
				throw new System.ArgumentException("name: " + name + ", event: " + @event + ", coolDownTime: " + coolDownTime + ", questions: " + questions + ", contextSensors: " + contextSensors);
			}
			this.@event = @event;
			this.coolDownTime = coolDownTime;
			this.questions = new List<Question>(questions);
			this.contextSensors = contextSensors;
			this.name = name;
		}

		public virtual Event Event
		{
			get
			{
				return @event;
			}
		}

		public virtual long CoolDownTime
		{
			get
			{
				return coolDownTime;
			}
		}

//JAVA TO C# CONVERTER WARNING: Java wildcard generics have no direct equivalent in .NET:
//ORIGINAL LINE: public java.util.List<? extends edu.kit.lymmy.shared.question.Question> getQuestions()
		public virtual IList<Question> Questions
		{
			get
			{
				return new List<Question>(questions);
			}
		}

		public virtual IList<string> ContextSensors
		{
			get
			{
				return this.contextSensors;
			}
		}

		public virtual string Name
		{
			get
			{
				return name;
			}
		}
	}

}