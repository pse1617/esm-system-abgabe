﻿namespace edu.kit.lymmy.shared.user
{


	/// <summary>
	/// Created by mo on 19.01.17.
	/// </summary>
	public class Proband
	{

		private readonly string id;
		private readonly string studyID;
		private readonly string certificateSerialNumber;

		public Proband(string id, string studyID, string certificateSerialNumber)
		{
			if (string.ReferenceEquals(id, null) || id.Length == 0 || string.ReferenceEquals(studyID, null) || studyID.Length == 0 || string.ReferenceEquals(certificateSerialNumber, null) || certificateSerialNumber.Length == 0)
			{
				throw new System.ArgumentException("id: " + id + ", studyID: " + studyID + ", certificateSerialNumber: " + certificateSerialNumber);
			}

			this.id = id;
			this.studyID = studyID;
			this.certificateSerialNumber = certificateSerialNumber;
		}

		public virtual string Id
		{
			get
			{
				return id;
			}
		}

		public virtual string StudyID
		{
			get
			{
				return studyID;
			}
		}

		public virtual string CertificateSerialNumber
		{
			get
			{
				return certificateSerialNumber;
			}
		}

		public override bool Equals(object o)
		{
			if (o == null || !(o is Proband))
			{
				return false;
			}
			else
			{
				Proband @object = (Proband) o;
				return this.id.Equals(@object.id);
			}
		}
	}

}