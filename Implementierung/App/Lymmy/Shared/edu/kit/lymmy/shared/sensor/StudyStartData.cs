﻿namespace edu.kit.lymmy.shared.sensor
{

	/// <summary>
	/// @author Lukas
	/// @since 09.01.2017
	/// </summary>
	public class StudyStartData : SensorData
	{
		public StudyStartData() : base(SensorType.STUDY_START)
		{
		}
	}

}