﻿namespace edu.kit.lymmy.shared.user
{

	/// <summary>
	/// Data Transport class between App and Backend.
	/// </summary>
	public class ProbandDetails
	{

		private readonly Proband proband;
		private readonly object certificate;

		public ProbandDetails(Proband proband, object certificate)
		{
			if (proband == null)
			{
				throw new System.ArgumentException("Proband or certificate null");
			}
			this.proband = proband;
			this.certificate = certificate;
		}

		public virtual Proband Proband
		{
			get
			{
				return proband;
			}
		}

		public virtual object Certificate
		{
			get
			{
				return certificate;
			}
		}
	}

}