﻿namespace edu.kit.lymmy.shared.sensor
{

	/// <summary>
	/// @author Lukas
	/// @since 09.01.2017
	/// </summary>
	public class WeatherData : SensorData
	{
		public enum Weathertype
        {
            Thunderstorm,
            Drizzle,
            Rain,
            Snow,
            Atmosphere,
            Clear,
            Clouds,
            Extreme,
            Additional
        }
        private readonly bool not;

        private readonly Weathertype weather;

		public WeatherData(Weathertype weather, bool not) : base(SensorType.WEATHER)
		{
			this.weather = weather;
            this.not = not;
		}

        public virtual bool Not
        {
            get
            {
                return not;
            }
        }

        public virtual Weathertype Weather
		{
            get
            {
                return weather;
            }			
		}
	}

}