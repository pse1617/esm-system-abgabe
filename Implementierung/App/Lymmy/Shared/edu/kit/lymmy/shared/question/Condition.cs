﻿namespace edu.kit.lymmy.shared.question
{

	/// <summary>
	/// Created by mo on 19.01.17.
	/// </summary>
	public class Condition
	{

		private readonly int question;
		private readonly int choice;

		public Condition(int question, int choice)
		{
			this.question = question;
			this.choice = choice;
		}

		public virtual int Question
		{
			get
			{
				return question;
			}
		}

		public virtual int Choice
		{
			get
			{
				return choice;
			}
		}

		public virtual bool isValid(int choice)
		{
			return this.choice == choice;
		}
	}

}