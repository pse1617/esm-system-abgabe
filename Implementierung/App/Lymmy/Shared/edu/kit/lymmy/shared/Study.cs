﻿using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace edu.kit.lymmy.shared
{



	/// <summary>
	/// Updated as http://www.oracle.com/technetwork/articles/java/jf14-date-time-2125367.html explains a new date api,
	/// which is thread-safe and easier to handle.
	/// 
	/// @author Lukas
	/// @since 21.12.2016
	/// </summary>
	public sealed class Study
	{
		public const long STUDY_CHANGE_DAY_BUFFER = 1;

		private readonly string id;
		private readonly string userID;
		private readonly string name;
		private readonly string description;
		private readonly DateTime startDate;
		private readonly DateTime endDate;
		private readonly IList<Questionnaire> questionnaires;
		private long registeredProbands;

		public Study(string id, string userID, string name, string description, DateTime startDate, DateTime endDate, IList<Questionnaire> questionnaires)
		{
			string errorMessage = "";
			if (string.ReferenceEquals(id, null) || string.ReferenceEquals(userID, null) || string.ReferenceEquals(name, null) || string.ReferenceEquals(description, null))
			{
				errorMessage += "id: " + id + ", userID: " + userID + ", name: " + name + ", description: " + description;
			}
			if (questionnaires == null)
			{
				errorMessage += "No questionnaires;";
			}

			if (!errorMessage.Equals(""))
			{
				throw new System.ArgumentException(errorMessage);
			}

			//If questionnaires == null the previous exception will be thrown
			Debug.Assert(questionnaires != null);

			this.id = id;
			this.userID = userID;
			this.name = name;
			this.description = description;
			this.startDate = startDate;
			this.endDate = endDate;
			this.questionnaires = new List<Questionnaire>(questionnaires);
			this.registeredProbands = 0;
		}

		public string Id
		{
			get
			{
				return id;
			}
		}

		public string UserID
		{
			get
			{
				return userID;
			}
		}

		public string Name
		{
			get
			{
				return name;
			}
		}

		public string Description
		{
			get
			{
				return description;
			}
		}

		public DateTime StartDate
		{
			get
			{
				return startDate;
			}
		}

		public DateTime EndDate
		{
			get
			{
				return endDate;
			}
		}

		public IList<Questionnaire> Questionnaires
		{
			get
			{
				return new List<Questionnaire>(questionnaires);
			}
		}

		public long getRegisteredProbands()
		{
			return registeredProbands;
		}

		public void setRegisteredProbands(int registeredProbands)
		{
			this.registeredProbands = registeredProbands;
		}
	}

}