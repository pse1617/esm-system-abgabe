﻿using System.Collections.Generic;

namespace edu.kit.lymmy.shared.question
{

	/// <summary>
	/// Created by Marc on 04.02.2017.
	/// </summary>
	public class YesNoQuestion : ChoiceQuestion
	{
		public YesNoQuestion(Condition condition, string text, IList<string> answers) : base(QuestionType.YES_NO, condition, text, answers)
		{
		}
	}

}