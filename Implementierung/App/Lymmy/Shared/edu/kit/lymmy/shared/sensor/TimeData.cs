﻿using System;

namespace edu.kit.lymmy.shared.sensor
{

	/// <summary>
	/// @author Lukas
	/// @since 09.01.2017
	/// </summary>
	public class TimeData : SensorData
	{
		private readonly DateTime date;
		private readonly long periodLength;

		public TimeData(DateTime date, long periodLength) : base(SensorType.TIME)
		{
			this.date = date;
			this.periodLength = periodLength;
		}

		public virtual DateTime Date
		{
			get
			{
				return date;
			}
		}

		public virtual long PeriodLength
		{
			get
			{
				return periodLength;
			}
		}
	}

}