﻿namespace edu.kit.lymmy.shared.@event
{

	using SensorData = edu.kit.lymmy.shared.sensor.SensorData;

	/// <summary>
	/// @author Lukas
	/// @since 04.01.2017
	/// </summary>
	public class SensorEvent : Event
	{
		private readonly SensorData data;

		/// <summary>
		/// Persistence-required default constructor
		/// </summary>
		private SensorEvent() : base(EventType.SENSOR)
		{
			data = null;
		}

		public SensorEvent(SensorData data) : base(EventType.SENSOR)
		{
			this.data = data;
		}

		public virtual SensorData Data
		{
			get
			{
				return data;
			}
		}
	}

}