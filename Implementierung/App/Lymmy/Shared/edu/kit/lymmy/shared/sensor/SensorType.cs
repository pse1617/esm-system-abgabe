﻿namespace edu.kit.lymmy.shared.sensor
{

	/// <summary>
	/// @author Lukas
	/// @since 11.01.2017
	/// </summary>
	public enum SensorType
	{
		STUDY_START,
		TIME,
		PARTTIME,
		COARSE_LOCATION,
		DISPLAY_STATE,
		WEATHER,
        ACCELERATION,
		LOCATIONTYPE
	}

}