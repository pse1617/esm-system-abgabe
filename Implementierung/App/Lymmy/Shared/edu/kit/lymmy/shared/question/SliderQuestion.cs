﻿namespace edu.kit.lymmy.shared.question
{

	/// <summary>
	/// @author Lukas
	/// @since 13.02.2017
	/// </summary>
	public class SliderQuestion : StepQuestion
	{
		public SliderQuestion(Condition condition, string text, string leftSliderText, string rightSliderText) : base(QuestionType.SLIDER, condition, text, leftSliderText, rightSliderText)
		{
		}
	}

}