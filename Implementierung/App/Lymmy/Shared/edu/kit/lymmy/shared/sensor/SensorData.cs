﻿namespace edu.kit.lymmy.shared.sensor
{


	/// <summary>
	/// @author Lukas
	/// @since 04.01.2017
	/// </summary>
//JAVA TO C# CONVERTER TODO TASK: Most Java annotations will not have direct .NET equivalent attributes:
//ORIGINAL LINE: @JsonTypeInfo(use = JsonTypeInfo.Id.CLASS) public abstract class SensorData
	public abstract class SensorData
	{
		private readonly SensorType type;

		internal SensorData(SensorType type)
		{
			this.type = type;
		}

		public virtual SensorType Type
		{
			get
			{
				return type;
			}
		}
	}

}