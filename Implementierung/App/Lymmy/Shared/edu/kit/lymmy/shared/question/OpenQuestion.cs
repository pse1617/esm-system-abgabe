﻿namespace edu.kit.lymmy.shared.question
{

	/// <summary>
	/// @author Lukas
	/// @since 21.12.2016
	/// </summary>
	public class OpenQuestion : Question
	{
		public OpenQuestion(Condition condition, string text) : base(QuestionType.OPEN, condition, text)
		{
		}
	}

}