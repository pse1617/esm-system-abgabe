﻿namespace edu.kit.lymmy.shared.sensor
{

	public class PartTimeData : SensorData
	{
		public enum Timetype
		{
			Morning,
			Afternoon,
			Evening,
			Night,
		}
		private readonly bool not;

		private readonly Timetype timeType;

		public PartTimeData(Timetype timeType, bool not) : base(SensorType.PARTTIME)
		{
			this.timeType = timeType;
			this.not = not;
		}

		public virtual bool Not
		{
			get
			{
				return not;
			}
		}

		public virtual Timetype TimeType
		{
			get
			{
				return timeType;
			}
		}
	}

}