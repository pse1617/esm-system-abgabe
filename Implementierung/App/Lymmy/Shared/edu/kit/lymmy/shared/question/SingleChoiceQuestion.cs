﻿using System.Collections.Generic;

namespace edu.kit.lymmy.shared.question
{

	/// <summary>
	/// @author Lukas
	/// @since 21.12.2016
	/// </summary>
	public class SingleChoiceQuestion : ChoiceQuestion
	{
		public SingleChoiceQuestion(Condition condition, string text, IList<string> answers) : base(QuestionType.SINGLE_CHOICE, condition, text, answers)
		{
		}
	}

}