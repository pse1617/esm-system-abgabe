﻿namespace edu.kit.lymmy.shared.sensor
{
    public class AccelerationData : SensorData
    {
        private readonly double acceleration;
        private readonly bool bigger;
        public AccelerationData(double acceleration, bool bigger) : base(SensorType.ACCELERATION)
        {
            this.acceleration = acceleration;
            this.bigger = bigger;
        }

        public virtual double Acceleration
        {
            get
            {
                return acceleration;
            }
        }
        public virtual bool Bigger
        {
            get
            {
                return bigger;
            }
        }
    }
}
