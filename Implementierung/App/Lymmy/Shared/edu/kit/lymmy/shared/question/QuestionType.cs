﻿namespace edu.kit.lymmy.shared.question
{

	/// <summary>
	/// @author Lukas
	/// @since 11.01.2017
	/// </summary>
	public enum QuestionType
	{
		OPEN,
		SINGLE_CHOICE,
		MULTIPLE_CHOICE,
		SLIDER,
		LIKERT,
		YES_NO
	}

}