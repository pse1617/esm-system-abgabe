﻿namespace edu.kit.lymmy.shared.answer
{

	/// <summary>
	/// Created by mo on 19.01.17.
	/// </summary>
	public class Answer<T>
	{

		private readonly int questionIndex;
		private readonly T choice;

		public Answer(int questionIndex, T choice)
		{
			if (questionIndex < 0 || choice == null)
			{
				throw new System.ArgumentException("questionIndex: " + questionIndex + ", choice: " + choice);
			}
			this.questionIndex = questionIndex;
			this.choice = choice;
		}

		public virtual int QuestionIndex
		{
			get
			{
				return this.questionIndex;
			}
		}

		public virtual T Choice
		{
			get
			{
				return choice;
			}
		}
	}

}