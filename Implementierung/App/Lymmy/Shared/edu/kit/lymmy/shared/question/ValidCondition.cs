﻿namespace edu.kit.lymmy.shared.question
{

	/// <summary>
	/// Created by mo on 19.01.17.
	/// </summary>
	public class ValidCondition : Condition
	{
		public ValidCondition(int questionIndex, int choice) : base(questionIndex, choice)
		{
		}

		public ValidCondition() : this(-1, -1)
		{
		}

		public override bool isValid(int choice)
		{
			return true;
		}
	}

}