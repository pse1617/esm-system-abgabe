﻿namespace edu.kit.lymmy.shared.messages
{

	/// <summary>
	/// Created by mo on 26.01.17.
	/// </summary>
	public sealed class CommunicationConstants
	{

		public const string LEADER_REGISTER_START = "/leader/auth/register";
		public const string LEADER_RESEND_CODE = "/leader/auth/resend";
		public const string LEADER_REGISTER_FINISH = "/leader/auth/registerLogin";
		public const string LEADER_DELETE = "/leader/auth/delete";
		public const string LEADER_LOGIN = "/login";
		public const string LEADER_LOGOUT = "/logout";

		public const string GET_STUDIES = "/leader/studies";
		public const string CREATE_STUDY_ID = "/leader/studies/createId";
		public const string STORE_STUDY = "/leader/studies/store";
		public const string DELETE_STUDY = "/leader/studies/delete";
		public const string EXPORT_STUDY = "/leader/studies/export";

		public const string PROBAND_REGISTER = "/proband/auth";
		public const string PROBAND_GET_STARTDATE = "/proband/study/startdate";
		public const string PROBAND_GET_STUDY = "/proband/study/fetch";
		public const string PROBAND_STORE_ANSWER = "/proband/study/answers";

		public const string PARAM_STUDY_ID = "studyID";
		public const string PARAM_PASSWORD = "password";
		public const string PARAM_USERNAME = "username";
		public const string PARAM_MAIL = PARAM_USERNAME;
		public const string PARAM_CLIENT_PUBKEY = "clientPubKey";
		public const string PARAM_VERIFY_CODE = "verifyCode";
		public const string PARAM_CODE = PARAM_VERIFY_CODE;

		public const string CONFIRM_REGISTRATION = "/register";

		public const string HOST = "https://psews16esm2.teco.edu:80";
		public static readonly string EMAIL_REGISTER_VERIFY = HOST + CONFIRM_REGISTRATION + "?" + PARAM_USERNAME + "={0}&" + PARAM_VERIFY_CODE + "={1}";

		public const string HEADER_PROBAND_ID = "probandId";

		private CommunicationConstants()
		{
			//To guarantee this class won't be initialized
		}

		public enum LoginResult
		{
			OK,
			BAD_CREDENTIALS,
			NOT_ACTIVE,
			NOT_FOUND,
			UNKNOWN
		}
	}

}