﻿namespace edu.kit.lymmy.shared.question
{

	/// <summary>
	/// @author Lukas
	/// @since 13.02.2017
	/// </summary>
	public class StepQuestion : Question
	{
		private readonly string leftSliderText;
		private readonly string rightSliderText;

		internal StepQuestion(QuestionType type, Condition condition, string text, string leftSliderText, string rightSliderText) : base(type, condition, text)
		{
			this.leftSliderText = leftSliderText;
			this.rightSliderText = rightSliderText;
		}

		public virtual string LeftSliderText
		{
			get
			{
				return leftSliderText;
			}
		}

		public virtual string RightSliderText
		{
			get
			{
				return rightSliderText;
			}
		}
	}

}