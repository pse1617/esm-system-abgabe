﻿namespace edu.kit.lymmy.shared.question
{


	/// <summary>
	/// @author Lukas
	/// @since 21.12.2016
	/// </summary>
//JAVA TO C# CONVERTER TODO TASK: Most Java annotations will not have direct .NET equivalent attributes:
//ORIGINAL LINE: @JsonTypeInfo(use = JsonTypeInfo.Id.CLASS) public abstract class Question
	public abstract class Question
	{
		private readonly QuestionType type;
		private readonly Condition condition;
		private readonly string text;

		internal Question(QuestionType type, Condition condition, string text)
		{
			this.type = type;
			this.condition = condition;
			this.text = text;
		}

		public virtual string Text
		{
			get
			{
				return text;
			}
		}

		public virtual Condition Condition
		{
			get
			{
				return condition;
			}
		}

		public virtual QuestionType Type
		{
			get
			{
				return type;
			}
		}
	}

}