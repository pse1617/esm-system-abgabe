﻿namespace edu.kit.lymmy.shared.question
{

	/// <summary>
	/// @author Lukas
	/// @since 04.01.2017
	/// </summary>
	public class LikertQuestion : StepQuestion
	{
		private readonly int steps;

		public LikertQuestion(Condition condition, string text, string leftSliderText, string rightSliderText, int steps) : base(QuestionType.LIKERT, condition, text, leftSliderText, rightSliderText)
		{
			this.steps = steps;
		}

		public virtual int Steps
		{
			get
			{
				return steps;
			}
		}
	}

}