﻿namespace edu.kit.lymmy.shared
{

	/// <summary>
	/// @author Lukas
	/// @since 26.12.2016
	/// </summary>
	public enum LogicalOperator
	{
		AND,
		OR
	}

}