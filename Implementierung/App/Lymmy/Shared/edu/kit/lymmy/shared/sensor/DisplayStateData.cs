﻿namespace edu.kit.lymmy.shared.sensor
{

    /// <summary>
    /// @author Lukas
    /// @since 09.01.2017
    /// </summary>
    public class DisplayStateData : SensorData
    {
        public enum DisplayState
        {
            ON,
            OFF,
            UNLOCKED
        }
        private readonly DisplayState state;

        public DisplayStateData(DisplayState state) : base(SensorType.DISPLAY_STATE)
        {
            this.state = state;
        }

        public virtual DisplayState State
        {
            get
            {
                return state;
            }
        }
    }
}