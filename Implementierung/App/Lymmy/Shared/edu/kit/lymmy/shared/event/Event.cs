﻿namespace edu.kit.lymmy.shared.@event
{


	/// <summary>
	/// @author Lukas
	/// @since 21.12.2016
	/// </summary>
//JAVA TO C# CONVERTER TODO TASK: Most Java annotations will not have direct .NET equivalent attributes:
//ORIGINAL LINE: @JsonTypeInfo(use = JsonTypeInfo.Id.CLASS) public abstract class Event
	public abstract class Event
	{
		private readonly EventType type;

		internal Event(EventType type)
		{
			this.type = type;
		}

		public virtual EventType Type
		{
			get
			{
				return type;
			}
		}
	}

}