﻿namespace edu.kit.lymmy.shared.@event
{

	/// <summary>
	/// @author Lukas
	/// @since 11.01.2017
	/// </summary>
	public enum EventType
	{
		COMPOSITE,
		SENSOR
	}

}