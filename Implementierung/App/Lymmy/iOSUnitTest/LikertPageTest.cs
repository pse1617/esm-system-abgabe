﻿using System;
using System.Collections.Generic;
using Lymmy.view;
using NUnit.Framework;
using XLabs.Forms.Controls;

namespace AppTest
{
	public class LikertPageTest
	{
		private LikertQuestionPage _likertPage;
		private readonly string _question = "Here is the leftquestion";
		private readonly string _left = "left";
		private readonly string _right = "right";
		private readonly int _choices = 5;

		[SetUp]
		public void BeforeEachTest()
		{
			_likertPage = new LikertQuestionPage(_question, _left, _right, _choices);
		}

		[TearDown]
		public void AfterEachTest()
		{
			_likertPage = null;
		}

		[Test]
		public void PageIsNotNullTest()
		{
			Assert.IsNotNull(_likertPage, "Likert question page is not displaying correctly!");
		}

		[Test]
		[TestCase(0, 3)]
		[TestCase(1, 5)]
		[TestCase(2, 7)]
		public void getAnswer(int _answer, int _steps)
		{
			List<String> l = new List<string>(_steps);

			for (int i = 0; i < _steps; i++)
			{
				l.Insert(i, "");
			}
			BindableRadioGroup cho = new BindableRadioGroup() { ItemsSource = l };
			cho.SelectedIndex = _answer;
			Assert.AreEqual(cho.SelectedIndex, _answer, "The answer of likert question is incorrect!");
		}
	}
}
