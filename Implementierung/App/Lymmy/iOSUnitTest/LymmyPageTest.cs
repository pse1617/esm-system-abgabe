﻿using System;
using Lymmy.view;
using NUnit.Framework;

namespace AppTest
{
	public class LymmyPageTest
	{
		private LymmyPage _lyPage = null;

		[SetUp]
		public void BeforeEachTest()
		{
			_lyPage = new LymmyPage();
		}

		[TearDown]
		public void AfterEachTest()
		{
			_lyPage = null;
		}

		[Test]
		public void PageIsNotNullTest()
		{
			Assert.IsNotNull(_lyPage, "Lymmy page is not displaying correctly!");
		}

	}
}
