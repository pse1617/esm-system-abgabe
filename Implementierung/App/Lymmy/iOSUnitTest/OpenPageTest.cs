﻿using Lymmy.view;
using NUnit.Framework;

namespace AppTest
{
	public class OpenPageTest
	{
		private OpenQuestionPage _openPage;
		private readonly string _question = "Here is the leftquestion";

		[SetUp]
		public void BeforeEachTest()
		{
			_openPage = new OpenQuestionPage(_question);
		}

		[TearDown]
		public void AfterEachTest()
		{
			_openPage = null;
		}

		[Test]
		public void PageIsNotNullTest()
		{
			Assert.IsNotNull(_openPage, "Open question page is not displaying correctly!");
		}

		[Test]
		[TestCase("answer")]
		[TestCase("/.'][")]
		[TestCase("ßÄÖü")]
		public void getAnswer(string _answer)
		{
			Assert.IsNotNull(_answer, "The answer of open question is empty!");
		}
	}
}
